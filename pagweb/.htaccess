# ACTIVATES THE MODULE ###########################################################
RewriteEngine On
RewriteBase /

# FORCE WWW URL ##################################################################
#RewriteCond %{HTTP_HOST} !^www\.(.+)$ [NC]
#RewriteRule ^(.*)$ https://www\.%{HTTP_HOST}/$1 [R=301,L]
#RewriteCond %{SERVER_PORT} 80 
#RewriteRule ^(.*)$ https://%{HTTP_HOST}/$1 [R=301,L]

# DISABLE INDEXING DIRECTORIES OR CGI ############################################
Options All -Indexes -ExecCGI

# FIX SOME MIME TYPES ############################################################
<IfModule mod_mime.c>
	# Add correct content-type for fonts
	AddType application/vnd.ms-fontobject .eot
	AddType font/ttf .ttf
	AddType font/otf .otf
	AddType font/x-woff .woff
	# All others
	AddType image/svg+xml .svg
    AddType text/css .css
    AddType application/x-javascript .js
    AddType text/html .html .htm
    AddType text/richtext .rtf .rtx
    AddType image/svg+xml .svg .svgz
    AddType text/plain .txt
    AddType text/xsd .xsd
    AddType text/xsl .xsl
    AddType text/xml .xml
    AddType video/asf .asf .asx .wax .wmv .wmx
    AddType video/avi .avi
    AddType image/bmp .bmp
    AddType application/java .class
    AddType video/divx .divx
    AddType application/msword .doc .docx
    AddType application/x-msdownload .exe
    AddType image/gif .gif
    AddType application/x-gzip .gz .gzip
    AddType image/x-icon .ico
    AddType image/jpeg .jpg .jpeg .jpe
    AddType application/vnd.ms-access .mdb
    AddType audio/midi .mid .midi
    AddType video/quicktime .mov .qt
    AddType audio/mpeg .mp3 .m4a
    AddType video/mp4 .mp4 .m4v
    AddType video/mpeg .mpeg .mpg .mpe
    AddType application/vnd.ms-project .mpp
    AddType application/vnd.oasis.opendocument.database .odb
    AddType application/vnd.oasis.opendocument.chart .odc
    AddType application/vnd.oasis.opendocument.formula .odf
    AddType application/vnd.oasis.opendocument.graphics .odg
    AddType application/vnd.oasis.opendocument.presentation .odp
    AddType application/vnd.oasis.opendocument.spreadsheet .ods
    AddType application/vnd.oasis.opendocument.text .odt
    AddType audio/ogg .ogg
    AddType application/pdf .pdf
    AddType image/png .png
    AddType application/vnd.ms-powerpoint .pot .pps .ppt .pptx
    AddType audio/x-realaudio .ra .ram
    AddType application/x-shockwave-flash .swf
    AddType application/x-tar .tar
    AddType image/tiff .tif .tiff
    AddType audio/wav .wav
    AddType audio/wma .wma
    AddType application/vnd.ms-write .wri
    AddType application/vnd.ms-excel .xla .xls .xlsx .xlt .xlw
    AddType application/zip .zip
    AddType application/json .json
</IfModule>

# DEFINE EXPIRATION TIME FOR CACHE ###############################################
<ifModule mod_expires.c>
	ExpiresActive on

	# Set expiration for common graphical assets
	ExpiresByType image/jpg "access plus 1 months"
	ExpiresByType image/gif "access plus 1 months"
	ExpiresByType image/jpeg "access plus 1 months"
	ExpiresByType image/png "access plus 1 months"

	# Set expiration for all applicatble script types
	ExpiresByType text/javascript "access plus 1 weeks"
	ExpiresByType application/javascript "access plus 1 weeks"
	ExpiresByType text/js "access plus 1 weeks"
	ExpiresByType application/js "access plus 1 weeks"
	ExpiresByType text/css "access plus 1 weeks"
	ExpiresByType application/x-shockwave-flash "access plus 1 weeks"

	# Add a far future Expires header for fonts
	ExpiresByType application/vnd.ms-fontobject "access plus 1 year"
	ExpiresByType font/ttf "access plus 1 year"
	ExpiresByType font/otf "access plus 1 year"
	ExpiresByType font/x-woff "access plus 1 year"
	ExpiresByType image/svg+xml "access plus 1 year"
	ExpiresByType application/vnd.ms-fontobject "access plus 1 year"

	<FilesMatch "\.(html|php)$">
		ExpiresActive Off
	</FilesMatch>
</ifModule>

# ENABLE FILE COMPRESSION ########################################################
<IfModule mod_deflate.c>
	# Compress compressible fonts
	AddOutputFilterByType DEFLATE font/x-woff font/ttf font/otf image/svg+xml application/vnd.ms-fontobject

	# Compress all js & css:
	AddOutputFilterByType DEFLATE text/html text/css text/plain text/xml application/x-javascript application/x-woff application/json
	
	BrowserMatch ^Mozilla/4 gzip-only-text/html
	BrowserMatch ^Mozilla/4\.0[678] no-gzip
	BrowserMatch \bMSIE !no-gzip !gzip-only-text/html
</ifModule>

# ENABLE GZIP AS COMPRESOR FORMAT ################################################
<ifModule mod_gzip.c>
	mod_gzip_on Yes
	mod_gzip_dechunk Yes
	mod_gzip_item_include file \.(xml|html|txt|css|js|php|pdf|doc|xls)$
	mod_gzip_item_include handler ^cgi-script$
	mod_gzip_item_include mime ^text/.*
	mod_gzip_item_include mime ^font/.*
	mod_gzip_item_include mime ^application/x-javascript.*
	mod_gzip_item_exclude mime ^image/.*
	mod_gzip_item_exclude rspheader ^Content-Encoding:.*gzip.*
</ifModule>

# CONFIGURE THE HEADERS ##################################################
<IfModule mod_headers.c>
 
	# Some server security
	Header unset Server
	Header unset X-Powered-By

	# Only accept secure connections
	Header set Strict-Transport-Security "max-age=31536000" env=HTTPS

	# SPEED UP THE SSL HANDSHAKE
	Header set Connection keep-alive
	
</IfModule>

# AVOIDS INFINITE LOOPS DUE TO THE SAME RULE #############################
RewriteCond %{ENV:REDIRECT_STATUS} !^$
RewriteRule ^ - [L]

# BLOCK ACCESS TO THE INDEX FILE #########################################
RewriteCond %{THE_REQUEST} ^index\.php [NC]
RewriteRule ^ - [F]

# THESE FILES WON'T BE REWRITED ##########################################
RewriteCond $1 !^(phpinfo\.php|sitemap\.xml|robots\.txt)

# IF THE REQUEST IS NOT A FILE ###########################################
RewriteCond %{REQUEST_FILENAME} !-f

# IF THE REQUEST IS NOT A DIRECTORY ######################################
RewriteCond %{REQUEST_FILENAME} !-d

# REWRITE ALL THE OTHER REQUEST ##########################################
RewriteRule ^(.*)$ /index.php/$1 [NC,L,QSA]