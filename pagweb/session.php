<?php

date_default_timezone_set("America/Mexico_City");
define("HOST", $_SERVER["HTTP_HOST"]);
define("USER_AGENT", $_SERVER["HTTP_USER_AGENT"]);
define("REMOTEIP", $_SERVER["REMOTE_ADDR"]);
define("EXPIRATION", 20); // 20 Segundos

class Session {

	private static $strictMode = false;
	private static $name = "client";
	private static $expired = false;

	private static function id($name) {
		return isset($_COOKIE[$name]) ? $_COOKIE[$name] : self::regenerate();
	}
	// Para generar el id de sessión de los visitantes utilizaremos 
	// a) La dirección ip "REMOTEIP"
	// b) El agente de usuario o cadena de navegador
	// c) un timestamp para no causar conflictos en redes donde compartan la misma IP Pública
	// De esta forma si la dirección IP cambia o cambia el navegador, también cambiará la id se sessión
	private static function regenerate() {
		$iplong = sprintf("%u", ip2long(REMOTEIP));
		$time = strtotime(date("Y-m-d H:i:s"));
		return "node".md5($iplong.USER_AGENT).$time;
	}
	private static function origin($sessionId) {
		return substr($sessionId, 0, -10);
	}
	private static function expiration($sessionId) {
		return substr($sessionId, -10);
	}
	private static function timeToDate($time=null) {
		return date("Y-m-d H:i:s", empty($time) ? time() : $time);
	}
	private static function newExpiration($sessionId) {
		return self::expiration($sessionId) + EXPIRATION;
	}
	private static function valid($sessionId) {
		return self::origin($sessionId) === self::origin(self::regenerate());
	}
	private static function destroy() {
		$params = session_get_cookie_params();
		setcookie(session_name(), '', time() - 42000,
		    $params["path"], $params["domain"],
		    $params["secure"], $params["httponly"]
		);
		session_destroy();
	}

	public static function debug() {
		//echo (self::$expired?"EXPIRED":"ACTIVE")." / ".self::timeToDate($_SESSION["expiration"])." < ".self::timeToDate();
	}

 	private static function timeDiff($from, $to) {
        return abs(abs($from) - abs($to));
    }

	public static function start() {
		self::$strictMode = !empty(ini_get("session.use_strict_mode"));
		if (self::$strictMode) { exit("Can't setup session."); }

		// Verificamos que la cookie sea válida, esto es:
		// Cuando la dirección IP de la petición es la misma que la almacenada en la cookie
		// Y también cuando el agente de usuario no ha cambiado
		// En caso contrario, no bloqueamos, sino que generámos un nuevo ID de sesión
		$sessionId = self::id(self::$name);
		$sessionId = self::valid($sessionId) ? $sessionId : self::regenerate();

		session_id($sessionId);
		session_name(self::$name);
		session_start();

		// Si no esta definido el tiempo de expiración lo guardamos
		if (!isset($_SESSION["expiration"])) { $_SESSION["expiration"] = self::newExpiration($sessionId); }

		// Después de iniciar la sesión comprobamos que el tiempo de expiración de la cookie coincida con el guardado en el servidor
		// Si no coinciden signifíca que el usuario intentó engañar al sistema modificando el tiempo de expiración (merece sansión?)
		// También comprobamos que la expiración guardada no sea menor a la fecha actual
		self::$expired = ($_SESSION["expiration"] !== self::newExpiration($sessionId)) || ($_SESSION["expiration"] < time());

		// Cuando una sesión ya expiró
		if (self::$expired) {
			// Solo para estar seguro
			Session::debug();
			// La destruimos
			self::destroy();
			// Después la regenerámos
			$sessionId = self::regenerate();
			session_id($sessionId);
			session_start();
			// Actualizamos la fecha de expiración
			$_SESSION["expiration"] = self::newExpiration($sessionId);
		}

		 // Comprobamos que la sesión no haya estado inactiva por mucho tiempo
        $lastActivity = isset($_SESSION["lastActivity"]) ? $_SESSION["lastActivity"] : time();
        $endForInactivity = self::timeDiff($lastActivity, time()) > 3600;
        echo self::timeDiff($lastActivity, time());

        $_SESSION["lastActivity"] = time();
	}
}

Session::start();
$_SESSION["nombre"] = "Jaime";
$_SESSION["idusuario"] = 3458;

?>