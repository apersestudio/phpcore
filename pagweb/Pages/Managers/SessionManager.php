<?php

namespace App\Managers {

	use PC\Manager;
	use App\Models\SessionModel;

	class SessionManager extends Manager implements \SessionHandlerInterface {

		// 0 = Guest User 
		private $iduser;
		// 0 = Guest User 
		private $connected;

		private $model;

		public function __construct() {
			$this->model = new SessionModel();
			register_shutdown_function('session_write_close');
		}

		public function open($savePath, $sessionName) {
			return true;
		}
		public function close() {
			return true;
		}
		public function read($idses) {
			$data = $this->model->select(["session_data"])->where("session_id", "=", $idses)->first();
			return $data["session_data"];
		}
		public function write($idses, $data) {
			$fields = [
				"session_id"=>$idses,
				"session_data"=>$data, 
				"session_ip"=>REMOTEIP
			];
			return $this->model->createOrUpdate($fields, $idses);
		}
		public function destroy($idses) {
			echo "DESTROY";
			/*
			if ($idses > 0) {
				$updated = PHPCore::db("Company")->update(array(
					"tabla"=>COMPANY_SHARD.".sesi", "campos"=>array(
						"_data"=>array("tipo"=>"string", "valor"=>""),
						"_esta"=>array("tipo"=>"number", "valor"=>0),
						"_feua"=>array("tipo"=>"date", "valor"=>"now")
					),
					"reglas"=>array("_idses"=>array("tipo"=>"string", "valor"=>$idses))
				));
				return ($updated !== false);
			}
			*/
			return true;
		}
		public function gc($maxlifetime) {
			/*
			$updated = PHPCore::db("Company")->delete(array(
				"tabla"=>COMPANY_SHARD.".sesi",
				"reglas"=>array("_feex"=>array("tipo"=>"date", "valor"=>"now", "union"=>"<"))
			));
			return ($updated !== false);
			*/
			return true;
		}
		public function sid() {
			$newsid = md5("time");
			return $newsid;
		}
	}

}

?>