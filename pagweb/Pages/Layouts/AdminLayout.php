<?php
echo $this->idusuario;
$this->html->addStyles(array(
//	array("media"=>"screen", "href"=>"https://fonts.googleapis.com/css?family=Noto+Sans:400,400i,700,700i|Varela+Round|Material+Icons"),
	array("media"=>"screen", "href"=>ASSETS."libraries/lilium/lilium.css"),
//	array("media"=>"screen", "href"=>ASSETS."fonts/moltobig/moltobig.css"),
	array("media"=>"screen", "href"=>ASSETS."css/corporativo.css"),
));
$this->html->addScripts(array(
	array("type"=>"text/javascript", "src"=>ASSETS."libraries/jquery/jquery.min.js"),
	array("type"=>"text/javascript", "src"=>ASSETS."libraries/lilium/lilium.min.js"),
	array("type"=>"text/javascript", "src"=>ASSETS."libraries/same-height.js"),
	array("type"=>"text/javascript", "src"=>ASSETS."scripts/docfis/docfis.js"),
	array("type"=>"text/javascript", "src"=>ASSETS."scripts/common.js")
));
/*$this->html->setTitle($this->pageTitle);*/
$this->html->headers();
$this->html->top();
?>
	
	<div id="page">
		
		<?php /*PHPCore::widget('corporativo/header', $this);*/ ?>

		<?php echo $content; ?>

		<?php /*PHPCore::widget('corporativo/footer', $this);*/ ?>

		<?php /*PHPCore::widget('BodyData', $this);*/ ?>

	</div>

<?php $this->html->bottom(); ?>