<?php

use PC\Routes;
use PC\View;
use PC\Types\DataTypes;
use PC\Libraries\FilesLibrary;
use Com\Rackspace\RackspaceCloudFiles;

Routes::get([
	"rule"=>"/"
], function(){

	View::getInstance(Routes::params())->load("InicioView")->show();
	
});

Routes::get([
	"rule"=>"{/rango}",
	"where"=>["rango"=>[
		["type"=>DataTypes::RANGE_TYPE, "params"=>[2, 5]]
	]]
], function($rango){

	echo "$rango esta en el rango permitido.";
	//View::getInstance(Routes::params())->load("InicioView")->show();
	
});

// Páginas en español
Routes::any([
	"rule"=>"esmx{/vista?}",
], function($vista){

	View::getInstance(Routes::params())->load("esmx.".ucfirst($vista)."View")->show();
	//Routes::callMethod("App.Controllers.UsuariosController");
	
});

Routes::group([
	'domain' => '{empresa}.testing.local',
	"rule" => 'api'
], function ($empresa) {

	Routes::get([
		"rule"=>'access{/method}',
		"middleware"=>[
			"App\Middlewares\TenantMiddleware",
			"App\Middlewares\SessionMiddleware",
			"App\Middlewares\CheckMiddleware"
		]
	], function ($empresa, $method) {

		Routes::callMethod("App.Controllers.AccessController", $method);

	});

});

?>