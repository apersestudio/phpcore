<?php

return array(
	"session"=>array(
		// Si hay una manejador de sesión por base de datos
		"databaseManager"=>"App\Managers\SessionManager",
		// Tiempo en segundos en el que la configuración de un cliente estará guarda en su dispositivo
		// 60 segundos * 60 minutos * 12 horas = 43200 + Tiempo actual
		"expiration"=>43200,
		// Tiempo de inactividad en segundos en el que la sesión se cerrará si no es utilizada
		// 60 segundos * 60 minutos = 3600 + Tiempo actual
		'inactivity'=>3600
	),
	"language"=>array(
		// Idioma predeterminado
		"default"=>"es-MX",
		"manager"=>"language",
		"available"=>array(
			"es-MX"=>array("unix"=>"es_MX", "windows"=>"Spanish_Mexico", "timezone"=>"America/Mexico_City"),
			"en-US"=>array("unix"=>"en_US", "windows"=>"English_USA", "timezone"=>"America/New_York")
		)
	),
	// Si los datos que viajan entre el servidor y el cliente deben encriptarse
	'encrypted'=>false,
	// Configuración de la base de datos del servidor principal
	'databases'=>array(
		// Esta es la conexión principal, usada para administrar las bases de datos secundarias
		'Master'=>array(
			'driver'=>'postgresql',
			"host"=>"localhost",
			'base'=>'postgres',
			'user'=>'postgres',
			'pass'=>'apers123'
		) 
	),
);
?>