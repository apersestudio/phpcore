<?php

namespace App\Middlewares {

	use App\Models\UsuariosModel;

	class CheckMiddleware {

		public static function run($params=null) {
			$model = new UsuariosModel();
			$usuario = $model->select(["usuarios_id", "usuarios_apellidos", "usuarios_email"])->first();
			print_r($usuario);

			//$model->delete()->where("usuarios_nombres", "=", "Fulano")->execute();

			$columns = ["usuarios_nombres", "usuarios_apellidos", "usuarios_email", "usuarios_empresa_id"];
			$data = [
				["Fulano 1", "Pérez 1", "muchologo001@gmail.com@.mx", 1],
				["Fulano 2", "Pérez 2", "muchologo002@gmailcom", 1],
				["Fulano 3", "Pérez 3", "muchologo003", 1],
				["Fulano 4", "Pérez 4", "@gmail.com", 1],
				["Fulano 5", "Pérez 5", "gmail.com", 1],
				["Fulano 6", "Pérez 6", "muchologo006@gmail.com", 1],
				["Fulano 7", "Pérez 7", "muchologo007@gmail.com", 1],
				["Fulano 8", "Pérez 8", "muchologo008@gmail.com", 1],
				["Fulano 9", "Pérez 9", "muchologo009@gmail.com", 1],
				["Fulano 10", "Pérez 10", "muchologo010@gmail.com", 1],
				["Fulano 11", "Pérez 11", "muchologo011@gmail.com", 1],
				["Fulano 12", "Pérez 12", "muchologo012@gmail.com", 1],
				["Fulano 13", "Pérez 13", "muchologo013@gmail.com", 1],
				["Fulano 14", "Pérez 14", "muchologo014@gmail.com", 1],
				["Fulano 15", "Pérez 15", "muchologo015@gmail.com", 1],
				["Fulano 16", "Pérez 16", "muchologo016@gmail.com", 1],
				["Fulano 17", "Pérez 17", "muchologo017@gmail.com", 1],
				["Fulano 18", "Pérez 18", "muchologo018@gmail.com", 1],
				["Fulano 19", "Pérez 19", "muchologo019@gmail.com", 1],
				["Fulano 20", "Pérez 20", "muchologo020@gmail.com", 1],
				["Fulano 21", "Pérez 21", "muchologo021@gmail.com", 1],
				["Fulano 22", "Pérez 22", "muchologo022@gmail.com", 1],
				["Fulano 23", "Pérez 23", "muchologo023@gmail.com", 1],
				["Fulano 24", "Pérez 24", "muchologo024@gmail.com", 1],
				["Fulano 25", "Pérez 25", "muchologo025@gmail.com", 1],
				["Fulano 26", "Pérez 26", "muchologo026@gmail.com", 1],
				["Fulano 27", "Pérez 27", "muchologo027@gmail.com", 1]
			];

			$inserts = $model->createMultiple($columns, $data, 10);
			if (!$inserts) {
				//print_r($model->getErrors());
			} else {
				//echo "Se crearon ".$inserts." registros...";
			}

			return true;
		}

	}

}

?>