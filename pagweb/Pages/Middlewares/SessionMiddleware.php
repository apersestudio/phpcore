<?php

namespace App\Middlewares {

	use PC\Libraries\SessionLibrary;

	class SessionMiddleware {

		public static function run($params=null) {

			SessionLibrary::start();
			return true;
		}

	}

}

?>