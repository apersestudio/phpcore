<?php

namespace App\Middlewares {

	use PC\Config;

	class TenantMiddleware {

		public static function run($params=null) {
			// Aquí definimos la base de datos particular
			Config::set("databases.company", [
				'driver'=>'postgresql',
				'host'=>'localhost',
				'base'=>'postgres',
				'user'=>'postgres',
				'pass'=>'apers123',
				'schema'=>'test'
			]);
			return true;
		}

	}

}

?>