<?php

namespace App\Controllers {

	use PC\Controller;

	class UsuariosController extends Controller {

		public function index(){
			echo "DEFAULT: ".$this->idusuario;
		}

		public function search(){
			echo "SEARCH: ".$this->idusuario;
		}

		public function create() {
			echo "CREATE: ".$this->idusuario;
		}

		public function update() {
			echo "UPDATE: ".$this->idusuario;
		}

		public function delete() {
			echo "DELETE: ".$this->idusuario;
		}

	}

}

?>