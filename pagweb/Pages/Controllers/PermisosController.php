<?php

class Permisos {

	private $numbersTable = [
		"ver"=>1,
		"crear"=>2,
		"modificar"=>4,
		"borrar"=>8,
		"imprimir"=>16,
		"descargar"=>32,
		"subir"=>64,
		"compartir"=>128,
		"enviar"=>256,
		"buscar"=>512,
		"autorizar"=>1024,
		"rechazar"=>2048,
		"deshacer"=>4096,
		"exportar"=>8192,
		"importar"=>16384,
		"sincronizar"=>32768
	];

	/*
	* USAGE
	* $p = new Permisos();
	* $p->setUser($iduser);
	* $p->puede("ver", $idsec);
	*/
	
	public function puede($permiso, $idsec) {
		$n = $numbersTable[$permiso];
		$select = "SELECT (((".$n."::bit(20)) & _per)::integer = ".$n.") AS ".$permiso." FROM public.menu WHERE _idsec = $1";
		$resultado = PHPCore::db("Company")->select(array(
			"command"=>$select, "params"=>array(array("tipo"=>"number", "valor"=>$idsec))
		));
		return $resultado[$permiso];			
	}

}