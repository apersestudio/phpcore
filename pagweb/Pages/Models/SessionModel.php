<?php

namespace App\Models {

	use PC\Model;

	class SessionModel extends Model {

		protected $connection = 'company';
		//protected $schema = 'test';
		protected $table = 'session';
		protected $primaryKey = 'session_id';
		// No hay secuencia porque el ID de sesión se genera, no es autoincremental
		//protected $sequence = "session_id_seq";

		protected $definition = [
			"session_id"=>["type"=>"string"],
			"session_data"=>["type"=>"string"],
			"session_fecha"=>["type"=>"datetime", "required"=>true, "default"=>"CURRENT_TIMESTAMP"],
			"session_ip"=>["type"=>"inet", "required"=>true]
		];

		protected $fillable = [
			"session_data",
			"session_fecha",
			"session_ip"
		];

	}

}

?>