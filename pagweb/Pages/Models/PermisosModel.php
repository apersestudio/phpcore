<?php

namespace App\Models {

	use PC\Model;

	class PermisosModel extends Model {

		protected $connection = 'company';
		protected $schema = 'test';
		protected $table = 'usuarios';
		protected $primaryKey = 'usuarios_id';
		protected $sequence = "usuarios_usuarios_id_seq";

		protected $definition = [
			"usuarios_id"=>["type"=>"int"],
			"usuarios_nombres"=>["type"=>"string", "required"=>true],
			"usuarios_apellidos"=>["type"=>"string", "required"=>true],
			"usuarios_email"=>["type"=>"email", "constraints"=>"unique", "required"=>true],
			"usuarios_telefono"=>["type"=>"string"],
			"usuarios_edad"=>["type"=>"int", "constraints"=>"range:18-60"],
			"usuarios_empresa_id"=>["type"=>"int", "constraints"=>"foreign:empresas"],
			"usuarios_cumple"=>["type"=>"date"],
		];

		protected $fillable = [
			"usuarios_nombres",
			"usuarios_apellidos",
			"usuarios_email",
			"usuarios_telefono",
			"usuarios_edad",
			"usuarios_empresa_id",
			"usuarios_cumple"
		];

		protected $relations = [
			"productos"=>[
				"foreignKey"=>"productos_idusuario",
				"primaryKey"=>"usuarios_id",
				"model"=>"App\Models\ProductosModel"
			],
			"empresas"=>[
				"foreignKey"=>"empresa_id",
				"primaryKey"=>"usuarios_empresa_id",
				"model"=>"App\Models\EmpresasModel"
			]
		];

	}

}

?>