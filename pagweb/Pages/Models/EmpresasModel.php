<?php

namespace App\Models {

	use PC\Model;

	class EmpresasModel extends Model {

		protected $connection = 'company';
		protected $schema = 'test';
		protected $table = 'empresas';
		protected $primaryKey = 'empresa_id';

		protected $definition = [
			"empresa_id"=>["type"=>"int"],
			"empresa_nombre"=>["type"=>"string"]
		];

		protected $fillable = [
			"empresa_nombre"
		];

	}

}

?>