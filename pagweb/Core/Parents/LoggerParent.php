<?php
namespace PC\Parents {

    use PC\Interfaces\LoggerInterface;

    abstract class LoggerParent implements LoggerInterface {

        protected $logger;
        
        // System is unusable.
        public function emergency($message, array $context = array()) {
            $this->log("emergency", $message, $context);
        }
        // Action must be taken immediately: Entire website down, database unavailable, etc.
        public function alert($message, array $context = array()) {
            $this->log("alert", $message, $context);
        }
        // Critical conditions such as Application component unavailable, unexpected exception.
        public function critical($message, array $context = array()) {
            $this->log("critical", $message, $context);
        }
        // Runtime errors that do not require immediate action but should typically be logged and monitored.
        public function error($message, array $context = array()) {
            $this->log("error", $message, $context);
        }
        // Exceptional occurrences that are not errors.
        public function warning($message, array $context = array()) {
            $this->log("warning", $message, $context);
        }
        // Normal but significant events.
        public function notice($message, array $context = array()) {
            $this->log("notice", $message, $context);
        }
        // Interesting events.
        public function info($message, array $context = array()) {
            $this->log("info", $message, $context);
        }
        // Detailed debug information.
        public function debug($message, array $context = array()) {
            $this->log("debug", $message, $context);
        }

        abstract public function log($level, $message, array $context = array());
    }

}

?>