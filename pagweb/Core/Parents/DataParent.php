<?php

namespace PC\Parents {

	abstract class DataParent {

		protected $messages = [];

		protected $success = false;

		// Mensages de error
		protected $errors = array(
			// Validation
			"not_empty"=>"No puede estar vacio.",
			"not_string"=>"Cadena inválida.",
			"not_number"=>"Numero inválido.",
			"not_email"=>"Correo inválido",
			"not_min"=>"Valor mínimo __min__.",
			"not_max"=>"Valor máximo __max__.",
			"not_between"=>"Valor mínimo __min__ y máximo __max.",
			"not_min_length"=>"Mínimo __min__ caractéres.",
			"not_max_length"=>"Máximo __max__ caractéres.",
			"not_between_length"=>"Mínimo __min__ y máximo __max caractéres.",
			"not_phone"=>"Teléfono inválido.",
			"not_ip"=>"IP inválida.",
			"not_id"=>"ID inválida.",
			"not_md5"=>"MD5 inválido.",
			"not_sha1"=>"SHA1 inválido.",
			"not_sha256"=>"SHA256 inválido.",
			"not_available"=>"No disponible",
			"not_found"=>"Sin resultados.",
			"not_params"=>"Sin parametros",
			"not_url"=>"URL inválida",
			
			// Database
			"cant_create_record"=>"No se pudo crear",
			"cant_delete_record"=>"No se pudo borrar",
			"cant_update_record"=>"No se pudo actualizar",
			"cant_find_record"=>"No se encontraron resultados",
			"cant_generate_sql"=>"No se pudo generar SQL.",
			"cant_sincronize"=>"No se pudo sincronizar",
			
			// Common
			"sincronized"=>"Sincronización completa",
			"success"=>"OK",
			"available"=>"Disponible",
			"deleted"=>"Borrado",
			"created"=>"Creado",
			"updated"=>"Actualizado",
			
			// Media
			"cant_create"=>"No se pudo crear el archivo local",
			"cant_delete"=>"No se pudo eliminar el archivo local",
			"cant_update"=>"No se pudo actualizar el archivo local",
			"cant_upload"=>"No se pudo subir",
			"cant_rename_dir"=>"No se pudo renombrar el directorio $1",
			
			"cant_optimize_image"=>"No se pudo optimizar la imagen local.",
			"cant_create_image"=>"No se pudo crear la imagen local",
			"cant_delete_image"=>"No se pudo borrar la imagen local",
			
			"cant_optimize_thumbs"=>"No se pudieron optimizar las miniaturas locales.",
			"cant_create_thumbs"=>"No se pudieron crear las miniaturas locales",
			"cant_delete_thumbs"=>"No se pudieron borrar las miniaturas locales",
			
			"cant_upload_image"=>"No se pudo subir la imagen",
			"cant_upload_thumbs"=>"No se pudieron subir las miniaturas",
			"cant_create_remote"=>"No se pudo crear el archivo remoto",
			"cant_delete_remote"=>"No se pudo eliminar el archivo remoto",
			"cant_update_remote"=>"No se pudo actualizar el archivo remoto",
			
			"cant_optimize_image_remote"=>"No se pudo optimizar la imagen remoto",
			"cant_create_image_remote"=>"No se pudo crear la imagen remoto",
			"cant_delete_image_remote"=>"No se pudo borrar la imagen remoto",
			
			"cant_optimize_thumbs_remote"=>"No se pudieron optimizar las miniaturas remotas",
			"cant_create_thumbs_remote"=>"No se pudieron crear las miniaturas remotas",
			"cant_delete_thumbs_remote"=>"No se pudieron borrar las miniaturas remotas"
			
		);
		
		/* ************************************************************************************** */
		/* FUNCIONES PROTEGIDAS PARA REALIZAR COMPROBACIONES ************************************ */
		/* ************************************************************************************** */
		public final function addMessage($message=false, $field=false) {
			if (!empty($message)) { $this->messages[] = (empty($field)?"":$field.": ").$message; }
		}

		public final function clear() {
			$this->messages = [];
			$this->success = false;
		}

		private final function evaluateValue($v, $error, $field=false) {
			$m = $v ? '' : $this->errors[$error];
			$this->addMessage($m, $field);
			return $v;
		}
		
		/* ************************************************************************************** */
		/* FUNCIONES PUBLICAS PARA REALIZAR COMPROBACIONES ************************************** */
		/* ************************************************************************************** */
		public final function getCommonErrors($customErrors=array()) {
			if (empty($this->errors)) { return $customErrors; }
			return array_merge($this->errors, $customErrors);
		}
		public final function validString($value='', $field=false){
			$v = preg_match("/[\w]+$/", $value);
			return $this->evaluateValue($v, "not_string", $field);
		}
		public final function validNumber($value='', $field=false){
			$v = is_numeric($value);
			return $this->evaluateValue($v, "not_number", $field);
		}
		public final function validMin($value='', $min=1, $field=false){
			$v = is_numeric($value) && $value >= $min;
			$m = $v ? '' : str_replace("__min__", $min, $this->errors["not_min"]);
			$this->addMessage($m, $field);
			return $v;
		}
		public final function validMax($value='', $max=2, $field=false){
			$v = is_numeric($value) && $value <= $max;
			$m = $v ? '' : str_replace("__max__", $min, $this->errors["not_max"]);
			$this->addMessage($m, $field);
			return $v;
		}
		public final function validRange($value='', $min=1, $max=2, $field=false){
			$v = is_numeric($value) && $value >= $min && $value <= $max;
			$m = $v ? '' : str_replace(array("__min__", "__max__"), array($min, $max), $this->errors["not_between"]);
			$this->addMessage($m, $field);
			return $v;
		}
		public final function validMinLength($value='', $min=1, $field=false){
			$v = strlen($value) >= $min;
			$m = $v ? '' : str_replace("__min__", $min, $this->errors["not_min_length"]);
			$this->addMessage($m, $field);
			return $v;
		}
		public final function validMaxLength($value='', $max=2, $field=false){
			$v = strlen($value) <= $min;
			$m = $v ? '' : str_replace("__max__", $min, $this->errors["not_max_length"]);
			$this->addMessage($m, $field);
			return $v;
		}
		public final function validRangeLength($value='', $min=1, $max=2, $field=false){
			$l = strlen($value);
			$v = ($l >= $min) && ($l <= $max);
			$m = $v ? '' : str_replace(array("__min__", "__max__"), array($min, $max), $this->errors["not_between_length"]);
			$this->addMessage($m, $field);
			return $v;
		}
		public final function validHash($string, $length, $error, $field=false) {
			$v = preg_match('/^[a-fA-F0-9]{'.$length.'}$/i', $string);
			return $this->evaluateValue($v, $error, $field);
		}
		public final function validMD5($string='z', $field=false) {
			return $this->validHash($string, 32, "not_md5", $field);
		}
		public final function validSHA1($string='z', $field=false){
			return $this->validHash($string, 40, "not_sha1", $field);
		}
		public final function validSHA256($string='z', $field=false){
			$v = $this->validHash($string, 64, "not_sha256", $field);
		}

		public final function validID($id, $field=false) {
			$v = is_numeric($id) && floatval($id) > 0;
			return $this->evaluateValue($v, "not_id", $field);
		}

		public final function validEmail($email, $field=false) {
			$v = false;
			if ($email != "" && filter_var($email, FILTER_VALIDATE_EMAIL)) { $v = true; }
			return $this->evaluateValue($v, "not_email", $field);
		}

		public final function validIP($ip, $field=false) {
			$v = false;
			if ($ip != "" && filter_var($ip, FILTER_VALIDATE_IP)) { $v = true; }
			return $this->evaluateValue($v, "not_ip", $field);
		}

		public final function validURL($url, $field=false) {
			$v = false;
			if ($url != "" && filter_var($url, FILTER_VALIDATE_URL)) { $v = true; }
			return $this->evaluateValue($v, "not_url", $field);
		}
		
	}

}

?>