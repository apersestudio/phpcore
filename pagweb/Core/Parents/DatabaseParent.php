<?php 

namespace PC\Parents {

	use PC\Interfaces\LoggerInterface;
	use PC\Constants\LogLevel;
	use PC\Libraries\Crypto;

	abstract class DatabaseParent implements LoggerInterface {

		use \PC\Traits\LoggerTrait;
		use \PC\Traits\DataTrait;

		protected $connection = false;
		protected $connected = false;
		protected $command = '';
		protected $query = false;

		protected $user = false;
		protected $pass = false;
		protected $host = 'localhost';
		protected $base = false;
		protected $port = 0;
		protected $utf8 = true;
		protected $className = '';

		/* Transacciones */
		abstract protected function rollback();
		abstract protected function commit();
		abstract protected function startTransaction();
		abstract protected function connect();

		abstract protected function close();
		abstract protected function escape($value);
		abstract protected function escapeBoolean($bool);
		abstract protected function query($command);
		abstract public function lasterror();

		abstract protected function numrows($query);
		abstract protected function numfields($query);
		abstract protected function fieldname($query, $index);
		abstract protected function fetcharray($query);
		abstract protected function fetchall($query);
		abstract protected function fetchobject($query);
		abstract protected function affectedrows($query);
		abstract protected function resultseek($query, $index);
		abstract protected function nextvalue($sequence);
		abstract protected function currentvalue($sequence);

		// Aliases
		public function rb() { return $this->rollback(); }
		public function cm() { return $this->commit(); }
		public function st() { return $this->startTransaction(); }
		public function nr($query=false) { return $this->numrows($query); }
		public function nf($query=false) { return $this->numfields($query); }
		public function fn($query=false, $index=0) { return $this->fieldname($query); }
		public function fa($query=false) { return $this->fetcharray($query); }
		public function faa($query=false) { return $this->fetchall($query); }
		public function fo($query=false) { return $this->fetchobject($query); }
		public function ar($query=false) { return $this->affectedrows($query); }
		public function rs($query=false, $index=0) { return $this->resultseek($query, $index); }
		public function nv($sequence=[]) { return $this->nextvalue($sequence); }
		public function cv($sequence) { return $this->currentvalue($sequence); }
		public function q($command) { return $this->query($command); }

		public final function __get($property) {
			if (isset($this->$property)) {
				return $this->$property;
			}
			return null;
		}

		public final function field($str) {
			return preg_replace("/[^_\p{L}0-9]/u", "", $str);
		}
		public final function fullTrim($string) {
			return trim(preg_replace('/\s+/', ' ', $string));
		}
		public final function oneLineTrim($string) {
			return $this->fullTrim(preg_replace("/[\r\n\t]/", " ", $string));
		}
		public final function now() {
			return date("Y-m-d h:i:s a");
		}
		public final function timestamp() {
			return date("Y-m-d H:i:s");
		}

		public final function generatePassword($length=8) {
			$length = ($length > 8) ? 8 : $length;
			return bin2hex(random_bytes($length));
		}

		public final function escapeByType($type, $value) {
			switch ($type) {
				case 'string' : case 'char' : case 'character' : case 'inet' : case 'url' : case 'email' :
				$value = "'".$this->escape($value)."'";
				break;

				case 'pass' : case 'password' :
				$value = "'".Crypto::hash_blowfish_2a($this->escape($value))."'";
				break;
				
				case 'array' :
				$value = (empty($value) || !is_array($value)) ? 'NULL' : "'{".$this->escape(implode(",", $value))."}'";
				break;
				
				case 'int' : case 'integer' : case 'number' :
				$value = intval($value);
				break;
				
				case 'float' : case 'double' : case 'decimal' : case 'numeric' :
				$value = floatval($value);
				break;
				
				case 'bool' : case 'boolean' :
				$value = $this->escapeBoolean($value);
				break;
				
				case 'date' :
				$value = "'".date("Y-m-d", strtotime($value))."'";
				break;
				
				case 'datetime' :
				$value = "'".date("Y-m-d H:i:s", strtotime($value))."'";
				break;
				
				case 'field' :
				$value = $this->field($value);
				break;

				default :
				$value = 'NULL';
			}
			return $value;
		}

		public final function log($level, $message, array $context=[]) {

			$logerror = "---------------------------------------------------------------------------------------------\r\n";
			$logerror.= "[".$this->now()." / ".__CLASS__."]\r\n";
			$logerror.= "[".$message.": ".$this->oneLineTrim($this->command)."]\r\n";

			foreach (debug_backtrace() as $index=>$trace) {
				if (empty($trace["file"]) || empty($trace["function"]) || empty($trace["line"])) { continue; }
				$class = !empty($trace["object"]) ? get_class($trace["object"])."::" : "";
				$logerror.= "FILE ".$trace["file"]." ON LINE ".$trace["line"]." FUNCTION ".$class.$trace["function"]."()"."\r\n";
			}
			
			$logerror.= "---------------------------------------------------------------------------------------------\r\n";
			switch ($level) {
				case LogLevel::ERROR : $this->error($logerror); break;
				case LogLevel::CRITICAL : $this->critical($logerror); break;
			}
		}
		public final function connected() {
			return $this->connected;
		}
		protected final function vq($query, $command) {
			if (is_bool($query)) {
				$this->log(LogLevel::ERROR, "Query is not a valid resource.");
				return false;
			}
			return true;
		}

		protected final function validateQuery($query, $command) {
			return $this->vq($query, $command);
		}
	}
	
}
?>