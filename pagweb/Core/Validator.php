<?php

namespace PC {

	use PC\Databases\QueryBuilder;
	use PC\Libraries\ImageLibrary;
	use PC\Libraries\FilesLibrary;
	use PC\Libraries\MimeLibrary;

	class Validator {

		use \PC\Traits\DataTrait;

		private $fields;
		private $fieldsErrors;
		private $fieldsValids;

		private $rules;
		private $messages;
		private $errors;

		// Para almacenar models cuando las restricciones son de tipo database
		private $models;
		private $images;
		private $mode; 

		public function __construct() {
			$this->fieldsErrors = array();
			$this->models = array();
			$this->images = array();
			$this->mode = 'create';
		}
		public function setMode($mode) {
			$this->mode = $mode;
		}

		public function fieldsErrors() {
			return $this->fieldsErrors;
		}
		public function has($fieldName) {
			return isset($this->fields[$fieldName]);
		}
		public function input($fieldName) {
			return $this->has($fieldName) ? $this->fields[$fieldName] : null;
		}
		public function setInput($fieldName, $value) {
			$this->fields[$fieldName] = $value;
		}
		public function fields() {
			return $this->fields;
		}
		private function fieldMessage($field, $type, $default) {
			if (isset($this->messages[$field.".".$type])) {
				$default = $this->messages[$field.".".$type];
			}
			$this->fieldsErrors[$field][$type] = $default;
		}

		private function includeModel($model) {
			if (!isset($this->models[$model])) {
				$class = "\\".str_replace(".", "\\", $model);
				if (class_exists($class)) {
					$this->models[$model] = new $class();
				} else {
					$this->errors[] = "No se pudo comprobar unique porque no se encontró el modelo ".$model;
				}
			}
			return isset($this->models[$model]) ? $this->models[$model] : false;
		}

		private function includeImage($path) {
			$key = md5($path);
			if (!isset($this->images[$key])) {
				$this->images[$key] = new ImageLibrary($path);
			}
			return $this->images[$key];
		}

		private function parseRules($constraints) {
			$ruleSet = array();
			$rules = explode("|", $constraints);

			foreach ($rules as $rule) {
				if (strpos($rule, ":") !== false) {
					$ruleParts = explode(":", $rule, 2);
					$ruleName = array_shift($ruleParts);
					$ruleParams = $ruleParts[0];
				} else {
					$ruleParams = false;
					$ruleName = $rule;
				}
				$ruleSet[$ruleName] = $ruleParams;
			}
			return $ruleSet;
		}

		private function validateRequired($field, $definition) {
			// Un campo requerido es válido cuando:
			// El nombre de indice esta definido en los campos enviados ó
			// Existe un valor predeterminado para dicho campo
			if (!array_key_exists($field, $this->fields)) {
				// Cuando existe un valor predeterminado:
				// Agregamos dicho valor al arreglo de campo que el usuario envió
				if (isset($definition["default"])) {
					$this->fields[$field] = $definition["default"];
				} else {
					$defaultMessage = 'Es requerido y no tiene un valor predeterminado.';
					$keyError = 'required';
					$this->fieldsValids[$field] = false;
					$this->fieldMessage($field, $keyError, $defaultMessage);
				}
			}
		}

		private function validateType($field, $type) {
			// Solo continuamos con la validación cuando el campo existe o no es requerido
			if ($this->fieldsValids[$field]) {
				$value = $this->fields[$field];
				// Si el tipo de datos esta presente lo validamos
				if (!self::validByType($type, $value)) {
					$defaultMessage = 'No contiene un dato '.$type.' válido.';
					$keyError = 'type';
					$this->fieldsValids[$field] = false;
					$this->fieldMessage($field, $keyError, $defaultMessage);
				} else {
					if ($type == 'date') {
						$this->fields[$field] = date(DATE_ATOM, strtotime($value));
					}
					$this->fieldsValids[$field] = true;
				}
			}
		}

		private function validateUnique($field, $modelName) {
			// Solo continuamos con la validación cuando el campo existe o no es requerido
			if ($this->fieldsValids[$field]) {
				// $ruleParams representa un nombre de clase de modelo
				$model = $this->includeModel($modelName);
				if (!!$model) {
					$first = $model->count()->where($field, '=', $this->fields[$field])->first();
					if ($first["count"] > 0) {
						$defaultMessage = "El valor ".$this->fields[$field]." ya existe";
						$keyError = 'unique';
						$this->fieldMessage($field, $keyError, $defaultMessage);
					}
				}
			}
		}

		private function validateExists($field, $modelName) {
			// Solo continuamos con la validación cuando el campo existe o no es requerido
			if ($this->fieldsValids[$field]) {
				$model = $this->includeModel($modelName);
				if (!!$model) {
					$first = $model->count()->where($field, '=', $this->fields[$field])->first();
					if ($first["count"] == 0) {
						$defaultMessage = 'El valor '.$this->fields[$field].' no existe.';
						$keyError = 'exists';
						$this->fieldMessage($field, $keyError, $defaultMessage);
					}
				}
			}
		}

		private function validateForeign($field, $modelName, $columnName) {
			// Solo continuamos con la validación cuando el campo existe o no es requerido
			if ($this->fieldsValids[$field]) {
				$model = $this->includeModel($modelName);
				if (!!$model) {
					$first = $model->count()->where($columnName, '=', $this->fields[$field])->first();
					if ($first["count"] == 0) {
						$defaultMessage = 'No existe la referencia '.$this->fields[$field];
						$keyError = 'foreign';
						$this->fieldMessage($field, $keyError, $defaultMessage);
					}
				}
			}
		}

		private function validateDateAfter($field, $timestamp) {
			// Solo continuamos con la validación cuando el campo existe o no es requerido
			if ($this->fieldsValids[$field]) {
				if ($this->fields[$field] <= $timestamp) {
					$defaultMessage = 'La fecha debe ser posterior a '.date(DATE_ATOM, $timestamp);
					$keyError = 'after';
					$this->fieldMessage($field, $keyError, $defaultMessage);
				}
			}
		}

		private function validateDateAfterOrEqual($field, $timestamp) {
			// Solo continuamos con la validación cuando el campo existe o no es requerido
			if ($this->fieldsValids[$field]) {
				if ($this->fields[$field] < $timestamp) {
					$defaultMessage = 'La fecha debe ser posterior o igual a '.date(DATE_ATOM, $timestamp);
					$keyError = 'after_or_equal';
					$this->fieldMessage($field, $keyError, $defaultMessage);
				}
			}
		}

		private function validateDateBefore($field, $timestamp) {
			// Solo continuamos con la validación cuando el campo existe o no es requerido
			if ($this->fieldsValids[$field]) {
				if ($this->fields[$field] >= $timestamp) {
					$defaultMessage = 'La fecha debe ser anterior a '.date(DATE_ATOM, $timestamp);
					$keyError = 'before';
					$this->fieldMessage($field, $keyError, $defaultMessage);
				}
			}
		}

		private function validateDateBeforeOrEqual($field, $timestamp) {
			// Solo continuamos con la validación cuando el campo existe o no es requerido
			if ($this->fieldsValids[$field]) {
				if ($this->fields[$field] > $timestamp) {
					$defaultMessage = 'La fecha debe ser anterior o igual a '.date(DATE_ATOM, $timestamp);
					$keyError = 'before_or_equal';
					$this->fieldMessage($field, $keyError, $defaultMessage);
				}
			}
		}

		private function validateDateBetween($field, $from, $to) {
			// Solo continuamos con la validación cuando el campo existe o no es requerido
			if ($this->fieldsValids[$field]) {
				$value = $this->fields[$field];
				if ($value < $from || $value > $to) {
					$defaultMessage = 'La fecha debe estar entre '.date(DATE_ATOM, $from).' y '.date(DATE_ATOM, $to);
					$keyError = 'between';
					$this->fieldMessage($field, $keyError, $defaultMessage);
				}
			}
		}
		private function validateActiveURL($field) {
			// Solo continuamos con la validación cuando el campo existe o no es requerido
			if ($this->fieldsValids[$field]) {
				$value = $this->fields[$field];
				$urlParts = parse_url($value);
				$aRecord = dns_get_record($urlParts["host"], DNS_A);
				if (empty($aRecord)) {
					$defaultMessage = 'La url '.$value.' no tiene un registro A válido.';
					$keyError = 'active_url';
					$this->fieldMessage($field, $keyError, $defaultMessage);
				}
			}
		}
		
		private function validateAlnumDash($field) {
			// Solo continuamos con la validación cuando el campo existe o no es requerido
			if ($this->fieldsValids[$field]) {
				if (!preg_match('/^[[:alnum:]-_]+$/', $this->fields[$field])) {
					$defaultMessage = 'Solo puede contener letras, numeros y guiones.';
					$keyError = 'alnum_dash';
					$this->fieldMessage($field, $keyError, $defaultMessage);
				}
			}
		}
		
		private function validateAlnum($field) {
			// Solo continuamos con la validación cuando el campo existe o no es requerido
			if ($this->fieldsValids[$field]) {
				if (!preg_match('/^[[:alnum:]]+$/', $this->fields[$field])) {
					$defaultMessage = 'Solo puede contener letras y numeros.';
					$keyError = 'alnum';
					$this->fieldMessage($field, $keyError, $defaultMessage);
				}
			}
		}

		private function validateStringRange($field, $min, $max) {
			// Solo continuamos con la validación cuando el campo existe o no es requerido
			if ($this->fieldsValids[$field]) {
				$value = $this->fields[$field];
				if (strlen($value) < $min || strlen($value) > $max) {
					$defaultMessage = 'Debe tener entre '.$min.' y '.$max.' caractéres.';
					$keyError = 'string_range';
					$this->fieldMessage($field, $keyError, $defaultMessage);
				}
			}
		}
		private function validateNumberRange($field, $min, $max) {
			// Solo continuamos con la validación cuando el campo existe o no es requerido
			if ($this->fieldsValids[$field]) {
				$value = $this->fields[$field];
				if ($value < $min || $value > $max) {
					$defaultMessage = 'Debe estar entre '.$min.' y '.$max.'.';
					$keyError = 'number_range';
					$this->fieldMessage($field, $keyError, $defaultMessage);
				}
			}
		}
		private function validateMin($field, $min) {
			// Solo continuamos con la validación cuando el campo existe o no es requerido
			if ($this->fieldsValids[$field]) {
				$value = $this->fields[$field];
				if ($value < $min) {
					$defaultMessage = 'Debe tener un valor mímino de '.$min.'.';
					$keyError = 'min';
					$this->fieldMessage($field, $keyError, $defaultMessage);
				}
			}
		}
		private function validateMax($field, $max) {
			// Solo continuamos con la validación cuando el campo existe o no es requerido
			if ($this->fieldsValids[$field]) {
				$value = $this->fields[$field];
				if ($value > $max) {
					$defaultMessage = 'Debe tener un valor máximo de '.$max.'.';
					$keyError = 'max';
					$this->fieldMessage($field, $keyError, $defaultMessage);
				}
			}
		}
		   
		private function validateDimensions($field, $width, $height) {
			// Solo continuamos con la validación cuando el campo existe o no es requerido
			if ($this->fieldsValids[$field]) {
				$value = $this->fields[$field];

				$image = $this->includeImage($value["tmp_name"]);
				$size = $image->getSize();

				if ($size["width"] != $width || $size["height"] != $height) {
					$defaultMessage = 'Debe tener un tamaño de '.$width.' por '.$height.' pixéles.';
					$keyError = 'dimensions';
					$this->fieldMessage($field, $keyError, $defaultMessage);
				}
			}
		}

		private function validateImage($field) {
			// Solo continuamos con la validación cuando el campo existe o no es requerido
			if ($this->fieldsValids[$field]) {
				$value = $this->fields[$field];
				if (!FilesLibrary::isImage($value["name"])) {
					$defaultMessage = 'No es una imagen.';
					$keyError = 'image';
					$this->fieldMessage($field, $keyError, $defaultMessage);
				}
			}
		}

		private function validateMimes($field, $mimes) {
			// Solo continuamos con la validación cuando el campo existe o no es requerido
			if ($this->fieldsValids[$field]) {
				$value = $this->fields[$field];
				$mimeDetected = MimeLibrary::guess($value["name"], $value["type"]);
				if (!in_array($mimeDetected, $mimes)) {
					$defaultMessage = 'Solo admite los siguientes tipos: '.implode(',',$mimes).'.';
					$keyError = 'mimes';
					$this->fieldMessage($field, $keyError, $defaultMessage);
				}
			}
		}

		private function validateIn($field, $list) {
			// Solo continuamos con la validación cuando el campo existe o no es requerido
			if ($this->fieldsValids[$field]) {
				$value = $this->fields[$field];
				if (!in_array($value, $list)) {
					$defaultMessage = 'Solo admite los siguientes valores: '.implode(',',$list).'.';
					$keyError = 'in';
					$this->fieldMessage($field, $keyError, $defaultMessage);
				}
			}
		}
		private function validateNotIn($field, $list) {
			// Solo continuamos con la validación cuando el campo existe o no es requerido
			if ($this->fieldsValids[$field]) {
				$value = $this->fields[$field];
				if (in_array($value, $list)) {
					$defaultMessage = 'No debe tener ninguno de los siguientes valores: '.implode(',',$list).'.';
					$keyError = 'not_in';
					$this->fieldMessage($field, $keyError, $defaultMessage);
				}
			}
		}
		
		private function validateJSON($field) {
			// Solo continuamos con la validación cuando el campo existe o no es requerido
			if ($this->fieldsValids[$field]) {
				$value = $this->fields[$field];
				if (!$this->validJSON($value)) {
					$defaultMessage = 'No contiene un JSON válido.';
					$keyError = 'json';
					$this->fieldMessage($field, $keyError, $defaultMessage);
				}
			}
		}

		private function validateSame($field, $otherField) {
			// Solo continuamos con la validación cuando el campo existe o no es requerido
			if ($this->fieldsValids[$field]) {
				$value = $this->fields[$field];
				$keyError = 'same';
				if (!isset($this->fields[$otherField])) {
					$defaultMessage = 'No existe el campo '.$otherField.'.';
					$this->fieldMessage($field, $keyError, $defaultMessage);
				} else if ($this->fields[$field] != $this->fields[$otherField]) {
					$defaultMessage = 'Los campos no coinciden.';
					$this->fieldMessage($field, $keyError, $defaultMessage);
				}
			}
		}

		private function validateNullable($field) {
			if (!isset($this->fields[$field])) {
				$this->fields[$field] = 'NULL';
			} else {
				$value = $this->fields[$field];
				if (!$this->validNumber($value) && !$this->validArray($value) && !$this->validBool($value) && empty($value)) {
					$this->fields[$field] = 'NULL';
				}
			}
		}

		public function validate($fields, $rules, $messages=array()) {
			$this->fields = $fields;
			$this->rules = $rules;
			$this->messages = $messages;
			
			// Validamos los campos requeridos
			foreach($this->rules as $field=>$constraints) {

				$ruleSet = $this->parseRules($constraints);
				$this->fieldsValids[$field] = true;

				// Si el campo es requerido y no estamos en modo edición
				$required = isset($ruleSet['required']) && ($this->mode != 'edit');
				// Si el campo fué enviado por el usuario
				$wasSent = isset($this->fields[$field]);
				// Si el campo no esta vacio de acuerdo a su tipo
				$notEmpty = $wasSent && !$this->emptyByType($ruleSet['type'], $this->fields[$field]);
				// La combinación de estos tres campos, determina si debemos verificar el contenido del mismo
				$check = ($wasSent && $notEmpty) || $required;

				if ($required) {
					$this->validateRequired($field, $ruleSet);
				}

				if (isset($ruleSet['nullable'])) {
					$this->validateNullable($field);
				}

				if (isset($ruleSet['type']) && $check) {
					$this->validateType($field, $ruleSet["type"]);
				}
					
				if (isset($ruleSet['unique']) && $check) {
					$this->validateUnique($field, $ruleSet['unique']);
				}

				if (isset($ruleSet['exists']) && $check) {
					$this->validateExists($field, $ruleSet['exists']);
				}

				if (isset($ruleSet['foreign']) && $check) {
					$params = explode(",", $ruleSet['foreign']);
					$modelName = $params[0];
					$columnName = $params[1];
					$this->validateForeign($field, $modelName, $columnName);
				}

				if (isset($ruleSet['after']) && $check) {
					$timestamp = $ruleSet['after'];
					$this->validateDateAfter($field, $timestamp);
				}

				if (isset($ruleSet['after_or_equal']) && $check) {
					$timestamp = $ruleSet['after_or_equal'];
					$this->validateDateAfterOrEqual($field, $timestamp);
				}

				if (isset($ruleSet['before']) && $check) {
					$timestamp = $ruleSet['before'];
					$this->validateDateBefore($field, $timestamp);
				}

				if (isset($ruleSet['before_or_equal']) && $check) {
					$timestamp = $ruleSet['before_or_equal'];
					$this->validateDateBeforeOrEqual($field, $timestamp);
				}

				if (isset($ruleSet['between']) && $check) {
					$params = explode(",", $ruleSet['between']);
					$from = $params[0];
					$to = $params[1];
					$this->validateDateBetween($field, $from, $to);
				}

				if (isset($ruleSet['active_url']) && $check) {
					$this->validateActiveURL($field);
				}

				if (isset($ruleSet['alnum_dash']) && $check) {
					$this->validateAlnumDash($field);
				}

				if (isset($ruleSet['alnum']) && $check) {
					$this->validateAlnum($field);
				}

				if (isset($ruleSet['string_range']) && $check) {
					$params = explode(",", $ruleSet['string_range']);
					$min = $params[0];
					$max = $params[1];
					$this->validateStringRange($field, $min, $max);
				}

				if (isset($ruleSet['number_range']) && $check) {
					$params = explode(",", $ruleSet['number_range']);
					$min = $params[0];
					$max = $params[1];
					$this->validateNumberRange($field, $min, $max);
				}

				if (isset($ruleSet['min']) && $check) {
					$min = $ruleSet['min'];
					$this->validateMin($field, $min);
				}

				if (isset($ruleSet['max']) && $check) {
					$max = $ruleSet['max'];
					$this->validateMax($field, $max);
				}

				if (isset($ruleSet['dimensions']) && $check) {
					$params = explode(",", $ruleSet['dimensions']);
					$width = $params[0];
					$height = $params[1];
					$this->validateDimensions($field, $width, $height);
				}

				if (isset($ruleSet['image']) && $check) {
					$this->validateImage($field);
				}

				if (isset($ruleSet['mimes']) && $check) {
					$mimes = explode(",", $ruleSet['mimes']);
					$this->validateMimes($field, $mimes);
				}

				if (isset($ruleSet['in']) && $check) {
					$list = explode(",", $ruleSet["in"]);
					$this->validateIn($field, $list);
				}

				if (isset($ruleSet['not_in']) && $check) {
					$list = explode(",", $ruleSet["not_in"]);
					$this->validateNotIn($field, $list);
				}

				if (isset($ruleSet['json']) && $check) {
					$this->validateJSON($field);
				}

				if (isset($ruleSet['same']) && $check) {
					$otherField = $ruleSet['same'];
					$this->validateSame($field, $otherField);
				}
			}

			// Liberámos la memoria de los modelos que instanciamos
			foreach ($this->models as &$model) { unset($model); }
			foreach ($this->images as &$image) { unset($image); }
			
			return empty($this->fieldsErrors);
		}
	}

}

?>