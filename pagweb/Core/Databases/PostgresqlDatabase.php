<?php

namespace PC\Databases {

	use PC\Parents\DatabaseParent;
	use PC\Constants\LogLevel;

	class PostgreSQLDatabase extends DatabaseParent {

		// Valores predeterminados
		protected $port = 5432;

		public function rollback() {
			return @pg_query($this->connection, "ROLLBACK");
		}

		public function commit() {
			return @pg_query($this->connection, "COMMIT");
		}

		public function startTransaction() {
			return @pg_query($this->connection, "BEGIN WORK");
		}

		public function setup($p=[]) {
			// Set database data for connection
			if (isset($p["user"])) { $this->user = $p["user"]; }
			if (isset($p["pass"])) { $this->pass = $p["pass"]; }
			if (isset($p["host"])) { $this->host = $p["host"]; }
			if (isset($p["base"])) { $this->base = $p["base"]; }
			if (isset($p["port"])) { $this->port = $p["port"]; }
			if (isset($p["utf8"])) { $this->utf8 = $p["utf8"]; }
			return $this->connect();
		}
		public function connect() {

			$utf8 = $this->utf8 ? " options='--client_encoding=UTF8'" : "";
			$connected = false;
			
			// Los parametros mínimos necesarios son usuario, password y base de datos
			if (!empty($this->user) && !empty($this->pass) && !empty($this->base)) {

				/* Formamos la cadena de conexión */
				$this->command = "host=".$this->host." port=".$this->port." dbname=".$this->base." user=".$this->user." password=".$this->pass.$utf8;
				
				/* Check if there's an open connection */
				if ($this->connection == false) {

					$this->connection = @pg_connect($this->command);
					$connected = $this->connection !== false;
					if (!$connected) {
						$this->log(LogLevel::CRITICAL, "Cannot connect with the given arguments!");
						$connected = false;
					}

				/* Try to connect again with the last known parameters */
				} else if (!@pg_ping($this->connection)) {
					
					$this->connection = @pg_connect($this->command);
					$connected = $this->connection !== false;
					if (!$connected) {
						$this->log(LogLevel::CRITICAL, "Cannot reconnect with the server!");
						$connected = false;
					}

				}
			/* If the necessary parameters are all empty then refuse the connection */
			} else {
				$this->log(LogLevel::CRITICAL, "Are user, pass and database defined to connect?");
				$connected = false;
			}

			$this->connected = $connected;
			return $connected;
		}
		public function query($command) {
			$this->command = $this->fullTrim($command);
			$this->query = @pg_query($this->connection, $this->command);

			// Si la última cadena de error esta vacia
			$lasterror = @pg_last_error($this->connection);
			if (empty($lasterror)) {
				return $this->query;
			} else {
				$this->log(LogLevel::CRITICAL, "Bad formed query");
				return false;
			}
		}
		public function numrows($query=false) {
			$q = !$query ? $this->query : $query;
			if (!$this->vq($q, "num_rows")) { return false; }
			return @pg_num_rows($q);
		}
		public function numfields($query=false) {
			$q = !$query ? $this->query : $query;
			if (!$this->vq($q, "num_fields")) { return false; }
			return @pg_num_fields($q);
		}
		public function fieldname($query=false, $index=0) {
			$q = !$query ? $this->query : $query;
			$i = !$index ? 0 : $index;
			if (!$this->vq($q, "field_name")) { return false; }
			return @pg_field_name($q, $i);
		}
		public function fetcharray($query=false) {
			$q = !$query ? $this->query : $query;
			if (!$this->vq($q, "fetch_array")) { return false; }
			return @pg_fetch_array($q, NULL, PGSQL_ASSOC);
		}
		public function fetchall($query=false) {
			$q = !$query ? $this->query : $query;
			if (!$this->vq($q, "fetch_all")) { return false; }
			return @pg_fetch_all($q);
		}
		public function fetchobject($query=false) {
			$q = !$query ? $this->query : $query;
			if (!$this->vq($q, "fetch_object")) { return false; }
			return @pg_fetch_object($q);
		}
		public function affectedrows($query=false) {
			$q = !$query ? $this->query : $query;
			if (!$this->vq($q, "affected_rows")) { return false; }
			return @pg_affected_rows($q);
		}
		public function resultseek($query=false, $index=0) {
			$q = !$query ? $this->query : $query;
			$i = !$index ? 0 : $index;
			if (!$this->vq($q, "result_seek")) { return false; }
			return @pg_result_seek($q, $i);
		}
		public function nextvalue($sequence) {
			$Q = $this->q("SELECT nextval('".$this->escape($sequence)."') as id");
			$F = $this->fa($Q);
			if (!!$Q && !!$F) { return $F["id"]; }
			return false;
		}
		public function currentvalue($sequence) {
			$Q = $this->q("SELECT currval('".$this->escape($sequence)."') as id");
			$F = $this->fa($Q);
			if (!!$Q && !!$F) { return $F["id"]; }
			return false;
		}
		public function lasterror() {
			return @pg_last_error($this->connection);
		}
		
		
		public function close($conn=false) {
			$c = $conn === false ? $this->connection : $conn;
			if ($c) { return @pg_close($c); }
			return false;
		}

		public function escape($value) {
			return function_exists("pg_real_escape_string") ? pg_real_escape_string($value) : pg_escape_string($value);
		}

		public function escapeBoolean($bool) {
			return self::validBool($bool) ? "true" : "false";
		}
		
		public function __construct($p) {
			$this->setup($p);
		}
	}


}

?>