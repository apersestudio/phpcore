<?php

namespace PC\Databases {
	
	class QueryBuilder {

		protected $model;

		protected $aggregates;
		protected $columns;
		protected $from;
		protected $joins;
		protected $wheres;
		protected $groups;
		protected $havings;
		protected $orders;
		protected $update;
		protected $limit;
		protected $offset;
		protected $lock;

		protected $schema;

		private $whereConditions = ["AND", "OR", "NOT"];
		private $havingConditions = [">=", "<=", "=", "BETWEEN"];
		private $havingAggregates = ["COUNT", "MAX", "MIN", "AVG", "SUM"];

		public function __construct($model) {
			$this->clear();
			$this->model = $model;
		}

		public function setModel($model) {
			$this->model = $model;
		}

		public function clear() {
			$this->aggregates = [];
			$this->columns = [];
			$this->from = "";
			$this->joins = [];
			$this->wheres = [];
			$this->groups = [];
			$this->havings = [];
			$this->orders = [];
			$this->update = "";
			$this->limit = 0;
			$this->offset = 0;
			$this->lock = false;
		}
		private function extract($term) {
			$e = explode(".", $term);
			return array_pop($e);
		}
		private function whereCondition($operator=false) {
			if (empty($operator)) { return "AND"; }
			$operator = strtoupper($operator);
			return in_array($operator, $this->whereConditions) ? $operator : "AND";
		}
		private function havingCondition($operator=false) {
			if (empty($operator)) { return "="; }
			$operator = strtoupper($operator);
			return in_array($operator, $this->havingConditions) ? $operator : "=";
		}
		private function havingAggregates($aggregate=false) {
			if (empty($aggregate)) { return "SUM"; }
			$operator = strtoupper($operator);
			return in_array($aggregate, $this->havingAggregates) ? $aggregate : "SUM";
		}
		public function withSchema($table, $schema=false) {
			$schema = empty($schema) ? "" : $schema.".";
			if (!empty($schema)) { return is_array($table) ? preg_filter("/^/", $schema, $table) : ($schema.$table); }
			return $table;
		}
		public function withTable($column, $table=false) {
			$table = empty($table) ? "" : $table.".";
			if (!empty($table)) { return is_array($column) ? preg_filter("/^/", $table, $column) : ($table.$column); }
			return $column;
		}
		public function withTableAndSchema($column, $table, $schema) {
			$table = empty($table) ? "" : $table.".";
			if (!empty($table)) { $column = is_array($column) ? preg_filter("/^/", $table, $column) : ($table.$column); }

			$schema = empty($schema) ? "" : $schema.".";
			if (!empty($schema)) { $column = is_array($column) ? preg_filter("/^/", $schema, $column) : ($schema.$column); }

			return $column;
		}
		public function avg($column) {
			$this->aggregates[] = "AVG(".$column.") AS average";
			return $this;
		}
		public function min($column) {
			$this->aggregates[] = "MIN(".$column.") AS min";
			return $this;
		}
		public function max($column) {
			$this->aggregates[] = "MAX(".$column.") AS max";
			return $this;
		}
		public function sum($column) {
			$this->aggregates[] = "SUM(".$column.") AS sum";
			return $this;
		}
		public function count($column=false) {
			$column = empty($column) ? $this->model->primaryKey : $column;
			$this->aggregates[] = "COUNT(".$column.") as count";
			return $this;
		}
		public function select($columns=["*"]) {
			if (is_array($columns)) {
				$this->columns = array_merge($this->columns, $columns);
			} else {
				$this->columns[] = $columns;
			}
			return $this;
		}
		public function from($table) {
			$this->from = $table;
			return $this;
		}

		public function join($table, $type=false) {
			$relation = $this->model->relations[$table];
		
			// Si no hemos registrado el modelo de la relación lo registramos
			if (!$this->model->hasRelation($table)) {
				$this->model->relationModels($table, new $relation["model"]);
				// Hacemos esto por si el usuario decide guardar información en las tablas relacionales
				$this->model->addDefinition($this->model->getRelationDefinition($table));
			}

			$types = ["INNER", "LEFT", "RIGHT", "FULL"];
			$type = !in_array($type, $types) ? "" : $type;

			$relationModel = $this->model->relationModels[$table];
			$table = $this->withSchema($relationModel->table, $relationModel->schema);
			$foreignColumn = $this->withTableAndSchema($relation["foreignKey"], $relationModel->table, $relationModel->schema);
			$primaryColumn = $this->withTableAndSchema($relation["primaryKey"], $this->model->table, $this->model->schema);
			//$foreignColumn = $this->withSchema($relation["foreignKey"], $relationModel->schema);
			//$primaryColumn = $this->withSchema($relation["primaryKey"], $this->model->schema);
			$this->joins[] = " ".$type." JOIN ".$table." ON ".$foreignColumn." = ".$primaryColumn." ";
			
			return $this;
		}

		public function insert($table, $fields) {
			$columns = [];
			$values = [];

			foreach ($fields as $field=>$value) {
				$fieldDef = $this->model->definition[$field];
				$columns[] = $field;
				$value = $this->model->driver->escapeByType($fieldDef["type"], $value);
				$values[] = $value;
			}

			return "INSERT INTO ".$table." (".implode(", ",$columns).") VALUES (".implode(", ",$values).")";
		}

		public function insertMultiple($table, $columns, $fieldsData, $groupCounter=100) {
			$valuesData = [];
			$insertGroups = [];
			$insert = "INSERT INTO ".$table." (".implode(", ",$columns).") VALUES \n";

			$counter = 1;
			$total = count($fieldsData);
			foreach ($fieldsData as $fieldData) {
				$values = [];
				foreach ($fieldData as $index=>$value) {
					$fieldDef = $this->model->definition[$columns[$index]];
					$values[] = $this->model->driver->escapeByType($fieldDef["type"], $value);
				}
				$valuesData[] = "(".implode(",",$values).")";
				$residuo = $counter % $groupCounter;
				if (($residuo == 0) || ($counter == $total)) {
					$insertGroups[] = $insert.implode(",\n", $valuesData);
					$valuesData = [];
				}
				$counter++;
			}
			return $insertGroups;
		}

		public function update($table, $fields) {
			$sets = array();
			foreach ($fields as $field=>$value) {
				$fieldDef = $this->model->definition[$field];
				$sets[] = $field." = ".$this->model->driver->escapeByType($fieldDef["type"], $value);
			}
			$this->update = "UPDATE ".$table." SET ".implode(", ", $sets);
			return $this;
		}

		public function delete($table) {
			$this->update = "DELETE FROM ".$table;
			return $this;
		}

		public function leftJoin($table) {
			return $this->join($table, "LEFT");
		}

		public function rightJoin($table) {
			return $this->join($table, "RIGHT");
		}

		private function specialWhere($c, $v) {
			switch ($c) {
				case "STARTWITH" : $v = " LIKE '%".substr($v, 1); break;
				case "ENDWITH" : $v = " LIKE ".substr($v, -1)."%'"; break;
				case "CONTAINS" : $v = " LIKE '%".substr($v, 1, -1)."%'"; break;

				case "DONTSTARTWITH" : $v = " NOT LIKE '%".substr($v, 1); break;
				case "DONTENDWITH" : $v = " NOT LIKE ".substr($v, -1)."%'"; break;
				case "DONTCONTAINS" : $v = " NOT LIKE '%".substr($v, 1, -1)."%'"; break;

				case "STARTWITHI" : $v = " ILIKE '%".substr($v, 1); break;
				case "ENDWITHI" : $v = " ILIKE ".substr($v, -1)."%'"; break;
				case "CONTAINSI" : $v = " ILIKE '%".substr($v, 1, -1)."%'"; break;

				case "DONTSTARTWITHI" : $v = " NOT ILIKE '%".substr($v, 1); break;
				case "DONTENDWITHI" : $v = " NOT ILIKE ".substr($v, -1)."%'"; break;
				case "DONTCONTAINSI" : $v = " NOT ILIKE '%".substr($v, 1, -1)."%'"; break;

				case "BETWEEN" : $v = $c." ".$v[0]." AND ".$v[1]; break;
				default : $v = $c." ".$v;
			}
		}

		public function where($column, $comparision, $value, $operator="AND") {
			// Obtenemos el nombre de la columna, eliminando los nombres de la tabla y el schema
			$columnName = $this->extract($column);
			// Comprobamos que la columna exista
			if (!isset($this->model->definition[$columnName])) {
				echo "There's no definition for the column ".$columnName;
				return $this;
			}
			// El tipo de dato lo obtenemos de la definición
			$type = $this->model->definition[$columnName]["type"];
			// Escapamos el valor del dato utilizando el tipo de dato de la definición
			$value = $this->model->driver->escapeByType($type, $value);
			// Formamos la cadena de busqueda
			$this->wheres[] = ["query"=>" ".$column." ".$comparision." ".$value." ", "operator"=>$this->whereCondition($operator)];
			return $this;
		}

		public function between($column, $first, $second, $operator="AND") {
			$type = $this->model->definition[$column]["type"];
			$first = $this->model->driver->escapeByType($type, $first);
			$second = $this->model->driver->escapeByType($type, $second);
			$this->wheres[] = ["query"=>$column." BETWEEN ".$first." AND ".$second." ", "operator"=>$this->whereCondition($operator)];
			
			return $this;
		}

		public function groupBy($columns) {
			if (is_array($columns)) {
				$this->groups = array_merge($this->groups, $columns);
			} else {
				$this->groups[] = $columns;
			}
			return $this;
		}

		public function having($aggregate, $column, $operator=false, $valueOne=false, $valueTwo=false) {
			$operator = $this->havingCondition($operator);
			$aggregate = $this->havingAggregates($aggregate);
			$columnDef = $this->model->definition[$column];
			$valueOne = $this->model->driver->escapeByType($columnDef["type"], $valueOne);
			$valueTwo = $this->model->driver->escapeByType($columnDef["type"], $valueTwo);
			$this->havings[] = $aggregate."(".$column.") ".$operator." ".$valueOne.($operator == "BETWEEN" ? " AND ".$valueTwo : "");
			return $this;
		}

		public function orderBy($columns, $order="ASC") {
			$this->orders[] = is_array($columns) ? implode(", ", $columns) : $columns." ".$order;
			return $this;
		}

		public function limit($limit=1) {
			$this->limit = $limit;
			return $this;
		}

		public function offset($offset=0) {
			$this->offset = $offset;
			return $this;
		}

		public function prepare($command, $params) {
			foreach ($params as $param) {
				$value = $this->model->driver->escapeByType($param["type"], $param["value"]);
				//$command = str_replace(':'.$param['name'], $value, $command);
				/* Adds whole word replacement with boundaries */
				$command = preg_replace('/:'.$param['name'].'(?!\w)/', $value, $command);
			}
			return $this->model->driver->fullTrim($command);
		}
		public function build() {
			$command = '';
			
			if (!empty($this->aggregates) || !empty($this->columns)) {
				$command .= 'SELECT ';
			}

			if (!empty($this->aggregates)) {
				$command .= implode(", ", $this->aggregates);
			}
			
			if (!empty($this->columns)) {
				$command .= implode(", ", $this->columns);
			}
			if (!empty($this->from)) {
				$command .= ' FROM '.$this->from;
			}			

			if (!empty($this->joins)) {
				$command .= implode(" ", $this->joins);
			}

			if (!empty($this->update)) {
				$command .= $this->update;
			}

			if (!empty($this->wheres)) {
				$whereFilters = false;
				$command .= " WHERE ";
				foreach ($this->wheres as $where) {
					$command .= ($whereFilters ? " ".$where["operator"]." " : " ").$where["query"];
					if (!$whereFilters) { $whereFilters = true; }
				}
			}

			if (!empty($this->groups)) {
				$command .= " GROUP BY ".implode(" ", $this->groups);
			}

			if (!empty($this->havings)) {
				$command .= " HAVING  ".implode(" ", $this->havings);
			}

			if (!empty($this->orders)) {
				$command .= " ORDER BY  ".implode(" ", $this->orders);
			}
			
			if (!empty($this->limit)) {
				$command .= " LIMIT ".$this->limit;
			}

			if (!empty($this->offset)) {
				$command .= " OFFSET  ".$this->offset;
			}
			
			$this->clear();

			return $this->model->driver->fullTrim($command);
		}

	}
}

?>