<?php
class DBFirebird extends Master {

	public static $user;
	public static $pass;
	public static $host;
	public static $port;
	public static $path;
	public static $base;
	public static $char;
	public static $cache;
	public static $dialect;
	public static $role;
	
	public static $strc;
	public static $conn;
	public static $error;
	public static $errno;
	
	public static $query;
	public static $command = '';
	public static $lastQuery;
	public static $lastCommand;
	public static $transaction;
	
	private static function setError($p) {
		self::$errno = self::getParam($p,'errno');
		self::$error = self::getParam($p,'error');
		if (self::$debugMode) {
			echo '<pre>Command:['.self::getParam($p,'command').'] >>> Error Message:['.self::$errno.']['.self::$error.']</pre>';
			exit(0);
		}
	}
	
	public static function connect($p) {
		// Set database data for connection
		self::$user = self::getParam($p,'user', 'sysdba');
		self::$pass = self::getParam($p,'pass', 'masterkey');
		self::$host = self::getParam($p,'host', 'localhost');
		self::$port = self::getParam($p,'port', 3050);
		self::$path = self::getParam($p,'path', '/');
		self::$base = self::getParam($p,'base', 'help');
		self::$char = self::getParam($p,'char', 'UTF8');
		self::$cache = self::getParam($p,'cache', 50);
		self::$dialect = self::getParam($p,'dialect', '3');
		self::$role = self::getParam($p,'role', '');
		
		// Try to connect with the information the user sent
		self::$strc = self::$host.'/'.self::$port.':'.self::$path.'/'.self::$base;
		self::$conn = @ibase_connect(self::$strc, self::$user, self::$pass, self::$char, self::$cache, self::$dialect, self::$role);
		
		if (self::$conn === false) {
			switch (ibase_errcode()) {
				case -902 : self::setError(array('command'=>'ibase_connect', 'errno'=>ibase_errcode(), 'error'=>'No existe la base de datos '.self::$base));
				default : self::setError(array('command'=>'ibase_connect', 'errno'=>ibase_errcode(), 'error'=>'Datos de conexión incorrectos'));
			}
		}
	}
	
	public static function fa() {
		return ibase_fetch_assoc(self::$query);
	}
	public static function fo() {
		return ibase_fetch_object(self::$query);
	}
	public static function fr() {
		return ibase_fetch_row(self::$query);
	}
	
	public static function cm($p) {
		return ibase_commit(self::getParam($p, 'transaction', self::$transaction));
	}
	public static function rb($p) {
		return ibase_rollback(self::getParam($p, 'transaction', self::$transaction));
	}
	
	public static function tr() {
		self::$transaction = ibase_trans(self::$conn);
	}
	
	// Num of Rows in a select statement
	public static function nr() {
	
		self::$lastQuery = self::$query;
		self::$lastCommand = self::$command;
		self::q(array("command"=>'SELECT count(*) '.substr(self::$lastQuery, strpos(self::$lastQuery, 'FROM'))));
		$count = self::fo();
		
		self::$query = self::$lastQuery;
		self::$command = self::$lastCommand;
		return $count->COUNT;
	}
	
	public static function q($p) {
		self::$command = self::getParam($p, 'command');
		self::$query = @ibase_query(self::getParam($p, 'transaction', self::$conn), self::$command);
		if (self::$query === false) {
			self::setError(array('command'=>'ibase_query', 'errno'=>ibase_errcode(), 'error'=>'La sentencia SQL --'.self::$lastQuery.'-- no es válida'));
		}
	}
	
	public static function close() {
		ibase_close(self::$conn);
	}

}
?>
