<?php

namespace PC\Databases {

	use PC\Parents\DatabaseParent;
	use PC\Constants\LogLevel;

	class MySqlDatabase extends DatabaseParent {


		private static $instance;

		// Valores predeterminados
		protected $port = 3306;

		public function rollback() {
			return @mysqli_rollback($this->connection);
		}

		public function commit() {
			return @mysqli_commit($this->connection);
		}

		public function startTransaction() {
			return @mysqli_begin_transaction($this->connection);
		}

		public function setup($p=[]) {
			// Set database data for connection
			if (isset($p["user"])) { $this->user = $p["user"]; }
			if (isset($p["pass"])) { $this->pass = $p["pass"]; }
			if (isset($p["host"])) { $this->host = $p["host"]; }
			if (isset($p["base"])) { $this->base = $p["base"]; }
			if (isset($p["port"])) { $this->port = $p["port"]; }
			if (isset($p["utf8"])) { $this->utf8 = !empty($p["utf8"]); }
			return $this->connect();
		}
		public function connect() {

			$connected = true;
			
			// Los parametros mínimos necesarios son usuario, password y base de datos
			if (!empty($this->user) && !empty($this->pass) && !empty($this->base)) {

				/* Check if there's an open connection */
				if ($this->connection == false) {

					$this->connection = @mysqli_connect($this->host, $this->user, $this->pass, $this->base, $this->port);
					if ($this->connection == false) {
						$this->log(LogLevel::CRITICAL, "Cannot connect with the given arguments!");
						$connected = false;
					}

				/* Try to connect again with the last known parameters */
				} else if (!@mysqli_ping($this->connection)) {
					
					$this->connection = @mysqli_connect($this->host, $this->user, $this->pass, $this->base, $this->port);
					if ($this->connection == false) {
						$this->log(LogLevel::CRITICAL, "Cannot reconnect with the server!");
						$connected = false;
					}

				}
			/* If the necessary parameters are all empty then refuse the connection */
			} else {
				$this->log(LogLevel::CRITICAL, "Are user, pass and database defined to connect?");
				$connected = false;
			}

			/* Si la conexión fué exitosa y se habilitó el juego de caracteres utf8 */
			if ($this->connection && $this->utf8) { mysqli_set_charset($this->connection, "utf8"); }

			$this->connected = $connected;
			return $connected;
		}
		public function query($command) {
			$this->command = $this->fullTrim($command);
			$this->query = @mysqli_query($this->connection, $this->command);

			// Si la última cadena de error esta vacia
			$lasterror = @mysqli_error($this->connection);
			if (empty($lasterror)) {
				return $this->query;
			} else {
				$this->log(LogLevel::CRITICAL, "Bad formed query");
				return false;
			}
		}
		public function numrows($query=false) {
			$q = !$query ? $this->query : $query;
			if (!$this->vq($q, "num_rows")) { return false; }
			return @mysqli_num_rows($q);
		}
		public function numfields($query=false) {
			$q = !$query ? $this->query : $query;
			if (!$this->vq($q, "num_fields")) { return false; }
			return @mysqli_num_fields($q);
		}
		public function fieldname($query=false, $index=0) {
			$q = !$query ? $this->query : $query;
			$i = !$index ? 0 : $index;
			if (!$this->vq($q, "field_name")) { return false; }
			return @mysqli_fetch_field_direct($q, $i);
		}
		public function fetcharray($query=false) {
			$q = !$query ? $this->query : $query;
			if (!$this->vq($q, "fetch_array")) { return false; }
			return @mysqli_fetch_array($q, MYSQLI_ASSOC);
		}
		public function fetchall($query=false) {
			$q = !$query ? $this->query : $query;
			if (!$this->vq($q, "fetch_all")) { return false; }
			return @mysqli_fetch_all($q, MYSQLI_ASSOC);
		}
		public function fetchobject($query=false) {
			$q = !$query ? $this->query : $query;
			if (!$this->vq($q, "fetch_object")) { return false; }
			return @mysqli_fetch_object($q);
		}
		public function affectedrows($query=false) {
			return @mysqli_affected_rows($this->connection);
		}
		public function resultseek($query=false, $index=0) {
			$q = !$query ? $this->query : $query;
			$i = !$index ? 0 : $index;
			if (!$this->vq($q, "result_seek")) { return false; }
			return @mysqli_data_seek($q, $i);
		}
		public function nextvalue($table) {
			$Q = $this->q("SELECT auto_increment as id FROM INFORMATION_SCHEMA.TABLES WHERE table_name = '".$this->escape($table)."'");
			$F = $this->fa($Q);
			if (!!$Q && !!$F) { return $F["id"]; }
			return false;
		}
		public function currentvalue($id=false) {
			return @mysqli_insert_id($this->connection);
		}
		public function lasterror() {
			return @mysqli_error($this->connection);
		}
		public function close($conn=false) {
			$c = $conn === false ? $this->connection : $conn;
			if ($c) { return @mysqli_close($c); }
			return false;
		}

		public function escape($value) {
			return mysqli_real_escape_string($this->connection, $value);
		}

		public function escapeBoolean($bool) {
			return self::validBool($bool) ? 1 : 0;
		}
		
		/** Can't clone the object **/
		private function __clone() {}

		/** Can't unserialize the object **/
		private function __wakeup() {}

		/** Can't call new View outside the class **/
		protected function __construct($params) {
			$ccnParts = explode("\\", get_class($this));
			$this->className = strtolower(array_pop($ccnParts));
			$this->setup($params);
		}

		/** The only way to get the instance **/
		public static function getInstance($params=[]) {
			if (self::$instance === null) { self::$instance = new static($params); }
			return self::$instance;
		}
	}


}

?>