<?php

namespace Com\Rackspace {

	use PC\Types\StreamTypes;
	use PC\Managers\CurlManager;
	use Com\HttpMessage\StreamHandler;

	use PC\Libraries\FilesLibrary;

	class RackspaceBase {
		
		const USERNAME = 'juanaelvira2014';
		const APIKEY = '2eff787847074264998a4736c7c60846';
		const ENDPOINT = 'https://identity.api.rackspacecloud.com/';
		const AUTHPATH = 'v2.0/tokens';
		const DEFREG = 'DFW';
		const TOKENFILE = 'token.log';

		private $authData;
		private $temp;
		private $curl;
	
		private function saveTemp($authData) {
			// Generamos el archivo para almacenar el token
			$this->stream = fopen($this->temp, StreamTypes::WRITE_START_TRUNCATE);
			$this->streamHandler = new StreamHandler($this->stream);
			$this->streamHandler->write($authData);
			$this->streamHandler->close();
		}
		
		// Parseamos en el objeto la información del nuevo token
		private function parseAuth($authData) {
			$auth = array(
				'id'=>$authData->access->token->id,
				'tenant'=>$authData->access->token->tenant->id,
				'expires'=>$authData->access->token->expires,
				'account'=>$authData->access->user->id,
				'services'=>array()
			);
			foreach ($authData->access->serviceCatalog as $service) {
				$auth["services"][$service->name] = array();
				foreach ($service->endpoints as $endpoint) {
					$region = isset($endpoint->region) ? $endpoint->region : self::DEFREG;
					$auth["services"][$service->name][$region] = $endpoint->publicURL;
				}
			}
			return $auth;
		}
		
		public function requestToken() {
			$params = array(
				"auth"=>array(
					"RAX-KSKEY:apiKeyCredentials"=>array(
						"username"=>self::USERNAME,
						"apiKey"=>self::APIKEY
					)
				)
			);
			$headers = array("Content-Type:application/json");
			$url = self::ENDPOINT.self::AUTHPATH;
			if ($this->curl->headers($headers)->validCodes("200")->post($url, $params)) {
				$this->authData = $this->parseAuth($this->curl->getResponse());
				$this->saveTemp($this->curl->getRawResponse());
			}
			return false;
		}
		
		public function getTokenHeaders($contentType=false) {
			$contentType = !$contentType ? 'application/json' : $contentType;
			return array(
				"Content-Type: ".$contentType,
				"Accept: application/json",
				"X-Auth-Token: ".$this->getToken()
			);
		}
		
		public function getToken() {
			return $this->authData["id"];
		}
		
		public function getAccount() {
			return $this->authData["account"];
		}
		
		public function getExpires() {
			return strtotime($this->authData["expires"]);
		}
		
		public function getTenant() {
			return $this->authData["tenant"];
		}

		public function getAuth() {
			return $this->authData;
		}

		public function curl() {
			return $this->curl;
		}

		public function curlPlain($headers=[]) {
			$this->curl->clear();
			$headers = array_merge($headers, $this->getTokenHeaders('text/plain; charset=UTF-8'));
			return $this->curl->headers($headers);
		}		
		public function curlToken($headers=[]) {
			$this->curl->clear();
			$headers = array_merge($headers, $this->getTokenHeaders());
			return $this->curl->headers($headers);
		}		
		public function getService($service, $region=false) {
			$region = empty($region) ? self::DEFREG : $region;
			return $this->authData["services"][$service][$region] ?: false;
		}
		public function getServices() {
			return $this->authData["services"];
		}
		
		public function Auth() {
			$authenticated = false;
			
			// Si el archivo existe, intentamos leerlo
			if (file_exists($this->temp) && filesize($this->temp)>0) {
				// Convertimos el contenido en un objeto json para extraer la fecha de expiración
				$this->stream = fopen($this->temp, StreamTypes::READ_START);
				$this->streamHandler = new StreamHandler($this->stream);
				$authData = $this->parseAuth(json_decode($this->streamHandler->getContents()));
				$this->streamHandler->close();
				
				// Si la fecha actual es mayor que la fecha de expiración, significa que el token está caducado
				$expired = time() > $this->getExpires();
				if ($expired && $this->requestToken()) {
					$authenticated = true;
				// Si el token todavía es válido, lo utilizamos
				} else {
					$this->authData = $authData;
					$authenticated = true;
				}
			// Si el archivo no existe, intentamos generar un token por primera vez
			} elseif ($this->requestToken()) {
				$authenticated = true;
			}
			return $authenticated;
		}
		
		public function __construct() {
			if (!file_exists(TMP) || !is_dir(TMP)) {
				exit('Directory <strong>'.TMP.'</strong> must exists. Please create it');
			} else {
				$this->curl = new CurlManager();
				$this->curl->safe(false);
				// Ruta del archivo donde esta almacenada la sesión anterior
				$this->temp = TMP.self::TOKENFILE;
			}
		}

	}

}
	
?>