<?php

namespace Com\Rackspace {

	use Com\Rackspace\RackspaceBase;
	use PC\Libraries\FilesLibrary;
	use PC\Libraries\ImageLibrary;

	class RackspaceCloudFiles extends RackspaceBase {
	
		private $container;
		private $service = false;
		private $serviceCDN = false;
		private $fileinfo = false;
		private $enabled = false;
		private $limitListFiles = 10000;

		private $lastError = 'success';
		private $currentCDNSSLURL = false;

		// Variables para optimizar el rendimiento y ancho de banda
		private $containerInfo = array();
		private $cdnInfo = array();
		private $foldersInfo = array();

		public function getLastError(){
			return $this->lastError;
		}
		
		public function getEnabled() {
			return $this->enabled;
		}
		
		/* ==============================================================================================================================================================
		OBJECTS ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// //
		============================================================================================================================================================== */
		private function saveToLog($content) {
			error_log($content, 3, LOGS."rscloudfiles.log");
		}

		// OK
		private function authenticate() {
			if ($this->enabled == false) {
				// Intenta conseguir el token de autentificación con rackspace y establece las rutas de los servicios
				if ($this->Auth()) {
					$this->enabled = true;
					$this->service = $this->getService('cloudFiles');
					$this->serviceCDN = $this->getService('cloudFilesCDN');
				}
			}
		}

		// OK
		// Sube un archivo a un contenedor, esta función es usada internamente
		private function upload($container, $path, $keyFile, $mimetype=false) {
			// Convertimos links simbólicos a su URL auténtico
			$path = realpath($path);
			// Solo continuamos si el archivo existe
			if (file_exists($path)) {
				if (empty($mimetype) && $this->fileinfo) {
					$finfo = finfo_open(FILEINFO_MIME_TYPE);
					$mimetype = finfo_file($finfo, $path);
					finfo_close($finfo);
				}
				$this->authenticate();
				$headers = [
					"ETag: ".md5_file($path),
					"Content-Length: ".filesize($path),
					"Content-Disposition: attachment; filename=".$keyFile
				];
				if (!empty($mimetype)) {
					$headers[] = "Content-Type: ".$mimetype;
				}

				$url = $this->service.'/'.$container.'/'.$keyFile;
				return $this->curlToken($headers)->validCodes("201")->put($url, $path);
			}
			return false;
		}
		
		// OK
		// Sube multiples archivo a un contenedor, esta función es usada internamente
		private function multiupload($container, $srcFiles, $keyFiles, $mimetype=false) {
			// Si se presente un error y tenemos que borrar lo que subió de forma parcial
			// Utilizamos este arreglo para borrarlo y evitar tener basura
			$buklDelete = array();
			$success = true;
			$files = array();

			for ($i=0; $i<count($srcFiles); $i++) {
				if ($this->upload($container, $srcFiles[$i], $keyFiles[$i], $mimetype)) {
					$thumbFile = FilesLibrary::splitpath($srcFiles[$i]);
					$thumbFile["cloud"] = $keyFiles[$i];
					$files[] = $thumbFile;
					$buklDelete[] = $container."/".$keyFiles[$i];
				} else {
					$success = false;
					break;
				}
			}
			// Si hubo algun error al subir cualquiera de los archivos borramos los que si subieron
			if ($success === false && !empty($files)) {
				$this->bulkDelete($buklDelete);
				return false;
			}
			// Liberámos un poco de memoria
			unset($buklDelete);
			return $files;
		}

		// OK
		// Devuelve una lista de los nombres de archivo a recuperar del Servidor
		// No devuelve los archivos, solo sus nombres
		public function getThumbNames($originalFilename, $thumbSizes) {
			// Si el nombre de archivo no esta vacio
			if (!empty($originalFilename)) {
				// Parseamos el nombre del archivo principal y los tamaños de las miniaturas
				$pi = FilesLibrary::splitpath($originalFilename);

				// Generamos los nombres de los thumbnails a partir del archivo maestro
				foreach ($thumbSizes as $size) {
					$files[] = $pi["dirname"].$pi["filename"]."-".$size["width"]."x".$size["height"].".".$pi["extension"];
				}
				return $files;
			}
			return false;
		}

		// OK
		// Devuelve el contenido del archivo de un contenedor
		public function getFileContent($container, $file) {
			$this->authenticate();
			$url = $this->service.'/'.$container.'/'.$file;
			if ($this->curlToken()->validCodes("200-299")->get($url)) {
				return $this->curl()->getRawResponse();
			}
			return false;
		}
		
		// OK
		// Sube un archivo que se encuentra en el disco duro
		public function uploadFile($file, $container, $keyFile, $mimetype=false) {

			// Cuando el SO es windows, en ocasiones el nombre de archivo viene codificado distinto de UTF-8
			// Si la codificación es distinta, la modificamos para poder detectar si el archivo existe
			$encoding = mb_detect_encoding($file['uri'], 'auto');
			if ($encoding!=="UTF-8") { $file['uri'] = iconv('utf-8', $encoding, $file['uri']); }

			// Si el archivo tiene contenido continuamos
			if (file_exists($file['uri'])) {

				if ($this->upload($container, $file['uri'], $keyFile, $mimetype)) {

					// Devolvemos una referencia al nombre del archivo en la nube
					$file["cloud"] = $keyFile;
					return $file;

				} else {
					$this->lastError = "cant_upload";
				}
				
			} else {
				$this->lastError = "cant_create";
			}
			return false;
		}

		// OK
		// Sube un archivo de tipo imagen
		public function uploadImage($file, $container, $keyFile, $size=false, $thumbSizes=false) {
			
			// Si la imagen tiene contenido continuamos
			if ($file != false && file_exists($file["uri"])) {
				
				// Intetamos optimizar el tamaño de la imagen
				$image = new ImageLibrary($file["uri"]);

				// Si el usuario no envió las dimensiones de la imagen, las calculamos
				if (!$size) { $size = $image->getSize(); }

				// Intentamos optimizar la imagen
				if ($image->resize($size["width"], $size["height"])->save(80, false, false)) {

					// Intentamos subir la imagen de forma individual
					if ($this->upload($container, $image->getCurrentFile(), $keyFile)) {

						// Devolvemos una referencia al nombre del archivo en la nube
						$file["cloud"] = $keyFile;
						
						// Si hay thumbnails solo regresámos la imagen individual
						if (!$thumbSizes) {

							return $file;

						} else {

							// Cargamos la imagen base para crear las miniaturas
							$image = new ImageLibrary($file["uri"]);

							// Intentamos crear los archivos en miniatura
							$thumbs = $image->createthumbs($thumbSizes);
							
							// Si los archivos fueron creados correctamente
							if ($thumbs != false) {

								// Generamos los nombres que las miniaturas tendrán en el contenedor
								$names = $this->getThumbNames($keyFile, $thumbs["sizes"]);

								// Intentamos subir los thumbsnails al contenedor
								$thubmFiles = $this->multiupload($container, $thumbs["files"], $names);

								// En caso de éxito, cambiamos las rutas de los archivos locales por los del contenedor
								if ($thubmFiles != false) {

									// Guardamos una referencia de las miniaturas creadas en la nube
									$file["thumbs"] = $thubmFiles;
									return $file;

								} else {
									$this->lastError = "cant_upload_thumbs";
								}
							} else {
								$this->lastError = "cant_create_thumbs";
							}
						}
					} else {
						$this->lastError = "cant_upload_image";
					}
				} else {
					$this->lastError = "cant_optimize_image";
				}
			} else {
				$this->lastError = "not_found";
			}

			return false;
		}

		// OK
		// Elimina un archivo de un contenedor
		public function deleteFile($container, $keyName) {
			$this->authenticate();
			if (!empty($keyName)) {
				$url = $this->service.'/'.$container.'/'.$keyName;
				return $this->curlPlain()->validCodes("204,404")->delete($url);	
			} else {
				$this->lastError = '"keyName" must not be empty';
			}
			return false;
		}
		
		// OK
		// Elimina multiples archivos de un contenedor
		public function bulkDelete($files) {
			
			$uniqfile = TMP.FilesLibrary::uniqname("bulkdelete.log", true);
			$f = implode("\n", $files)."\n";
			FilesLibrary::write($uniqfile, $f);

			// Enviamos los datos generados
			$this->authenticate();
			$url = $this->service.'?bulk-delete';

			// Si no pudimos eliminar algun bloque de archivos, guardamos un registro para su posterior eliminación manual
			$success = $this->curlPlain()->validCodes("200")->delete($url, $uniqfile);
			if (!$success) {
				$logMessage = "No se pudieron eliminar:\n".implode("\n", $files)."\n";
				$this->saveToLog($logMessage);
			}

			@unlink($uniqfile);
			return $success;
		}
		
		/* ==============================================================================================================================================================
		CONTAINERS /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// //
		============================================================================================================================================================== */
		
		// OK
		public function containerFiles($container, $limit=10000, $marker=false, $format=false) {
			$this->authenticate();
			$params = array();
			if (!empty($limit)) { $params[] = 'limit='.$limit; }
			if (!empty($marker)) { $params[] = 'marker='.$marker; }
			if (!empty($format)) { $params[] = 'format='.$format; }
			$get = !empty($params) ? '?'.implode("&", $params) : '';
			$url = $this->service.'/'.$container.$get;

			if ($this->curlToken()->validCodes("200,204")->get($url)) {
				return $this->curl()->getResponse();
			}
			return false;
		}

		// Si subimos una imagen desde un formulario podemos borrar las imagenes locales
		// Puesto que ya las subimos a la nube y mantenerlas en el servidor local, sería un desperdicio de espacio
		public function deleteLocalImages($uploaded) {
			@unlink($uploaded["uri"]);
			foreach ($uploaded["thumbs"] as $thumb) {
				@unlink($thumb["uri"]);
			}
		}

		// OK
		// Elimina un pseudo folder de rackspace junto con todo su contenido
		public function deleteFolder($container, $folder='/') {
			$this->authenticate();
			$moreFiles = true;
			$marker = '';
			$limit = 10000;

			$deletedFiles=0;

			do {
				$url = $this->service.'/'.$container.'?prefix='.$folder.'&limit='.$limit.'&marker='.$marker;
				if ($this->curlToken()->validCodes("200-299")->get($url)) {
					$f = $this->curl()->getResponse();
					$moreFiles = count($f) == $limit;

					// Agregamos el nombre del contenedor a los nombres de archivos
					$files = [];
					foreach ($f as $file) { $files[] = "/".$container."/".$file->name; }

					if (!empty($files)) {
						if ($this->bulkDelete($files)) {
							$response = $this->curl()->getResponse();
							$deletedFiles += $response->{"Number Deleted"};
						}
					}
					
					if ($moreFiles) { $marker = $f[count($f)-1]->name; }
				}
			} while ($moreFiles);
		
			return $deletedFiles;
		}
		
		// OK
		// Elimina un contenedor junto con todo su contenido
		public function containerDelete($container) {
			$this->authenticate();

			// Primero averiguamos cuantos archivos tiene el contenedor
			$request = $this->getContainerInfo($container);
			// Si no se encontro el recurso
			if (!!$request) {

				$totalFiles = $request["X-Container-Object-Count"];
				
				// Si el contenedor tiene archivos procedemos a eliminarlos
				if ($totalFiles>0) {
				
					$deletedFiles = $this->deleteFolder($container);
					if ($deletedFiles == $totalFiles) {

						// Si el contenedor esta vacio, intentamos borrarlo
						$url = $this->service.'/'.$container;
						return $this->curlToken()->validCodes("204")->delete($url);

					}
					
				}
			}

			return false;
		}

		// OK
		public function containerExists($container) {
			$this->authenticate();
			$url = $this->service.'/'.$container;
			return $this->curlToken()->validCodes("200-299")->get($url);
		}

		// OK
		// Default value for ttl is 259200 (72 Hours)
		private function getEnableCDNHeaders($enableCDN, $ttl) {
			$ttl = intval($ttl);
			// Minimun ttl of 15 minutes
			if ($ttl < 900) {
				$ttl = 900;
			// Maximum ttl of 1 Year
			} else if ($ttl > 31536000) {
				$ttl = 31536000;
			}
			$headers = array("X-Cdn-Enabled: ".($enableCDN?"True":"False"));
			if ($enableCDN) { $headers[] = "X-Ttl: ".$ttl; }
			return $headers;
		}

		// OK
		public function enableCDN($container, $enableCDN=true, $ttl=259200) {
			$this->authenticate();
			$url = $this->serviceCDN.'/'.$container;
			$headers = $this->getEnableCDNHeaders($enableCDN, $ttl);
			return $this->curlToken($headers)->validCodes("201,202,204")->put($url);
		}
		
		// OK
		// Crea un contenedor nuevo
		public function containerCreate($container, $enableCDN=false, $ttl=259200) {
			$this->authenticate();
			$url = $this->service.'/'.$container;
			$headers = array();
			if ($enableCDN) { $headers = $this->getEnableCDNHeaders($enableCDN, $ttl); }
			return $this->curlToken($headers)->validCodes("201,202")->put($url);
		}
		
		// OK
		// Obtiene información sobre un contenedor con CDN activo
		public function getContainerCDNInfo($container) {
			$this->authenticate();
			$key = md5($container);
			if (!isset($this->cdnInfo[$key])) {
				$url = $this->serviceCDN.'/'.$container;
				if ($this->curlToken()->validCodes("200,202,204")->head($url)) {
					$this->cdnInfo[$key] = $this->curl()->getResponseHeaders();
					return $this->cdnInfo[$key];
				} else {
					$this->lastError = "cant_get_cdninfo";
				}
			} else {
				return $this->cdnInfo[$key];
			}
			return false;
		}
		
		// OK
		// Obtiene información sobre un contenedor
		public function getContainerInfo($container) {
			$this->authenticate();
			$key = md5($container);
			if (!isset($this->containerInfo[$key])) {
				$url = $this->service.'/'.$container;
				if ($this->curlToken()->validCodes("200,202,204")->head($url)) {
					$this->containerInfo[$key] = $this->curl()->getResponseHeaders();
					return $this->containerInfo[$key];
				}
			} else {
				return $this->containerInfo[$key];
			}
			return false;
		}
		
		/* ==============================================================================================================================================================
		CONSTRUCTOR ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// //
		============================================================================================================================================================== */
		
		public function __construct() {
			// Llamamos al consutrctor del padre para activar sus variables y heredarlas
			parent::__construct();
			// Verificamos si la extensión para leer el mimetype de los archivos esta cargada
			$this->fileinfo = extension_loaded('fileinfo');
			$this->authenticate();
		}
	}
}

?>