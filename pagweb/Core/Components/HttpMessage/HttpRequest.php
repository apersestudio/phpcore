<?php

namespace Com\HttpMessage {

	use Psr\Http\Message\RequestInterface;
	use Psr\Http\Message\StreamInterface;
	use Psr\Http\Message\UriInterface;

	class HttpRequest implements RequestInterface {

		private $methodsList = [
			"GET",
			"POST",
			"PUT",
			"DELETE",
			"PATCH",
			"OPTIONS"
		];
		protected $protocolVersion;
		protected $headers;
		protected $body;

		protected $requestTarget;
		protected $uri;
		protected $method;

		/** Message Interface **/

		public function getProtocolVersion() {
			return $this->protocolVersion;
		}

		public function withProtocolVersion($version) {
			$new = clone $this;
			$new->protocol = $version;
			return $new;
		}

		public function getHeaders() {
			return $this->headers;
		}

		public function setHeader($name, $value) {
			$this->headers[$name] = !is_array($value) ? [$value] : $value;
			return $this;
		}

		public function hasHeader($name) {
			return array_key_exists(strtolower($name), array_change_key_case($this->headers));
		}

		public function getHeader($name) {
			return $this->hasHeader($name) ? array_change_key_case($this->headers)[strtolower($name)] : [];
		}

		public function getHeaderLine($name) {
			return implode(",", $this->getHeader($name));
		}

		public function withHeader($name, $value) {
			$new = clone $this;
			foreach ($new->getHeaders() as $key=>$values) {
				if (strtolower($key) == strtolower($name)) {
					$new->setHeader($key, $value);
					break;
				}
			}
			return $new;
		}
		// Recupera el nombre de la cabecera respetando mayúsculas y minúsculas
		private function getHeaderOriginal($name) {
			$original = $name;
			$value = [];
			foreach ($this->getHeaders() as $key=>$values) {
				if (strtolower($key) == strtolower($name)) {
					$original = $key;
					$value = $values;
					break;
				}
			}
			return ["name"=>$original, "value"=>$value];
		}
		// Borra la cabecera con el nombre dado
		private function removeHeader($name) {
			if ($this->hasHeader($name)) {
				$original = $this->getHeaderOriginal($name);
				unset($this->headers[$original["name"]]);
			}
			return $this;
		}

		public function withAddedHeader($name, $value) {
			$new = clone $this;
			if ($new->hasHeader($name)) {
				$new->setHeader($name, $value);
			} else {
				$original = $new->getHeaderOriginal($name);
				$new->setHeader($original["name"], array_merge($original["value"], $value));
			}
			return $new;
		}

		public function withoutHeader($name) {
			$new = clone $this;
			$new->removeHeader($name);
			return $new;
		}

		public function setBody(StreamInterface $body) {
			$this->body = $body;
		}

		public function getBody() {
			return $this->body;
		}

		public function withBody(StreamInterface $body) {
			$new = clone $this;
			$new->setBody($body);
			return $new;
		}

		/** Request Interface **/

		public function getRequestTarget() {
			if (!empty($this->requestTarget)) {
				return $this->requestTarget;
			}
			$target = $this->uri->getPath();
			if (empty($target)) {
				$target = '/';
			}
			if ($this->uri->getQuery() != '') {
				$target .= '?' . $this->uri->getQuery();
			}
			return $target;
		}

		public function setRequestTarget($requestTarget) {
			$this->requestTarget = trim($requestTarget);
		}

		public function withRequestTarget($requestTarget) {
			$new = clone $this;
			$new->setRequestTarget($requestTarget);
			return $new;
		}

		public function setMethod($method) {
			if (!in_array($method, $this->methodsList)) {
				throw new \InvalidArgumentException("Método no permitido");
			}
			$this->method = $method;
		}

		public function getMethod() {
			return $this->method;
		}

		public function withMethod($method) {
			$new = clone $this;
			$new->setMethod($method);
			return $new;
		}

		public function setUri(UriInterface $uri) {
			$this->uri = $uri;
		}

		public function getUri() {
			return $this->uri;
		}

		public function withUri(UriInterface $uri, $preserveHost = false) {
			$new = clone $this;
			$new->setUri($uri);

			if (empty($new->getHeader("host")) && !empty($uri->getHost()) && !$preserveHost) {
				$new->setHeader("host", $uri->getHost());
			}

			return $new;
		}
		
		/*
		private $resource;
		private $error;
		
		private function request() {
			$exec = curl_exec($this->resource);
			$curlInfo = curl_getinfo($this->resource);
			if($exec === false) {
				$this->error = curl_error($this->resource);
			} else {
				$response = $this->parseHeaders($response);
			}
			curl_close($this->resource);
			return ["info"=>$curlInfo, "response"=>$response];
		}

		private function parseHeaders($raw) {
			$separator = "\n";
			$grabbody = false;
			$headers = array();

			$bodyLines = 0;
			foreach(explode($separator, $raw) as $line) {
				// Primero detectamos la versión del protocolo HTTP y el código devuelto
				if (preg_match("/HTTP\/(\d\.\d)\s(\d{3})\s(.*)/", $line, $match)) {
					$headers = array();
					$headers["http_version"] = $match[1];
					$headers["http_code"] = $match[2];
					$headers["http_message"] = $match[3];
					$headers["http_body"] = "";
					$grabbody = false;
				} else if ($grabbody == false) {
					// Si encontramos un salto de línea quiere decir que ya viene el cuerpo del mensaje
					if ($line == "\r") {
						$grabbody = true;
					// En todos los demás casos guardamos las cabeceras enviadas por el servidor
					} else {
						$pair = explode(":", $line, 2);
						$headers[$pair[0]] = $pair[1];
					}
				}

				// Cuando ya estamos almacenando el cuerpo
				if ($grabbody){
					// El cuerpo comienza con un salto de línea,
					// así que si es un salto lo omitimos
					if ($line == "\r") { continue; }
					// Anexamos el carácter de salto porque lo eliminamos cuando realizamos el explode
					// Excepto si se trata de la primera iteración para no dañar la cabecera de los archivos
					$headers["http_body"] .= ($bodyLines==0) ? $line : $separator.$line;
					$bodyLines++;
				}
			}

			return $headers;
		}
		
		private function setupHeaders($method, $url, $headers) {
			$this->resource = curl_init();
			switch ($method) {
				// PUT METHOD
				case 'put': curl_setopt($this->resource, CURLOPT_PUT, 1); break;
				// POST METHOD
				case 'post' : curl_setopt($this->resource, CURLOPT_POST, true); break;
				// GET / DELETE / HEAD
				default : curl_setopt($this->resource, CURLOPT_CUSTOMREQUEST, strtoupper($method)); break;
			}
			curl_setopt($this->resource, CURLOPT_URL, $url);
			curl_setopt($this->resource, CURLOPT_HTTPHEADER, $headers);
			curl_setopt($this->resource, CURLOPT_HEADER, 1);
			curl_setopt($this->resource, CURLOPT_SSL_VERIFYPEER, 0);
			curl_setopt($this->resource, CURLOPT_FOLLOWLOCATION, 1);
			curl_setopt($this->resource, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($this->resource, CURLINFO_HEADER_OUT, 1);
			return $this->resource;
		}
		
		public function get($url, $headers) {
			self::setupHeaders("get", $url, $headers);
			$request = self::request($this->resource);
			return $request;
		}
		
		public function head($url, $headers) {
			self::setupHeaders("head", $url, $headers);
			$request = self::request($this->resource);
			return $request;
		}
		
		public function delete($url, $headers, $params=[]) {
			self::setupHeaders($method, $url, $headers);
			curl_setopt($this->resource, CURLOPT_POSTFIELDS, $params);
			$return self::request($this->resource);
		}

		public function put($url, $headers, $path=false) {
			self::setupHeaders("put", $url, $headers);
			if (!!$path) {
				$fopen = fopen($path, 'r');
				curl_setopt($this->resource, CURLOPT_UPLOAD, 1);
				curl_setopt($this->resource, CURLOPT_INFILE, $fopen);
				curl_setopt($this->resource, CURLOPT_INFILESIZE, filesize($path));
				$request = self::request($this->resource);
				fclose($fopen);
			} else {
				$request = self::request($this->resource);
			}
			return $request;
		}

		public function post($url, $headers, $params) {
			self::setupHeaders("post", $url, $headers);
			curl_setopt($this->resource, CURLOPT_POSTFIELDS, $params);
			$request = self::request($this->resource);
			return $request;
		}
		*/
	}

}
	
?>