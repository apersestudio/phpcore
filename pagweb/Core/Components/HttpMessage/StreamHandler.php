<?php

namespace Com\HttpMessage {

	use Psr\Http\Message\StreamInterface;

	class StreamHandler implements StreamInterface {

		protected $stream;
		protected $size;
		protected $uri;
		protected $readable;
		protected $writable;
		protected $seekable;

		public function __construct($stream, $options=[]) {
			if (!is_resource($stream)) {
	            throw new \InvalidArgumentException('Stream must be a resource');
	        }
	        if (isset($options['size'])) {
	            $this->size = $options['size'];
	        }
	        $this->stream = $stream;
	        $this->customMetadata = isset($options['metadata']) ? $options['metadata'] : [];

	        $meta = $this->getMetadata();
	        $this->seekable = $meta['seekable'];
	        $this->readable = is_readable($meta['uri']);
	        $this->writable = is_writable($meta['uri']);
	        $this->uri = $meta['uri'];
		}

		public function __toString(){
			$this->seek(0);
			$contents = $this->getContents();
			return empty($contents) ? '' : $contents;
		}

		private function isValidStream($stream) {
			return is_resource($this->stream) && get_resource_type($this->stream) == "stream";
		}

		public function close(){
			if ($this->isValidStream($this->stream)) {
				fclose($this->stream);
				$this->detach();
			}
		}

		public function detach(){
			if (!$this->isValidStream($this->stream)) {
				return null;
			}

			$deletedStream = $this->stream;
			unset($this->stream);

			$this->size = $this->uri = null;
			$this->readable = $this->writable = $this->seekable = false;
			return $deletedStream;
		}

		public function getSize(){
			if ($this->size !== null) {
				return $this->size;
			}
			if (!isset($this->stream)) {
				return null;
			}
			// Clear the stat cache if the stream has a URI
			if ($this->uri) {
				clearstatcache(true, $this->uri);
			}
			$stats = fstat($this->stream);
			if (isset($stats['size'])) {
				$this->size = $stats['size'];
				return $this->size;
			}
			return null;
		}

		public function tell(){
			$ftell = ftell($this->stream);
			if ($ftell === false) {
				throw new \RuntimeException('Unable to determine stream position');
			}
			return $ftell;
		}

		public function eof(){
			return !$this->stream || feof($this->stream);
		}

		public function isSeekable(){
			return $this->seekable;
		}

		public function seek($offset, $whence = SEEK_SET){
			if (!$this->seekable) {
	            throw new \RuntimeException('Stream is not seekable');
	        } elseif (fseek($this->stream, $offset, $whence) === -1) {
	            throw new \RuntimeException('Unable to seek to stream position '.$offset.' with whence '.$whence);
	        }
		}

		public function rewind(){
			$this->seek(0);
		}

		public function isWritable(){
			 return $this->writable;
		}

		public function write($string){
			if (!$this->writable) {
				throw new \RuntimeException('Cannot write to a non-writable stream');
			}
			// We can't know the size after writing anything
			$this->size = null;
			$fwrite = fwrite($this->stream, $string);
			if ($fwrite === false) {
				throw new \RuntimeException('Unable to write to stream');
			}
			return $fwrite;
		}

		public function isReadable(){
			return $this->readable;
		}

		// 262144 bytes = 256kb/s = 15Mb/m
		// 524288 bytes = 512kb/s = 30Mb/m

		public function read($length=262144){
			if (!$this->readable) {
				throw new \RuntimeException('Cannot read from non-readable stream');
			}
			if ($length < 0) {
				throw new \RuntimeException('Length parameter cannot be negative');
			}
			if (0 === $length) {
				return '';
			}
			$fread = fread($this->stream, $length);
			if (false === $fread) {
				throw new \RuntimeException('Unable to read from stream');
			}
			return $fread;
		}

		public function getContents(){
			$contents = stream_get_contents($this->stream);
			if ($contents === false) {
				throw new \RuntimeException('Unable to read stream contents');
			}
			return $contents;
		}

		public function getMetadata($key = null){
			if (!$this->isValidStream($this->stream)) {
				return $key ? null : [];
			} elseif (!$key) {
				return $this->customMetadata + stream_get_meta_data($this->stream);
			} elseif (isset($this->customMetadata[$key])) {
				return $this->customMetadata[$key];
			}

			$meta = stream_get_meta_data($this->stream);
			return isset($meta[$key]) ? $meta[$key] : null;
		}

	}

}

?>