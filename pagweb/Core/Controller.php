<?php

namespace PC {

	use PC\Libraries\HeaderLibrary;

	class Controller {

		private $route = false;
		private $params;
		private $output;
		private $startTime;

		public function setParams($params) {
			$this->params = (object)$params;
		}
		public function addMessage($message) {
			$this->output["messages"][] = $message;
		}
		public function setSuccess($success) {
			$this->success = $success;
		}
		public function output($var, $value) {
			$this->output[$var] = $value;
		}

		public function toJson() {
			HeaderLibrary::content("json");
			HeaderLibrary::number(200);
			$this->output["time"] = microtime(true) - $this->startTime;
			echo json_encode($this->output);
		}

		public function route() {
			return $this->route;
		}

		public function __get($property) {
			return isset($this->params->$property) ? $this->params->$property : null;
		}

		public function __construct() {
			$this->params = new \stdClass();
			$this->startTime = microtime(true);
			$this->output = array(
				"success"=>false,
				"data"=>false,
				"messages"=>array()
			);
		}

	}

}

?>