<?php
namespace PC\Managers {

	use PC\Types\StreamTypes;
	use Com\HttpMessage\StreamHandler;
	use PC\Traits\DataTrait;

	class CurlManager {

		private $streamHandler;
		private $response;
		private $lastError;
		private $success;
		private $validCodes;
		private $method;

		private $params;
		private $headers;
		private $headersResponse;
		private $ssl=true;

		public function __construct() {
			$this->curlHandler = curl_init();
			$this->clear();
			return $this;
		}

		public function safe($ssl=true) {
			$this->ssl = $ssl;
			return $this;
		}
		private function isJson() {
			$isJson = false;
			foreach ($this->headers as $index=>$header) {
				if (stripos($header, "application/json") !== false){
					$isJson = true;
					break;
				}
			}
			return $isJson;
		}

		public function clear() {
			// Eliminamos las configuraciones establecidas durante la última petición
			curl_reset($this->curlHandler);
			$this->success = false;
			$this->params = [];
			$this->headers = [];
			$this->headersResponse = [];
			$this->lastError = null;
			$this->response = '';
			$this->validCodes = '';
			// Volvemos a configurar la captura de la respuesta del servidor
			//$this->capture();
			//$this->captureRequest();
			return $this;
		}

		private function close() {
			// Cerramos la sesión de CURL
			curl_close($this->curlHandler);
			return $this;
		}

		private function setMethod($method) {
			$this->method = $method;
 			curl_setopt($this->curlHandler, CURLOPT_CUSTOMREQUEST, strtoupper($method));
			return $this;
		}

		public function validCodes($validCodes) {
			$this->validCodes = $validCodes;
			return $this;
		}

		public function follow($enabled=true, $max=0) {
			// Si la petición debería de seguir las redirecciones subsecuentes a la petición
			curl_setopt($this->curlHandler, CURLOPT_FOLLOWLOCATION, $enabled);
			// Si el numero máximo se establece a cero, entonces no habrá limite de redirecciones a seguir
			if ($enabled && $max > 0) {
				curl_setopt($this->curlHandler, CURLOPT_MAXREDIRS, $max);
			}
			return $this;
		}

		public function capture($capture=true) {
			curl_setopt($this->curlHandler, CURLOPT_RETURNTRANSFER, $capture);
			curl_setopt($this->curlHandler, CURLOPT_BINARYTRANSFER, $capture);
			return $this;
		}

		public function captureRequest($capture=true) {
			curl_setopt($this->curlHandler, CURLINFO_HEADER_OUT, $capture);
			return $this;
		}

		public function execute($url) {
			// Establecémos la URL destino
			$url = empty($this->params) ? $url : $url."?".$this->params;
			curl_setopt($this->curlHandler, CURLOPT_URL, $url);

			// Establecemos el modo SSL según sea el caso
			$ssl = ($this->ssl==true) ? 2 : 0;
			curl_setopt($this->curlHandler, CURLOPT_SSL_VERIFYPEER, $ssl);
			curl_setopt($this->curlHandler, CURLOPT_SSL_VERIFYHOST, $ssl);
			if ($ssl == 2) { curl_setopt($this->curlHandler, CURLOPT_CAINFO, CORE.'certs/cacert.pem'); }

			// Detectámos si hay cabezales por enviar en la petición
			$hasheaders = !empty($this->headers);
			
			// Si incluimos esta línea, los cabezales estarán también en la función onWrite
			// Si establecemos la función onHead, entonces el siguiente comando no es necesario
			//curl_setopt($this->curlHandler, CURLOPT_HEADER, $hasheaders);

			// Si los hay, entonces los agregamos a la petición
			if ($hasheaders) {
				curl_setopt($this->curlHandler, CURLOPT_HTTPHEADER, $this->headers);
			}
			// Asignamos las funciones para manejar la petición y respuesta
			curl_setopt($this->curlHandler, CURLOPT_READFUNCTION, [$this, 'onRead']);
			curl_setopt($this->curlHandler, CURLOPT_WRITEFUNCTION, [$this, 'onWrite']);
			curl_setopt($this->curlHandler, CURLOPT_HEADERFUNCTION, [$this, 'onHead']);

			// Guardamos el último error si es que hay uno
			$this->success = curl_exec($this->curlHandler);
			if($this->success) {
				// Solo comprobamos los códigos http si el usuario definio la variable
				// Puedes ser un rango o un conjunto de valores
				if (!empty($this->validCodes) && !empty($this->headersResponse)) {
					foreach ($this->headersResponse["Http"] as $httpCode=>$httpItem) {
						$isRange = (strpos($this->validCodes, "-") !== false);
						if ($isRange) {
							$range = explode("-", $this->validCodes);
							$this->success = DataTrait::validRange($httpCode, $range[0], $range[1]);
						} else {
							$range = explode(",", $this->validCodes);
							$this->success = DataTrait::validIn($httpCode, $range);
						}
						if ($this->success) { break; }
					}
				}
			} else {
				$this->lastError = curl_error($this->curlHandler);
			}
			return $this->success;
		}

		public function convert($size) {
			$unit = array('b','kb','mb','gb','tb','pb');
			return @round($size/pow(1024,($i=floor(log($size,1024)))),2).' '.$unit[$i];
		}

		public function params($params=[]) {
			$this->params = http_build_query($params);
			return $this;
		}

		public function headers($headers=[]) {
			$this->headers = $headers;
			return $this;
		}
		public function getHeaders() {
			return $this->headers;
		}
		public function getResponseHeaders() {
			return $this->headersResponse;
		}
		public function getResponse() {
			return $this->isJson() ? json_decode($this->response) : $this->response;
		}
		public function getRawResponse() {
			return $this->response;
		}

		public function get($url) {
			return $this->setMethod("get")->execute($url);
		}
		
		public function head($url) {
			return $this->setMethod("head")->execute($url);
		}
		
		public function post($url, $params) {
			$params = $this->isJson() ? json_encode($params) : http_build_query($params);
			curl_setopt($this->curlHandler, CURLOPT_POSTFIELDS, $params);
			return $this->setMethod("post")->execute($url);
		}

		private function withFile($filepath) {
			if (file_exists($filepath)) {
				// Añadimos un flujo para manejar el archivo
				$this->stream = fopen($filepath, StreamTypes::READ_START);
				$this->streamHandler = new StreamHandler($this->stream);
				
				// Para decir a PHP que stream es el que va a manejar el archivo
				// Además de cuanta información debería de cargar
				curl_setopt($this->curlHandler, CURLOPT_INFILE, $this->stream);
				curl_setopt($this->curlHandler, CURLOPT_INFILESIZE, filesize($filepath));
			}
			curl_setopt($this->curlHandler, CURLOPT_UPLOAD, 1);
		}
		public function delete($url, $filepath=false) {
			$this->withFile($filepath);
			return $this->setMethod("delete")->execute($url);
		}

		public function put($url, $filepath=false) {
			$this->withFile($filepath);
			return $this->setMethod('put')->execute($url);
		}

		public function onHead($curlHandler, $response) {
			$len = strlen($response);
			$response = trim($response);
			if (!empty($response)) {
				// Ahora detectamos cada posible respuesta http 
				// porque en ocasiones el servidor envía más de una respuesta HTTP
				if (preg_match("/HTTP\/(\d\.\d)\s(\d{3})\s(.*)/", $response, $match)) {
					$this->headersResponse["Http"][$match[2]] = array(
						"Version" => $match[1],
						"Message" => $match[3]
					);
				} else {
					$pair = explode(":", $response, 2);
					$this->headersResponse[$pair[0]] = $pair[1];
				}
			}
			return $len;
		}

		public function onWrite($curlHandler, $response) {
			// Cada vez que agregamos contenido al buffer, la cantidad de memoria utilizada por PHP aumenta
			// Si no vamos a desplegar la información en pantalla, deberíamos guardar los datos en disco duro
			// La otra alternativa y enviar los datos como una descarga para que el navegador se administre
			$this->response .= $response;
			return strlen($response);
		}

		private function onRead($curlHandler, $fileHandler, $length) {
			$data = false;
			if (!$this->streamHandler->eof()) {
				$data = $this->streamHandler->read($length);
			}
			return $data;
		}

	}

}	

?>