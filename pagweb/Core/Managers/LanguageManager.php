<?php

namespace PC\Managers {

	use PC\Config;

	class LanguageManager {

		private $autodetec = false;
		private $languages = array();
		private $lang;
		private $language;
		private $timezone;
		private $locale;

		public function addLanguage($detect, $localeUnix, $localeWindows, $timezone) {
			$this->languages[$detect] = array("u"=>$localeUnix, "w"=>$localeWindows, "t"=>$timezone);
		}

		public function setLanguage($lang) {
			if (isset($this->languages[$lang])) {

				$this->lang = $lang;

				// Guardamos una cookie con el idioma
				setcookie("lang", $this->lang, time()+3600, "/", HOST, SSL, SSL);
				$this->language = $this->languages[$this->lang];
				$this->timezone = $this->language["timezone"];
				$this->locale = UNIXLIKE ? $this->language["unix"] : $this->language["windows"];

				// Configuramos la zona horaria
				date_default_timezone_set ($this->timezone);

				// Establecemos el idioma
				define("LANG", $this->lang);
				define("TIMEZONE", $this->timezone);
				define("LOCALE", $this->locale);
				setlocale(LC_ALL, LOCALE);

			}
		}

		public function __construct($languages=array()) {

			$this->language = Config::get("language.default");
			$this->languages = $languages;

			if (empty($_COOKIE["lang"])) {
				// Idioma predeterminado
				$lang = $this->language;

				if ($this->autodetec) {
					foreach ($this->languages as $detect=>$language) {
						// Idiomas alternativos
						if (strpos(ACCEPT_LANGUAGE, $detect) !== false) {
							$lang = $detect;
							break;
						}
					}
				}
			} else {
				// Recuperamos la cookie del idioma
				$lang = $_COOKIE["lang"];
			}

			$this->setLanguage($lang);
		}

	}

}

?>