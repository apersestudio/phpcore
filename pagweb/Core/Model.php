<?php

namespace PC {

	use PC\Databases\QueryBuilder;

	class Model {

		// Connection is the name of the database config entry where parameter to connect are defined
		protected $connection;

		// Some models have schemas which are like container between the database and the tables
		protected $schema = false;

		// Were all the model information will be stored
		protected $data;

		/** Filters are the parts of the where clause **/
		private $filters;

		/** The last command beign executed **/
		private $command;

		/** Some models hasve sequences such as PostgreSQL or Oracle **/
		protected $sequence;

		/** Doesn't need explanation :P **/
		protected $primaryKey;

		/* TableName Without schema */
		protected $table;

		/** Where we define the relations with other models **/
		protected $relations;

		/** Once we make a relation we store it here, to avoid overloading **/
		protected $relationModels;

		/** Variable to give back the name of the model's child **/
		protected $className;

		/** Valid Types for Filable are
		(int, integer, number),
		(float, double, decimal, numeric)
		(string, varchar, char)
		(bool, boolean, field)
		******************************** */
		protected $fillable = [];

		/** Variable to configure the datatypes of the model **/
		protected $definition = [];

		protected $rules = [];

		protected $aliases = [];

		/** Just to store the encountered errors **/
		protected $errors = [];

		public function __construct() {
			// Tenemos que hacer una instancia del controlador de base de datos desde el constructor
			// Porque este controlador servirá para escapar los valores para los campos
			$configKey = "databases.".$this->connection;
			$config = Config::get($configKey);
			
			// Nombre del modelo
			$g = explode("\\", get_class($this));
			$this->className = array_pop($g);

			if (!empty($config)) {

				$driver = "\\PC\\Databases\\".ucfirst($config["driver"])."Database";
				$this->driver = $driver::getInstance($config);
				$this->queryBuilder = new QueryBuilder($this);
				$this->data = array();

				// En base a las reglas formamos la definición del modelo
				foreach ($this->rules as $fieldName=>$rule) {
					$fieldDef = array();
					$defs = explode("|", $rule);
					foreach ($defs as $def) {
						$defParts = explode(":", $def);
						$defName = $defParts[0];
						$fieldDef[$defName] = isset($defParts[1]) ? $defParts[1] : true;
					}
					$this->definition[$fieldName] = $fieldDef;
				}

				if (isset($config["schema"])) {
					$this->schema = $config["schema"];
				}
			} else {
				$message = 'La variable $connection en el modelo '.$this->className.' apunta a una configuración de base de datos inexistente: '.$this->connection.'.';
				exit ($message);
			}
		}

		public function __get($property) {
			if (isset($this->$property)) {
				return $this->$property;
			} else if (isset($this->data[$property])) {
				return $this->data[$property];
			}
			return false;
		}
		public function toArray() {
			return $this->data;
		}
		public function getErrors() {
			$errors = $this->errors;
			$this->errors = [];
			return $errors;
		}

		public function definition() {
			return $this->definition;
		}

		public function addDefinition($definition) {
			$this->definition = array_merge($this->definition, $definition);
		}

		public function hasRelation($table) {
			return isset($this->relationModels[$table]);
		}

		public function relationModels($table, Model $model) {
			$this->relationModels[$table] = $model;
		}

		public function getRelationDefinition($table) {
			return $this->relationModels[$table]->definition();
		}
		private function enabled() {
			if (isset($this->driver)) {
				return $this->driver->connected();
			}
			return false;
		}
		private function schema() {
			return $this->schema === false ? "" : $this->schema.".";
		}

		public function getTableName() {
			return empty($this->table) ? strtolower($this->className) : $this->table;
		}

		private function whereStart($column) {
			return ($this->filters==0 ? " WHERE " : "")." ".$column;
		}

		private function field($value) {
			return preg_replace("/[^_\p{L}0-9]/u", "", $value);
		}

		private function escape($column, $value) {
			return $this->driver->escape($this->definition[$column], $value);
		}

		// Patrón de funciones estáticas para no tener la necesidad de instanciar
		public function find($id, array $columns = ['*']) {
			if ($this->enabled()) {
				return $this->select($columns)->where($this->primaryKey, '=', $id)->first();
			}
			return false;
		}

		public function createMultiple($columns, $data, $groupCounter=100) {
			if ($this->enabled()) {

				$ids = [];
				$fieldsData = [];

				foreach ($data as $fieldData) {

					$fieldsData[] = $fieldData;

				}

				if (!empty($this->errors)) {

					$table = $this->queryBuilder->withSchema($this->table, $this->schema);
					$this->command = $this->queryBuilder->insertMultiple($table, $columns, $fieldsData, $groupCounter);
					
					$numberRows = 0;
					foreach ($this->command as $insert) {
						// It returns the number of inserted rows
						$query = $this->driver->q($insert);
						$affected = $this->driver->ar($query);

						// If the data was inserted then we append the data to the object
						if ($query !== false && $affected !== false) {

							// Si el modelo tiene una sequencia la usamos para calcular el ID insertado
							if (!empty($this->sequence)) {
								$sequence = $this->queryBuilder->withSchema($this->sequence, $this->schema);
								// En base a ensallos, el ID devuelto es el de la última fila insertada
								$ids[] = $this->driver->currentvalue($sequence);
							}

							$numberRows += $affected;
						}
					}
					return $numberRows;
				}
			}
			return 0;
		}

		public function create($fields) {
			if ($this->enabled()) {
					
				$table = $this->queryBuilder->withSchema($this->table, $this->schema);
				$this->command = $this->queryBuilder->insert($table, $fields);

				// It returns the number of inserted rows
				$query = $this->driver->q($this->command);
				$numberRows = $this->driver->ar($query);

				// If the data was inserted then we append the data to the object
				if ($query !== false && $numberRows !== false) {

					// Si el modelo tiene una sequencia la usamos para calcular el ID insertado
					if (isset($this->sequence)) {
						$sequence = $this->queryBuilder->withSchema($this->sequence, $this->schema);
					// En caso contrario usamos el formato de autoincremental
					} else {
						$sequence = null;
					}
					
					$fields[$this->primaryKey] = $this->driver->currentvalue($sequence);
					$this->data = $fields;

					return $numberRows;
				}

			}
			return 0;
		}

		private function getID($id) {
			// Si argumento ID es numérico se convierte a positivo
			// Si encontramos la llave primaria definida, intentamos usar ese valor
			// Si los dos casos anteriores fallán, dejamos el valor tal cual
			if (is_numeric($id)) {
				$id = abs($id);
			} else if (isset($this->data[$this->primaryKey])) {
				$id = $this->data[$this->primaryKey];
			}

			// Si el ID esta vacio no podemos continuar
			if (empty($id)) {
				$this->errors[] = "No existe el elemento ".$id." en ".$this->className.".";
				return false;
			}
			return $id;
		}

		public function update($fields) {
			if ($this->enabled()) {
				$tableName = $this->queryBuilder->withSchema($this->table, $this->schema);
				$this->queryBuilder->update($tableName, $fields);
			}
			return $this;
		}

		public function updateByID($fields, $id) {
			if ($this->enabled()) {
				// Si hay un ID válido para actualizar
				$id = $this->getID($id);
				if ($id !== false) {
					$fieldName = $this->queryBuilder->withTableAndSchema($this->primaryKey, $this->table, $this->schema);
					$updated = $this->update($fields)->where($fieldName, '=', $id)->execute();
					
					// Se actualizo la información
					if (!empty($updated)) {
						$fields[$this->primaryKey] = $id;
						$this->data = $fields;
					}
					
					return $updated;
				}
			}
			return 0;
		}

		public function delete() {
			if ($this->enabled()) {
				$tableName = $this->queryBuilder->withSchema($this->table, $this->schema);
				$this->queryBuilder->delete($tableName);
			}
			return $this;
		}

		public function deleteByID($id) {
			if ($this->enabled()) {

				// Si hay un ID válido para borrar
				$id = $this->getID($id);
				if ($id !== false) {
					
					$fieldName = $this->queryBuilder->withTableAndSchema($this->primaryKey, $this->table, $this->schema);
					return $this->delete()->where($fieldName, '=', $id)->execute();
				}
			}
			return 0;
		}

		public function createOrUpdate($fields, $id) {
			$item = $this->find($id, [$this->primaryKey]);
			// Create
			if ($item === false) { return $this->create($fields); }
			// Update
			return $this->updateByID($fields, $id);
		}

		public function avg($column) {
			if ($this->enabled()) {
				$this->from($this->table);
				$this->queryBuilder->avg($column);
			}
			return $this;
		}

		public function min($column) {
			if ($this->enabled()) {
				$this->from($this->table);
				$this->queryBuilder->min($column);
			}
			return $this;
		}

		public function max($column) {
			if ($this->enabled()) {
				$this->from($this->table);
				$this->queryBuilder->max($column);
			}
			return $this;
		}

		public function sum($column) {
			if ($this->enabled()) {
				$this->from($this->table);
				$this->queryBuilder->sum($column);
			}
			return $this;
		}

		public function count($column=false) {
			if ($this->enabled()) {
				$this->from($this->table);
				$this->queryBuilder->count($column);
			}
			return $this;
		}

		public function select(array $columns = ['*']) {
			if ($this->enabled()) {
				$this->from($this->table);
				$columns = $this->queryBuilder->withTableAndSchema($columns, $this->table, $this->schema);
				$this->queryBuilder->select($columns);
			}
			return $this;
		}

		public function from($table) {
			if ($this->enabled()) {
				$tableName = $this->queryBuilder->withSchema($table, $this->schema);
				$this->queryBuilder->from($tableName);
			}
			return $this;
		}

		public function join($table, $type=false) {
			if ($this->enabled()) { $this->queryBuilder->join($table, $type); }
			return $this;
		}

		public function selectJoin($table, array $columns=["*"], $type=false) {
			if ($this->enabled()) {
				// Después de hacer el join hacel el select porque ya debe estar definido el esquema de dicho join
				$this->queryBuilder->join($table, $type);
				$columns = $this->queryBuilder->withTableAndSchema($columns, $table, $this->relationModels[$table]->schema);
				$this->queryBuilder->select($columns);
			}
			return $this;
		}
		
		public function leftJoin($table) {
			return $this->join($table, "LEFT");
		}

		public function rightJoin($table) {
			return $this->join($table, "RIGHT");
		}

		public function where($column, $comparision, $value, $operator="AND") {
			if ($this->enabled()) { $this->queryBuilder->where($column, $comparision, $value, $operator); }
			return $this;
		}
		public function whereJoin($table, $column, $comparision, $value, $operator="AND") {
			if ($this->enabled()) {
				$schema = $this->relationModels[$table]->schema;
				$column = $this->queryBuilder->withTableAndSchema($column, $table, $this->relationModels[$table]->schema);
				$this->queryBuilder->where($column, $comparision, $value, $operator); 
			}
			return $this;
		}

		public function between($column, $first, $second, $operator="AND") {
			if ($this->enabled()) { $this->queryBuilder->between($column, $first, $second, $operator); }
			return $this;
		}

		public function groupBy(array $columns=[]) {
			if ($this->enabled()) { $this->queryBuilder->groupBy($columns); }
			return $this;	
		}

		private function having($aggregate, $column, $operator=false, $valueOne=false, $valueTwo=false) {
			if ($this->enabled()) { $this->queryBuilder->having($aggregate, $column, $operator, $valueOne, $valueTwo); }
			return $this;
		}

		public function orderBy($columns, $order="ASC") {
			if ($this->enabled()) { $this->queryBuilder->orderBy($columns, $order); }
			return $this;
		}

		public function orderByJoin($table, $column, $order="ASC") {
			if ($this->enabled()) {
				$schema = $this->relationModels[$table]->schema;
				$column = $this->queryBuilder->withTableAndSchema($column, $table, $this->relationModels[$table]->schema);
				$this->queryBuilder->orderBy($column, $order);
			}
			return $this;
		}

		public function limit($limit=1) {
			if ($this->enabled()) { $this->queryBuilder->limit($limit); }
			return $this;
		}

		public function offset($offset=0) {
			if ($this->enabled()) { $this->queryBuilder->offset($offset); }
			return $this;
		}

		//[UNION [ALL] SELECTCommand]

		public function get() {
			if ($this->enabled()) {
				$this->command = $this->queryBuilder->build();
				$query = $this->driver->query($this->command);
				return $this->driver->fetchall($query);
			}
			return false;
		}

		public function first($limit=1) {
			if ($this->enabled()) {
				$this->command = $this->queryBuilder->limit($limit)->build();
				$query = $this->driver->query($this->command);
				// Si no hubieron errores de SQL
				if ($query !== false) {
					return $this->driver->fetcharray($query);
				}
			}
			return false;
		}

		// Prepare acts like build, it retuns a command
		private function prepareQuery($command, $params) {
			$this->command = $this->queryBuilder->prepare($command, $params);
			return $this->driver->q($this->command);
		}		

		// Prepare acts like build, it retuns a command
		public function prepare($command, $params) {
			$query = $this->prepareQuery($command, $params);
			if ($query !== false) {
				// Numero de filas afectadas por la sentencia
				$affected = $this->driver->ar($query);
				return $affected;
			}
			return false;
		}
		// Prepare acts like build, it retuns a command
		public function prepareSelect($command, $params) {
			$query = $this->prepareQuery($command, $params);
			if ($query !== false) {
				return $this->driver->fetchall($query);
			}
			return false;
		}

		public function execute() {
			if ($this->enabled()) {
				// No ho hubieron errores al validar 
				if (empty($this->errors)) {
					// Formamos el comando a ejecutar
					$this->command = $this->queryBuilder->build();

					// Significa que no hubieron errores de SQL
					$query = $this->driver->q($this->command);
					if ($query !== false) {
						// Numero de filas afectadas por la sentencia
						$affected = $this->driver->ar($query);
						return $affected;
					}
				}
			}
			return 0;
		}

		public function setSchema($schema=false) {
			$this->schema = $schema;
		}

		public function transaction() {
			$this->driver->st();
		}

		public function commit() {
			$this->driver->cm();
		}

		public function rollback() {
			$this->driver->rb();
		}

	}

}

?>