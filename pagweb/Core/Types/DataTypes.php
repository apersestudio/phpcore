<?php

namespace PC\Types {

	class DataTypes {

		const MIN_TYPE = 1;
		const MINLENGTH_TYPE = 2;

		const MAX_TYPE = 4;
		const MAXLENGTH_TYPE = 8;

		const RANGE_TYPE = 16;
		const RANGELENGTH_TYPE = 32;

		const DATE_TYPE = 64;
		const DATERANGE_TYPE = 128;
		
		const ALPHA_TYPE = 256;
		const ALPHANUMERIC_TYPE = 512;
		const STRING_TYPE = 1024;

		const ARRAY_TYPE = 2048;
		const IN_TYPE = 4096;
		const BOOLEAN_TYPE = 8192;

		const IPV4_TYPE = 16384;
		const IPV6_TYPE = 32768;
		const URL_TYPE = 65536;
		
		const NUMERIC_TYPE = 131072;
		const INTEGER_TYPE = 262144;
		const FLOAT_TYPE = 524288;

		const HASH_TYPE = 1048576;
		const MD5_TYPE = 2097152;
		const SHA1_TYPE = 4194304;
		const SHA256_TYPE = 8388608;
		const ID_TYPE = 16777216;
		const UUID_TYPE = 33554432;
		const EMAIL_TYPE = 67108864;
	}

}

?>