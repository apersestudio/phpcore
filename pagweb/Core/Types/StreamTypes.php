<?php

namespace PC\Types {

	class StreamTypes {

		const READ_START = 'rb';
		const READWRITE_START = 'r+b';
		const WRITE_START_TRUNCATE = 'w';
		const READWRITE_START_TRUNCATE = 'w+b';
		const WRITE_END = 'a';
		const READWRITE_END = 'a+b';
		const WRITE_START_PROTECT = 'x';
		const READWRITE_START_PROTECT = 'x+b';
		const WRITE_START_SAFE = 'c';
		const READWRITE_START_SAFE = 'c+b';
	}

}

?>