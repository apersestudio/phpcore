<?php

namespace PC {

	use PC\Types\DataTypes;
	use PC\Validator;
	use PC\HttpData;

	class Routes {

		use \PC\Traits\DataTrait;

		private static $keepSearching = true;
		private static $vars = [];
		private static $messages = [];
		private static $url = '';
		private static $lastMatch = '';

		public static function params() {
			return self::$vars;
		}

		private $utf8_errors = array(
			PREG_NO_ERROR => 'Code 0 : Malformed regular expresion',
			PREG_INTERNAL_ERROR => 'Code 1 : There was an internal PCRE error',
			PREG_BACKTRACK_LIMIT_ERROR => 'Code 2 : Backtrack limit was exhausted',
			PREG_RECURSION_LIMIT_ERROR => 'Code 3 : Recursion limit was exhausted',
			PREG_BAD_UTF8_ERROR => 'Code 4 : The offset didn\'t correspond to the begin of a valid UTF-8 code point',
			PREG_BAD_UTF8_OFFSET_ERROR => 'Code 5 : Malformed UTF-8 data',
		);

		public static function callMethod($controller, $method=null) {
			$class = "\\".str_replace(".", "\\", $controller);
			if (class_exists($class)) {
				$MyClass = new $class();
				$MyClass->setParams(self::$vars);

				if (empty($method)) {
					if (METHOD == 'GET' && is_numeric($MyClass->id)) {
						$method = 'find';
					} else if (METHOD == 'GET') {
						$method = 'search';
					} else if (METHOD == 'POST') {
						$method = 'create';
					} else if (METHOD == 'PUT') {
						$method = 'update';
					} else if (METHOD == 'DELETE') {
						$method = 'delete';
					} else {
						$method = 'index';
					}
				}

				if (method_exists($MyClass, $method)) {
					$MyClass->$method(new HttpData());
				} else {
					// No existe el método
				}
			}
		}

		private static function urlMatch($rule, $where=[], $end=true) {

			// Si esta existe la parte del query la removemos para hacer el match
			$uri = parse_url(URI, PHP_URL_QUERY);
			$uri = str_replace("?".$uri, '', URI);

			// Si tenemos una coincidencia previa, quitamos esa parte de la URL
			$url = !empty(self::$lastMatch) ? str_replace(self::$lastMatch, "", self::$url) : $uri;
			
			// *** Optimización (Si la regla y la URL hacen referencia al home, acelerámos le proceso)
			if ($url == $rule) { return true; }

			// Si el primer caracter es una barra la eliminamos
			self::$url = substr($url, 0, 1) == "/" ? substr($url, 1) : $url;

			// Parseamos la URL para comprobar que la estructura sea válida
			$parse = parse_url(self::$url);

			// Solo usamos la parte de la ruta para comprobar si este coincide con algunas de las reglas
			$path = "/".$parse["path"];

			// Si el último character de la URL es una diagonal, lo removemos
			$lastChar = substr($path, -1);
			if ($lastChar == "/" && $path !== "/") { $path = substr($path, 0, -1); }

			// Nombres de variables donde irémos almacenando los resultados de la busqueda
			$vars = [];
			$values = [];
			$names = [];
			
			// Extraemos los nombres de variables que contenga la URL
			$regPattern = "\{\/([\w]+)(\??)\}";
			if (preg_match_all("/".$regPattern."/", $rule, $matches)) {
				$total2 = count($matches[1]);
				for ($i=0; $i<$total2; $i++) {
					$names[$i] = $matches[1][$i];
				}
			}
			
			// Si el patrón coinciden con la URL proporcionada (se aceptan guiones medios, letras, numeros, puntos y arroba)
			$match = false;
			$regexp = preg_replace("/".$regPattern."/", "(/[-\w\.@]+)$2", $rule);
			$regexp = "/".str_replace("/", "\/", $regexp).($end?"$":"")."/";

			if (preg_match($regexp, $path, $matches)) {

				self::$lastMatch = $matches[0];
				
				$total = count($matches);
				for ($i=1; $i<$total; $i++) {
					$values[] = $matches[$i];
				}

				// Si la URL contenía nombres de variables
				if (!empty($names)) {
					for ($i=0; $i<$total2; $i++) {
						if (isset($values[$i])) {
							$vars[$names[$i]] = substr($values[$i], 1);
						}
					}
				}

				// Si la URL contiene parametros vía GET
				if (isset($parse["query"])) {
					parse_str($parse["query"], $queryVars);
					foreach ($queryVars as $key=>$value) {
						if (isset($vars[$key])) {
							$vars[$key."2"] = $value;
						} else {
							$vars[$key] = $value;
						}
					}
				}

				// Si el usuario necesita que las variables contengan un tipo de datos en particular
				if (!empty($vars) && !empty($where)) {
					
					// Validamos las variables de la URL
					$validator = new Validator();
					if (!$validator->validate($vars, $where)) {
						self::$messages = array_merge(self::$messages, $validator->fieldsErrors());
					}
					unset($validator);

					// Si no hay mensajes de error, entonces pasó la prueba
					if (empty(self::$messages)) {
						$match = true;
					}

				// Si no hay reglas
				} else {
					$match = true;
				}
			}

			// Si todo coincidió asignamos las variables encontradas
			if ($match) { self::$vars = array_merge(self::$vars, $vars); }
			return $match;
		}

		private static function callHandler($handler) {
			call_user_func_array($handler, self::$vars);
		}
		public static function group($params, $handler=null) {
			if (isset($params["domain"])) {
				$regPattern = "\{(\w+)\}";
				$regexp = preg_replace("/".$regPattern."/", "(\w+)", $params["domain"]);
				$regexp = "/".str_replace(".", "\.", $regexp)."$/";

				if (preg_match($regexp, HOST, $matches)) {
					preg_match_all("/".$regPattern."/", $params["domain"], $names);
					self::$vars[$names[1][0]] = $matches[1];
				}
			}

			// Primero obtenemos el resultado del middelware
			if (isset($params["middleware"]) && !self::middleware($params["middleware"], self::$vars)) {
				return false;
			}
			
			if (isset($params["rule"])) {
				// En el caso de los grupos, debemos de separar la primera regla del resto de la URL
				// Pasamos falso en el quinto argumento para que la coincidencia no sea absoluta
				// Ya que las rutas absolutas, deben de coincidir de principio a fin sin excepción
				if (self::urlMatch($params["rule"], @$params["where"], false)) {
					self::callHandler($handler);
				}
			} else if (!empty(self::$vars)) {
				self::callHandler($handler);
			}
		}

		private static function callClass($class, $params) {
			if (class_exists($class)) {
				return $class::run($params);
			}
			return false;
		}

		private static function middleware($rules, $params) {
			if (is_array($rules)) {
				foreach ($rules as $rule) {
					if (!self::callClass($rule, $params)) {
						return false;
					}
				}
			} else {
				if (!self::callClass($rules, $params)) {
					return false;
				}
			}
			return true;
		}

		private static function call($params, $handler=null) {
			if (self::urlMatch($params["rule"], @$params["where"])) {
				
				self::$keepSearching = false;

				// Primero obtenemos el resultado del middelware
				if (isset($params["middleware"]) && !self::middleware($params["middleware"], self::$vars)) {
					return false;
				}
				// Llamamos al controlador				
				call_user_func_array($handler, self::$vars);
				return true;
			}
			return false;
		}

		public static function any($params, $handler=null) {
			if (!self::$keepSearching) { return false; }
			self::$messages = [];
			return self::call($params, $handler);
		}

		public static function get($params, $handler=null) {
			if (METHOD == 'GET') { return self::any($params, $handler); }
			return false;
		}

		public static function post($params, $handler=null) {
			if (METHOD == 'POST') { return self::any($params, $handler); }
			return false;
		}

		public static function put($params, $handler=null) {
			if (METHOD == 'PUT') { return self::any($params, $handler); }
			return false;
		}

		public static function delete($params, $handler=null) {
			if (METHOD == 'DELETE') { return self::any($params, $handler); }
			return false;
		}

		public static function head($params, $handler=null) {
			if (METHOD == 'HEAD') { return self::any($params, $handler); }
			return false;
		}

		public static function options($params, $handler=null) {
			if (METHOD == 'OPTIONS') { return self::any($params, $handler); }
			return false;
		}

		public static function patch($params, $handler=null) {
			if (METHOD == 'PATCH') { return self::any($params, $handler); }
			return false;
		}

	}

}

?>