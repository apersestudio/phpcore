<?php

namespace PC {

	use PC\Managers\LanguageManager;

	class Framework {

		public static $databases = false;
		public static $controller = false;
		public static $uriparts = false;

		public static $session = false;
		public static $language = false;

		public static function setup($configFile="Main") {

			// Cargamos la configuración
			$configPath = CONFIG.$configFile.".php";
			if (!file_exists($configPath)) {
				exit("No se pudo cargar el archivo de configuración: /".$configFile);
			} else {
				Config::register(require_once($configPath));
			}

			// Realizamos una instancia del manejador con los idiomas designados por el usuario
			$languages = Config::get("language.available");
			self::$language = new LanguageManager($languages);

			$routesFile = "Routes.php";
			$routesPath = CONFIG.$routesFile;
			if (!file_exists($routesPath)) {
				exit("El archivo de rutas esta perdido: /".$routesFile);
			} else {
				require_once($routesPath);
			}
			
		}
	}

}

?>