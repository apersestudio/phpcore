<?php

namespace PC {

	use PC\Libraries\HtmlLibrary;

	/** Singleton Pattern **/
	class View {

		private static $instance = null;

		private $html = '';
		private $content = '';
		private $view = '';
		private $layout = '';
		private $params = [];

		private $layoutPath = '';
		private $viewPath = '';

		/* Function to the clean up the buffer */
		private function cleanOuputWithTidy($buffer) {
			if (function_exists("tidy_parse_string")) {
				$config = array(
					'char-encoding'=>'utf8',
					'output-xhtml'=>true,
					'indent'=>true,
					'markup'=>true,
					'input-xml'=>false,
					'wrap'=>false
				);
				return tidy_parse_string($buffer, $config, 'utf8');
			} else {
				return self::cleanContent($buffer);
			}
		}

		private function cleanContent($buffer) {
			$search = array('/\>[^\S ]+/s', '/[^\S ]+\</s', '/(\s)+/s');
			$replace = array('>', '<', '\\1');
			return preg_replace($search, $replace, $buffer);
		}

		// Función privada para ejecutar código antes de devolver la salida al navegador
		private function getContent($path, $content='') {
			ob_start("self::cleanContent");
			require_once($path);
			$content = ob_get_contents();
			ob_clean();
			return $content;
		}

		public function load($view) {
			$this->view = "App.Views.".$view;
			$this->viewPath = str_replace("App\\", PAGES, str_replace(".", "\\", $this->view).".php");
			return $this;
		}

		public function layout($layout) {
			$this->layout = "App.Layouts.".$layout;
			$this->layoutPath = str_replace("App\\Layouts\\", LAYOUTS, str_replace(".", "\\", $this->layout).".php");
			return $this;
		}

		public function html() {
			return $this->html;
		}

		public function show() {
			if (file_exists($this->viewPath)) {
				$this->content = self::getContent($this->viewPath);
			}
			if (file_exists($this->layoutPath)) {
				$this->content = self::getContent($this->layoutPath, $this->content);
			}
			exit($this->content);
		}

		/** Can't clone the object **/
		private function __clone() {}

		/** Can't unserialize the object **/
		private function __wakeup() {}

		/** Can't call new View outside the class **/
		protected function __construct($params) {
			$this->params = $params;
			$this->html = new HtmlLibrary();
		}

		/** Access to the parameters from the outside **/
		public function __get($property) {
			if (isset($this->params[$property])) {
				return $this->params[$property];
			} else if (isset($this->$property)) {
				return $this->$property;
			}
			return null;
		}

		/** The only way to get the instance **/
		public static function getInstance($params=[]) {
			if (self::$instance === null) { self::$instance = new static($params); }
			return self::$instance;
		}

	}

}

?>