<?php

namespace PC\Libraries {

	use PC\Libraries\StringsLibrary;

	class FilesLibrary {

		public static function set($var, $value='::default::') {
			if (is_array($var)) {
				foreach($var as $key=>$value) { $_FILES[$key] = $value; }
				return true;
			} else {
				$_FILES[$var] = $value;
				return true;
			}
		}
		public static function get($var, $return=false) {
			if (isset($_FILES[$var])) {
				$file = $_FILES[$var];
				// Temporary Location
				$file["tmp"] = $file["tmp_name"];
				unset($file["tmp_name"]);
				// Mime Type
				$mime = self::getMime($file["tmp"]);
				if (!!$mime) { $file["type"] = $mime; }
				return $file;
			}
			return $return;
		}
		private static function normalizePath($path) {
			return str_replace("\\", "/", $path);
		}
		
		public static function isJPG($url) {
			return preg_match("/\.(jpg|jpeg)$/", strtolower($url));
		}
		
		public static function isGIF($url) {
			return preg_match("/\.gif$/", strtolower($url));
		}
		
		public static function isPNG($url) {
			return preg_match("/\.png$/", strtolower($url));
		}
		
		public static function isAVI($url) {
			return preg_match("/\.avi$/", strtolower($url));
		}
		
		public static function isMOV($url) {
			return preg_match("/\.mov$/", strtolower($url));
		}
		
		public static function isMP4($url) {
			return preg_match("/\.mp4$/", strtolower($url));
		}
		
		public static function isFLV($url) {
			return preg_match("/\.flv$/", strtolower($url));
		}
		
		public static function isImage($url) {
			return preg_match("/\.(jpg|jpeg|gif|png)$/", strtolower($url));
		}
		
		public static function isVideo($url) {
			return preg_match("/\.(avi|mov|mp4|flv)$/", strtolower($url));
		}
		public static function isDocument($url) {
			return preg_match("/\.(doc|xls|ppt|txt|pdf|xml)$/", strtolower($url));
		}
		public static function isSecure($url) {
			return preg_match("/\.(cer|crt|der|key|pem|csr|pfx|p12|p7b|p7r|spc|sst|slt)$/", strtolower($url));
		}
		
		public static function getPrefix($uri) {
			if (self::isImage($uri)) {
				return "img";
			} elseif (self::isVideo($uri)) {
				return "vid";
			} elseif (self::isDocument($uri)) {
				return "doc";
			} elseif (self::isSecure($uri)) {
				return "enc";
			} else {
				return "xyz";
			}
		}

		public static function getMime($file) {
			if (file_exists($file) && self::supportFileInfo()) {
				$handler = finfo_open(FILEINFO_MIME_TYPE);
				$fileinfo = finfo_file($handler, $file);
				finfo_close($handler);
				return $fileinfo;
			}
			return false;
		}

		public static function supportThumbs() {
			return extension_loaded("gd");
		}

		public static function supportFileInfo() {
			return extension_loaded("fileinfo");
		}
		
		public static function supportVideoThumbs() {
			return extension_loaded("ffmpeg") && extension_loaded("gd");
		}
		
		public static function removeExtension($url) {
			return preg_replace('/\.[^.]+$/', '', $url);	
		}
		
		public static function isRemote($url) {
			return preg_match("/ftp|http|https|ssl/", strtolower($url));	
		}

		public static function absoluteToURL($url) {
			$url = (strpos($url, "./") !== false) ? realpath($url) : $url;
			return str_replace("\\", "/", str_replace(DOCUMENTROOT, DOMAIN, $url));
		}
		
		public static function URLToAbsolute($url) {
			return realpath(str_replace(DOMAIN, DOCUMENTROOT, $url));
		}
		public static function insertScriptRemote($url, $nocache=false) {
			$time = $nocache ? '?nocache='.time() : '';
			return '<script type="text/javascript" src="'.$url.$time.'"></script>';
		}
		public static function insertScriptRemoteDefer($url, $type="text/javascript") {
			return array("type"=>$type, "src"=>$url);
		}
		public static function insertScript($url, $nocache=false, $html=true) {
			$time = $nocache ? '?nocache='.time() : '';
			$src = self::absoluteToURL($url);
			if ($html) { return file_exists($url) ? '<script type="text/javascript" src="'.$src.$time.'"></script>' : false; }
			return file_exists($url) ? $src.$time : false;
		}
		public static function insertScriptDefer($url, $type="text/javascript") {
			$src = self::absoluteToURL($url);
			if (file_exists($url)) {
				$cache = filemtime($url);
				return array("type"=>$type, "src"=>$src."?cache=".$cache);
			}
			return false;	
		}

		public static function insertLink($url, $rel="stylesheet", $html=true, $nocache=false) {
			$time = !empty($nocache) ? '?nocache='.time() : '';
			$rel = !empty($rel) ? $rel : 'stylesheet';
			$type = ($rel == 'stylesheet') ? ' type="text/css"' : '';
			if (self::isRemote($url)) {
				return '<link rel="'.$rel.'"'.$type.' href="'.$url.$time.'" />';
			} else {
				$src = self::absoluteToURL($url);
				if ($html) { return file_exists($url) ? '<link rel="'.$rel.'"'.$type.' href="'.$src.$time.'" />' : false; }
				return file_exists($url) ? $src.$time : false;
			}
		}
		
		public static function insertStyleRemote($url, $media="screen", $nocache=false) {
			$time = $nocache ? '?nocache='.time() : '';
			return '<link rel="stylesheet" type="text/css" media="'.$media.'" href="'.$url.$time.'" />';
		}
		public static function insertStyleRemoteDefer($url, $media="screen") {
			return array("type"=>"text/css", "media"=>$media, "src"=>$url);
		}

		public static function insertStyle($url, $media="screen", $html=true, $nocache=false) {
			$time = $nocache ? '?nocache='.time() : '';
			$absolute = self::absoluteToURL($url);
			if ($html) { return file_exists($url) ? '<link rel="stylesheet" type="text/css" media="'.$media.'" href="'.$absolute.$time.'" />' : false; }
			return file_exists($url) ? $absolute.$time : false;
		}

		public static function insertStyleDefer($url, $media="screen") {
			$src = self::absoluteToURL($url);
			if (file_exists($url)) {
				$cache = filemtime($url);
				return array("type"=>"text/css", "media"=>$media, "src"=>$src."?cache=".$cache);
			}
			return false;
		}

		public static function clean($url) {
			if (file_exists($url) || is_link($url)) { return @unlink($url); }
			return false;
		}

		public static function cleanDirname($dirname) {
			switch ($dirname) {
				case '\\' : $dirname = '/'; break;
				case '.' : $dirname = ''; break;
				default : $dirname = $dirname.'/'; break;
			}
			return $dirname;
		}
		
		public static function splitpath($uri) {
			$pi = pathinfo($uri);
			$rp = realpath($uri);

			// Solo para windows
			$pi["dirname"] = self::normalizePath($pi["dirname"]);
			$uri = self::normalizePath($uri);
			$rp = self::normalizePath($rp);

			$pi['extension'] = isset($pi['extension']) ? $pi['extension'] : '';
			$pi['uri'] = !$rp ? $uri : $rp;
			$pi['filename'] = self::removeExtension($pi['basename']);
			$pi['original'] = $pi['basename'];
			$pi['dirname'] = self::cleanDirname($pi['dirname']);
			return $pi;	
		}
		
		public static function getExtension($url) {
			$pi = self::splitpath($url);
			return $pi["extension"];
		}
		
		public static function uniqname($originalname, $seo=false, $subfix='') {
			// Dirección IP desde donde se genera el nombre [12 caracteres]
			$ip = ''; foreach(explode(".", REMOTEIP) as $octet) { $ip.= str_pad($octet, 3, "0", STR_PAD_LEFT); }
			// Puerto en el que el cliente esta conectado al servidor [5 caracteres]
			$port = str_pad(REMOTEPORT, 5, "0", STR_PAD_LEFT);
			// Timestamp en formato Unix [10 caracteres]
			list($microseconds, $unixtimestamp) = explode(" ", microtime());
			// Microsegundos transcurridos al momento de la ejecución del script [6 caracteres]
			$microseconds = str_pad(substr($microseconds, 2, 6), 6, "0");
			$newname = $ip.$port.$unixtimestamp.$microseconds;
			// Extension [4 caracteres]
			$extension = ".".self::getExtension($originalname);
			// Seo Name [ 60 caracteres]
			$basename = basename($originalname, $extension);
			$seopart = $seo ? "-".substr(StringsLibrary::urify($basename), 0, 60) : "";
			// Prefijo [3 caracteres]
			$prefix = self::getPrefix($originalname);
			// Nombre Final del archivo
			$newname = strtolower($prefix.$newname.$seopart.$subfix).$extension;
					
			unset($ip, $port, $unixtimestamp, $microseconds, $seopart, $prefix, $extension, $basename);
			return $newname;
		}
		
		public static function upload($name=false, $path=false, $seo=true) {
			if (!empty($name) && !empty($path)) {
				$file = is_array($name) ? $name : self::get($name);
				if (is_uploaded_file(@$file["tmp"])) {
					
					$newname = self::uniqname($file["name"], $seo);
					$uri = $path.$newname;
					
					// Solo regresamos falso cuando el usuario envio un archivo pero no pudimos subirlo
					$moved = move_uploaded_file($file["tmp"], $uri);
					if ($moved == false) { return false; }

					// Aprovechamos para optimizar el archivo
					$ssu = strtolower($uri);
					if (strpos($ssu, "png") !== false) {
						$cmd = "pngout /c6 /f5 /s0 /k0 ".$uri." 2>&1";
						$shell_return = shell_exec($cmd);
					} else if (strpos($ssu, "jpg") !== false) {
						$cmd = "jpegoptim -f -m 90 -t --strip-all ".$uri." 2>&1";
						$shell_return = shell_exec($cmd);
					}
					
					// Regresamos información sobre el archivo subido
					$pi = self::splitpath($uri);
					$pi["original"] = $file["name"];
					return $pi;
				}
			}
			return false;
		}
		
		public static function download($file) {
			$filename = basename($file);
			$isie = (stripos($_SERVER['HTTP_USER_AGENT'], "msie") !== false);
			$ctype = $isie ? "force-download" : "octet-stream";
			header("Content-type: application/".$ctype);
			header("Content-Length: ".filesize($file));
			header("Content-Disposition: attachment; filename=".$filename);
			readfile($file);
		}
		public static function move($file, $dir) {
			if (!is_file($file) || !is_dir($dir)) { return false; }
		
			$pi = self::splitpath($file);
			$copy = $dir."/".$pi["basename"];
			if ($file == $copy) {
				return self::splitpath($file);
			} else {
				copy($file, $copy);
				self::clean($file);
				return self::splitpath($copy);
			}
		}
		public static function getMode($mode) {
			$modes = array();
			$modes["readonly"] = "r";
			$modes["writeonly"] = "w";
			$modes["appendonly"] = "a";
			$modes["readandwrite"] = "r+";
			$modes["forcewrite"] = "w+";
			$modes["append"] = "a+";
			return $modes[$mode];
		}
		public static function read($fname, $fread='', $mode='readonly') {
			if (is_file($fname) == false) {
				return $fread;
			} else if (filesize($fname) > 0) {
				$fopen = fopen($fname, self::getMode($mode));
				$fread = fread($fopen, filesize($fname));
				fclose($fopen);
				return $fread;
			}
			return $fread;
		}
		public static function write($fname, $content='', $mode='forcewrite') {
			$mode = self::getMode($mode);
			$fopen = fopen($fname, $mode);
			$fwrite = fwrite($fopen, $content);
			fclose($fopen);
			return $fwrite;
		}
		
		public static function listDirectory($dir, $sort) {
			$dh = opendir($dir);
			$files = array();
			while (false !== ($file = readdir($dh))) {
				if ($file == "." || $file == "..") { continue; }
				$files[] = $file;
			}
			if ($sort == "ASC") { sort($files); } else { rsort($files); }
			return $files;
		}
		
		public static function listTreeDirectory($dir='.', $sort) {
			if(!is_dir($dir)) { return false; }
		
			$files = array();
			$dirs = array($dir);
			while($dir = array_pop($dirs)) {
				if($dh = opendir($dir)) {
					while(($file = readdir($dh)) !== false) {
						if($file == '.' || $file == '..') { continue; }
						$path = $dir.SEPARATOR.$file;
						
						if(is_dir($path)) { $dirs[] = $path; }
						if(is_file($path)) { $files[] = $path; }
					}
					closedir($dh);
				}
			}
			if ($sort == "ASC") { sort($files); } else { rsort($files); }
				return $files;
		}
		
		public static function copyDirectory($src, $dst) {
			if ($dir = opendir($src)) {
				$success = true;
				while(false !== ($file=readdir($dir))) {
					if (($file != '.') && ($file != '..')) {
						if (is_dir($src.$file)) {
							mkdir($dst.$file, 0755);
							self::copyDirectory($src.$file.SEPARATOR, $dst.$file.SEPARATOR);
						} else {
							$copied = copy($src.$file, $dst.$file);
							if (!$copied) { $success = false; break; }
							chmod($dst.$file, 0755);
						}
					}
				}
				closedir($dir);
				return $success;
			}
			return false;
		}

		public static function rename($oldpath, $newpath) {
			// Si PHP tiene abierto el archivo que intentamos renombrar
			// Con esta línea permitimos que se pueda realizar la operación
			sleep(1);
			// Intentamos renombrar el archivo en cuestion
			return @rename($oldpath, $newpath);
		}
		
		public static function delTree($src) {	
			if ($dir = opendir($src)) {
				while(false !== ($file=readdir($dir))) {
					if (($file != '.') && ($file != '..')) {
						$path = $src.SEPARATOR.$file;
						if (is_dir($path)) {
							self::delTree($path);
						} else {
							self::clean($path);
						}
					}
				}
				closedir($dir);
			}
			return @rmdir($src);
		}

		public static function deleteImage($path, $na, $tm) {
		
			// Borramos los thumbnails
			$pi = Files::splitpath($na);
			$sizes = explode(",", $tm);
			$files = 0;
			foreach ($sizes as $size) {
				$image = $path.$pi["filename"]."-".$size.".".$pi["extension"];
				if (file_exists($image)) { $files += intval(@unlink($image)); }
			}
			
			// Borramos la imagen principal
			$image = $path.$na;
			if (file_exists($image)) { $files += intval(@unlink($image)); }
			
			// Notificamos cuantos archivos se pudieron borrar
			return $files;
		}
	}
	
}

?>