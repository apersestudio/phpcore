<?php
namespace PC\Libraries {

	class PostLibrary {
		
		public static function set($var, $value='::default::') {
			if (is_array($var)) {
				foreach($var as $key=>$value) { $_POST[$key] = $value; }
				return true;
			} else {
				$_POST[$var] = $value;
				return true;
			}
		}
		
		public static function get($var, $return=null) {
			if (isset($_POST[$var])) {
				$return = $_POST[$var];
				if (MQUOTES) {
					if (is_array($return)) {
						$return = array_map("stripslashes", $return);
					} else {
						$return = stripslashes($return);
					}
				}
			}
			return $return;
		}
		
	}

}

?>