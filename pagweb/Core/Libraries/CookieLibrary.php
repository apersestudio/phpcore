<?php

namespace PC\library {

	class CookieLibrary {

		public static function setEncriptedArray($name, $array=false, $time=0) {
			// Si el tipo de dato no es correcto, terminamos el proceso
			if (!is_array($array) || empty($array)) { return false; }
			// Desencriptar
			$udata = !isset($_COOKIE["udata"]) ? array() : json_decode(Crypto::decrypt_aes($_COOKIE["udata"], REMOTEIP), true);
			// Actualizar
			$udata[$name] = http_build_query($array);
			// Encriptar
			$udata = Crypto::encrypt_aes(json_encode($udata), REMOTEIP);
			// Guardar
			self::set("udata", $udata);
			return true;
		}

		public static function getEncriptedArray($name) {
			$udata = self::getAllEncripted();
			if (isset($udata[$name])) {
				parse_str($udata[$name], $array);
				return $array;
			}
			return false;
		}

		public static function setEncripted($name, $value=false, $time=0) {
			// Desencriptar
			$udata = !isset($_COOKIE["udata"]) ? array() : json_decode(Crypto::decrypt_aes($_COOKIE["udata"], REMOTEIP), true);
			// Actualizar
			if (is_array($name)) {
				foreach ($name as $k=>$v) { $udata[$k] = $v; }
			} else {
				$udata[$name] = $value;
			}
			// Encriptar
			$udata = Crypto::encrypt_aes(json_encode($udata), REMOTEIP);
			// Guardar
			self::set("udata", $udata, $time);
			return true;
		}

		public static function getAllEncripted() {
			if (!isset($_COOKIE["udata"])) { return false; }
			return json_decode(Crypto::decrypt_aes($_COOKIE["udata"], REMOTEIP), true);
		}

		public static function getEncripted($name, $return=false) {
			$udata = self::getAllEncripted();
			if (isset($udata[$name])) { return $udata[$name]; }
			return $return;
		}
		
		public static function set($name, $value=false, $time=0) {
			if (is_array($name)) {
				foreach ($name as $k=>$v) {
					$_COOKIE[$k] = $v;
					setcookie($k, $v, $time, "/", HOST, SSL, SSL);
				}
			} else {
				$_COOKIE[$name] = $value;
				setcookie($name, $value, $time, "/", HOST, SSL, SSL);
			}
			return true;
		}
		public static function get($name, $return=false) {
			if (isset($_COOKIE[$name])) { return $_COOKIE[$name]; }
			return $return;
		}
		public static function delete($key) {
			unset($_COOKIE[$key]);
			setcookie($key, false, time()-3600, "/", HOST, SSL, SSL);
			return true;
		}
		public static function clear() {
			foreach ($_COOKIE as $k=>$v) {
				self::delete($k);
			}
		}
	}	
}
?>