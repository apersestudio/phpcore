<?php

namespace PC\Libraries {

    use PC\Manager;
    use PC\Config;

    class SessionLibrary {

        private static $name = "client";
        private static $lastActivity = 0;
        private static $manager;
        private static $settings = [
            "use_strict_mode"=>false,
            "cookie_httponly"=>true,
            "use_only_cookies"=>true,
            "auto_start"=>false
        ];

        // Para generar el id de sessión de los visitantes utilizaremos 
        // a) La dirección ip "REMOTEIP"
        // b) El agente de usuario o cadena de navegador
        // c) un timestamp para no causar conflictos en redes donde compartan la misma IP Pública
        // De esta forma si la dirección IP cambia o cambia el navegador, también cambiará la id se sessión
        private static function regenerate() {
            $iplong = sprintf("%u", ip2long(REMOTEIP));
            $time = strtotime(date("Y-m-d H:i:s"));
            return "node".md5($iplong.USER_AGENT).$time;
        }
        // Extra la dirección IP y el agente del usuario de un ID de sesión
        private static function origin($sessionId) {
            return substr($sessionId, 0, -10);
        }
        // Extrae la marca de tiempo de un ID de sesión
        private static function expiration($sessionId) {
            return substr($sessionId, -10);
        }
        // Convierte una marca de tiempo en una fecha con formato
        private static function timeToDate($time=null) {
            return date("Y-m-d H:i:s", empty($time) ? time() : $time);
        }
        // Genera una nueva marca de tiempo para la expiración de la sesión a partir de un ID de sesión
        private static function newExpiration($sessionId) {
            return self::expiration($sessionId) + Config::get("session.expiration");
        }

        // Verificamos que la cookie sea válida, esto es:
        // Cuando la dirección IP de la petición es la misma que la almacenada en la cookie
        // Y también cuando el agente de usuario no ha cambiado
        // En caso contrario, no bloqueamos, sino que generámos un nuevo ID de sesión
        private static function valid($sessionId) {
            return self::origin($sessionId) === self::origin(self::regenerate());
        }

        // Después de iniciar la sesión comprobamos que el tiempo de expiración de la cookie coincida con el guardado en el servidor
        // Si no coinciden signifíca que el usuario intentó engañar al sistema modificando el tiempo de expiración (merece sansión?)
        // También comprobamos que la expiración guardada no sea menor a la fecha actual
        private static function expired($sessionId) {
            // Si no esta definido el tiempo de expiración lo guardamos
            if (!isset($_SESSION["expiration"])) {
                $_SESSION["expiration"] = self::newExpiration($sessionId);
                return false;
            }
            return ($_SESSION["expiration"] !== self::newExpiration($sessionId)) || ($_SESSION["expiration"] < time());
        }

        // Comproeba que la sesión no haya estado inactiva por mucho tiempo
        // Realizamos esta operación sacando la diferencia entre el último tiempo de actividad y el tiempo actual
        // Si esta diferencia es mayor al tiempo de inactividad permitido entonces activamos la bandera
        private static function endForInactivity() {
            $lastActivity = isset($_SESSION["lastActivity"]) ? $_SESSION["lastActivity"] : time();
            return self::timeDiff($lastActivity, time()) > Config::get("session.inactivity");
        }

        // Destruye la sesión actual
        private static function destroy() {
            $params = session_get_cookie_params();
            setcookie(session_name(), '', time() - 42000,
                $params["path"], $params["domain"],
                $params["secure"], $params["httponly"]
            );
            session_destroy();
        }

        // Diferencia de segundos entre dos marcas de tiempo
        private static function timeDiff($from, $to) {
            return abs(abs($from) - abs($to));
        }

        public static function debug() {
            //echo (self::$expired?"EXPIRED":"ACTIVE")." / ".self::timeToDate($_SESSION["expiration"])." < ".self::timeToDate();
        }

        // Elimina la sesión actual e inicia una nueva sesión
        private static function restore() {
            // Solo para estar seguro
            self::debug();
            // Destruimos la sesión anterior
            self::destroy();
            // Después la regenerámos
            $sessionId = self::regenerate();
            session_id($sessionId);
            // Volvemos a iniciar la sesión
            session_start();
        }

        // Recupera el ID de la sesión desde la cookie del usuario o crea una nueva
        private static function getId($name) {
            return isset($_COOKIE[$name]) ? $_COOKIE[$name] : self::regenerate();
        }

        // Comprueba que la sesión este configurada de una forma segura desde apache:
        // Verificando que podemos cambiar el id de sesión
        // JavaScript no puede leer ni escribir las cookies
        // Las sesiónes nunca se enviarán como parte de la URL
        // Las sesiones no deben de iniciarse de forma automática
        private static function isSafe() {
            $puntos=0;
            foreach (self::$settings as $setting=>$isEmpty) {
                if (empty(ini_get("session.".$setting)) == $isEmpty) {
                    echo "The ini setting session.".$setting." should be: ".intval($isEmpty)."<br />";
                } else {
                    $puntos++;
                }
            }
            return $puntos == count(self::$settings);
        }

        // Hacemos una instancia del manejador de sesiones y lo asignamos como el nuevo manejador
        private static function manager(Manager $manager) {
            session_set_save_handler($manager, true);
        }

        // Inicia la sesión:
        // Validando el origen de la misma
        // Comprobando la expiración
        // Comprobando el tiempo de inactividad
        public static function start() {

            // Si el usuario ha especificado un manejador de sesión por base de datos
            $manager = Config::get("session.databaseManager");
            if (!empty($manager)) { self::manager(new $manager); }

            // Comprobamos que la sesión sea segura
            if (!self::isSafe()) { exit("Can't setup session."); }

            $sessionId = self::getId(self::$name);
            $sessionId = self::valid($sessionId) ? $sessionId : self::regenerate();

            session_id($sessionId);
            session_name(self::$name);
            session_start();

            // Cuando una sesión ya expiró:
            // Restauramos la sesión actual y
            // Actualizamos la fecha de expiración
            $expired = self::expired($sessionId);
            if ($expired) {
                self::restore();
                $_SESSION["expiration"] = self::newExpiration($sessionId);
            }

            // Cuando una sesión cierra por el tiempo de inactividad: 
            // Restauramos la sesión actual
            $endForInactivity = self::endForInactivity();
            if ($endForInactivity) {
                self::restore();
            }

            // Si llegámos aquí el usuario es libre de continuar y
            // Actualizamos la marca de tiempo de la última actividad
            $_SESSION["lastActivity"] = time();
        }

        public static function get($varname, $default=false) {
            if (isset($_SESSION[$varname])) { return $_SESSION[$varname]; }
            return $default;
        }
    }

}

?>