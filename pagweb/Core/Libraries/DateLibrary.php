<?php

namespace PC\library {

	class DateLibrary {

		public static $months = array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
		public static $days = array("Domingo","Martes","Miércoles","Jueves","Viernes","Sábado","Lunes");
		
		public static $months_s = array("Ene","Feb","Mar","Abr","May","Jun","Jul","Ago","Sep","Oct","Nov","Dic");
		public static $days_s = array("Dom","Mar","Mié","Jue","Vie","Sáb","Lun");

		public static function microts() {
			$mt = explode(" ", microtime());
			return $mt[1].substr($mt[0], 2);
		}
		public static function microdate() {
			$mt = explode(" ", microtime());
			return date("YmdHis",$mt[1]).substr($mt[0], 2);
		}
		public static function tostring($time, $long=true){
			$time = is_string($time) ? strtotime($time) : $time;
			if ($long) {
				$dow = self::$days[date("w", $time)];
				$moy = self::$months[date("n", $time)-1];
				return $dow.date(", j \d\e ", $time).$moy.date(" \d\e Y", $time);
			} else {
				$moy = self::$months[date("n", $time)-1];
				return date("j/", $time).$moy.date("/Y", $time);
			}
		}
		/* Datediff */
		public static function diff($time1, $time2) {
			if(function_exists('date_diff')){
				$time1 = is_string($time1) ? $time1 : date("Y-m-d H:i:s", $time1);
				$time2 = is_string($time2) ? $time2 : date("Y-m-d H:i:s", $time2);
				$d = date_diff(date_create($time1), date_create($time2));
				return array("y"=>$d->y,"m"=>$d->m,"d"=>$d->d,"h"=>$d->h,"i"=>$d->i,"s"=>$d->s,"v"=>"php5");
			} else {
				$time1 = is_int($time1) ? $time1 : strtotime($time1);
				$time2 = is_int($time2) ? $time2 : strtotime($time2);
			
				// If time1 is bigger than time2 then swap time1 and time2
				if ($time1 > $time2) {
					$ttime = $time1;
					$time1 = $time2;
					$time2 = $ttime;
				}

				// Set up intervals and diffs arrays
				$intervals = array('year','month','day','hour','minute','second');
				$identifiers = array('y','m','d','h','i','s');
				$d = array();

				// Loop thru all intervals
				foreach ($intervals as $index=>$interval) {
					// Create temp time from time1 and interval
					$ttime = strtotime('+1 ' . $interval, $time1);
					// Set initial values
					$add = 1;
					$looped = 0;
					// Loop until temp time is smaller than time2
					while ($time2 >= $ttime) {
						// Create new temp time from time1 and interval
						$add++;
						$ttime = strtotime("+" . $add . " " . $interval, $time1);
						$looped++;
					}
					$time1 = strtotime("+" . $looped . " " . $interval, $time1);
					$d[$identifiers[$index]] = $looped;
				}
				// Return string with times
				$d["v"] = "php4";
				return $d;
			}
		}
	}

}
?>