<?php
namespace PC\Libraries {

	class GetLibrary {
		
		public static function set($var, $value='::default::') {
			if (is_array($var)) {
				foreach($var as $key=>$value) { $_GET[$key] = $value; }
				return true;
			} else {
				$_GET[$var] = $value;
				return true;
			}
		}

		public static function get($var, $return=false) {
			if (isset($_GET[$var])) {
				$return = $_GET[$var];
				if (MQUOTES && is_array($return)) {
					$return = array_map("stripslashes", $return);
				} else {
					$return = stripslashes($return);
				}
			}
			return $return;
		}
		
	}

}
?>