<?php

namespace PC\Libraries {

	/* //////////////////////// IMAGE TYPE public static functionS //////////////////////// */
	class ImageLibrary {

		private $uri;
		private $originalWidth = 0;
		private $originalHeight = 0;
		private $basename = false;
		private $filename = false;
		private $dirname = false;
		private $extension = false;
		private $currentWidth = false;
		private $currentHeight = false;
		private $currentFilename = false;
		private $currentFile = false;
		
		private $originalSource = false;
		private $destinySource = false;
		private $quality = 90;

		// Constructor
		public function __construct($uri) {
			$this->load($uri);
		}
		
		private function setSource($width, $height) {
			$this->destinySource = imagecreatetruecolor($width, $height);
			if (preg_match("/png/i", $this->extension)) {
				$this->originalSource = imagecreatefrompng($this->uri);
				imagesavealpha($this->originalSource, true);
				imagealphablending($this->originalSource, false);
				imagesavealpha($this->destinySource, true);
				imagealphablending($this->destinySource, false);
			} else if (preg_match("/jpg|jpeg/i", $this->extension)) {
				$this->originalSource = imagecreatefromjpeg($this->uri);
			} else if (preg_match("/gif/i", $this->extension)) {
				$this->originalSource =  imagecreatefromgif($this->uri);
			}
		}
		
		private function fit($width, $height) {
			$this->currentWidth = $width;
			$this->currentHeight = $height;
		}
		private function fitWidth($width) {
			$this->currentWidth = $width;
			$this->currentHeight = ($this->currentWidth / $this->originalWidth) * $this->originalHeight;
		}
		private function fitHeight($height) {
			$this->currentHeight = $height;
			$this->currentWidth = ($this->currentHeight / $this->originalHeight) * $this->originalWidth;
		}

		public function getCurrentFilename() {
			return $this->currentFilename;
		}

		public function getCurrentFile() {
			return $this->currentFile;
		}
		
		public function getSize() {
			list($w, $h) = getimagesize($this->uri);
			return array("width"=>$w, "height"=>$h);
		}
		
		public function load($uri) {
			$rp = realpath($uri);
			if (!file_exists($rp)) { return false; }
			
			$this->uri = $rp;
			$pi = pathinfo($this->uri);
			$this->extension = $pi["extension"];
			$this->basename = $pi["basename"];
			$this->filename = $pi["filename"];
			$this->dirname = $pi["dirname"].SEPARATOR;
			
			$size = $this->getSize();
			$this->originalWidth = $size["width"];
			$this->originalHeight = $size["height"];
			return $pi;
		}

		public function delete() {
			if (!file_exists($this->currentFile)) { return false; }
			return @unlink($this->currentFile);
		}
		
		public function resize($width=false, $height=false, $mode="FIT_BOX") {
			if (!$width || !$height) { return false; }
			
			// Cambiamos el ancho y alto para tener el tamaño exácto definidos por el usuario
			if ($mode == "FIT_BOTH") {
				$this->fit($width, $height);
			// El ancho fijo por lo que de de alto
			} else if ($mode == "FIT_WIDTH") {
				$this->fitWidth($width);
			// La altura fija por lo que de de ancho
			} else if ($mode == "FIT_HEIGHT") {
				$this->fitHeight($height);
			// Cambiamos el ancho y alto para entrar en los limites establecidos sin deformar la imagen	
			} else if ($mode == "FIT_BOX") {
				$this->fitWidth($width);
				if ($this->currentHeight > $height) { $this->fitHeight($height); }
			}
			
			$this->currentWidth = ceil($this->currentWidth);
			$this->currentHeight = ceil($this->currentHeight);
			
			$this->setSource($this->currentWidth, $this->currentHeight);
			imagecopyresampled ($this->destinySource, $this->originalSource, 0, 0, 0, 0, $this->currentWidth, $this->currentHeight, $this->originalWidth, $this->originalHeight);
			return $this;
		}
		
		public function setQuality($quality) {
			if (preg_match("/png/i", $this->extension)) {
				$this->quality = round((9/100) * $quality);
			} else {
				$this->quality = $quality;
			}
		}
		
		// Memory Cleanup
		public function clear() {
			if ($this->originalSource!==false) { imagedestroy($this->originalSource); }
			if ($this->destinySource!==false) { imagedestroy($this->destinySource); }
		}
		
		public function save($quality=80, $destiny=false, $includeSize=true) {
			if ($destiny==false) { $destiny = $this->dirname; }
			
			// Write Capabilities
			if (!is_writable($destiny)) { return false; }
			
			$sizeInfo = $includeSize ? "-".$this->currentWidth."x".$this->currentHeight : "";
			$this->currentFilename = $this->filename.$sizeInfo.".".$this->extension;
			$this->currentFile = $destiny.$this->currentFilename;
			
			// Quality Setup
			$this->setQuality($quality);
			
			// Save Image to Disk
			if (preg_match("/png/i", $this->extension)) {
				imagepng($this->destinySource, $this->currentFile, $this->quality);
			} else if (preg_match("/jpg|jpeg/i", $this->extension)) {
				imagejpeg($this->destinySource, $this->currentFile, $this->quality);
			} else if (preg_match("/gif/i", $this->extension)) {
				imagegif($this->destinySource, $this->currentFile, $this->quality);
			}
			
			$this->clear();
			return file_exists($this->currentFile);
		}

		public function createthumbs($sizes) {
			$tm = array();
			$files = array();
			foreach ($sizes as $size) {
				// Si todos los thumbnails se guardan, el arreglo de archivos se llenará
				if ($this->resize($size["width"], $size["height"])->save()) {
					$tm[] = array("width"=>$this->currentWidth, "height"=>$this->currentHeight);
					$files[] = $this->currentFile;
				// Si cualquiera de los thumbnails fallá en crearse, devolvemos falso
				} else {
					return false;
					break;
				}
			}

			// Si no hubo ningun error devolvemos la lista de archivos generados
			return array("sizes"=>$tm, "files"=>$files);
		}
	}
}
?>