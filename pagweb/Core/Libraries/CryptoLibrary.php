<?php

namespace PC\Libraries {

	class CryptoLibrary {

		// Variables para acelerar el proceso de encriptación
		protected static $rb_e;
		protected static $orsb_e;
		protected static $oe_e;
		protected static $od_e;

		// This is a static class, instances are disabled.
		final private function __construct() {}
		final private function __clone() {}
		
		protected static function random_pseudo_bytes($length) {

			if (!isset(self::$rb_e)) { self::$rb_e = function_exists('random_bytes'); }
			if (!isset(self::$orsb_e)) { self::$orsb_e = function_exists('openssl_random_pseudo_bytes'); }

			if (self::$rb_e) {
				return random_bytes($length);
			} else if (self::$orsb_e) {
				return openssl_random_pseudo_bytes($length);
			} else {
				$rnd = '';
				for ($i = 0; $i < $length; $i++) {
					$sha = hash('sha256', mt_rand());
					$char = mt_rand(0, 30);
					$rnd .= chr(hexdec($sha[$char].$sha[$char + 1]));
				}
				return $rnd;
			}
		}
		
		protected static function pkcs7_pad($string) {

			$blocksize = 16;
			// 128 bits
			$blocksize = mcrypt_get_block_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_CBC);
			$pad = $blocksize - (strlen($string) % $blocksize);
			return $string.str_repeat(chr($pad), $pad);
		}
		
		 protected static function remove_pkcs7_pad($string) {

			$blocksize = 16;
			// 128 bits
			$blocksize = mcrypt_get_block_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_CBC);
			$len = strlen($string);
			$pad = ord($string[$len - 1]);
			if ($pad > 0 && $pad <= $blocksize) {
				$valid_pad = true;
				for ($i = 1; $i <= $pad; $i++) {
					if (ord($string[$len - $i]) != $pad) {
						$valid_pad = false;
						break;
					}
				}
				if ($valid_pad) {
					$string = substr($string, 0, $len - $pad);
				}
			}
			return $string;
		}
		
		protected static function aes_256_cbc_encrypt($string, $key, $iv) {

			// Si la versión de PHP no es mayor a 5.3.3 tendrémos problemas con el vector
			if (!isset(self::$oe_e)) { self::$oe_e = function_exists('openssl_encrypt') && version_compare(PHP_VERSION, '5.3.3', '>='); }

			if (self::$oe_e) {
				return openssl_encrypt($string, 'aes-256-cbc', $key, true, $iv);
			} else {
				$cipher = mcrypt_module_open(MCRYPT_RIJNDAEL_128, '', MCRYPT_MODE_CBC, '');
				if (mcrypt_generic_init($cipher, $key, $iv) != -1) {
					$encrypted = mcrypt_generic($cipher, self::pkcs7_pad($string));
					mcrypt_generic_deinit($cipher);
					mcrypt_module_close($cipher);
					return $encrypted;
				}
			}

			return false;
		}

		protected static function aes_256_cbc_decrypt($crypted, $key, $iv) {

			if (!isset(self::$od_e)) { self::$od_e = function_exists('openssl_decrypt') && version_compare(PHP_VERSION, '5.3.3', '>='); }

			if (self::$od_e) {
				return openssl_decrypt($crypted, 'aes-256-cbc', $key, true, $iv);
			} else {
				$cipher = mcrypt_module_open(MCRYPT_RIJNDAEL_128, '', MCRYPT_MODE_CBC, '');

				if (mcrypt_generic_init($cipher, $key, $iv) != -1) {
					$decrypted = mdecrypt_generic($cipher, $crypted);
					mcrypt_generic_deinit($cipher);
					mcrypt_module_close($cipher);
					return self::remove_pkcs7_pad($decrypted);
				}
			}

			return false;
		}

		private static function generateSalt($pass, $length) {
			// Set a random salt.
			$salt = self::random_pseudo_bytes(8);

			$salted = '';
			$dx = '';

			// Salt the key(32) and iv(16) = 48
			$keyLength = 32;
			$lengthLimit = ($keyLength + $length);
			while (strlen($salted) < $lengthLimit) {
				$dx = md5($dx.$pass.$salt, true);
				$salted .= $dx;
			}

			$key = substr($salted, 0, 32);
			$iv = substr($salted, 32, $length);
			return array("salt"=>$salt, "key"=>$key, "iv"=>$iv);
		}

		private static function parseSalt($string, $pass, $length) {
			$data = base64_decode($string);
			$salt = substr($data, 8, 8);
			$content = substr($data, 16);

			$rounds = 3;
			$data00 = $pass.$salt;
			$md5_hash = array();
			$md5_hash[0] = md5($data00, true);
			$result = $md5_hash[0];
			for ($i = 1; $i < $rounds; $i++) {
				$md5_hash[$i] = md5($md5_hash[$i-1].$data00, true);
				$result .= $md5_hash[$i];
			}
			$key = substr($result, 0, 32);
			$iv = substr($result, 32, $length);

			return array("key"=>$key, "iv"=>$iv, "content"=>$content);
		}

		public static function encrypt_aes($string, $pass) {
			$salt = self::generateSalt($pass, 16);
			return base64_encode('Salted__' . $salt["salt"] . self::aes_256_cbc_encrypt($string, $salt["key"], $salt["iv"]));
		}

		public static function decrypt_aes($string, $pass) {
			$salt = self::parseSalt($string, $pass, 16);
			return self::aes_256_cbc_decrypt($salt["content"], $salt["key"], $salt["iv"]);
		}

		public static function encrypt_blowfish($string, $pass) {
			$salt = self::generateSalt($pass, 8);
			return base64_encode('Salted__' . $salt["salt"] . mcrypt_encrypt(MCRYPT_BLOWFISH, $salt["key"], $string, MCRYPT_MODE_CBC, $salt["iv"]));
		}

		public static function decrypt_blowfish($string, $pass) {
			$salt = self::parseSalt($string, $pass, 8);
			return mcrypt_decrypt(MCRYPT_BLOWFISH, $salt["key"], $salt["content"], MCRYPT_MODE_CBC, $salt["iv"]);
		}

		public static function hash_blowfish_2y($pass) {
			return password_hash($pass, PASSWORD_BCRYPT, ['cost'=>10, 'salt'=>mcrypt_create_iv(44, MCRYPT_DEV_URANDOM)]);
		}

		public static function hash_blowfish_2a($string, $rounds=10) {
			$chars = './ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
			$salt = sprintf('$2a$%02d$', $rounds);
			for($i=0; $i<22; $i++) { $salt .= $chars[mt_rand(0,63)]; }
			return crypt($string, $salt);
		}
		
		
	}

}

?>