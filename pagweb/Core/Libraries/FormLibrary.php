<?php

namespace PC\Libraries {

	class FormLibrary {
		
		function ake($key, $config, $value) {
			return !array_key_exists($key, $config) ? $value : $config[$key];
		}
		
		function base($config) {
			/* Options parameters [index, value, readonly, fieldclass, disabled] */
			$_id = UI::ake('id', $config, '');
			$name = UI::ake('name', $config, '');
			$title = UI::ake('title', $config, '');
			$multiple = UI::ake('multiple', $config, '');
			$multiple = !empty($multiple) ? '[]' : '';
			$customdata = UI::ake('customdata', $config, array());
			$index = UI::ake('index', $config, '');
			$value = UI::ake('value', $config, false);
			$isinput = UI::ake('isinput', $config, false);
			$checked = UI::ake('checked', $config, false);
			$fieldclass = UI::ake('fieldClass', $config, '');
			$readonly = UI::ake('readonly', $config, '');
			$disabled = UI::ake('disabled', $config, '');
			$cols = UI::ake('cols', $config, '');
			$rows = UI::ake('rows', $config, '');
			
			$tab = ''; $tabs = UI::ake('tabs', $config, 0);
			for ($i=0; $i<$tabs; $i++) { $tab.= "\t"; }
			
			$id = !empty($_id) ? ' id="'.$_id.$index.'"' : '';
			$name = empty($name) ? ' name="'.$_id.$index.$multiple.'"' : ' name="'.$name.'"';
			$title = !empty($title) ? ' title="'.$title.'"' : '';
			$value = ($value !== false && $isinput == true) ? ' value="'.$value.'"' : '';
			$checked = ($checked == "on" || $checked == 1 || $checked == "1" || $checked == "true" || $checked == true) ? ' checked="checked"': '';
			$fieldclass = !empty($fieldclass) ? ' class="'.$fieldclass.'"' : '';
			$readonly = !empty($readonly) ? ' readonly="readonly"' : '';
			$disabled = !empty($disabled) ? ' disabled="disabled"' : '';
			$cols = !empty($cols) ? ' cols="'.$cols.'"' : '';
			$rows = !empty($rows) ? ' rows="'.$rows.'"' : '';
			$multiple = !empty($multiple) ? ' multiple="multiple"' : '';
			
			$cd = '';
			foreach ($customdata as $k=>$v) { $cd.= ' data-'.$k.'="'.$v.'"'; }
			
			return array("common"=>$id.$name.$title.$value.$multiple.$checked.$fieldclass.$readonly.$disabled.$cols.$rows.$cd, "id"=>$_id.$index, "tab"=>$tab);
		}
			
		function buildLabel($params, $base) {
			$id = $params["id"];
			$element = $params["element"];
			$label = UI::ake('label', $params, false);
			$labelposition = UI::ake('labelPosition', $params, 'before');
			$labelclass = UI::ake('labelClass', $params, false);
			$labelclass = $labelclass ? ' class="'.$labelclass.'"' : '';
			
			/* Labels */
			$labelelement = '<label for="'.$base["id"].'"'.$labelclass.'>'.$label.'</label>';
				
			if (empty($label)) {
				return $element;
			} else {
				switch ($labelposition) {
					case 'before' : $build = $labelelement."\n".$element; break;
					case 'after' : $build = $element."\n".$base["tab"].$labelelement; break;
				}
				return $build;
			}
		}

		function buildSelect($config) {
			/* Options parameters [id, index, value, fieldclass, readonly, disabled, label, multiple, data, pairs, defaultSelected] */
			$config["isinput"] = false;
			$config["type"] = "select";
			$base = UI::base($config);
			$value = UI::ake('value', $config, false);
			$pairs = UI::ake('pairs', $config, false);
			$defaultSelected = UI::ake('defaultSelected', $config, false);
			$selected = ' selected="selected"';
			
			// Start building the element
			$element = $base["tab"].'<select'.$base['common'].'>'."\n";
			
			/* Only add option tags if the data provider is not empty */
			if (!empty($config["data"])) {			
				// If the value is an array it's possible that the field contains multipled selected items
				if (is_array($value)) {
					foreach ($config["data"] as $k=>$v) {
						$s = '';
						$ov = $pairs ? $k : $v;
						if (in_array($ov, $value)) { $s = $selected; }
						$element.= $base["tab"].'	<option'.$s.' value="'.$ov.'">'.$v.'</option>'."\n";
					}
				// Verify one value in the config["data"] provider
				} else {
					$i=0;
					foreach ($config["data"] as $k=>$v) {
						$s = '';
						$ov = $pairs ? $k : $v;
						
						// Actualización para poder incluir datos de HTML5 en las etiquetas OPTION del elemento SELECT
						$data = '';
						if (is_array($v)) {
							foreach ($v as $kk=>$vv) {
								if (stripos($kk, "data-") !== false) {
									$data.= ' '.$kk.'="'.$vv.'"';
								} else if ($kk == "label") {
									$ov = $k;
									$v = $vv;
								}
							}
						} else {
							$ov = ($v == '::default::') ? $v : $ov;
						}
						// FIN DE LA ACTUALIZACIÓN
						
						if (((string)$ov == (string)$value) || ($ov === "::default::") || ($i === 0 && $defaultSelected === true)) { $s = $selected; }
						$element.= $base["tab"].'	<option'.$s.' value="'.$ov.'"'.$data.'>'.$v.'</option>'."\n";
						$i++;
					}
				}
			}
			$element.= $base["tab"].'</select>';
			$config["element"] = $element;
			return UI::buildLabel($config, $base);
		}
		
		function buildTextarea($config) {
			$config["isinput"] = false;
			$config["type"] = "textarea";
			$base = UI::base($config);
			$value = UI::ake('value', $config, '');
			
			$config["element"] = $base["tab"].'<textarea'.$base['common'].'>'.$value.'</textarea>';
			return UI::buildLabel($config, $base);
		}
		
		function buildInput($config) {
			$config["isinput"] = true;
			$base = UI::base($config);
			
			$config["element"] = $base["tab"].'<input'.$base['common'].' type="'.$config["type"].'" />';
			return UI::buildLabel($config, $base);
		}
		
		function buildInputFile($config) {
			$config["isinput"] = true;
			$config["type"] = "file";
			$base = UI::base($config);		
			
			$config["element"] = $base["tab"].'<input'.$base['common'].' type="file" />';
			return UI::buildLabel($config, $base);
		}
		
		function buildInputText($config) {
			$config["isinput"] = true;
			$config["type"] = "text";
			$base = UI::base($config);
			
			$config["element"] = $base["tab"].'<input'.$base['common'].' type="text" />';
			return UI::buildLabel($config, $base);
		}
		
		function buildInputCheckbox($config) {
			$config["isinput"] = true;
			$config["type"] = "checkbox";
			$base = UI::base($config);
			
			$config["labelPosition"] = UI::ake('labelPosition', $config, 'after');
			$config["element"] = $base["tab"].'<input'.$base['common'].' type="checkbox" />';
			return UI::buildLabel($config, $base);
		}
		
		function buildInputRadio($config) {
			$config["isinput"] = true;
			$config["type"] = "radio";
			$base = UI::base($config);
			
			$config["labelPosition"] = UI::ake('labelPosition', $config, 'after');
			$config["element"] = $base["tab"].'<input'.$base['common'].' type="radio" />';
			return UI::buildLabel($config, $base);
		}
		
		function buildInputPassword($config) {
			$config["isinput"] = true;
			$config["type"] = "password";
			$base = UI::base($config);
			
			$config["element"] = $base["tab"].'<input'.$base['common'].' type="password" />';
			return UI::buildLabel($config, $base);
		}
		
		function buildInputHidden($config) {
			$config["isinput"] = true;
			$config["type"] = "hidden";
			$base = UI::base($config);
			
			$config["element"] = $base["tab"].'<input'.$base['common'].' type="hidden" />';
			return UI::buildLabel($config, $base);
		}

	}

}

?>