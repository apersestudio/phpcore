<?php

namespace PC\Libraries {

	class QuantitiesLibrary {

		// Novecientos Setenta y Ocho Millones Seiscientos Cincuenta y Cuatro Mil Trescientos Veintiun Pesos Con Veintiun Centavos
		// 978,654,321.21
		
		private static $HUNDREDS_ARR = array("ciento","doscientos","trescientos","cuatrocientos","quinientos","seiscientos","setecientos","ochocientos","novecientos");
		private static $TENS_ARR = array("diez","veinte","treinta","cuarenta","cincuenta","sesenta","setenta","ochenta","noventa");
		private static $UNITS_ARR = array("un","dos","tres","cuatro","cinco","seis","siete","ocho","nueve");
		private static $TENS_MORE = array("once","doce","trece","catorce","quince","dieciseis","diecisiete","dieciocho","diecinueve");
		private static $TWENTIES_MORE = array("veintiun","veintidos","veintitres","veinticuatro","veinticinco","veintiseis","veintisiete","veintiocho","veintinueve");
		private static $JOINTS_ARR = array("y", "millon","billon","trillon");
		private static $LEVELS_ARR = array("","mil","millones","billones","trillones");
		private static $CURRENCIES = array("gb"=>"GBP","us"=>"USD","mx"=>"MXP","au"=>"AUD","ar"=>"ARP","es"=>"EUR","it"=>"EUR","br"=>"BRL","jp"=>"JPY");
		
		public static function makeLevel($n, $level) {
			if ($n == "00") { return "cero"; }

			$units = $n % 10;
			$tens = ($n % 100) - $units;
			$hundreds = ($n % 1000) - $tens - $units;
			
			$tens /= 10;
			$hundreds /= 100;

			$q = '';
			if ($hundreds>0) { $q .= " ".self::$HUNDREDS_ARR[$hundreds-1]; }
			if ($tens>0) { $q .= " ".self::$TENS_ARR[$tens-1]; }
			if ($tens>0 && $units>0) { $q .= " ".self::$JOINTS_ARR[0]; }
			if ($units>0) { $q .= " ".self::$UNITS_ARR[$units-1]; }
			if ($n == 1 && $level>2) { $q .= " ".self::$JOINTS_ARR[$level-2]; } else if ($level>0) { $q .= " ".self::$LEVELS_ARR[$level-1]; }
			return $q;

		}
		
		public static function makeReplacements($n) {
			$maxReplacement = count(self::$UNITS_ARR);
			for ($i=0; $i<$maxReplacement; $i++) {
				$n = preg_replace("/diez y ".self::$UNITS_ARR[$i]."/", self::$TENS_MORE[$i], $n);
				$n = preg_replace("/veinte y ".self::$UNITS_ARR[$i]."/", self::$TWENTIES_MORE[$i], $n);
			}
			return $n;
		}
		
		public static function NumbersToLetters($Quantity, $CurrencyTitle='', $FractionTitle='', $Precision=2, $CurrencyCode="mx") {
			
			$Decimals = is_float(floatval($Quantity)) ? substr(str_pad(substr(strstr($Quantity, '.'), 1), $Precision, STR_PAD_LEFT), 0, 2) : 0;
			$Blocks = explode(",", number_format(intval($Quantity)));
			
			$Integer = '';
			$FractionSimple = !empty($Decimals) ? " con ".self::makeLevel($Decimals, 1)." ".$FractionTitle : '';
			$FractionStandard = !empty($Decimals) ? " ".str_pad($Decimals, 2, "0", STR_PAD_LEFT)."/100" : '';
			
			$maxLevel = count($Blocks);
			for ($level=0; $level<$maxLevel; $level++) {
				$Integer .= self::makeLevel($Blocks[$level], $maxLevel-$level);
			}
			
			$Integer = self::makeReplacements($Integer);			
			$FractionSimple = self::makeReplacements($FractionSimple);			
			$FractionStandard = self::makeReplacements($FractionStandard);
			
			/* Remove spaces */
			$Integer = trim(preg_replace("/\s{2,}/", " ", $Integer));
			$FullQuantitySimple = trim(preg_replace("/\s{2,}/", " ", $Integer." ".$CurrencyTitle.$FractionSimple));
			$FullQuantityStandard = trim(preg_replace("/\s{2,}/", " ", $Integer." ".$CurrencyTitle.$FractionStandard));
			$Formatted = self::$CURRENCIES[$CurrencyCode]." ".number_format($Quantity, $Precision, ".", ",");
			
			/* Format the output */
			return array("Formatted"=>$Formatted, "Simple"=>ucwords(utf8_decode($FullQuantitySimple)), "Standard"=>ucwords(utf8_decode($FullQuantityStandard)));
			
		}
	}
	
}
?>