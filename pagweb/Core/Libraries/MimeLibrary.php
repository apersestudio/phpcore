<?php

namespace PC\Libraries {

	use PC\Libraries\FilesLibrary;

	class MimeLibrary {

		const MIMETYPES = array(
			"ai" => "application/postscript",
			"psb" => "application/x-photoshop",
			"psd" => "application/photoshop",
			"doc" => "application/msword",
			"docx"=> "application/vnd.openxmlformats-officedocument.wordprocessingml.document",
			"eps" => "application/postscript",
			"cdr" => "application/coreldraw",

			"gtar" => "application/x-gtar",
			"gz" => "application/x-gzip",
			"hqx" => "application/mac-binhex40",
			"js" => "application/x-javascript",
			"pdf" => "application/pdf",
			"pps" => "application/mspowerpoint",
			"ppt" => "application/mspowerpoint",
			"ps" => "application/postscript",
			"sit" => "application/x-stuffit",
			"swf" => "application/x-shockwave-flash",
			"tar" => "application/x-tar",
			"xlc" => "application/vnd.ms-excel",
			"xls" => "application/vnd.ms-excel",
			"zip" => "application/zip",
			"7z" => "application/x-7z-compressed",
			"rar" => "application/x-rar-compressed",

			"aac" => "audio/x-aac",
			"m4a" => "audio/mp4",
			"aif" => "audio/x-aiff",
			"aiff" => "audio/x-aiff",
			"au" => "audio/basic",
			"mid" => "audio/midi",
			"midi" => "audio/midi",
			"mp2" => "audio/mpeg",
			"mp3" => "audio/mpeg",
			"mpga" => "audio/mpeg",
			"ra" => "audio/x-realaudio",
			"ram" => "audio/x-pn-realaudio",
			"rm" => "audio/x-pn-realaudio",
			"snd" => "audio/basic",
			"wav" => "audio/x-wav",
			"pls" => "audio/x-scpls",
			"oga" => "audio/ogg",
			"ogg" => "audio/ogg",
			"mka" => "audio/x-matroska",

			"c" => "text/plain",
			"cc" => "text/plain",
			"css" => "text/css",
			"h" => "text/plain",
			"hh" => "text/plain",
			"htm" => "text/html",
			"html" => "text/html",
			"m" => "text/plain",
			"rtf" => "text/rtf",
			"rtx" => "text/richtext",
			"sgm" => "text/sgml",
			"sgml" => "text/sgml",
			"txt" => "text/plain",
			"xml" => "text/xml",
			"xsl" => "text/xml",

			"avi" => "video/x-msvideo",
			"flv" => "video/x-flv",
			"mov" => "video/quicktime",
			"mpe" => "video/mpeg",
			"mpeg" => "video/mpeg",
			"mpg" => "video/mpeg",
			"mp4" => "video/mp4",
			"wmv" => "video/x-ms-wmv",
			"m4u" => "video/x-mpegur",
			"ogv" => "video/ogg",
			"mkv" => "video/x-matroska",

			"bmp" => "image/bmp",
			"gif" => "image/gif",
			"jpg" => "image/jpeg",
			"jpeg" => "image/jpeg",
			"png" => "image/png",
			"tif" => "image/tiff",
			"tiff" => "image/tiff",

			"ttf" => "application/x-font-ttf",
			"pfa" => "application/x-font-type1",
			"pfb" => "application/x-font-type1",
			"otf" => "application/x-font",
			"eot" => "application/vnd.ms-fontobject",

			"mb" => "application/octet-stream",
			"exe" => "application/octet-stream"
		);
		
		public static function getExtension($mime) {
			return array_search($mime, self::MIMETYPES);
		}

		public static function get($file) {
			$ext = pathinfo($file, PATHINFO_EXTENSION);
			return self::getByExtension($ext);
		}
		
		public static function getByExtension($ext) {
			if (array_key_exists($ext, self::MIMETYPES)) { return self::MIMETYPES[$ext]; }
			return false;
		}

		public static function guess($file, $default=false) {
			$mime = FilesLibrary::getMime($file);
			return !$mime ? self::get($file) : $default;
		}
		
	}

}

?>