<?php

namespace PC\Libraries {

	class HeaderLibrary {
	
		public static function redirect($url) {
			header ("Location: ".$url);
		}
		public static function nocache() {
			header('Cache-Control: no-cache, must-revalidate');
			header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
		}
		public static function content($f='plain') {
			$charset = "; charset=utf-8;";
			$ct = 'Content-type: ';

			// Caso especial donde internet explorer no sabe como tratar con json
			$ua = strtolower(USER_AGENT);
			if($f == "json" && preg_match('/(msie|trident)/', $ua)) { $f = "plain"; }
			
			switch ($f) {
				case 'json' : header($ct.'application/'.$f.$charset); break;
				case 'plain' : case 'html' : case 'xml' : header($ct.'text/'.$f.$charset);
			}
		}
		public static function number($number=200) {
			$p = $_SERVER["SERVER_PROTOCOL"];
			switch ($number) {
				case 100 : $message = 'Continue'; break; // 10.1.1
				case 101 : $message = 'Switching Protocols'; break; // 10.1.2

				/* 10.2 Successful 2xx */
				case 200 : $message = 'OK'; break; // 10.2.1
				case 201 : $message = 'Created'; break; // 10.2.2
				case 202 : $message = 'Accepted'; break; // 10.2.3
				case 203 : $message = 'Non-Authoritative Information'; break; // 10.2.4
				case 204 : $message = 'No Content'; break; // 10.2.5
				case 205 : $message = 'Reset Content'; break; // 10.2.6
				case 206 : $message = 'Partial Content'; break; // 10.2.7

				/* 10.3 Redirection 3xx */
				case 300 : $message = 'Multiple Choices'; break; // 10.3.1
				case 301 : $message = 'Moved Permanently'; break; // 10.3.2
				case 302 : $message = 'Found'; break; // 10.3.3
				case 303 : $message = 'See Other'; break; // 10.3.4
				case 304 : $message = 'Not Modified'; break; // 10.3.5
				case 305 : $message = 'Use Proxy'; break; // 10.3.6
				case 306 : $message = 'Unused'; break; // 10.3.7
				case 307 : $message = 'Temporary Redirect'; break; // 10.3.8

				/* 10.4 Client Error 4xx */
				case 400 : $message = 'Bad Request'; break; // 10.4.1
				case 401 : $message = 'Unauthorized'; break; // 10.4.2
				case 402 : $message = 'Payment Required'; break; // 10.4.3
				case 403 : $message = 'Forbidden'; break; // 10.4.4
				case 404 : $message = 'Not Found'; break; // 10.4.5
				case 405 : $message = 'Method Not Allowed'; break; // 10.4.6
				case 406 : $message = 'Not Acceptable'; break; // 10.4.7
				case 407 : $message = 'Proxy Authentication Required'; break; // 10.4.8
				case 408 : $message = 'Request Timeout'; break; // 10.4.9
				case 409 : $message = 'Conflict'; break; // 10.4.10
				case 410 : $message = 'Gone'; break; // 10.4.11
				case 411 : $message = 'Length Required'; break; // 10.4.12
				case 412 : $message = 'Precondition Failed'; break; // 10.4.13
				case 413 : $message = 'Request Entity Too Large'; break; // 10.4.14
				case 414 : $message = 'Request-URI Too Long'; break; // 10.4.15
				case 415 : $message = 'Unsupported Media Type'; break; // 10.4.16
				case 416 : $message = 'Requested Range Not Satisfiable'; break; // 10.4.17
				case 417 : $message = 'Expectation Failed'; break; // 10.4.18

				/* 10.5 Server Error 5xx */
				case 500 : $message = 'Internal Server Error'; break; // 10.5.1
				case 501 : $message = 'Not Implemented'; break; // 10.5.2
				case 502 : $message = 'Bad Gateway'; break; // 10.5.3
				case 503 : $message = 'Service Unavailable'; break; // 10.5.4
				case 504 : $message = 'Gateway Timeout'; break; // 10.5.5
				case 505 : $message = 'HTTP Version Not Supported'; break; // 10.5.6		
			}
			$header = $p.' '.$number.' '.$message;
			header($header);
		}
		
	}

}

?>