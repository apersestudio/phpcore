<?php

namespace PC\Libraries {

	class XMLLibrary {

		private static function structXml($array) {
			$xml = '';
			foreach ($array as $k => $v) {
				$tag = preg_replace('/^[0-9]{1,}/', 'item', $k);
				if (is_array($v)) {
					$xml.= "<$tag>".self::structXml($v)."</$tag>";
				} else {
					$xml.= "<$tag><![CDATA[$v]]></$tag>";
				}
			}
			return $xml;
		}
		
		private static function readNode($node, &$item) {
			$namespaces = $node->getNameSpaces(true);
			$name = $node->getName();
			$item[$name] = array();
		   
			// Analizamos los attibutos del nodo
			if (count($node->attributes()) > 0) {
				$item[$name]["attr"] = array();
				foreach ($node->attributes() as $k=>$v) {
					$item[$name]["attr"][$k] = (string)$v;
				}
			}
			// Buscamos contenido CDATA en el nodo
			$content = trim($node);
			if (strlen($content) > 0) {
				$item[$name]["content"] = (string)$content;
			}
			
			foreach ($namespaces as $key=>$ns) {
				foreach ($node->children($ns) as $child) {
					self::readNode($child, $item[$name]);
				}
			}
		}
		
		private static function displayXmlError($err) {
			$error = "";
			switch ($err->level) {
				case LIBXML_ERR_WARNING: $error .= "Warning"; break;
				case LIBXML_ERR_ERROR: $error .= "Error"; break;
				case LIBXML_ERR_FATAL: $error .= "Fatal Error"; break;
			}
			$error .= ":".$err->code.", Line:$err->line".", Column:$err->column [".trim($err->message)."]";
			if ($err->file) { $error .= "\tFile: $err->file"; }
			return $error;		
		}
		
		public static function fromArray($array) {
			$root_name = '';
			$xml = (is_array($array) && count($array) > 0) ? self::structXml($array) : "";
			return '<?xml version="1.0" encoding="utf-8"?><response>'.$xml.'</response>';
		}
		
		public static function loadString($xml) {
			// Activamos el registro de errores XML
			libxml_use_internal_errors(true);
			// Intentamos cargar la cadena XML
			$xml = simplexml_load_string($xml, null, LIBXML_NOCDATA);
			if (!$xml) {
				$strError = "";
				$errors = libxml_get_errors();
				foreach($errors as $error) { $strError .= self::displayXmlError($error)."\r\n"; }
				// Liberamos de la memoria los errores anotados
				libxml_clear_errors();
				// Liberamos de la memoria el contenido del archivo XML y la variables de errores
				unset($errors, $xml);
				// Establecemos la variable de exito en falso y enviamos los errores encontrados
				return array("success"=>false, "error"=>$strError);
			} else {
				// Detectamos la codificación y si es distinta de UTF8, la cambiamos a UTF-8
				$encodes = array("UTF-8","ISO-8859-1","ASCII","windows-1252");
				$encoding = mb_detect_encoding($xml, $encodes);
				if ($encoding != "UTF-8") { $xml = @mb_convert_encoding($xml, "UTF-8", $encoding); }
				// Regresamos una variable de exito y el contenido del archivo XML
				return array("success"=>true, "xml"=>$xml);
			}
		}
		
		public static function parse($path) {
			// Si la variable path contiene información XML en lugar de una ruta de archivo
			if (stripos($path, "<?xml") !== false) {
				$xmlstr = $path;
			// Si path es una ruta valida y es leible leemos el contenido del archivo XML
			} else if (file_exists($path) && is_readable($path)) {
				$xmlstr = file_get_contents($path);
			// Hubo algún tipo de error al leer el archivo
			} else {
				return false;
			}
			
			// Intentamos leer el contenido XML
			$sxl = self::loadString($xmlstr);
			if ($sxl["success"]) {
				// Contenedor del archivo
				$arr = array();
				// Comenzamos el parseo del archivo
				self::readNode($sxl["xml"], $arr);
				// Liberamos la memoria del contenido del XML
				unset($sxl, $xmlstr);
				// Regresamos un array con el contenido parseado
				return array("success"=>true, "data"=>$arr);
			} else {
				// Liberamos la memoria del contenido del XML
				unset($xmlstr);
				return $sxl;
			}
		}
	}

}

?>