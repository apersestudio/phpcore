<?php
namespace PC\Libraries {

	class ServerLibrary {
		
		public static function get($props, $def=null) {
			$props = explode(",", $props);
			foreach ($props as $prop) {
				if (isset($_SERVER[$prop])) {
					$def = $_SERVER[$prop];
					break;
				}
			}
			return $def;
		}
		
	}

}
?>