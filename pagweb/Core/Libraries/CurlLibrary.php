<?php

namespace PC\Libraries {

	class CurlLibrary {

		public static function getFile($url) {
			$ch = curl_init();

			curl_setopt($ch, CURLOPT_HEADER, 0);
			//Set curl to return the data instead of printing it to the browser.
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_URL, $url);

			$data = curl_exec($ch);
			curl_close($ch);

			return $data;
		}

		public function ApiCall($resource, $params=array()) {
			// Crear un nuevo recurso cURL
			$ch = curl_init();
			
			// Establecer URL y otras opciones apropiadas
			curl_setopt($ch, CURLOPT_URL, $resource);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($ch, CURLOPT_POST, true);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
			
			// Capturar la URL y pasarla al navegador
			ob_start();
			curl_exec($ch);
			$response = ob_get_contents();
			ob_end_clean();
			
			// JSON
			return json_decode((string)$response);
		}
	}
}
?>