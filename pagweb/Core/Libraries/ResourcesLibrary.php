<?php

namespace PC\Libraries {

	use PC\Libraries\FilesLibrary;

	class ResourcesLibrary {

		public static function fragments($arr=false) {
			if (is_array($arr)) {
				$scriptHTML = '';
				foreach ($arr as $script) {
					$s = $script["src"];
					$t = $script["type"];
					if (file_exists($s)) {
						$scriptHTML.= '<script type="'.$t.'">'.file_get_contents($s).'</script>';
					}
				}
				return $scriptHTML;
			}
			return '';
		}

		public static function links($arr=false) {
			$linksHTML = '';
			if (is_array($arr)) {
				foreach ($arr as $link) { $linksHTML.= FilesLibrary::insertLink($link["href"], $link["rel"])."\n\t\t"; }
			}
			return $linksHTML;
		}
		
		public static function scripts($arr=false, $defered=true) {
			if (is_array($arr)) {
				if ($defered) {
					$a = array();
					foreach ($arr as $script) {
						$s = $script["src"];
						$t = $script["type"];
						$a[] = FilesLibrary::isRemote($s) ? FilesLibrary::insertScriptRemoteDefer($s, $t) : FilesLibrary::insertScriptDefer($s, $t);
					}
					return array_filter($a);
				} else {
					$scriptHTML = '';
					foreach ($arr as $script) {
						$s = $script["src"];
						$t = $script["type"];
						$scriptHTML.= FilesLibrary::isRemote($s) ? FilesLibrary::insertScriptRemote($s) : FilesLibrary::insertScript($s)."\n\t\t";
					}
					return $scriptHTML;
				}
			}
			return '';
		}

		public static function styles($arr=false, $defered=true) {
			if (is_array($arr)) {
				if ($defered) {
					$a = array();
					foreach ($arr as $style) {
						$h = $style["href"];
						$m = $style["media"];
						$a[] = FilesLibrary::isRemote($h) ? FilesLibrary::insertStyleRemoteDefer($h, $m) : FilesLibrary::insertStyleDefer($h, $m);
					}
					return array_filter($a);
				} else {
					$styleHTML = '';
					foreach ($arr as $style) {
						$h = $style["href"];
						$m = $style["media"];
						$styleHTML.= FilesLibrary::isRemote($h) ? FilesLibrary::insertStyleRemote($h, $m) : FilesLibrary::insertStyle($h, $m)."\n\t\t";
					}
					return $styleHTML;
				}
			}
			return '';
		}
		
	}

}
?>