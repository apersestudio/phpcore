<?php

namespace PC\Libraries {

	use PC\Libraries\ResourcesLibrary;
	use PC\Libraries\FilesLibrary;
	
	class HtmlLibrary {

		public $public=true;
		public $title=false;
		public $keywords=false;
		public $description=false;
		public $author=false;
		public $styles=false;
		public $scripts=false;
		public $bodyData=false;
		public $favicon=false;
		public $fragments=false;

		private $html=false;
		private $links=false;
		private $lang=false;
		private $browser=false;
		private $version=false;
		private $engine=false;
		private $type=false;

		private $IDSeguimiento;

		public function __construct() {
			$this->styles=[];
			$this->scripts=[];
		}

		public function setFavicon($favicon) {
			$favicon = FilesLibrary::isRemote($favicon) ? $favicon : FilesLibrary::absoluteToURL($favicon);
			$this->favicon = $favicon;
		}

		public function setIDSeguimiento($id) {
			$this->IDSeguimiento = $id;
		}
		public function setLang($lang) {
			$this->lang = $lang;
		}
		public function setPublic($public) {
			$this->public = $public;
		}
		public function setTitle($title) {
			$this->title = $title;
		}
		public function setFragments($fragments) {
			$this->fragments = $fragments;
		}
		public function setKeywords($keywords) {
			$this->keywords = $keywords;
		}
		public function setDescription($description) {
			$this->description = $description;
		}
		public function setAuthor($author) {
			$this->author = $author;
		}
		public function setLinks($links) {
			$this->links = $links;
		}
		public function addStyles(array $styles) {
			$this->styles = array_merge($this->styles, $styles);
		}
		public function addStyle(array $style) {
			$this->styles[] = $style;
		}
		public function addScripts(array $scripts) {
			$this->scripts = array_merge($this->scripts, $scripts);
		}
		public function addScript(array $script) {
			$this->scripts[] = $script;
		}
		public function setBodyData($data){
			$this->bodyData = $data;
		}

		public function top() {
			/* HTML HEADER */
			$this->html = '';
			$this->html.= '<!DOCTYPE html>'."\n";

			// Mobile
			if (MOBILE) {
				$this->type = 'mobile';
			} else {
				$this->type = 'desktop';
				// Browser and Version
				if (preg_match('/(opr|opera)\/([0-9.]{1,})/i', USER_AGENT, $pm)) {
					$this->browser = 'opera';
					$this->version = intval($pm[2]);
				} else if (preg_match('/(firefox|chrome)\/([0-9.]{1,})/i', USER_AGENT, $pm)) {
					$this->browser = strtolower($pm[1]);
					$this->version = intval($pm[2]);
				} else if (preg_match('/safari/i', USER_AGENT)) {
					preg_match('/version\/([0-9.])/i', USER_AGENT, $pm);
					$this->browser = 'safari';
					$n = isset($pm[1]) ? $pm[1] : isset($pm[0]) ? $pm[0] : 0;
					$this->version = intval($n);
				} else if (preg_match('/msie ([0-9.]{1,})/i', USER_AGENT, $pm)) {
					$n = isset($pm[1]) ? $pm[1] : isset($pm[0]) ? $pm[0] : 0;
					$this->browser = 'ie';
					$this->version = intval($n);
				}
			}
			// Engine
			if (preg_match('/trident|webkit|gecko/i', USER_AGENT, $pm)) {
				$this->engine = strtolower($pm[0]);
			}

			$this->html.= '<html class="'.trim($this->browser." ".$this->browser.$this->version." ".$this->type." ".$this->engine).'"'.(!empty($this->lang)?' lang="'.$this->lang.'"':'').'>'."\n";
			$this->html.= '	<head>'."\n";
			if (!empty($this->title)) {  $this->html.= '		<title>'.$this->title.'</title>'."\n"; }
			
			$this->html.= '		<!-- WEBPAGE INFORMATION -->'."\n";
			$this->html.= '		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />'."\n";
			$this->html.= '		<meta charset="utf-8">'."\n";

			if ($this->public === false) {
				$this->html.= '		<meta name="robots" content="NOINDEX,NOFOLLOW,NOARCHIVE" />'."\n";
			} else {
				$this->html.= '		<meta name="robots" content="follow" />'."\n";
			}
			if (!empty($this->keywords)) { $this->html.= '		<meta name="keywords" content="'.$this->keywords.'" />'."\n"; }
			if (!empty($this->description)) { $this->html.= '		<meta name="description" content="'.$this->description.'" />'."\n"; }
			if (!empty($this->author)) { $this->html.= '		<meta name="author" content="'.$this->author.'" />'."\n"; }

			$this->html.= '		<meta name="rating" content="General" />'."\n";
			$this->html.= '		<base href="'.DOMAIN.'" />'."\n";
			$this->html.= "\n";

			$this->html.= '		<!-- WEBSITE ICON -->'."\n";
			if (!empty($this->favicon)) {
				$this->html.= '		<link rel="icon" href="'.$this->favicon.'" type="image/x-icon" />'."\n";
				$this->html.= '		<link rel="shortcut icon" href="'.$this->favicon.'" type="image/x-icon" />'."\n";
			}

			if (!empty($this->links)) { $this->html.= ResourcesLibrary::links($this->links); }

			$bodyData = '';
			if (!empty($this->bodyData)) {
				foreach ($this->bodyData as $k=>$v) {
					$bodyData.= ' data-'.$k.'="'.$v.'"';
				}
			}

			if ($this->browser == 'ie') {
				$this->html.= '<!--[if lt IE 9]>';
				$this->html.= '<script type="text/javascript" src="'.DOMAIN.'/cdn/libraries/html5shiv.min.js"></script>';
				$this->html.= '<script type="text/javascript" src="'.DOMAIN.'/cdn/libraries/html5shiv-printshiv.min.js"></script>';
				$this->html.= '<script type="text/javascript" src="'.DOMAIN.'/cdn/libraries/respond.min.js" /></script>';
				$this->html.= '<![endif]-->';
			}

			if (isset($this->IDSeguimiento)) {
				$this->html.= "<!-- Google Analytics -->";
				$this->html.= "<script>";
				$this->html.= "(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){";
				$this->html.= "(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),";
				$this->html.= "m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)";
				$this->html.= "})(window,document,'script','//www.google-analytics.com/analytics.js','ga');";
				$this->html.= "ga('create', '".$this->IDSeguimiento."', 'auto');";
				$this->html.= "ga('send', 'pageview');";
				$this->html.= "</script>";
				$this->html.= "<!-- End Google Analytics -->";
			}

			$this->html.= '	</head>'."\n";
			$this->html.= '	<body'.$bodyData.'>'."\n";
			echo $this->html;
		}
		
		public function bottom() {
			$this->html = '';

			if (!empty($this->fragments)) {
				$this->html.= ResourcesLibrary::fragments($this->fragments);
			}

			$a = array();
			if (!empty($this->styles)) { $a = array_merge($a, ResourcesLibrary::styles($this->styles)); }
			$styletList = array();
			foreach ($a as $s) { $styletList[] = json_encode($s); }

			$a = array();
			if (!empty($this->scripts)) { $a = array_merge($a, ResourcesLibrary::scripts($this->scripts)); }

			$scriptList = array();
			foreach ($a as $s) { $scriptList[] = json_encode($s); }

			$resources = array_filter(array_merge($styletList, $scriptList));
			
			$this->html.= '<script type="text/javascript">';

			$this->html.= '(function(w){"use strict";var e,r,s,i=0,d=document,h=d.getElementsByTagName("head")[0],resources=['.implode(', ',$resources).'],l=resources.length;function f(){function n(){if(i>=l){return false;}s=resources[i];if(s.media) {e=d.createElement("link");e.type=s.type;e.media=s.media;e.rel="stylesheet";e.href=s.src;}else{e=d.createElement("script");e.type=s.type;e.src=s.src;}h.appendChild(e);i=i+1;e.onload=e.onreadystatechange=function(){e.onload=e.onreadystatechange=null;n();};}n();}if(w.addEventListener){w.addEventListener("load",f,false);}else if(w.attachEvent){w.attachEvent("onload",f);}else{w.onload=f;}}(window));';

			$this->html.= '</script>';

			$this->html.= '	</body>'."\n";
			$this->html.= '</html>';
			echo $this->html;
		}

		public function headers() {
			header("Content-Type: text/html; charset=utf-8");
			header('X-UA-Compatible: IE=edge,chrome=1');

			if ($this->public === false) {
				header("Pragma: no-cache");
				header("Cache-Control: no-cache, must-revalidate");
				header("Expires: Sat, 26 Jul 1997 05:00:00 GMT");
			} else {
				header("Cache-Control: max-age=2592000");
			}
		}

		public function index($echo, $tabs=0, $newline=false) {
			$nl = "\n"; $tab = '';
			$lines = substr_count($echo, $nl)-1;
			for ($i=0; $i<$tabs; $i++) { $tab.= "\t"; }
			if ($newline) { $echo = $tab.$echo; }
			return preg_replace("/".$nl."(?!$)/", $nl.$tab, $echo);
		}
	}

}
?>