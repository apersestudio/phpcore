<?php

namespace PC\Libraries {

	class StringsLibrary {

		public static $expUTF8Hex = "=[a-fA-F0-9]{2}=[a-fA-F0-9]{2}";
		public static $expUTF8URLEncoded = "=[a-fA-F0-9]{2}";
		public static $expUTF8UnicodeHex = "(u[a-fA-F\d]{4}u[a-fA-F\d]{4})";
		public static $expSubjectBase64 = "=\?(.*)?(?:\?(\w)\?)(.*)?\?";
		
		public static $expAttachmentXIDStart = '[\w]_[\w\s]{9}';
		public static $expAttachmentXIDEnd = '--[\w\d]{1,30}--';
		public static $expEmlPart = '-+=_(?:next|part).*?.*?(?:\s|\")';
		public static $expEMLSectionEnd = '-+[_=]{1,2}[\w\d.-]+?(\r|$)';

		function URLHexToUTF8($str) {
			// Si la variable proviene de una busqueda de expresión regular, la cedena vendrá en un array
			if (is_array($str)) { $str = $str[0]; }
			// Las cadenas doblemente codificadas tienen signos de igualidad que deben de ser quitados
			$strclean = str_replace("=", "", $str);
			// Una vez que tenemos el código UTF8 hexadecimal hacemos la conversión a utf8 tradicional
			$utf8 = pack('H*', $strclean);
			return $utf8;
		}
		
		function URLDecode($str) {
			// Si la variable proviene de una busqueda de expresión regular, la cedena vendrá en un array
			if (is_array($str)) { $str = $str[0]; }
			// Si la codificación se realizó con signos de igualidad, los cambiamos por signos de porcentaje
			$char = str_replace("=", "%", $str);
			// Decodificamos el código de URL a ISO
			$decode = urldecode($char);
			return $decode;
		}

		function URLDecodeToUTF8($str) {
			// Decodificamos el código de URL a ISO
			$decode = self::URLDecode($str);
			// Transformamos el código ISO a UTF8
			$utf8 = utf8_encode($decode);
			return $utf8;
		}
		
		public static function UnicodeHexToUTF8($str) {
			// Si la variable proviene de una busqueda de expresión regular, la cedena vendrá en un array
			if (is_array($str)) { $str = $str[0]; }
			// Limpiamos la cadena de los residuos Unicode
			$strclean = str_replace("u00", "", $str);
			// Una vez que tenemos el código UTF8 hexadecimal hacemos la conversión a utf8 tradicional
			$utf8 = @pack('H*', $strclean);
			return $utf8;
		}
		
		public static function UnknownToUTF8($str) {
			// Si la variable proviene de una busqueda de expresión regular, la cedena vendrá en un array
			if (is_array($str)) { $str = $str[0]; }
			// Juegos de caracteres más comunes
			$encodes = array("UTF-8","ISO-8859-1","ASCII","windows-1252");
			// Intentamos detectar el juego de caracteres
			$encoding = mb_detect_encoding($str, $encodes);
			// Si el juego reconocido no es UTF8 intentamos convertirlo a este
			if ($encoding != "UTF-8") { $str = @mb_convert_encoding($str, "UTF-8", $encoding); }
			return $str;
		}
		
		public static function normalize($strs){
			$strf = array('À','Á','Â','Ã','Ä','Å','Æ','Ç','È','É','Ê','Ë','Ì','Í','Î','Ï','Ð','Ñ','Ò','Ó','Ô','Õ','Ö','Ø','Ù','Ú','Û','Ü','Ý','ß','à','á','â','ã','ä','å','æ','ç','è','é','ê','ë','ì','í','î','ï','ñ','ò','ó','ô','õ','ö','ø','ù','ú','û','ü','ý','ÿ','A','a','A','a','A','a','C','c','C','c','C','c','C','c','D','d','Ð','d','E','e','E','e','E','e','E','e','E','e','G','g','G','g','G','g','G','g','H','h','H','h','I','i','I','i','I','i','I','i','I','i','?','?','J','j','K','k','L','l','L','l','L','l','?','?','L','l','N','n','N','n','N','n','?','O','o','O','o','O','o','Œ','œ','R','r','R','r','R','r','S','s','S','s','S','s','Š','š','T','t','T','t','T','t','U','u','U','u','U','u','U','u','U','u','U','u','W','w','Y','y','Ÿ','Z','z','Z','z','Ž','ž','?','ƒ','O','o','U','u','A','a','I','i','O','o','U','u','U','u','U','u','U','u','U','u','?','?','?','?','?','?');
			$strt = array('A','A','A','A','A','A','AE','C','E','E','E','E','I','I','I','I','D','N','O','O','O','O','O','O','U','U','U','U','Y','s','a','a','a','a','a','a','ae','c','e','e','e','e','i','i','i','i','n','o','o','o','o','o','o','u','u','u','u','y','y','A','a','A','a','A','a','C','c','C','c','C','c','C','c','D','d','D','d','E','e','E','e','E','e','E','e','E','e','G','g','G','g','G','g','G','g','H','h','H','h','I','i','I','i','I','i','I','i','I','i','IJ','ij','J','j','K','k','L','l','L','l','L','l','L','l','l','l','N','n','N','n','N','n','n','O','o','O','o','O','o','OE','oe','R','r','R','r','R','r','S','s','S','s','S','s','S','s','T','t','T','t','T','t','U','u','U','u','U','u','U','u','U','u','U','u','W','w','Y','y','Y','Z','z','Z','z','Z','z','s','f','O','o','U','u','A','a','I','i','O','o','U','u','U','u','U','u','U','u','U','u','A','a','AE','ae','O','o');
			return str_replace($strf, $strt, $strs);
		}

		public static function normalize2($s) {
			$o = htmlentities($s, ENT_QUOTES, 'UTF-8');
			$o = preg_replace('~&([a-z]{1,2})(acute|caron|cedilla|macron|stroke|cedil|circ|grave|lig|orn|ring|slash|th|tilde|uml);~i', '$1', $o);
			$o = preg_replace('/&#?[a-z0-9]{2,8};/i','', $o); 
			$o = trim($o, ' ');
			return $o;
		}

		public static function isotoutf8_html($text) {
			return htmlentities($text, ENT_NOQUOTES);
		}
		public static function utf8toiso_html($text) {
			return htmlentities($text, ENT_NOQUOTES, "UTF-8");
		}
		public static function isotoutf8_plain($text) {
			return utf8_encode($text);
		}
		public static function utf8toiso_plain($text) {
			return utf8_decode($text);
		}
		public static function hextoutf8($text) {
			return rawurldecode(preg_replace('/=(\w{1}\d{1})=(\w{1}\d{1})/', '%$1%$2', $text));
		}
		public static function text2html($text) {
			return trim(stripslashes(htmlentities($text,ENT_QUOTES)));
		}
		public static function addprefix($str, $prefix) {
			return $prefix.$str;
		}
		public static function addsubfix($str, $subfix) {
			return $str.$subfix;
		}
		public static function addpresubfix($str, $prefix, $subfix) {
			return $prefix.$str.$subfix;
		}
		public static function deletealpha($string) {
			return preg_replace("/[^a-zA-Zs]/", "", $string);
		}
		public static function deletenumbers($string) {
			return preg_replace("/[^0-9]/", "", $string);
		}
		public static function toutf8($string) {
			$ENCODING = mb_detect_encoding($string, "iso-8859-1,windows-1252,utf-16", true);
			return iconv($ENCODING, 'UTF-8', $string);
		}
		public static function firstchars($str, $sstr, $o=''){
			$str = self::normalize(strtolower($str));
			$w = explode(" ", $str, 3);
			for ($i=0; $i<count($w); $i++) {
				if (strlen($w[$i]) >= $sstr) {
					$o.= substr($w[$i], 0, $sstr);
				} else {
					$o.= substr($w[$i], 0, 1);
				}
			}
			return $o;
		}
		public static function minimalName($name) {
			$e = explode(" ", $name);
			$o = "";
			foreach($e as $i=>$w) {
				$o.= ($i == 0) ? $w : " ".substr($w, 0, 1).".";
			}
			return $o;
		}
		public static function supertrim($str) {
			return trim(preg_replace(array('/\s+/','/\t+/','/\r+/','/\n+/'),' ',$str));
		}
		public static function explodeitem($pattern, $string, $index) {
			$a = explode($pattern, $string);
			if (isset($a[$index])) { return $a[$index]; }
			return NULL;
		}
		public static function singleline($text) {
			$regs = array('/\s+/','/\t+/','/\r+/','/\n+/');
			$text = strip_tags($text);
			return preg_replace($regs, ' ', $text);
		}
		public static function shorttext($text, $limit, $etc='') {
		
			if (strlen($text) < $limit) { return $text; }
			if (strpos($text, " ") == false) { return substr($text, 0, $limit).$etc; }
			
			$text = self::singleline($text);
			$last = substr($text, $limit-1, 1);
			$full = substr($text, 0, $limit);
		
			if ($last == " ") { $short = substr($text, 0, $limit-1); }
			if ($last != " ") { $short = substr($text, 0, strrpos($full, " ")); }
			return $short.$etc;
		}
		public static function urify($str, $limit=0) {
			$str = strip_tags($str);
			$str = preg_replace('/[[:punct:]]/', ' ', self::normalize2($str));
			$str = strtolower(preg_replace("/\s+/", "-", self::singleline($str)));
			return ($limit==0) ? $str : substr($str, 0, $limit);
		}
		public static function keywords($str) {
			return explode("-", self::urify($str));
		}
		public static function camelcase($str) {
			$str = preg_replace("/-/", " ", $str);
			return ucwords($str);
		}
		public static function underscore($str) {
			$str = preg_replace("/\s/", "_", trim(self::normalize($str)));
			return strtolower($str);
		}
		public static function hyphenate($str) {
			$str = preg_replace("/\s/", "-", trim($str));
			return ucwords($str);
		}
		public static function entitle($str) {
			return ucfirst(strtolower($str));
		}
		public static function monefy($n, $code="", $decimals=0) {
			return trim($code." ".number_format($n, $decimals, ".", ","));
		}
		public static function extract($str, $separator) {
			return empty($str) ? array() : explode($separator, substr($str,1,-1));
		}
		public static function words($str, $count) {
			if (strpos($str," ") === false) { return $str; }
			$str = explode(" ", $str); $o = ''; for ($i=0; $i<$count; $i++) { $o.= $str[$i]; }
			return $o;
		}
	}

}

?>