<?php

namespace PC {

	use PC\Libraries\ServerLibrary;

	class HttpData {

		protected $rawData;

		// Campos como son capturados
		protected $inputs;

		// Campos como serán usados en las sentencias SQL
		protected $aliasInputs = [];
		protected $aliasErrors = [];

		public function aliasInputs($model) {
			foreach ($model->aliases as $fieldName=>$aliasName) {
				if ($this->has($aliasName)) {
					$this->aliasInputs[$fieldName] = $this->inputs[$aliasName];
				}
			}
			return $this->aliasInputs;
		}

		public function aliasErrors($model, $errors) {
			$aliases = $model->aliases;
			foreach ($errors as $fieldName=>$errorList) {
				if (isset($aliases[$fieldName])) {
					$this->aliasErrors[$aliases[$fieldName]] = $errorList;
				}
			}
			return $this->aliasErrors;
		}

		public function __get($property) {
			if (isset($this->$property)) {
				return $this->$property;
			}
			return false;
		}

		public function has($varname) {
			return isset($this->inputs[$varname]);
		}

		public function input($varname) {
			return isset($this->inputs[$varname]) ? $this->inputs[$varname] : null;
		}

		// NOTA: Si la variable enable_post_data_reading estubiera establecida, 
		// podríamos leer el contenido en bruto desde la entrada de PHP
		// de tal forma que POST y FILES estarían vacios de forma predeterminada
		public function __construct() {

			// Contenedor para toda la información que vamos captando
			$inputs = array();
			$aliasInputs = array();

			switch (ServerLibrary::get('REQUEST_METHOD')) {
				case 'POST' :
					$inputs = array_merge($inputs, $_POST, $_FILES);
					break;

				case 'PUT' :

					// Leemos la información de los campos desde la entrada estándar de PHP
					$this->rawData = '';
					$stream = fopen("php://input", "rb");
					while(($dataStream = fgets($stream)) !== false) {
						$this->rawData .= $dataStream;
					}

					// Cerrámos el flujo para evitar fugas de memoria
					fclose($stream);

					// Buscamos las partes del flujo
					$contentType = ServerLibrary::get('CONTENT_TYPE, REDIRECT_HTTP_CONTENT_TYPE');
					preg_match('/boundary=(.*)$/', $contentType, $boundaries);

					// Si no encontrámos partes, probablemente la petición fué codificada como texto plano (www-urlencoded)
					if (empty($boundaries)) {

						parse_str(urldecode($this->rawData), $inputs);

					// En caso contrario, posiblemente se trate de una petición con multipartes (multipart/form-data)
					} else {

						// Firma que contienen todas las partes de la petición
						$boundary = $boundaries[1];

						// Dividimos cada campo enviado
						$blocks = preg_split("/-+$boundary/", $this->rawData);
						array_pop($blocks);

						// Leemos todos los bloques del flujo
						foreach ($blocks as $id => $block) {

							// Si el bloque no contiene información, lo saltámos
							if (empty($block)) { continue; }

							// Si se trata de un archivo
							if (preg_match("/filename=\"/", $block)) {

								// Buscamos nombre, nombre de archivo y tipo de contenido
								preg_match('/name=\"([^\"]*)\"; filename=\"([^\"]*)\"[\n|\r]+([^\n\r].*)?\r$/s', $block, $field);
								$name = $field[1];
								$filename = $field[2];
								$contentType = $field[3];

								// Buscamos el tipo mime
								preg_match('/Content-Type: (.*)?/', $contentType, $mime);

								// Generámos el archivo como lo haría PHP de forma nativa
								$content = preg_replace('/Content-Type: (.*)[^\n\r]/', '', $contentType);
								$uniqName = substr(sha1(rand()), 0, 6);
								$path = sys_get_temp_dir().SEPARATOR.'php'.$uniqName;
								$err = file_put_contents($path, $content);

								// Si el campo de archivo es multiple
								if (preg_match('/^(.*)\[\]$/i', $name, $multiple)) {
									$index = $multiple[1];
									$inputs[$index]['name'][] = $filename;
									$inputs[$index]['type'][] = $mime[1];
									$inputs[$index]['tmp_name'][] = $path;
									$inputs[$index]['error'][] = ($err === FALSE) ? $err : 0;
									$inputs[$index]['size'][] = filesize($path);
								// Si el campo de archivo es simple
								} else {
									$index = $name;
									$inputs[$index]['name'] = $filename;
									$inputs[$index]['type'] = $mime[1];
									$inputs[$index]['tmp_name'] = $path;
									$inputs[$index]['error'] = ($err === FALSE) ? $err : 0;
									$inputs[$index]['size'] = filesize($path);
								}
								
							// Es un campo
							} else {

								// Buscamos el nombre del campo
								preg_match('/name=\"(.*)?\"[\n|\r]+(.*)?\r$/', $block, $field);
								$inputs[$field[1]] = $field[2];

							}
						}
					}
					break;
			}

			// Si hay contenido query en la URL
			$query = ServerLibrary::get('QUERY_STRING, REDIRECT_QUERY_STRING');
			if (!empty($query)) {
				parse_str($query, $getParams);
				$inputs = array_merge($getParams, $inputs);
			}

			$this->inputs = $inputs;
		}

	}

}

?>