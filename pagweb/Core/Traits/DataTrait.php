<?php

namespace PC\Traits {

	use PC\Types\DataTypes;

	trait DataTrait {

		public static function validAlpha($alpha){
			return preg_match("/[a-zA-Z]+$/", $alpha);
		}
		public static function validAlphaNumeric($value){
			return preg_match("/[\w]+$/", $value);
		}
		public static function validString($str){
			return !is_numeric($str) && is_string($str);
		}
		public static function validArray($arr){
			return is_array($arr);
		}
		public static function validIn($value, $arr){
			return in_array($value, $arr);
		}
		public static function validBool($value){
			$list = array("t","f","on","off","yes","no","true","false","enabled","disabled",1,"1",0,"0",true,false);
			return in_array($value, $list, true);
		}
		public static function validNumber($num){
			return is_numeric($num);
		}
		public static function validInteger($int){
			return is_integer($int);
		}
		public static function validFloat($float){
			return is_float($float);
		}
		public static function validMin($num, $min=1){
			return is_numeric($num) && $num >= $min;
		}
		public static function validMax($num, $max=2){
			return is_numeric($num) && $num <= $max;
		}
		public static function validRange($num, $min=1, $max=2){
			return is_numeric($num) && $num >= $min && $num <= $max;
		}
		public static function validMinLength($str, $min=1){
			return strlen($str) >= $min;
		}
		public static function validMaxLength($str, $max=2){
			return strlen($str) <= $min;
		}
		public static function validRangeLength($str, $min=1, $max=2){
			$l = strlen($str);
			return ($l >= $min) && ($l <= $max);
		}
		public static function validHash($str, $length) {
			return preg_match('/^[a-fA-F0-9]{'.$length.'}$/i', $str);
		}
		public static function validMD5($str) {
			return $this->validHash($str, 32);
		}
		public static function validSHA1($str){
			return $this->validHash($str, 40);
		}
		public static function validSHA256($str='z'){
			return $this->validHash($str, 64);
		}
		public static function validID($id) {
			return is_numeric($id) && floatval($id) > 0;
		}
		public static function validUUID($uuid) {
			return preg_match('/^[0-9A-F]{8}-[0-9A-F]{4}-4[0-9A-F]{3}-[89AB][0-9A-F]{3}-[0-9A-F]{12}$/i', trim($uuid));
		}
		public static function validEmail($email) {
			if ($email != "" && filter_var($email, FILTER_VALIDATE_EMAIL)) { return true; }
			return false;
		}
		public static function validIPV4($ip) {
			if ($ip != "" && filter_var($ip, FILTER_VALIDATE_IP, FILTER_FLAG_IPV4)) { return true; }
			return false;
		}
		public static function validIPV6($ip) {
			if ($ip != "" && filter_var($ip, FILTER_VALIDATE_IP, FILTER_FLAG_IPV6)) { return true; }
			return false;
		}
		public static function validIP($ip) {
			if ($ip != "" && filter_var($ip, FILTER_VALIDATE_IP)) { return true; }
			return false;
		}
		public static function validURL($url) {
			if ($url != "" && filter_var($url, FILTER_VALIDATE_URL)) { return true; }
			return false;
		}
		public static function validDate($date) {
			$date = ($date == "CURRENT_TIMESTAMP") ? "now" : $date;
			return strtotime($date) !== false;
		}
		public static function validDateRange($date, $startDate, $endDate) {
			$date = strtotime($date);
			return (($date >= strtotime($startDate)) && ($date <= strtotime($endDate)));
		}
		public static function validFile($file) {
			return is_array($file) && isset($file["tmp_name"]) && file_exists($file["tmp_name"]);
		}
		public static function validJSON($json) {
			if (self::validNumber($json) || self::validBool($json)) { return false; }
			$data = json_decode($json, true); unset($data);
			return (json_last_error() == JSON_ERROR_NONE);
		}
		public static function validate($mask, $params) {
			switch ($mask) {
				case DataTypes::MIN_TYPE : return call_user_func_array(array("self", "validMin"), $params);
				case DataTypes::MINLENGTH_TYPE : return call_user_func_array(array("self", "validMinLength"), $params);

				case DataTypes::MAX_TYPE : return call_user_func_array(array("self", "validMax"), $params);
				case DataTypes::MAXLENGTH_TYPE : return call_user_func_array(array("self", "validMaxLength"), $params);

				case DataTypes::RANGE_TYPE : return call_user_func_array(array("self", "validRange"), $params);
				case DataTypes::RANGELENGTH_TYPE : return call_user_func_array(array("self", "validRangeLength"), $params);

				case DataTypes::DATE_TYPE : return call_user_func_array(array("self", "validDate"), $params);
				case DataTypes::DATERANGE_TYPE : return call_user_func_array(array("self", "validDateRange"), $params);
				case DataTypes::ALPHA_TYPE : return call_user_func_array(array("self", "validAlpha"), $params);
				case DataTypes::ALPHANUMERIC_TYPE : return call_user_func_array(array("self", "validAlphaNumeric"), $params);
				case DataTypes::STRING_TYPE : return call_user_func_array(array("self", "validString"), $params);

				case DataTypes::ARRAY_TYPE : return call_user_func_array(array("self", "validArray"), $params);
				case DataTypes::IN_TYPE : return call_user_func_array(array("self", "validIn"), $params);
				case DataTypes::BOOLEAN_TYPE : return call_user_func_array(array("self", "validBool"), $params);

				case DataTypes::IPV4_TYPE : return call_user_func_array(array("self", "validIPV4"), $params);
				case DataTypes::IPV6_TYPE : return call_user_func_array(array("self", "validIPV6"), $params);
				case DataTypes::URL_TYPE : return call_user_func_array(array("self", "validURL"), $params);
				
				case DataTypes::NUMERIC_TYPE : return call_user_func_array(array("self", "validNumber"), $params);
				case DataTypes::INTEGER_TYPE : return call_user_func_array(array("self", "validInteger"), $params);
				case DataTypes::FLOAT_TYPE : return call_user_func_array(array("self", "validFloat"), $params);

				case DataTypes::HASH_TYPE : return call_user_func_array(array("self", "validHash"), $params);
				case DataTypes::MD5_TYPE : return call_user_func_array(array("self", "validMD5"), $params);
				case DataTypes::SHA1_TYPE : return call_user_func_array(array("self", "validSHA1"), $params);
				case DataTypes::SHA256_TYPE : return call_user_func_array(array("self", "validSHA256"), $params);
				case DataTypes::ID_TYPE : return call_user_func_array(array("self", "validID"), $params);
				case DataTypes::UUID_TYPE : return call_user_func_array(array("self", "validUUID"), $params);
				case DataTypes::EMAIL_TYPE : return call_user_func_array(array("self", "validEmail"), $params);
			}
		}

		public static function validByType($type, $value) {
			switch ($type) {
				case 'pass' : case 'password' : case 'string' : case 'varchar' : case 'char' : case 'character' :
				$validField = self::validString($value);
				break;

				case 'int' : case 'integer' : case 'number' : case 'float' : case 'double' : case 'decimal' : case 'numeric' :
				$validField = self::validNumber($value);
				break;

				case 'bool' : case 'boolean' :
				$validField = self::validBool($value);
				break;

				case 'date' : case 'datetime' :
				$validField = self::validDate($value);
				break;

				case 'array' :
				$validField = self::validArray($value);
				break;

				case 'inet' : 
				$validField = self::validIP($value);
				break;

				case 'url' : 
				$validField = self::validURL($value);
				break;

				case 'email' : 
				$validField = self::validEmail($value);
				break;

				case 'file' :
				$validField = self::validFile($value);
				break;

				default : $validField = false;
			}
			return $validField;
		}

		private static function emptyByType($type, $value) {
			switch ($type) {
				case 'int' : case 'integer' : case 'number' : case 'float' : case 'double' : case 'decimal' : case 'numeric' :
				$isEmpty = (trim($value) == '');
				break;

				case 'array' : case 'file' :
				$isEmpty = empty($value);

				default :
				$isEmpty = (strlen(trim($value)) == 0);
			}
			return $isEmpty;
		}
	}

}

?>