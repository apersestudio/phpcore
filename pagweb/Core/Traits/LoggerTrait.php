<?php

namespace PC\Traits {

	trait LoggerTrait {

		// Falta definir en alguna variable donde se deben de almacenar los archivos log

		public function emergency($message, array $context = array()){
			
		}
		
		public function alert($message, array $context = array()){
		}
	   
		public function critical($message, array $context = array()){
			error_log($message, 3, LOGS.'/'.$this->className.'.log');
		}
		
		public function error($message, array $context = array()){
			error_log($message, 3, LOGS."/postgresql.log");
		}
	   
		public function warning($message, array $context = array()){
		}
		
		public function notice($message, array $context = array()){
		}
		
		public function info($message, array $context = array()){
		}
		
		public function debug($message, array $context = array()){
		}

	}
	
}

?>