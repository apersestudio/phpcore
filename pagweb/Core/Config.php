<?php

namespace PC {

	use PC\Framework;

	class Config {

		private static $data = [];

		public static function register($data) {
			self::$data = $data;
		}

		public static function get($indexes='') {
			// Cada parte de la cadena representa un indice del array
			$vars = explode(".", $indexes);
			// Si la cadena tiene piezas
			if (count($vars) > 0) {
				// Usamos el primer indice para extraer el valor
				$first = array_shift($vars);
				$value = self::$data[$first];
				foreach ($vars as $var) {
					$value = GET($value[$var]);
				}
				return $value;	
			}
			// Si la cadena no contiene piezas regresámos nulo
			return null;
		}

		public static function set($indexes, $value) {
			// Cada parte de la cadena representa un indice del array
			$vars = explode(".", $indexes);
			// Si la cadena tiene piezas
			$total = count($vars);
			if ($total > 0) {
				// Usamos el primer indice para extraer el valor
				$first = array_shift($vars);
				// Si el indice no existe lo creamos
				if (!isset(self::$data[$first])) { self::$data[$first] = []; }
				// Hacemos una referencia a este elemento
				$item = &self::$data[$first];
				foreach ($vars as $var) {
					// Si el subindice no existe lo creamos
					if (!isset($item[$var])) { $item[$var] = []; }
					$item = &$item[$var];
				}
				$item = $value;
			}
		}

	}
}

?>