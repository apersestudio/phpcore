<?php

// Nos aseguramos que document root siempre apunte a raíz
chdir("/");
define("DEBUGMODE", true);
define("MIN_SUPPORT", "5.4");
define("SEPARATOR", DIRECTORY_SEPARATOR);

// Mostramos los errores
error_reporting(E_ALL);
ini_set("display_errors", "On");
ini_set("max_execution_time", 0);

// ONLY GLOBAL VARIABLE
function GET(&$prop, $def=null) { return isset($prop) ? $prop : $def; }

// URL visible en el navegador
$REQUESTURI = "/".ltrim(rtrim(GET($_SERVER["REQUEST_URI"]), "/"), "/");

define("MODIFIED_SINCE", GET($_SERVER['HTTP_IF_MODIFIED_SINCE']));
define("NONE_MATCH", GET($_SERVER['HTTP_IF_NONE_MATCH']));

define("METHOD", strtoupper($_SERVER["REQUEST_METHOD"]));
define("USER_AGENT", $_SERVER["HTTP_USER_AGENT"]);
define("ACCEPT", $_SERVER["HTTP_ACCEPT"]);
define("ACCEPT_LANGUAGE", strtoupper($_SERVER["HTTP_ACCEPT_LANGUAGE"]));
define("MOBILE", preg_match('/mobile/i', USER_AGENT));

define("SERVER", !empty($_SERVER["SERVER_SOFTWARE"]) ? $_SERVER["SERVER_SOFTWARE"] : 'UNKNOWN');
define("SERVERIP", !empty($_SERVER["SERVER_ADDR"]) ? $_SERVER["SERVER_ADDR"] : (!empty($_SERVER["LOCAL_ADDR"]) ? $_SERVER["LOCAL_ADDR"] : '127.0.0.1'));
define("SERVERPORT", !empty($_SERVER["SERVER_PORT"]) ? $_SERVER["SERVER_PORT"] : 80);
define("REMOTEIP", !empty($_SERVER["REMOTE_ADDR"]) ? $_SERVER["REMOTE_ADDR"] : '127.0.0.1');
define("REMOTEPORT", intval(GET($_SERVER["REMOTE_PORT"])));

define("URI", $REQUESTURI);
define("DOCUMENTROOT", __DIR__);
define("PATHINFO", DOCUMENTROOT.SEPARATOR);

//Carpetas en Raíz
define("CORE", PATHINFO."Core".SEPARATOR);
define("ASSETS", PATHINFO."Assets".SEPARATOR);
define("PAGES", PATHINFO."Pages".SEPARATOR);

// Carpetas de trabajo
define("CONFIG", PAGES."Config".SEPARATOR);
define("CONTROLLERS", PAGES."Controllers".SEPARATOR);
define("LAYOUTS", PAGES."Layouts".SEPARATOR);
define("LOGS", PAGES."Logs".SEPARATOR);
define("MANAGERS", PAGES."Managers".SEPARATOR);
define("MIDDLEWARES", PAGES."Middlewares".SEPARATOR);
define("MODELS", PAGES."Models".SEPARATOR);
define("TMP", PAGES."Tmp".SEPARATOR);
define("VIEWS", PAGES."Views".SEPARATOR);
define("WIDGETS", PAGES."Widgets".SEPARATOR);
define("TEMPLATES", PAGES."Templates".SEPARATOR);
define("THIRDPARTY", PAGES."ThirdParty".SEPARATOR);

define("SSL", (SERVERPORT == 443));
define("PROTOCOL", "http".(SSL ? "s" : ""));
define("HOST", $_SERVER["SERVER_NAME"]);
define("DOMAIN", PROTOCOL."://".HOST);
define("PRIMARY", implode(".", array_slice(explode(".", HOST), 1)));
define("PRIMARY_HOST", "www.".PRIMARY);
define("PRIMARY_DOMAIN", PROTOCOL."://".PRIMARY_HOST);
define("HTTP_CDN", DOMAIN."/cdn/");

define("SELF", DOMAIN.URI);
define("REFERER", GET($_SERVER["HTTP_REFERER"]));
define("QUERYSTRING", GET($_SERVER["QUERY_STRING"]));
define("UNIXLIKE", stripos(PHP_OS, "win") === false);

define("PAYPAL_URL", "https://www".(DEBUGMODE?'.sandbox':'').".paypal.com/cgi-bin/webscr");
define("PAYPAL_SSL", "ssl://www".(DEBUGMODE?'.sandbox':'').".paypal.com");
define("PAYPAL_ACC", false);

define("MQUOTES", get_magic_quotes_gpc());
parse_str(QUERYSTRING, $_GET);

register_shutdown_function(function() {
	$error = error_get_last();
	if (!(error_reporting() & $error["type"])) { return; }
	throw new ErrorException($error["message"], 0, $error["type"], $error["file"], $error["line"]);
});
set_error_handler(function($severidad, $mensaje, $fichero, $línea) {
    if (!(error_reporting() & $severidad)) { return; }
    throw new ErrorException($mensaje, 0, $severidad, $fichero, $línea);
});

$autoload = require_once(CONFIG."Autoload.php");
spl_autoload_register(function ($clase) {
	global $autoload;
	$parts = explode("\\", $clase);
	$folder = array_shift($parts);
	$phpFile = implode("\\", $parts).".php";

	$url = false;
	switch ($folder) {
		case 'App' :
		$url = PAGES.$phpFile;
		break;

		case 'PC' :
		$url = PATHINFO."Core\\".$phpFile;
		break;

		case 'Com' :
		$url = PATHINFO."Core\\Components\\".$phpFile;
		break;

		case 'Psr' :
		$url = PATHINFO."Core\\Psr\\".$phpFile;
		break;

		default :
		if (array_key_exists($folder, $autoload)) {
			$url = COMPONENTS.$autoload[$folder]."\\".$phpFile;
		}
	}
	if (!empty($url) && !file_exists($url)) {
		$debugLines = debug_backtrace(4);
		$output = "<strong>No se encontró el archivo ".$url."</strong><br />";
		foreach ($debugLines as $line) {
			if (isset($line["file"]) && $line["function"] !== "spl_autoload_call") {
				$class = isset($line["class"]) ? $line["class"].$line["type"] : "";
				$output .= $line["file"]." <strong>línea ".$line["line"]."</strong> función ".$class.$line["function"]."()<br />";
			}
		}
		//print_r($debugLines);
		exit ($output);
	} else {
		//echo $url."\r\n";
		if (file_exists($url)) {
			require_once($url);
		} else {
			
		}
		
	}
});
?>