(function(){

	var _class;

	// Creamos la clase principal que sirve para contruir clases
	// También agregamos un acceso global a la clase desde el objeto window
	window["JSClass"] = function(cName, params) {

		// Parametros para crear la nueva clase
		params = params || {};

		// Contenedor de la clase
		_class = window.JSClass[cName] = function() {
			// Funciones a ejecutar antes de comunicarse con el servidor
			this._before = [];
			// Funciones que se ejecutan una vez que el servidor responde
			this._complete = [];
			// Si la nueva clase tiene una
			if (this["initialize"]) {
				return this["initialize"].apply(this, arguments);
			}
		};

		// Heredamos solo si el objetivo tiene un prototipo
		if (params.extend && params.extend.prototype) {
			$.extend(_class.prototype, params.extend.prototype);
		}

		// Propiedades
		if (params["properties"]) {
			$.each(params["properties"], function(propName, propValue) {
				_class.prototype[propName] = propValue;
			});
		}

		// Métodos
		if (params["methods"]) {
			$.each(params["methods"], function(methodName, methodFunction) {
				// Antes de sobre escribir a initialize, llamamos la función heredada del padre
				if(methodName == "initialize" && typeof(_class.prototype[methodName]) === "function") {
					_class.prototype[methodName]();
				}
				// Después ya podemos sobreescribir el resta de las funciones
				_class.prototype[methodName] = methodFunction;
			});
		}

		// Funciones comunes
		$.extend(_class.prototype, {
			// Guardamos el nombre del constructor para un mejor debugueo
			"className": cName,
			// Función para extender
			"extend": function(params) {
				$.extend(this, params);
				return this;
			},
			// Función para ligar acciones con botones de la interfaz de usuario
			"bind": function(method, params, event, elem){

				// Si se ocupa un método en lugar de una función
				if ($.type(method) == "string") {
					
					$(elem).bind(event, {"hostClass":this, "classMethod":method, "params":params || {}}, function(e) {
						if ($(this).data('disabled') !== true) {
							$(this).data('disabled', true);
							if (typeof(e.data.hostClass[e.data.classMethod]) === "function") {
								e.data.hostClass[e.data.classMethod].apply(e.data.hostClass, [this, e.data.params]);
							} else {
								JSClass.log("The class "+e.data.hostClass.className+" has not the method "+e.data.classMethod);
							}
						}
					});
				// El elemento es externo y forma parte de otro componente
				} else {
					this[elem].bind(event, params, method);
				}
				return this;
			},
			// Función para ejecutar manejadores
			"trigger": function(command, action, arguments) {
				if (typeof(this["_"+command][action]) === "function") {
					this["_"+command][action].apply(this, [arguments]);
				} else {
					JSClass.log(this.className+": The action "+command+" "+action+" is not a function");
				}
				return this;
			},
			// Ligar un manejador antes de que se ejecute una acción
			"onBefore": function(action, handler) {
				this._before[action] = handler;
				return this;
			},
			// Ligar un manejador una vez que se ejecute una acción
			"onComplete": function(action, handler) {
				this._complete[action] = handler;
				return this;
			},
			// Función para comunicarse con la capa de datos
			"ajaxCall": function(action, url, method, data, params) {
				this.trigger("before", action, data);
				_obj = {"context":this, "params":$.extend(params, data), "action":action};
				$.ajax({
					"async":false,
					"url":url,
					"method":method,
					"data":data,
					"dataType":"json"
				}).done($.proxy(function(_response){
					this.context.trigger("complete", this.action, {"status":"success", "content":_response, "message":"", "params":this.params});
				}, _obj)).fail($.proxy(function(_response, textStatus, errorThrown){
					this.context.trigger("complete", this.action, {"status":textStatus, "content":_response, "message":"No se pudo completar tu petición.", "params":this.params});
				}, _obj));
				return this;
			}
		});

		if (params["ready"]) {
			params["ready"].apply(this, params["properties"] || []);
		}

		// Recolector de basura
		_class = null;
	};

	window.JSClass.log = function(message) {
		if (console && typeof(console.log) === "function") {
			console.log(message);
		}
	};
	window.JSClass.support = {};

})();