/* MOLTOPIXEL ////////////////////////////////////////////////////////////////////////////////////////////////////////////// */
(function(){

	/* SAMEHEIGHT /////////////////// */
	var maxHeight, currentHeight;

	JSClass("SameHeight", {
		"methods": {
			"initialize": function(container, target){
				this.target = $(target);
				this.container = $(container);
				$(window).bind("resize.SameHeight", $.proxy(function(){
					this.onResize();
				}, this)).trigger("resize.SameHeight");
			},
			"onResize": function() {
				// Evitamos hacer llamadas multiples
				window.clearTimeout(this.lastInterval);

				// Con este pequeño retraso de 100 milisegundos le damos tiempo al navegador para renderizar los elementos
				this.lastInterval = window.setTimeout($.proxy(function(){

					$(this.container).each($.proxy(function(indiceFila, fila){
						maxHeight = 0;
						$(this.target, fila).each(function(indiceColumna, columna){
							currentHeight = $(columna).css({"height":"auto"}).height();
							maxHeight = maxHeight >= currentHeight ? maxHeight : currentHeight;
						});
						$(this.target, fila).height(maxHeight);
					}, this));

				}, this), 100);
			}
		}
	})

}());