(function($){

	// Funciones Comunes
	function convertir(valor) {
		return isNaN(valor) ? String(valor) : Number(valor);
	}

	function contiene(valor, valores) {
		valor = convertir(valor);
		r = false;
		$.each(valores, function(i,o){
			v = convertir(o);
			if (v == valor) { r = true; return r; }
		});
		return r;
	}

	(function(){

		var i, j, l, m, s, q, p, d, v, r, o, h, b, c, w;
		
		function DatePicker(target) {
			// Variables privadas
			var element = $(target);
			var interface = $('<div class="datepicker"></div>').insertBefore(element);
			var container = false;
			var containerMonths = false;
			var containerYears = false;
			var currentYear;
			var currentDate = new Date();
			var displayDate = new Date();
			var currentMonth;
			var currentMonthName;
			var monthDays = 0;
			var firstDayOfMonth;
			var lastDayOfMonth;
			var firstDayOfPreviousMonth;
			var lastDayOfPreviousMonth;
			var firstDayOfNextMonth;
			var lastDayOfNextMonth;

			
			var firstDayOfMonthDiff = 0;
			var lastDayOfMonthDiff = 0;
			var visibleDays = 42;
			
			var months = ["Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"];
			var monthsShort = ["Ene","Feb","Mar","Abr","May","Jun","Jul","Ago","Sep","Oct","Nov","Dic"];
			var weekDays = ["Domingo","Lunes","Martes","Miércoles","Jueves","Viernes","Sábado"];
			var weekDaysShort = ["D","L","M","M","J","V","S"];
			var totalDays = weekDays.length;

			// Propiedades iniciales del contenedor principal
			interface.css({
				"display":"none",
				"position":"absolute",
				"z-index":9999,
				"min-width":element.outerWidth(),
				"min-height":100,
			}).attr({
				"tabindex":-1
			});
			element.parent().css({"position":"relative"});
			
			// Funciones privadas
			this.getElement = function(){
				return element;
			}
			this.yearIsLeap = function(year) {
			  return ((year % 4 == 0) && (year % 100 != 0)) || (year % 400 == 0);
			}
			this.getDaysPerMonth = function(year) {
				year = (isNaN(year)||year==undefined) ? Date.prototype.getFullYear() : year;
				return [31,this.yearIsLeap(year)?29:28,31,30,31,30,31,31,30,31,30,31];
			}
			this.setDate = function(date) {
				d = this.convertToDate(date);
				if (d === false) { return false; }
				this.setCurrentDate(d);
				return d;
			}
			this.getCurrentDate = function() {
				return currentDate;
			}
			this.getFirstDay = function(_basedate) {
				return new Date(_basedate.getFullYear(), _basedate.getMonth(), 1);
			}
			this.getLastDay = function(_basedate) {
				return new Date(_basedate.getFullYear(), _basedate.getMonth()+1, 0);
			}
			this.getDateParts = function(_basedate) {
				o = {};
				o["year"] = _basedate.getFullYear();
				o["month"] = _basedate.getMonth()
				o["monthName"] = monthsShort[_basedate.getMonth()];
				o["monthDays"] = this.getDaysPerMonth(_basedate.getFullYear())[_basedate.getMonth()];
				o["firstDayOfMonth"] = this.getFirstDay(_basedate);
				o["lastDayOfMonth"] = this.getLastDay(_basedate);
				d = o.firstDayOfMonth.getDay();

				// Si la diferencia es cero, agregamos 7 para rellenar la primera fila con los dias del mes anteriro
				//console.log(d, o.firstDayOfMonth.getDate());
				if (d == 0) { d = 7; }

				o["previousMonth"] = this.convertToDate("-1 month", _basedate);
				o["firstDayOfPreviousMonth"] = this.convertToDate("-"+d+" days", o.firstDayOfMonth).getDate();
				o["lastDayOfPreviousMonth"] = this.getLastDay(o.previousMonth).getDate();
				o["nextMonth"] = this.convertToDate("+1 month", _basedate);
				o["firstDayOfNextMonth"] = 1;
				o["lastDayOfNextMonth"] = this.convertToDate("+"+(visibleDays - (d + o.monthDays)).toString()+" days", o.lastDayOfMonth).getDate();
				return o;
			}
			this.setCurrentDate = function(newdate) {
				// p = Parts
				p = this.getDateParts(newdate);
				currentYear = p.year;
				currentMonth = p.month;
				currentMonthName = p.monthName;
				monthDays = p.monthDays;
				firstDayOfMonth = p.firstDayOfMonth;
				lastDayOfMonth = p.lastDayOfMonth;
				firstDayOfPreviousMonth = p.firstDayOfPreviousMonth;
				lastDayOfPreviousMonth = p.lastDayOfPreviousMonth;
				firstDayOfNextMonth = p.firstDayOfNextMonth;
				lastDayOfNextMonth = p.lastDayOfNextMonth;
				currentDate = newdate;
				displayDate = newdate;
			}
			this.getPreviousMonth = function() {
				displayDate = this.convertToDate("-1 month", displayDate);
				return displayDate;
			}
			this.getNextMonth = function() {
				displayDate = this.convertToDate("+1 month", displayDate);
				return displayDate;
			}
			this.getPreviousYear = function() {
				displayDate = this.convertToDate("-1 year", displayDate);
				return displayDate;
			}
			this.getNextYear = function() {
				displayDate = this.convertToDate("+1 year", displayDate);
				return displayDate;
			}
			this.convertToDate = function(_expression, _basedate) {
				// i = Incremental
				// q = Quantifier
				// d = Date
				// s = Split
				// v = Value
				// m = Match
				_basedate = _basedate ? new Date(_basedate.toString()) : new Date();
				if (m=_expression.match(/[+-]\d+\s+(year|month|day|hour|minute|second|millisecond)?/ig)) {
					i = m.length;
					while (i>0) {
						s = m[i-1].split(" ");
						q = parseInt(s[0]);
						v = s[1];
						switch (v) {
							case 'year' : _basedate.setFullYear(_basedate.getFullYear() + q); break;
							case 'month' : _basedate.setMonth(_basedate.getMonth() + q); break;
							case 'day' : _basedate.setDate(_basedate.getDate() + q); break;
							case 'hour' : _basedate.setHours(_basedate.getHours() + q); break;
							case 'minute' : _basedate.setMinutes(_basedate.getMinutes() + q); break;
							case 'second' : _basedate.setSeconds(_basedate.getSeconds() + q); break;
							case 'millisecond' : _basedate.setMilliseconds(_basedate.getMilliseconds() + q); break;
						}
						i--;
					}
				} else if (m=_expression.match(/(\d{2})[/.-](\d{2})[/.-](\d{4})/)) {
					_basedate.setFullYear(m[3]);
					_basedate.setMonth(parseInt(m[2]-1));
					_basedate.setDate(m[1]);
				} else if (m=_expression.match(/(\d{4})[/.-](\d{2})[/.-](\d{2})/)) {
					_basedate.setFullYear(m[1]);
					_basedate.setMonth(parseInt(m[2]-1));
					_basedate.setDate(m[3]);
				} else {
					_basedate = false;
				}
				return _basedate;
			};
			this.diffDate = function(_basedate, _expression) {
				_basedate = this.convertToDate(_basedate);
				if (_basedate !== false) {
					return this.convertToDate(_expression);
				}
				return false;
			}
			this.drawInterface = function() {

				p = this.getDateParts(displayDate);

				// Eliminamos versiones anteriores del calendario
				if (container !== false) { container.remove(); }

				// Creamos el contenedor principal
				container = $('<div class="datepicker-container"></div>').appendTo(interface);

				w = $('<div class="wrapper"></div>').appendTo(container);
				containerMonths = $('<div class="datepicker-months"></div>').appendTo(w);
				containerYears = $('<div class="datepicker-years"></div>').appendTo(w);
				

				// Creamos los controles para cambiar de mes
				h = '<a class="previous-month"><i class="fa fa-fw fa-chevron-left"></i></a>';
				h+= '<strong class="current-month">'+p.monthName+'</strong>';
				h+= '<a class="next-month"><i class="fa fa-fw fa-chevron-right"></i></a>';
				d = $(h).appendTo(containerMonths);

				$('a.previous-month', containerMonths).bind('mousedown', {"root":this}, function(e){
					e.data.root.getPreviousMonth();
					e.data.root.drawInterface();
					e.preventDefault();
				});

				$('a.next-month', containerMonths).bind('mousedown', {"root":this}, function(e){
					e.data.root.getNextMonth();
					e.data.root.drawInterface();
					e.preventDefault();
				});

				// Creamos los controles para cambiar de año
				h = '<a class="previous-year"><i class="fa fa-fw fa-chevron-left"></i></a>';
				h+= '<strong class="current-year">'+p.year+'</strong>';
				h+= '<a class="next-year"><i class="fa fa-fw fa-chevron-right"></i></a>';
				d = $(h).appendTo(containerYears);

				$('a.previous-year', containerYears).bind('mousedown', {"root":this}, function(e){
					e.data.root.getPreviousYear();
					e.data.root.drawInterface();
					e.preventDefault();
				});

				$('a.next-year', containerYears).bind('mousedown', {"root":this}, function(e){
					e.data.root.getNextYear();
					e.data.root.drawInterface();
					e.preventDefault();
				});

				// Horizontal separator
				$('<div class="datepicker-hseparator"></div>').appendTo(container);
				
				// d = Date Options
				d = $('<ul class="datepicker-days"></ul>').appendTo(container);

				for (i=0; i<totalDays; i++) {
					h = '<li class="datepicker-dayname">'+weekDaysShort[i]+'</li>';
					$(h).appendTo(d);
				}
				//weekDays

				for (i=p.firstDayOfPreviousMonth; i<=p.lastDayOfPreviousMonth; i++) {
					h = '<li class="previous-month"><a class="datepicker-day" data-month="'+p.previousMonth.getMonth()+'" data-year="'+p.previousMonth.getFullYear()+'" data-day="'+i+'">'+i+'</a></li>';
					$(h).appendTo(d);
				}
				
				for (i=p.firstDayOfMonth.getDate(); i<=p.lastDayOfMonth.getDate(); i++) {
					c = ((currentDate.getFullYear() == p.year) && (currentDate.getMonth() == p.month) && (i == currentDate.getDate())) ? ' current-day' : '';
					h = '<li class="current-month"><a class="datepicker-day'+c+'" data-month="'+p.month+'" data-year="'+p.year+'" data-day="'+i+'">'+i+'</a></li>';
					$(h).appendTo(d);
				}

				for (i=p.firstDayOfNextMonth; i<=p.lastDayOfNextMonth; i++) {
					h = '<li class="next-month"><a class="datepicker-day" data-month="'+p.nextMonth.getMonth()+'" data-year="'+p.nextMonth.getFullYear()+'" data-day="'+i+'">'+i+'</a></li>';
					$(h).appendTo(d);
				}

				$('a.datepicker-day', d).bind('mousedown', {"root":this, "interface":interface}, function(e){
					d = $(this).data("day");
					d = (d < 10) ? "0"+d : d;

					m = $(this).data("month") + 1;
					m = (m < 10) ? "0"+m : m;

					element.val(d+'-'+m+'-'+$(this).data("year"));
					e.data.root.setCurrentDate(e.data.root.convertToDate(element.val()));
					e.data.root.drawInterface();
					e.data.interface.blur();

				});
			}
			this.showInterface = function() {
				p = element.position();
				interface.css({
					"top":p.top+element.outerHeight(),
					"left":p.left
				}).show().focus();
			}
			
			// Desencadenadores
			element.bind('focus', {"root":this}, function(e){
				e.data.root.setCurrentDate(currentDate);
				e.data.root.drawInterface();
				e.data.root.showInterface();
			});
			interface.bind("blur", function(e){
				interface.hide();
			});

			// Configuramos el calendario para tener la fecha actual
			this.setCurrentDate(currentDate);

			// Inicializamos la interfaz
			this.drawInterface();
		}

		window.DatePicker = DatePicker;

	})();

	/** ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- **/
	/** ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- **/
	/** ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- **/

	(function(){

		var THIS, VALIDO, _h, _cdata, _s, _ts, _v, _t, _w;

		function aeFieldEditor(objetivo, opciones) {

			this._fE = $(objetivo);
			this._fEID = this._fE.prop("id");
			this._fEViwer = false;
			this._fEControls = false;
			this._fEAlert = false;

			this._fEInterface = false;
			this._fEEdit = false;
			this._fESave = false;
			this._fESaveIcon = false;
			this._fECancel = false;

			this._fEAutoComplete = false;
			this._fEAutoCompleteInput = false;
			this._fEAutoCompleteInterval = false;
			this._fEAutoCompleteTable = false;

			this._fEValue = false;
			this._fEText = false;
			this._fEData = false;
			this._fESelected = false;
			this._fEOpciones = $.extend({
				"columnaClase":false,
				"abierto":false,
				"vacio":false,
				"letrasMinimas":3,
				"valor":"",
				"texto":""
			}, opciones);

			this.init();

		}

		aeFieldEditor.prototype.getField = function(disabled, classvalue) {
			switch (this._fEOpciones.tipo) {
				case 'password' : 
				_h = '<input type="password" class="form-control'+(classvalue!=undefined?' fieldeditor-'+classvalue:'')+'" value="'+this._fEOpciones.texto+'"'+(disabled?' disabled="disabled"':'')+' />';
				break;

				default :
				_h = '<input type="text" class="form-control'+(classvalue!=undefined?' fieldeditor-'+classvalue:'')+'" value="'+this._fEOpciones.texto+'"'+(disabled?' disabled="disabled"':'')+' />';
			}
			return _h;
		}

		aeFieldEditor.prototype.getData = function(data) {
			_cdata = '';
			$.each(data, function(prop, value){
				_cdata += ' data-'+prop+'="'+value+'"';
			});
			return _cdata;
		}

		aeFieldEditor.prototype.getTextarea = function(disabled, classvalue) {
			_h+= '<textarea class="form-control'+(classvalue!=undefined?' fieldeditor-'+classvalue:'')+'"'+(disabled?' disabled="disabled"':'')+'>'+this._fEOpciones.valor+'</textarea>';
			return _h;
		}

		aeFieldEditor.prototype.getList = function(disabled, classvalue) {
			THIS = this;
			_h = '';
			_ts = Math.round(Date.now() * Math.random());
			switch (this._fEOpciones.tipo) {
				case 'list' : case 'boolean' :
				_h+= '<select class="form-control'+(classvalue!=undefined?' fieldeditor-'+classvalue:'')+'">';
				$.each(this._fEOpciones.data, function(i,o){
					_cdata = ''; if (o.data) { _cdata = THIS.getData(o.data); }
					_s = (THIS._fEOpciones.valor == convertir(o.val)) ? ' selected="selected"' : '';
					_h+= '<option value="'+o.val+'"'+_cdata+_s+'>'+o.text+'</option>';
				});
				_h+= '</select>';
				break;

				case 'radiogroup':
				_w = !!this._fEOpciones.columnaClase ? ' wrapper' : '';
				_h+= '<div class="list-group'+_w+'">';
				$.each(this._fEOpciones.data, function(i,o){
					_cdata = ''; if (o.data) { _cdata = THIS.getData(o.data); }
					_s = (THIS._fEOpciones.valor == convertir(o.val)) ? ' checked="checked"' : '';
					_w = !!THIS._fEOpciones.columnaClase ? ' col-'+THIS._fEOpciones.columnaClase : '';
					_h+= '<div class="list-group-item'+_w+'"><label><input type="radio"'+(classvalue!=undefined?' class="fieldeditor-'+classvalue+'"':'')+' value="'+o.val+'"'+_cdata+' name="radio-'+_ts+'"'+_s+(disabled?' disabled="disabled"':'')+' title="'+o.text+'" /> '+o.text+'</label></div>';
				});
				_h+= '</div>';
				break;

				case 'checkgroup':
				_w = !!this._fEOpciones.columnaClase ? ' wrapper' : '';
				_h+= '<div class="list-group'+_w+'">';
				$.each(this._fEOpciones.data, function(i,o){
					_cdata = ''; if (o.data) { _cdata = THIS.getData(o.data); }
					_s = contiene(o.val, THIS._fEOpciones.valor) ? ' checked="checked"' : '';
					_w = !!THIS._fEOpciones.columnaClase ? ' col-'+THIS._fEOpciones.columnaClase : '';
					_h+= '<div class="list-group-item'+_w+'"><label><input type="checkbox"'+(classvalue!=undefined?' class="fieldeditor-'+classvalue+'"':'')+' value="'+o.val+'"'+_cdata+' name="checkbox-'+_ts+'"'+_s+(disabled?' disabled="disabled"':'')+' title="'+o.text+'" /> '+o.text+'</label></div>';
				});
				_h+= '</div>';
				break;				
			}
				
			return _h;
		}

		aeFieldEditor.prototype.clearAutoComplete = function(){
			this._fEAutoCompleteTable.html('');
		}

		aeFieldEditor.prototype.alertar = function(classvalue, message) {
			message = (message == '') ? '•••' : message;
			this._fEAlert.removeClass("alert warning danger info success").addClass('alert '+classvalue).text(message);
		}

		aeFieldEditor.prototype.cancelar = function() {
			this._fEViwer.show();
			this._fEControls.hide();
			this.cambiarIcono(false);
		}

		aeFieldEditor.prototype.editar = function() {
			this._fEViwer.hide();
			this._fEControls.show();
			this.alertar("alert", "");
		}

		aeFieldEditor.prototype.cambiarIcono = function(cargando) {
			if (cargando) {
				// Mostramos una animación para esperar la respuesta del servidor
				this._fESaveIcon.removeClass("fa-floppy-o");
				this._fESave.css({
					"background-image":"url('data:image/gif;base64,R0lGODlhEgASAKIEAEBAQH9/f7+/vwAAAP///wAAAAAAAAAAACH/C05FVFNDQVBFMi4wAwEAAAAh/wtYTVAgRGF0YVhNUDw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuNS1jMDE0IDc5LjE1MTQ4MSwgMjAxMy8wMy8xMy0xMjowOToxNSAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wTU09Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9tbS8iIHhtbG5zOnN0UmVmPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvc1R5cGUvUmVzb3VyY2VSZWYjIiB4bWxuczp4bXA9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC8iIHhtcE1NOk9yaWdpbmFsRG9jdW1lbnRJRD0ieG1wLmRpZDo2NTQzYmU3Yi00YTE1LWYxNDItOTIwMy0zYmE1ZWI5YmE0NWQiIHhtcE1NOkRvY3VtZW50SUQ9InhtcC5kaWQ6NERDNUUyODkyNzNDMTFFNTg3NTRCRDBFMkVGRDU4NkQiIHhtcE1NOkluc3RhbmNlSUQ9InhtcC5paWQ6NERDNUUyODgyNzNDMTFFNTg3NTRCRDBFMkVGRDU4NkQiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENDIChXaW5kb3dzKSI+IDx4bXBNTTpEZXJpdmVkRnJvbSBzdFJlZjppbnN0YW5jZUlEPSJ4bXAuaWlkOmViZDgxNGY5LTIzNzYtYjA0NC05NjA5LTZmMWU3MjI1M2IwYSIgc3RSZWY6ZG9jdW1lbnRJRD0ieG1wLmRpZDo2NTQzYmU3Yi00YTE1LWYxNDItOTIwMy0zYmE1ZWI5YmE0NWQiLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz4B//79/Pv6+fj39vX08/Lx8O/u7ezr6uno5+bl5OPi4eDf3t3c29rZ2NfW1dTT0tHQz87NzMvKycjHxsXEw8LBwL++vby7urm4t7a1tLOysbCvrq2sq6qpqKempaSjoqGgn56dnJuamZiXlpWUk5KRkI+OjYyLiomIh4aFhIOCgYB/fn18e3p5eHd2dXRzcnFwb25tbGtqaWhnZmVkY2JhYF9eXVxbWllYV1ZVVFNSUVBPTk1MS0pJSEdGRURDQkFAPz49PDs6OTg3NjU0MzIxMC8uLSwrKikoJyYlJCMiISAfHh0cGxoZGBcWFRQTEhEQDw4NDAsKCQgHBgUEAwIBAAAh+QQFCgAEACwAAAAAEgASAAADHUi63P4wykmrvVBo4TZfXrcxYVMqJzhibOu+MJYAACH5BAUKAAQALAIABwAJAAQAAAMLGBokLos9uNqL1iUAIfkEBQoABAAsAgAHAAkABAAAAwsIChQeiz242ovWJQAh+QQFCgAEACwCAAcADgAEAAADEjg6BA4kyLDYg5Ou9uKsnONJCQAh+QQFCgAEACwCAAcADgAEAAADDggKND5kSfbgXO3FS7VMACH5BAUKAAQALAIABwAOAAQAAAMSGBoEDmTIsdiDk6724qyc40kJACH5BAkKAAQALAIABwAOAAQAAAMSKCoUHgRIsNiDk6724qyc40kJACH5BAUKAAQALAAAAAASABIAAAMeSLrc/jDKSau9VWhBgg/Rxn0gJHZfuKEe5r5wLLsJADs=')",
					"background-position":"center center",
					"background-repeat":"no-repeat"
				});
			} else {
				// Regresamos el icono a su estado original
				this._fESaveIcon.addClass("fa-floppy-o");
				this._fESave.css({"background-image":""});
			}
		}

		aeFieldEditor.prototype.actualizar = function(valor, texto, datos) {

			// Almacenamos temporalmente en variables los valores elegidos por el usuario, pero no lo enviamos todavía
			switch (this._fEOpciones.tipo) {
				case 'autocomplete':
				$.extend(datos, {"valor":valor, "texto":texto});
				this._fEAutoCompleteInput.data(datos).val("");
				break;

				case 'boolean' :
				this._fEInterface.val(valor);
				if (datos!=undefined) { this._fEInterface.data(datos); }
				break;
				
				case 'list' :
				this._fEInterface.val(valor);
				break;
				
				case 'password' :
				this._fEInterface.val(valor);
				break;

				case 'radiogroup' :
				this._fEInterface.each(function(i,o){
					if ($(o).val() == valor) {
						$(o).prop({"checked":true});
						return false;
					}
				});
				break;

				case 'checkgroup' :
				this._fEInterface.each(function(i,o){
					$(o).prop({"checked":contiene($(o).val(), valor)});
				});
				break;
				
				default :
				this._fEInterface.val(valor);
			}

			this.procesar();
		}

		aeFieldEditor.prototype.guardar = function(valor, texto, datos, respuesta) {
			
			// Almacenamos temporalmente en variables los valores elegidos por el usuario, pero no lo enviamos todavía
			switch (this._fEOpciones.tipo) {
				case 'radiogroup' :
				this._fEValue.each(function(i,o){
					if ($(o).val() == valor) {
						$(o).prop({"checked":true});
						this._fEText.val($(o).prop("title"));
						return false;
					}
				});
				break;

				case 'checkgroup' :
				this._fEValue.each(function(i,o){
					$(o).prop({"checked":contiene($(o).val(), valor)});
				});
				this._fEText.val("Selección ("+valor.length+")");
				break;
				
				default :
				this._fEValue.val(valor);
				this._fEText.val(texto);
			}

			if (typeof(this._fEOpciones["onChange"]) === "function") {
				this._fEOpciones.onChange(valor, texto, datos, respuesta, this._fEOpciones.params);
			}
		}

		aeFieldEditor.prototype.valor = function() {
			
			_v = false;
			switch (this._fEOpciones.tipo) {
				case 'radiogroup' :
				this._fEInterface.each(function(i,o){
					if ($(o).prop("checked") == true) {
						_v = $(o).val();
						return false;
					}
				});
				break;

				case 'checkgroup' :
				_v = [];
				this._fEInterface.each(function(i,o){
					if ($(o).prop("checked") == true) {
						_v[_v.length] = $(o).val();
					}
				});
				break;
				
				default :
				_v = this._fEInterface.val();
			}
			return _v;
		}

		aeFieldEditor.prototype.procesar = function() {
			
			THIS = this;
			_cdata = false;
			_v = false;

			// Almacenamos temporalmente en variables los valores elegidos por el usuario, pero no lo enviamos todavía
			switch (this._fEOpciones.tipo) {
				case 'autocomplete':
				_cdata = this._fEAutoCompleteInput.data();
				_t = _cdata["texto"];
				_v = _cdata["valor"];
				break;

				case 'boolean' :
				this._fESelected = $('option:selected', this._fEInterface);
				_t = this._fESelected.text();
				_v = this._fESelected.val();			
				break;
				
				case 'list' :
				this._fESelected = $('option:selected', this._fEInterface);
				_t = this._fESelected.text();
				_v = this._fESelected.val();
				_cdata = this._fESelected.data();
				break;
				
				case 'password' :
				_v = this._fEInterface.val().toString();
				_t = new Array(_v.length+1).join("*");
				if (_v.length > 0) { _v = CryptoJS.MD5(_v).toString(); }
				break;

				case 'radiogroup' :
				_t = this._fEInterface.prop("title");
				this._fEInterface.each(function(i,o){
					if ($(o).prop("checked")) {
						_v = $(o).val();
						return false;
					}
				});
				break;

				case 'checkgroup' :
				_t = this._fEInterface.length;
				_v = [];
				this._fEInterface.each(function(i,o){
					if ($(o).prop("checked")) {
						_v.push($(o).val());
					}
				});
				break;
				
				default :
				_v = this._fEInterface.val();
				_t = this._fEInterface.val();
			}

			// Si el usuario no selecciona ningun valor usando la interface, cancelamos la operación
			if (!this._fEOpciones.vacio && (_v == 0 || _v == undefined || _v == '')) {

				_v = this._fEOpciones.valor;
				this.alertar("alert", "Sin cambios...");
				this.cancelar();

			} else {

				// Si el usuario definio la variable beforeSend, significa que debemos de realizar comprobaciones antes de enviar el campo
				VALIDO = (typeof(this._fEOpciones.beforeSend) === 'function') ? this._fEOpciones.beforeSend.apply(this, [this._fEInterface]) : true;
				
				// Si valido la función del usuario, continuamos
				if (VALIDO) {

					if (this._fEOpciones.guardarConfig) {

						// Como estamos llamando a un servidor, mostramos un preloader
						this.cambiarIcono(true);
						
						// Asignamos el valor que será enviado al servidor
						this._fEOpciones.guardarConfig.data[this._fEOpciones.guardarConfig.nombre] = _v;
						
						// Comprobamos los valores que serán enviados al botonEditar, y si detectamos objetos, asignamos el valor de dicho objeto a su respectiva variable
						$.each(this._fEOpciones.guardarConfig.data, function(i,o){
							if (typeof(o.val) === "function") {
								THIS._fEOpciones.guardarConfig.data[i] = o.val();
							} else if (typeof(o.valor) === "function") {
								THIS._fEOpciones.guardarConfig.data[i] = o.valor();
							}
						});

						$.ajax({
							"url":this._fEOpciones.guardarConfig.url, 
							"method":"POST", 
							"data":this._fEOpciones.guardarConfig.data, 
							"context":this,
							"dataType":'json',
							"success":function(respuesta){

								this.cambiarIcono(false);
									
								// Si el comando tuvo éxito mostramos un mensaje genérico
								if (respuesta.success) {
									
									this.guardar(_v, _t, _cdata, respuesta);
									this.alertar("alert success", respuesta.messages);
								
								// En caso de error, mostramos los mensages de ayuda
								} else {
									
									this.alertar("alert danger", respuesta.messages);

								}

								// Cerramos el modo editor
								this.cancelar();
							}
						});
						
					} else {

						this.guardar(_v, _t, _cdata);
						this.alertar("alert success", "Cambios guardados");
						
					}
				} else {

					this.alertar("alert danger", "Dato inválido");

				}
			}
		}

		aeFieldEditor.prototype.populate = function(opciones) {
			THIS = this;
			if (opciones != undefined) {
				this._fEOpciones = $.extend(this._fEOpciones, opciones);
			}

			// Eliminamos el contenido de los controles actuales
			this._fEViwer.children().remove();
			this._fEControls.children().remove();

			// Creamos la nueva Estructura
			switch (this._fEOpciones.tipo) {
				case 'text': case 'number' : case 'password' : case 'date' :
					
					if (this._fEOpciones.tipo == 'password') { this._fEOpciones.texto = '********'; }
					_h = '<div class="input-group">';
					_h+= THIS.getField(true, "text");
					_h+= '	<input type="hidden" class="fieldeditor-value" value="'+this._fEOpciones.texto+'" />';
					_h+= '	<a class="fieldeditor-edit addon"><i class="fa fa-fw fa-pencil"></i></a>';
					_h+= '</div>';
					$(_h).appendTo(this._fEViwer);

					_h = '<div class="input-group">';
					_h+= THIS.getField(false, "interface");
					_h+= '<a class="fieldeditor-save addon" title="Guardar"><i class="fa fa-fw fa-floppy-o"></i></a>';
					_h+= '<a class="fieldeditor-cancel addon" title="Cancelar"><i class="fa fa-fw fa-times"></i></a>';
					_h+= '</div>';
					$(_h).appendTo(this._fEControls);

				break;

				case 'textarea' : 
					if (this._fEOpciones.tipo == 'password') { this._fEOpciones.texto = '********'; }
					_h = '<div class="input-group">';
					_h+= THIS.getField(true, "text");
					_h+= '	<input type="hidden" class="fieldeditor-value" value="'+this._fEOpciones.texto+'" />';
					_h+= '	<a class="fieldeditor-edit addon"><i class="fa fa-fw fa-pencil"></i></a>';
					_h+= '</div>';
					$(_h).appendTo(this._fEViwer);

					_h = '<div class="input-group">';
					_h+= THIS.getTextarea(false, "interface");
					_h+= '<a class="fieldeditor-save addon" title="Guardar"><i class="fa fa-fw fa-floppy-o"></i></a>';
					_h+= '<a class="fieldeditor-cancel addon" title="Cancelar"><i class="fa fa-fw fa-times"></i></a>';
					_h+= '</div>';
					$(_h).appendTo(this._fEControls);
				break;

				case 'autocomplete':

					_h = '<div class="input-group">';
					_h+= '<input type="text" class="fieldeditor-text form-control" value="'+this._fEOpciones.texto+'" disabled="disabled" />';
					_h+= '<input type="hidden" class="fieldeditor-value" value="'+this._fEOpciones.valor+'" />';
					_h+= '<a class="fieldeditor-edit addon"><i class="fa fa-fw fa-pencil"></i></a>';
					_h+= '</div>';
					$(_h).appendTo(this._fEViwer);

					_h = '<div class="input-group">';
					_h+= '<input type="text" class="fieldeditor-autocomplete-input form-control" value="'+this._fEOpciones.texto+'" data-texto="'+this._fEOpciones.texto+'" data-valor="'+this._fEOpciones.valor+'" />';
					_h+= '<a class="fieldeditor-save addon" title="Guardar"><i class="fa fa-fw fa-floppy-o"></i></a>';
					_h+= '<a class="fieldeditor-cancel addon" title="Cancelar"><i class="fa fa-fw fa-times"></i></a>';
					_h+= '</div>';
					_h+= '<div class="fieldeditor-autocomplete">';
					_h+= '<table class="fieldeditor-autocomplete-table table table-striped table-condensed">';
					_h+= '</div>';
					_h+= '<tr><td>Buscando...</td></tr>';
					_h+= '</table>';
					$(_h).appendTo(this._fEControls);

					//Objetos particulares de AutoComplete
					this._fEAutoCompleteInput = $('input.fieldeditor-autocomplete-input', this._fEControls);
					this._fEAutoCompleteTable = $('table.fieldeditor-autocomplete-table', this._fEControls);
					this._fEAutoComplete = $('div.fieldeditor-autocomplete', this._fEControls).css({
						'position':'absolute',
						'z-index':1001
					}).hide();

				break;

				case 'list' :
					_h = '<div class="input-group">';
					_h+= '	<input type="text" class="fieldeditor-text form-control" value="'+this._fEOpciones.texto+'" disabled="disabled" />';
					_h+= '	<input type="hidden" class="fieldeditor-value" value="'+this._fEOpciones.valor+'" />';
					_h+= '	<a class="fieldeditor-edit addon"><i class="fa fa-fw fa-pencil"></i></a>';
					_h+= '</div>';
					$(_h).appendTo(this._fEViwer);
				
					_h = '<div class="input-group">';
					_h+= THIS.getList(false, "interface");
					_h+= '	<a class="fieldeditor-save addon" title="Guardar"><i class="fa fa-fw fa-floppy-o"></i></a>';
					_h+= '	<a class="fieldeditor-cancel addon" title="Cancelar"><i class="fa fa-fw fa-times"></i></a>';
					_h+= '</div>';
					$(_h).appendTo(this._fEControls);

				break;

				case 'checkgroup' :

					_h = '<div class="input-group">';
					_h+= '<input type="text" class="fieldeditor-text form-control" value="Selección ('+this._fEOpciones.valor.length+')" disabled="disabled" />';
					_h+= '<a class="fieldeditor-edit addon"><i class="fa fa-fw fa-pencil"></i></a>';
					_h+= '</div>';
					_h+= THIS.getList(true, "value");
					$(_h).appendTo(this._fEViwer);
				
					_h = '<div class="input-group">';
					_h+= '<input type="text" class="form-control" value="Selección ('+this._fEOpciones.valor.length+')" disabled="disabled" />';
					_h+= '<a class="fieldeditor-save addon" title="Guardar"><i class="fa fa-fw fa-floppy-o"></i></a>';
					_h+= '<a class="fieldeditor-cancel addon" title="Cancelar"><i class="fa fa-fw fa-times"></i></a>';
					_h+= '</div>';
					_h+= THIS.getList(false, "interface");
					$(_h).appendTo(this._fEControls);
				
				break;

				case 'radiogroup' :

					_h = '<div class="input-group">';
					_h+= '<input type="text" class="fieldeditor-text form-control" value="'+this._fEOpciones.texto+'" disabled="disabled" />';
					_h+= '<a class="fieldeditor-edit addon"><i class="fa fa-fw fa-pencil"></i></a>';
					_h+= '</div>';
					_h+= THIS.getList(true, "value");
					$(_h).appendTo(this._fEViwer);
				
					_h = '<div class="input-group">';
					_h+= '<input type="text" class="form-control" value="'+this._fEOpciones.texto+'" disabled="disabled" />';
					_h+= '<a class="fieldeditor-save addon" title="Guardar"><i class="fa fa-fw fa-floppy-o"></i></a>';
					_h+= '<a class="fieldeditor-cancel addon" title="Cancelar"><i class="fa fa-fw fa-times"></i></a>';
					_h+= '</div>';
					_h+= THIS.getList(false, "interface");
					$(_h).appendTo(this._fEControls);
				
				break;

				case 'boolean' :

					this._fEOpciones.data = [{"val":1, "text":"Sí"},{"val":0, "text":"No"}];

					_h = '<div class="input-group">';
					_h+= '	<input type="text" class="fieldeditor-text form-control" value="'+(this._fEOpciones.valor?this._fEOpciones.data[0].text:this._fEOpciones.data[1].text)+'" disabled="disabled" />';
					_h+= '	<input type="hidden" class="fieldeditor-value" value="'+this._fEOpciones.valor+'" />';
					_h+= '	<a class="fieldeditor-edit addon"><i class="fa fa-fw fa-pencil"></i></a>';
					_h+= '</div>';
					$(_h).appendTo(this._fEViwer);
				
					_h = '<div class="input-group">';
					_h+= THIS.getList(true, "interface");
					_h+= '	<a class="fieldeditor-save addon" title="Guardar"><i class="fa fa-fw fa-floppy-o"></i></a>';
					_h+= '	<a class="fieldeditor-cancel addon" title="Cancelar"><i class="fa fa-fw fa-times"></i></a>';
					_h+= '</div>';
					$(_h).appendTo(this._fEControls);
				
				break;
			}

			this._fEValue = $('input.fieldeditor-value', this._fEViwer);
			this._fEText = $('input.fieldeditor-text', this._fEViwer);
			this._fEEdit = $('a.fieldeditor-edit', this._fEViwer);

			this._fEInterface = $('.fieldeditor-interface', this._fEControls)
			this._fESave = $('a.fieldeditor-save', this._fEControls);
			this._fESaveIcon = $('i.fa', this._fESave);
			this._fECancel = $('a.fieldeditor-cancel', this._fEControls);

			this._fEEdit.bind('click', {"context":this}, function(e){
				e.data.context.editar();
			});

			this._fECancel.bind('click', {"context":this}, function(e){
				e.data.context.cancelar();
			})

			if (this._fEOpciones.abierto === false) {
				this._fECancel.trigger('click');
			}

			this._fESave.bind('click', {"context":this}, function(e) {
				e.data.context.procesar();
			});

			if (this._fEOpciones.tipo == 'date') {
				var myDP = new DatePicker(this._fEInterface);
				myDP.setDate(this._fEOpciones.valor);
			}

			if (this._fEAutoCompleteInput !== false) {
				
				this._fEAutoCompleteInput.bind("show", {"context":this}, function(e){

					e.data.context._fEAutoComplete.css({
						'width':e.data.context._fEAutoCompleteInput.outerWidth(),
						'top':e.data.context._fEAutoCompleteInput.outerHeight(),
						'left':0
					}).show();

				}).bind('keyup', {"context":this}, function(e){

					// Omitimos teclas de control, teclas de función y teclas de paginado
					if (contiene(e.which, [8,9,16,17,18,19,32,33,34,35,36,44,45,46,112,113,114,115,116,117,118,119,120,121,122,123,145])) { return false; }

					if ($(this).val().length >= e.data.context._fEOpciones.letrasMinimas) {

						$(this).trigger("show");

						// Cancelamos cualquier llamada mientras estemos escribiendo
						if (!isNaN(e.data.context._fEAutoCompleteInterval)) {
							clearInterval(e.data.context._fEAutoCompleteInterval);
						}
						
						// El usuario debe definir el nombre con el que será enviado el dato variable al servidor
						e.data.context._fEOpciones.autoCompleteConfig.data[e.data.context._fEOpciones.autoCompleteConfig.name] = $(this).val();
						
						// Hacemos la llamada al servidor
						e.data.context._fEAutoCompleteInterval = setTimeout(function(){

							$.ajax({
								"method":"post",
								"url":e.data.context._fEOpciones.autoCompleteConfig.url,
								"data":e.data.context._fEOpciones.autoCompleteConfig.data,
								"dataType":"json",
								"success":function(respuesta){

									clearAutoComplete();

									// Rellenamos la tabla con el contenido de la llamada al servidor
									$.each(respuesta.lista, function(indice, item){
										$('<tr>'+e.data.context._fEOpciones.autoCompleteConfig.render(item)+'</tr>').bind('click', {
											'item':item,
											"context":e.data.context
										}, function(e){

											e.data.context._fEAutoCompleteInput.data({
												"valor":e.data.item[e.data.context._fEOpciones.autoCompleteConfig.valor],
												"texto":e.data.item[e.data.context._fEOpciones.autoCompleteConfig.texto]
											}).val(e.data.item[e.data.context._fEOpciones.autoCompleteConfig.texto]);

											clearAutoComplete();

										}).appendTo(e.data.context._fEAutoCompleteTable);
									});
								}
							});

						}, 500);

					} else {

						e.data.context.clearAutoComplete();

					}

				}).bind("focus", function(e){

					$(this).trigger("show");

				});
			}
			return this;
		}

		aeFieldEditor.prototype.init = function() {

			THIS = this;

			/* Algunos controles necesitan posicionarse de manera absoluta con respecto del contenedor principal */
			this._fE.css({'position':'relative'}).addClass("this._fE");

			/* El visor es la parte visible que despliega la información inicial */
			this._fEViwer = $('<div class="fieldeditor-viwer"></div>').appendTo(this._fE);

			/* Los controles son los botones de guardar y cancelar, junto con la interfaz para editar los valores */
			this._fEControls = $('<div class="fieldeditor-controls"></div>').appendTo(this._fE);

			/* Los controles son los botones de guardar y cancelar, junto con la interfaz para editar los valores */
			this._fEAlert = $('<div class="fieldeditor-alert alert">•••</div>').appendTo(this._fE);

			this.populate();

		}
		
		window.aeFieldEditor = aeFieldEditor;

	})();
		
})(jQuery);