/* STRINGS ******************************************************************************************** */
(function($){

	var r, letter, result, niddle, counter, couple, integer, decimal, t, copy, chunks, pos, index;
	
	$.extend({
		'ALPHAS':"\u0030-\u0039\u0041-\u005A\u0061-\u007A\u00C0-\u00C5\u00C8-\u00CB\u00CC-\u00CF\u00E0-\u00E5\u00D2-\u00D6\u00F2-\u00F6\u00E8-\u00EB\u00EC-\u00EF\u00D9-\u00DC\u00F9-\u00FC\u00C7\u00D1\u00D8\u00E7\u00F1\u00F8\u00FF",
		'LATIN':"\u0041-\u005A\u0061-\u007A\u00C0-\u00C5\u00C8-\u00CB\u00CC-\u00CF\u00E0-\u00E5\u00D2-\u00D6\u00F2-\u00F6\u00E8-\u00EB\u00EC-\u00EF\u00D9-\u00DC\u00F9-\u00FC\u00C7\u00D1\u00D8\u00E7\u00F1\u00F8\u00FF",
		'EMAIL':"[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?",
		'URL':"^(https?|ftp):\/\/(((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:)*@)?(((\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5]))|((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?)(:\d*)?)(\/((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)+(\/(([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)*)*)?)?(\?((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|[\uE000-\uF8FF]|\/|\?)*)?(\#((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|\/|\?)*)?$",
		'TITLES':"0-9a-zA-ZÀÁÂÃÄÅàáâãäåÒÓÔÕÖØòóôõöøÈÉÊËèéêëÇçÌÍÎÏìíîïÙÚÛÜùúûüÿÑñ ",
		'ACCENTS':"ÀÁÂÃÄÅàáâãäåÒÓÔÕÖØòóôõöøÈÉÊËèéêëÇçÌÍÎÏìíîïÙÚÛÜùúûüÿÑñ",
		'NOACCENTS':"AAAAAAaaaaaaOOOOOOooooooEEEEeeeeCcIIIIiiiiUUUUuuuuyNn",
		'DATEISO':"^\d{4}[\/-]\d{1,2}[\/-]\d{1,2}$",
		'RFC':"^[a-zA-Z]{3,4}(\d{6})((\D|\d){3})?$",
		'LETTERS':"a-zA-Z",
		'DECIMALS':"0-9.",
		'NUMBERS':"0-9"
	});
			
	$.extend(String.prototype, {
		normalize:function() {
			copy = this.toString();
			letter = "";
			result = "";
			niddle = -1;
			counter = 0;
			for (counter=0; counter<copy.length; counter++) {
				letter = copy.substring(counter, counter+1);
				niddle = $.ACCENTS.indexOf(letter);
				result += (niddle == -1) ? letter:$.NOACCENTS.substring(niddle, niddle+1);
			}
			return result;
		},
		hiphenize:function() {
			return this.replace(/\s\s+/g, "-").replace(/\s+/g, "-");
		},
		title:function() {
			r = new RegExp("[^"+$.TITLES+"]", "g");
			return this.replace(r,'').toLowerCase().ucfirst();
		},
		ceo:function() {
			return $.trim(this).normalize().hiphenize().toLowerCase();
		},
		ucfirst:function(){
			return this.substr(0,1).toUpperCase() + this.substr(1).toLowerCase();
		},
		pad:function(len,pad,dir) {
			copy = this.toString();
			if (!pad) { pad = '0'; }
			if (!dir) { dir = 'LEFT'; }
			if (dir.toUpperCase() == 'LEFT') { while (copy.length < len) { copy = pad + copy; } }
			if (dir.toUpperCase() == 'RIGHT') { while (copy.length < len) { copy = copy + pad; } }
			return copy;
		},
		pos:function(needle, offset) {
			index = this.indexOf(needle, (offset || 0));
			return index === -1 ? false:index;
		},
		splitlen:function(len) {
			if (len === null) { len = 1; }
			copy = this.toString(); chunks = []; pos = 0;
			while (pos < copy.length) { chunks.push(copy.slice(pos, pos+=len)); }
			return chunks;
		},
		reverse:function() {
			return this.split('').reverse().join('');
		}
	});
})(jQuery);
/* NUMBERS ******************************************************************************************** */
(function($){

	var splitted, intPart, floatPart, csymbol;
			
	$.extend(Number.prototype, {
		monefy:function(declen, decchar, separator, ccode) {
			
			/* Separate decimal part from integer */
			splitted = this.toString().split('.');
			intPart = splitted[0];
			floatPart = (splitted.length == 2) ? splitted[1]:'';
			
			/* Setup the output */
			decchar = !decchar ? '.':decchar;
			separator = !separator ? ',':separator;
			ccode = !ccode ? '':' ' + ccode;
			
			// Currency Symbol
			switch ($.trim(ccode.toUpperCase())) {
				case 'EUR':csymbol = '€'; break;
				case 'JPY':csymbol = '¥'; break;
				case 'GBP':csymbol = '£'; break;
				default:csymbol = '$';
			}
			csymbol = ' ' + csymbol;
			
			/* Configura the decimal part if needed */
			floatPart = !declen ? '':decchar + floatPart.substr(0,declen).pad(declen,'0','RIGHT');
			/* Separate the thousand */
			intPart = intPart.reverse().replace(/(\d{3})(?=\d)/g, "$1" + separator).reverse();

			return csymbol + intPart + floatPart + ccode;
		}
	});
})(jQuery);