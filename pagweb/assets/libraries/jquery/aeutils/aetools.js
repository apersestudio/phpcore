(function ($) {

	// Algunas veces queremos mantener una referencia al constructor,
	// Particularmente cuando estamos dentro de un bucle o dentro de una función anónima
	var THIS;

	$.fn.extend({
		/* //////////////////////////////////////////////////////// //////////////////////////////////////////////////////// */
		'aeBridge': function (options) {
			THIS = this;
			this.element = $(this);
			this.options = $.extend({
				'data':{},
				'passwords':false,
				'encryptFields':true,
				'encryptForm':false
			}, options);
			this.firsttime = true;
			this.formData = false;
			this.formIndex = new Date().getTime();
			this.iframeName = 'formBridgeIframe'+this.formIndex;
			this.container = $('<div></div>').css({
				"position": "absolute",
				"overflow": "hidden",
				"width": 0,
				"height": 0
			}).insertBefore(this);

			// Datos adicionales a ser enviados con el formulario
			$.each(this.options.data, function(name, value){
				$('<input type="hidden" name="'+name+'" value="'+value+'" />').appendTo(THIS.element);
			});

			this.iframe = $('<iframe name="'+this.iframeName+'"></iframe>').bind('load', {'parent':this}, function(e){
				e.data.parent.onData();
			}).appendTo(this.container);
			
			this.form = $('<form></form>').prop({
				'enctype':'multipart/form-data',
				'accept-charset':'utf-8',
				'method':'post',
				'action':this.options.action,
				'target':this.iframeName
			}).appendTo(this.container);
			this.getBody = function() {
				return this.iframe.contents().find('body');
			}
			this.flush = function() {
				this.getBody().text('');
				$(':input', this.form).remove();
			}
			this.submitData = function() {
				this.element.trigger("beforesend.aebridge", [this.formData]);

				// Si algún password esta en modalidad de visible, lo cambiamos
				if (this.options.passwords) {
					$.each(this.options.passwords, function(pIndex, pItem){
						pItem.close();
					});
				}
				// Clonamos todos los campos que serán enviados
				$(':input', this).clone().appendTo(this.form);

				// Encriptamos todos los campos de password
				if (this.options.encryptFields) {
					$(this.options.encryptFields, this.form).each(function(passFieldIndex, passFieldItem){
						$(passFieldItem).val(CryptoJS.MD5($(passFieldItem).val()));
					});
				}
				
				// Si la encriptación para todo el formulario fué activada
				if (this.options.encryptForm) {
					$(':input', this.form).not('input[type=file]').prop({'disabled':true});
					$('<textarea name="encrypted">'+CryptoJS.AES.encrypt(this.form.serialize(), this.options.token)+'</textarea>').appendTo(this.form);
				}
				
				// Si el formulario tiene que ser validado
				if (this.hasClass('validate') && this.hasClass('invalid')) { return false; }

				// Enviamos los datos
				this.form.submit();
			}
			this.onData = function(){
				if (!this.firsttime) {
					this.formData = $.parseJSON(this.getBody().text());
					this.element.trigger("response.aebridge", [this.formData]);
				}
				this.flush();
				this.firsttime = false;
			}
			// Submit
			this.element.bind('submit', {'parent':this}, function (e) {
				e.preventDefault();
				e.data.parent.flush();
				e.data.parent.submitData();
			});
			return this;
		},
		/* //////////////////////////////////////////////////////// //////////////////////////////////////////////////////// */
		'aePassword': function(options) {
			this.element = $(this);
			this.visible = false;
			this.passTypes = [
				"-~^$%&/()¿?¡!=+.,{}[]", // symbols
				'ÁÉÍÓÚABCDEFGHIJKLMNÑOPQRSTUVWXYZ', // uppercase
				'áéíóúabcdefghijklmnñopqrstuvwxyz', // lowercase
				'0123456789' //numbers
			];
			this.passField=null;
			this.passCounter=null;
			this.passIndex=null;
			this.passChar=null;
			this.passPos=null;
			this.passRandom=null;
			this.passAttributes=null;

			this.getRandomInt = function(min, max) {
				return Math.floor(Math.random() * (max - min + 1)) + min;
			}
			this.toggle = function() {
				this.visible = !this.visible;
				try {
					this.element.prop({"type":(this.visible?"text":"password")});
				} catch (err) {
					alert("Función no disponible en tu navegador.");
				}
				return this.element;
			}
			this.close = function() {
				this.visible = true;
				this.toggle();
			}
			this.generate = function(){
				this.passRandom=[];
				for (this.passCounter=0; this.passCounter<16; this.passCounter++) {
					this.passPos = this.passCounter % this.passTypes.length;
					this.passIndex = this.getRandomInt(0, this.passTypes[this.passPos].length-1);
					this.passChar = this.passTypes[this.passPos].substr(this.passIndex,1);
					this.passRandom[this.passRandom.length] = this.passChar;
				}
				this.element.val(this.passRandom.join(""));
			}
			return this;
		},
	});
	
})(jQuery);