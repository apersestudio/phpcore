(function($){

	// El plugin se ejectua cuando el DOM esta listo
	$(function(e){

		var AvoidKeys = [
			16, //shift
			17, //Control
			32, //barra espaciadora
			33, //Retroceder pagina
			34, //Avanzar pagina
			35, //inicio
			36, //fin
			37, //atras
			38, //arriba
			39, //adelante
			40, //abajo
			45 //insert
		];

		var ForceKeys = [
			8, //backspace
			46 //suprimir
		];
		
		var THIS, Contenedor, Lista, Valor;
	
		function dibujarAutocompletador(padre, campo, lista, datos) {
			// Limpiamos el contenedor
			lista.html('');

			codigo = '<table class="table">';
			if (datos.success) {
				for (i=0; i<datos.lista.length; i++) {
					codigo+= '<tr>';
					codigo+= padre.options.render(datos.lista[i]);
					codigo+= '</tr>';
				}
			} else {
				codigo+= '<tr>';
				codigo+= '<td>'+datos.messages+'</td>';
				codigo+= '</tr>';
			}
			codigo+= '</table></div>';
			visor = $(codigo);

			if (datos.success) {
				$('td', visor).each(function(i,o){
					$(this).bind('click', {'lista':lista, 'indice':i}, function(e){
						padre.options.handler.apply(padre.options.context, [datos.lista[e.data.indice]]);
						campo.val('');
						e.data.lista.html('');
					});
				});
			}

			lista.css({
				'position':'absolute',
				'width':padre.outerWidth(),
				'top':padre.outerHeight(),
				'left':0
			});

			visor.appendTo(lista);
		}

		function realizarBusqueda(campo, padre, lista) {
			clearTimeout(padre.options.llamadaAnterior);
			padre.options.llamadaAnterior = setTimeout(function(){
				Valor = campo.val();
				if (Valor.length >= 3) {
					// Si ya había una llamada previa, la cancelamos
					if (padre.options.llamadaServidor) { padre.options.llamadaServidor.abort(); }
					// Asignamos el valor que será enviado al manejador de datos
					padre.options.data[padre.options.name] = Valor;
					// Buscamos el datos en el controlador enviado por el usuario
					padre.options.llamadaServidor = $.post(padre.options.url, padre.options.data, function(respuesta){
						dibujarAutocompletador(padre, campo, lista, respuesta);
					}, 'json');

				}
			}, 300);
		}
		
		// Definición del plugin
		$.fn.aeAutoComplete = function(options) {
		
			// Preparamos al elemento que llama al componente
			return this.each(function(index, item) {

				THIS = $(this);
				THIS.options = options;
				THIS.options.llamadaAnterior = false;
				THIS.options.llamadaServidor = false;
			
				Contenedor = $('<div class="autocomplete"></div>').insertBefore(THIS);
				Contenedor.css({
					'position':'relative',
					'z-index':'1000'
				});
				THIS.appendTo(Contenedor);
				Lista = $('<div class="autocomplete-list"></div>').appendTo(Contenedor);

				THIS.bind('keyup', {'padre':THIS, 'lista':Lista}, function(e){
					// Si el valor es menor que cero significa que no se encuentra en la lista
					if (AvoidKeys.indexOf(e.which)<0) {
						switch(e.which) {
							// ESCAPE -> Cancelar operación
							case 27 : e.data.lista.html(''); break;
							// En todos los demás casos realizamos la busqueda
							default : realizarBusqueda($(this), e.data.padre, e.data.lista);
						}
					}
				});
				
			});
			
		};
		
	});
})(jQuery);