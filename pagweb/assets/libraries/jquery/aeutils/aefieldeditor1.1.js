(function($){

	// El plugin se ejectua cuando el DOM esta listo
	$(function(e){
	
		var THIS;
		var html;
		var editor;
		var control;
		var guardar;
		var cancelar;
		
		var ajax;
		var hijos;
		var objetivo;
		var valor;
		var texto;
		var oculto;
		var manejador;
	
		function prepararEditor(THIS) {
			
			html = '<div class="input-group">';
			
			if (THIS.options.title) {
				html+= '<span class="addon">'+THIS.options.title+'</span>';
			}
			
			switch (THIS.options.type) {
				case 'number' :
				html+= '<input type="text" class="form-control interface-control" name="control" placeholder="Escribe un numero" />';
				break;
				case 'boolean' :
				html+= '<div class="special-field">';
				html+= '<label><input type="radio" class="interface-control" name="control" value="true" data-text="SI" /> <span>SI</span></label>';
				html+= '<label><input type="radio" class="interface-control" name="control" value="false" data-text="NO" /> <span>NO</span></label>';
				html+= '</div>';
				break;
				case 'list' :
				html+= '<select class="form-control interface-control" name="control">';
				$.each(THIS.options.data, function(i,o){
					html+= '<option value="'+o.val+'">'+o.text+'</option>';
				})
				html+= '</select>';
				break;
			}
			html+= '<a class="addon interface-save"><i class="fa fa-fw fa-floppy-o"></i></a>';
			html+= '<a class="addon interface-cancel"><i class="fa fa-fw fa-times"></i></a>';
			html+= '</div>';
			
			return $(html).appendTo(THIS);
		}
		
		// Definición del plugin
		$.fn.aeFieldEditor = function(options){
		
			// Preparamos al elemento que llama al componente
			return this.each(function(index, item) {
			
				THIS = $(this);
				THIS.options = options;
				
				oculto = $(THIS.options.hidden, THIS);
				objetivo = $(THIS.options.target, THIS);
				manejador = $(THIS.options.handler, THIS);
				
				hijos = THIS.children();
			
				// Preparamos la estructura del componente
				editor = prepararEditor(THIS);
				
				control = $('.interface-control', editor);
				guardar = $('.interface-save', editor);
				cancelar = $('.interface-cancel', editor);
				editor.hide();
				
				// Cuando el usuario presiona el botón de guardar
				guardar.bind('click', {'padre':THIS, 'manejador':manejador, 'objetivo':objetivo, 'oculto':oculto, 'control':control}, function(e){
					ajax = e.data.padre.options.ajax;
					if (ajax) {
					
						// Como estamos llamando a un servidor, mostramos un preloader
						$('i', this).removeClass('fa-floppy-o').addClass('fa-preloader');
						
						// Agregamos el valor que eleigió el usuario a la lista de variables enviadas al servidor
						switch (e.data.padre.options.type) {
							case 'boolean' :
							$.each(e.data.control, function(i,o){
								if ($(o).prop('checked')) {
									valor = $(o).val();
									texto = $(o).data('text');
									return false;
								}
							});
							break;
							
							case 'list' :
							texto = $('option:selected', e.data.control).text();
							valor = e.data.control.val();
							break;
							
							default :
							valor = e.data.control.val();
							texto = valor;
						}
						ajax.data[ajax.name] = valor;
						
						// Iniciamos la llamada al servidor
						$.when(
							[{
								'objetivo':e.data.objetivo,
								'oculto':e.data.oculto,
								'valor':valor,
								'texto':texto,
								'icon':$('i', this)
							}],
							$.post(ajax.url, ajax.data, 'json')
						).then(function(p, r){
							p = p[0];
							r = r[0];
							
							// Regresamos el icono a su estado original
							p.icon.removeClass('fa-preloader').addClass('fa-floppy-o');
							delete(p['icon']);
							
							// Cerramos el modo editor
							e.data.manejador.trigger('cancel');
							
							// Notificamos la respuesta al invocador
							if (r.success) {
								e.data.padre.trigger('edited', [p, r.data]);
							} else {
								e.data.padre.trigger('error', [p]);
							}
							
						});
						
					} else {
						e.data.padre.trigger('edited', [e.data.objetivo, e.data.control.val()]);
						e.data.manejador.trigger('cancel');
					}
				});
				
				// Cuando el usuario presiona el botón de cancelar
				cancelar.bind('click', {'manejador':manejador}, function(e){
					e.data.manejador.trigger('cancel');
				});
				
				manejador.bind('click', {'tipo':THIS.options.type, 'editor':editor, 'hijos':hijos, 'control':control, 'objetivo':objetivo, 'oculto':oculto}, function(e){
				
					e.data.hijos.hide();
					e.data.editor.show();
					
					if (e.data.tipo == 'number') {
						e.data.control.val(e.data.oculto.val());
					// List | Boolean
					} else {
						e.data.control.val([e.data.oculto.val()]);
					}
					
					// Centramos el foco en los input o select
					if (e.data.tipo == 'number' || e.data.tipo == 'list') {
						e.data.control.focus();
						if (e.data.tipo == 'list') {
							e.data.control.select();
						}
					}
					
				}).bind('cancel', {'editor':editor, 'hijos':hijos}, function(e){
					
					e.data.hijos.show();
					e.data.editor.hide();
					
				});
				
			});
			
		};
		
	});
})(jQuery);