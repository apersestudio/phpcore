(function($){
	$.fn.aeEditable = function(options) {
		
		var THIS, container, button, icon, mode;
		
		function cancelEditing(input, icon, button, value) {
			input.prop({'readonly':true}).unbind().val(value);
			icon.removeClass('fa-floppy-o').addClass('fa-pencil');
			button.data('mode', 'edit');
		}
		
		function sendValue(input, icon, button, value) {
			if (input.val() != value) {
				icon.removeClass('fa-floppy-o').addClass('fa-preloader');
				button.data('mode', 'load');
				input.prop({'readonly':true});
				$.when(
					[{'input':input, 'icon':icon, 'button':button}],
					$.post(input.data('handler'), $.extend({'value':input.val()}, input.data('data')), false, 'json')
				).then(function(p,r){
					p = p[0];
					r = r[0];
					p.icon.removeClass('fa-preloader').addClass('fa-pencil');
					p.input.prop({'readonly':true});
					p.button.data('mode', 'edit');
					
					// En caso de éxito, activamos el evento de "Editado"
					if (r.success) {
						p.input.trigger('edited', r);
					} else {
						cancelEditing(input, icon, button, value)
					}
				});
			} else {
				cancelEditing(input, icon, button, value);
			}
		}

        return this.each(function() {
			
			THIS = $(this);
			
			THIS.data(options);
			
			container = $('<div class="input-group"></div>').insertBefore(THIS);
			THIS.appendTo(container);
			
			button = $('<a class="addon"></a>').appendTo(container);
			icon = $('<i class="fa fa-fw fa-pencil"></i>').appendTo(button);
			
			//<i class="fa fa-fw fa-preloader"></i>
			
			button.data({'mode':'edit'}).bind('click', {'icon':icon, 'input':THIS}, function(e){
				mode = $(this).data('mode');
				
				switch (mode) {
					case 'edit' :
					
						// Habilitamos el campo de texto
						e.data.input.prop({'readonly':false}).unbind('keyup').bind('keyup', {'button':$(this), 'icon':e.data.icon, 'initvalue':e.data.input.val()}, function(e){
							switch (e.which) {
								// Escape
								case 27 : 
								cancelEditing($(this), e.data.icon, e.data.button, e.data.initvalue);
								break;
								
								// Enter
								case 13 :
								sendValue($(this), e.data.icon, e.data.button, e.data.initvalue);
								break;
							}
						}).setPosition().focus();
						e.data.icon.removeClass('fa-pencil').addClass('fa-floppy-o');
						$(this).data('mode', 'save');
						
					break;
				}
				
			});
			
		});
	}
})(jQuery);