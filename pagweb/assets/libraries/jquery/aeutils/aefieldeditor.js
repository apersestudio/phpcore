(function($){

	// El plugin se ejectua cuando el DOM esta listo
	$(function(e){
	
		var letrasMinimas=3;
		var codigo;
		var visor;
		var visorContenedor=false;
		var llamadaServidor=false;
		var i;

		var THIS;
		var html;
		var grupo;
		var editor;
		var control;
		var guardar;
		var cancelar;
		var grupoDeCampos;
		var unidad;
		
		var id;
		var alerta;
		
		var ajax;
		var hijos;
		var objetivo;
		var valor;
		var texto;
		var oculto;
		var manejador;
		var borrar;
		var valido;
		var customData;

		function dibujarAutocompletador(objetivo, contenedor, ajax, datos) {
			// Limpiamos el contenedor
			contenedor.html('');

			codigo = '<table class="table">';
			for (i=0; i<datos.lista.length; i++) {
				codigo+= '<tr>';
				codigo+= ajax.render(datos.lista[i]);
				codigo+= '</tr>';
			}
			codigo+= '</table></div>';
			visor = $(codigo);

			$('td', visor).each(function(i,o){
				$(this).bind('click', {'objetivo':objetivo, 'contenedor':contenedor, 'indice':i}, function(e){
					e.data.objetivo.data(datos.lista[e.data.indice]);
					e.data.objetivo.val(datos.lista[e.data.indice][ajax.texto]);
					e.data.contenedor.html('');
				});
			});

			contenedor.css({
				'width':objetivo.outerWidth(),
				'top':objetivo.outerHeight(),
				'left':0
			});

			visor.appendTo(contenedor);
			//console.log(objetivo);
			//console.log(datos);
		}
	
		function prepararEditor(THIS) {
			
			html = '<div class="input-group">';
			
			if (THIS.options.titulo) {
				html+= '<span class="addon addon-title">'+THIS.options.titulo+'</span>';
			}
			
			switch (THIS.options.type) {
				case 'password' :
				html+= '<input type="password" class="form-control interface-control" name="control" placeholder="Escribe una nueva contraseña." />';
				break;
				
				case 'text' : case 'autocomplete' :
				html+= '<input type="text" class="form-control interface-control" name="control" placeholder="Escribe aquí." />';
				break;
				
				case 'textarea' :
				html+= '<textarea class="form-control interface-control" name="control">Escribe aquí.</textarea>';
				break;
				
				case 'number' : case 'numericstepper' :
				html+= '<input type="text" class="form-control interface-control" name="control" placeholder="Escribe un numero" />';
				break;
				
				case 'boolean' :
				html+= '<div class="special-field">';
				html+= '<label><input type="radio" class="interface-control" name="control" value="true" data-text="SI" /> <span>SI</span></label>';
				html+= '<label><input type="radio" class="interface-control" name="control" value="false" data-text="NO" /> <span>NO</span></label>';
				html+= '</div>';
				break;
				
				case 'list' :
				html+= '<select class="form-control interface-control" name="control">';
				$.each(THIS.options.data, function(i,o){
					customData = '';
					if (o.data){
						$.each(o.data, function(prop, value){
							customData += ' data-'+prop+'="'+value+'"';
						});
					}
					html+= '<option value="'+o.val+'"'+customData+'>'+o.text+'</option>';
				})
				html+= '</select>';
				break;
			}
			if (THIS.options.type == 'numericstepper') {
				html+= '<a class="addon interface-less"><i class="fa fa-fw fa-minus"></i></a>';
				html+= '<a class="addon interface-more"><i class="fa fa-fw fa-plus"></i></a>';	
			}
			html+= '<a class="addon interface-save"><i class="fa fa-fw fa-floppy-o"></i></a>';
			html+= '<a class="addon interface-cancel"><i class="fa fa-fw fa-times"></i></a>';
			html+= '</div>';
			
			return $(html).prependTo(THIS);
		}
		
		// Definición del plugin
		$.fn.aeFieldEditor = function(options){
		
			// Preparamos al elemento que llama al componente
			return this.each(function(index, item) {
			
				THIS = $(this);
				THIS.options = options;
				THIS.css({'position':'relative'});
				
				if (THIS.options.modo == 'manual') {
					oculto = $(THIS.options.valorCampo, THIS);
					objetivo = $(THIS.options.textoCampo, THIS);
					manejador = $(THIS.options.editarBoton, THIS);
					borrar = $(THIS.options.borrarBoton, THIS);
				} else {
					id = THIS.prop('id');
					oculto = $('input.'+id+'-valor', THIS);
					objetivo = $('input.'+id+'-texto', THIS);
					manejador = $('a.'+id+'-editar', THIS);
					borrar = $('a.'+id+'-borrar', THIS);
				}

				if (borrar.length>0) {
					borrar.bind('click', {'ajax':THIS.options.borrar}, function(e){
						$.when(
							[{"fila":e.data.ajax.fila}],
							$.post(e.data.ajax.url, e.data.ajax.data, false, "json")
						).done(function(params,respuesta){
							params = params[0];
							respuesta = respuesta[0];
							if (respuesta.success) {
								params.fila.remove();
							} else {
								alert(respuesta.messages);
							}
						});
					});
				}

				if (THIS.options.titulo != undefined) {
					grupoDeCampos = $('.input-group', THIS);
					$('<span class="addon addon-title">'+THIS.options.titulo+'</span>').prependTo(grupoDeCampos);
				}
				
				if (THIS.options.texto != undefined) {
					unidad = (THIS.options.unidad == undefined || THIS.options.unidad == 'number') ? '' : THIS.options.unidad;
					objetivo.val(THIS.options.texto+' '+unidad);
				}
				if (THIS.options.valor != undefined) {
					oculto.val(THIS.options.valor);
				}

				hijos = THIS.children(':not(.alert)');
			
				// Preparamos la estructura del componente
				editor = prepararEditor(THIS);
				
				// Preparamos el contenedor para todas las alertas del componente
				alerta = $('<div class="alert">•••</div>').appendTo(THIS);
				
				control = $('.interface-control', editor);
				guardar = $('.interface-save', editor);
				cancelar = $('.interface-cancel', editor);
				editor.hide();
				
				THIS.bind('alert', {'alerta':alerta}, function(e, message, type){
					e.data.alerta.removeClass().addClass('alert '+type).html(message);
				});

				switch (THIS.options.type) {
					case 'autocomplete' :
						visorContenedor = $('<div id="autocomplete"></div>').appendTo(THIS);
						visorContenedor.css({'position':'absolute', 'z-index':1001});
						control.bind('keyup', {'ajax':THIS.options.controlador, 'objetivo':control, 'contenedor':visorContenedor}, function(e){
							valor = $(this).val();
							if (valor.length >= letrasMinimas) {
								// Cancelamos cualquier llamada mientras estemos escribiendo
								if (llamadaServidor) { llamadaServidor.abort(); }
								
								// Anexamos a la lista de parametros, el generado con nuestra escritura
								e.data.ajax.data[e.data.ajax.name] = valor;
								
								// Buscamos el datos en el controlador enviado por el usuario
								llamadaServidor = $.post(e.data.ajax.url, e.data.ajax.data, function(respuesta){
									// Dibujamos los contenidos del manejador
									dibujarAutocompletador(e.data.objetivo, e.data.contenedor, e.data.ajax, respuesta);
								}, 'json');

							}
						});
					break;

					case 'numericstepper' :
						//control.bind('keyup')
					break;
				}
				
				// Cuando el usuario presiona el botón de guardar
				guardar.bind('click', {'padre':THIS, 'manejador':manejador, 'objetivo':objetivo, 'oculto':oculto, 'control':control, 'alerta':alerta}, function(e){
				
					// Mostramos mensaje al usuario
					e.data.alerta.removeClass().addClass('alert').html('Procesando...');
				
					// Si el usuario definio la variable beforeSend, significa que debemos de realizar comprobaciones antes de enviar el campo
					valido = (typeof(e.data.padre.options.beforeSend) === 'function') ? e.data.padre.options.beforeSend.apply(e.data.control, [e.data.padre]) : true;
					
					// Si valido la función del usuario, continuamos
					if (valido) {
					
						ajax = e.data.padre.options.ajax;
						if (ajax) {
						
							// Como estamos llamando a un servidor, mostramos un preloader
							$('i', this).removeClass('fa-floppy-o').addClass('fa-preloader');
							
							// Agregamos el valor que eleigió el usuario a la lista de variables enviadas al servidor
							switch (e.data.padre.options.type) {
								case 'autocomplete':
								texto = e.data.control.val();
								valor = e.data.control.data(e.data.padre.options.controlador.valor);
								break;

								case 'boolean' :
								$.each(e.data.control, function(i,o){
									if ($(o).prop('checked')) {
										valor = $(o).val();
										texto = $(o).data('text');
										return false;
									}
								});
								break;
								
								case 'list' :
								texto = $('option:selected', e.data.control).text();
								valor = e.data.control.val();
								break;
								
								case 'password' :
								valor = e.data.control.val();
								texto = valor;
								// Convert to MD5
								if (valor.length > 0) {
									valor = CryptoJS.MD5(valor);
									valor = valor.toString(CryptoJS.enc.Hex);
								}
								break;
								
								default :
								valor = e.data.control.val();
								unidad = (e.data.padre.options.unidad == undefined || e.data.padre.options.unidad == 'number') ? '' : e.data.padre.options.unidad;
								texto = valor+' '+unidad;
							}

							// Si el usuario no modifica el valor original pero da click en guardar, restauramos el valor inicial
							if (valor == undefined) { valor = e.data.padre.options.valor; }

							// Asignamos el valor que será enviado al servidor
							ajax.data[ajax.name] = valor;
							
							// Comprobamos los valores que serán enviados al manejador, y si detectamos objetos, asignamos el valor de dicho objeto a su respectiva variable
							$.each(ajax.data, function(i,o){
								if (typeof(o) == 'object') {
									ajax.data[i] = o.val();
								}
							});						
							
							// Iniciamos la llamada al servidor
							$.when(
								[{
									'alerta':e.data.alerta,
									'padre':e.data.padre,
									'objetivo':e.data.objetivo,
									'oculto':e.data.oculto,
									'valor':valor,
									'texto':texto,
									'icon':$('i', this)
								}],
								$.post(ajax.url, ajax.data, 'json')
							).then(function(p, r){
								p = p[0];
								r = r[0];
								
								// Regresamos el icono a su estado original
								p.icon.removeClass('fa-preloader').addClass('fa-floppy-o');
								delete(p['icon']);
								
								// Cerramos el modo editor
								e.data.manejador.trigger('cancel');
								
								// Si el comando tuvo éxito mostramos un mensaje genérico
								if (r.success) {
									
									p.alerta.removeClass().addClass('alert success').html('Cambios guardados.');
									p.objetivo.val(p.texto);
									p.oculto.val(p.valor);
									p.padre.trigger('edited', [p.objetivo, p.valor]);
								
								// En caso de error, mostramos los mensages de ayuda
								} else {
									
									p.alerta.removeClass().addClass('alert danger');
									if (typeof(r.messages.join) === 'function') {
										p.alerta.html(r.messages.join('<br />'))
									} else {
										p.alerta.html(r.messages);
									}
								}
								
							});
							
						} else {
							e.data.padre.trigger('edited', [e.data.objetivo, e.data.control.val()]);
							e.data.manejador.trigger('cancel');
						}
					}
					
				});
				
				// Cuando el usuario presiona el botón de cancelar
				cancelar.bind('click', {'manejador':manejador, 'alerta':alerta, 'tipo':THIS.options.type, 'contenedor':visorContenedor}, function(e){
					e.data.manejador.trigger('cancel');
					
					// Vaciamos el contenido de la alerta
					e.data.alerta.removeClass().addClass('alert').html('•••');

					if (e.data.tipo == 'autocomplete') { e.data.contenedor.html(''); }
				});
				
				manejador.bind('click', {'tipo':THIS.options.type, 'editor':editor, 'hijos':hijos, 'control':control, 'objetivo':objetivo, 'oculto':oculto, 'alerta':alerta}, function(e){
				
					// Vaciamos el contenido de la alerta
					e.data.alerta.removeClass().addClass('alert').html('•••');
				
					// Reestablecemos el aspecto del control
					e.data.hijos.hide();
					e.data.editor.show();
					
					if (e.data.tipo == 'autocomplete') {
						e.data.control.val(e.data.objetivo.val());
					} else if (e.data.tipo == 'number' || e.data.tipo == 'password') {
						e.data.control.val(e.data.oculto.val());
					// List | Boolean
					} else {
						e.data.control.val([e.data.oculto.val()]);
					}
					
					// Centramos el foco en los input o select
					if (e.data.tipo == 'list') { e.data.control.select(); } else { e.data.control.focus(); }
					
				}).bind('cancel', {'editor':editor, 'hijos':hijos}, function(e){
					
					e.data.hijos.show();
					e.data.editor.hide();
					
				});				
				
			});
			
		};
		
	});
})(jQuery);