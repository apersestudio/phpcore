(function($){

	// Para optimizar el tiempo de respuesta del plugin, creamos algunas funciones fuera del constructor, 
	// ya que no hay necesidad de que las instancias tengan un duplicado de los controles
	// Pero primero nos aseguramos que el cuerpo del documento este cargado, 
	// ya que de lo contrario el contenedor del plugin no se incerta en el cuerpo del documento.
	$(document).ready(function(){
	
		var THIS, container, overlay, iconsl, search, searchText, target, screenW, screenH, containerW, containerH, lastCall, xPos, yPos, isHidden;
		var fa = ['glass','music','search','envelope-o','heart','star','star-o','user','film','th-large','th','th-list','check','times','search-plus','search-minus','power-off','signal','gear','trash-o','home','file-o','clock-o','road','download','arrow-circle-o-down','arrow-circle-o-up','inbox','play-circle-o','rotate-right','refresh','list-alt','lock','flag','headphones','volume-off','volume-down','volume-up','qrcode','barcode','tag','tags','book','bookmark','print','camera','font','bold','italic','text-height','text-width','align-left','align-center','align-right','align-justify','list','dedent','indent','video-camera','photo','pencil','map-marker','adjust','tint','edit','share-square-o','check-square-o','arrows','step-backward','fast-backward','backward','play','pause','stop','forward','fast-forward','step-forward','eject','chevron-left','chevron-right','plus-circle','minus-circle','times-circle','check-circle','question-circle','info-circle','crosshairs','times-circle-o','check-circle-o','ban','arrow-left','arrow-right','arrow-up','arrow-down','mail-forward','expand','compress','plus','minus','asterisk','exclamation-circle','gift','leaf','fire','eye','eye-slash','warning','plane','calendar','random','comment','magnet','chevron-up','chevron-down','retweet','shopping-cart','folder','folder-open','arrows-v','arrows-h','bar-chart-o','twitter-square','facebook-square','camera-retro','key','gears','comments','thumbs-o-up','thumbs-o-down','star-half','heart-o','sign-out','linkedin-square','thumb-tack','external-link','sign-in','trophy','github-square','upload','lemon-o','phone','square-o','bookmark-o','phone-square','twitter','facebook','github','unlock','credit-card','rss','hdd-o','bullhorn','bell','certificate','hand-o-right','hand-o-left','hand-o-up','hand-o-down','arrow-circle-left','arrow-circle-right','arrow-circle-up','arrow-circle-down','globe','wrench','tasks','filter','briefcase','arrows-alt','group','chain','cloud','flask','cut','copy','paperclip','save','square','navicon','list-ul','list-ol','strikethrough','underline','table','magic','truck','pinterest','pinterest-square','google-plus-square','google-plus','money','caret-down','caret-up','caret-left','caret-right','columns','unsorted','sort-down','sort-up','envelope','linkedin','rotate-left','legal','dashboard','comment-o','comments-o','flash','sitemap','umbrella','paste','lightbulb-o','exchange','cloud-download','cloud-upload','user-md','stethoscope','suitcase','bell-o','coffee','cutlery','file-text-o','building-o','hospital-o','ambulance','medkit','fighter-jet','beer','h-square','plus-square','angle-double-left','angle-double-right','angle-double-up','angle-double-down','angle-left','angle-right','angle-up','angle-down','desktop','laptop','tablet','mobile-phone','circle-o','quote-left','quote-right','spinner','circle','mail-reply','github-alt','folder-o','folder-open-o','smile-o','frown-o','meh-o','gamepad','keyboard-o','flag-o','flag-checkered','terminal','code','mail-reply-all','star-half-empty','location-arrow','crop','code-fork','unlink','question','info','exclamation','superscript','subscript','eraser','puzzle-piece','microphone','microphone-slash','shield','calendar-o','fire-extinguisher','rocket','maxcdn','chevron-circle-left','chevron-circle-right','chevron-circle-up','chevron-circle-down','html5','css3','anchor','unlock-alt','bullseye','ellipsis-h','ellipsis-v','rss-square','play-circle','ticket','minus-square','minus-square-o','level-up','level-down','check-square','pencil-square','external-link-square','share-square','compass','toggle-down','toggle-up','toggle-right','euro','gbp','dollar','rupee','cny','ruble','won','bitcoin','file','file-text','sort-alpha-asc','sort-alpha-desc','sort-amount-asc','sort-amount-desc','sort-numeric-asc','sort-numeric-desc','thumbs-up','thumbs-down','youtube-square','youtube','xing','xing-square','youtube-play','dropbox','stack-overflow','instagram','flickr','adn','bitbucket','bitbucket-square','tumblr','tumblr-square','long-arrow-down','long-arrow-up','long-arrow-left','long-arrow-right','apple','windows','android','linux','dribbble','skype','foursquare','trello','female','male','gittip','sun-o','moon-o','archive','bug','vk','weibo','renren','pagelines','stack-exchange','arrow-circle-o-right','arrow-circle-o-left','toggle-left','dot-circle-o','wheelchair','vimeo-square','turkish-lira','plus-square-o','space-shuttle','slack','envelope-square','wordpress','openid','institution','mortar-board','yahoo','google','reddit','reddit-square','stumbleupon-circle','stumbleupon','delicious','digg','pied-piper-square','pied-piper-alt','drupal','joomla','language','fax','building','child','paw','spoon','cube','cubes','behance','behance-square','steam','steam-square','recycle','automobile','cab','tree','spotify','deviantart','soundcloud','database','file-pdf-o','file-word-o','file-excel-o','file-powerpoint-o','file-photo-o','file-zip-o','file-sound-o','file-movie-o','file-code-o','vine','codepen','jsfiddle','life-bouy','circle-o-notch','ra','ge','git-square','git','hacker-news','tencent-weibo','qq','wechat','send','send-o','history','circle-thin','header','paragraph','sliders','share-alt','share-alt-square','bomb'];
		
		// Preparamos el cuerpo del documento
		$('body').css({'position':'relative'});

		overlay = $('<div></div>').appendTo(document.body);
		overlay.css({
			'background-color':'black',
			'opacity':0.9,
			'width':'100%',
			'height':'100%',
			'position':'fixed',
			'z-index':10001,
			'top':0,
			'left':0
		}).hide();

		// Creamos el contenedor principal
		container = $('<div id="icon-selector" style="-moz-border-radius:6px; -webkit-border-radius:6px; border-radius:6px;"></div>').appendTo(document.body);
		container.css({
			'position':'fixed',
			'z-index':10001,
			'padding':10,
			'border':'1px solid #ddd',
			'background-color':'#fff'
		}).hide();
		
		// Agregamos el contenedor de la lista de iconos el cual tendrá scroll
		iconsl = $('<ul class="icons-list nav-vertical"></ul>').appendTo(container);
		iconsl.css({
			'overflow-y':'scroll',
			'overflow-x':'hidden',
			'margin-top':10,
			'padding':0
		});

		// Agregamos los iconos al contenedor
		$.each(fa, function(indice,icon){
			$('<li data-icon="'+icon+'"><i class="fa fa-fw fa-'+icon+'"></i> '+icon+'</li>').css({'cursor':'pointer'}).appendTo(iconsl);
		});
		
		// Agregamos las acciones de click a los iconos
		$('li', iconsl).hover(function(){
			$(this).css({'background-color':'#f5f5f5'});
		}, function(){
			$(this).css({'background-color':'#fff'});
		}).bind('click', function(e){
			container.hide();
			overlay.hide();
			$('i', target).removeClass().addClass('fa fa-fw fa-'+$(this).data('icon'));
			$(target).trigger('change', 'fa-'+$(this).data('icon'));
		});
		
		// Agregamos la funcionalidad de busqueda en los iconos
		search = $('<input type="text" class="form-control" placeholder="Nombre del icono" />').prependTo(container);
		search.bind('keyup', {'target':iconsl}, function(e){
			searchText = $(this).val();
			if (searchText.length<2) {
				$('li', e.data.target).show();
			} else {
				$('li:not(:contains('+searchText+'))', e.data.target).hide();
				$('li:contains('+searchText+')', e.data.target).show();
			}
		}).bind('keydown', function(e){
			if (e.which == 27) {
				container.hide();
				overlay.hide();
			}
		});

		lastCall = 0;
		function placeComponent(){
			isHidden = !container.is(":visible");
			container.css({'visibility':'hidden'}).show();

			screenW = $(window).width();
			screenH = $(window).height();
			
			containerW = (screenW<=640) ? '80%' : 640;
			containerH = (screenH<=480) ? '80%' : 480;

			xPos = Math.ceil((screenW - containerW) / 2);
			yPos = Math.ceil((screenH - containerH) / 2);

			container.css({'width':containerW, 'height':containerH, 'left':xPos, 'top':yPos});
			iconsl.css({'height':container.height()-search.outerHeight()-parseInt(iconsl.css('margin-top'))});

			// Lo hacemos aparecer
			container.css({'visibility':'visible'});
			if (isHidden) { container.hide(); }
		};

		$(window).bind('resize.iconselector', function(e){
			clearTimeout(lastCall);
			lastCall = setTimeout(placeComponent, 100);
		}).trigger('resize.iconselector');

		// Todo lo que este dentro de esta función se ejecutara cada vez que se llame a la misma
		// Para ahorrar recursos de javascript deberíamos de sacar todas las funciones que no necesiten repetirse para cada instancia
		$.fn.aeIconSelector = function(options) {
			
			return this.each(function() {
				
				// Campo original
				THIS = $(this);
				THIS.bind('click', {'target':THIS}, function(e){
					target = e.data.target;
					container.show();
					overlay.show();
					search.focus();
				});

				return THIS;
				
			});
		}
	
	});
	
})(jQuery);