$(document).bind("ready", function(e){

	$.fn.aeBanner = function(options) {
		
		var THIS, images, buttons, items, total, next;
		
		function initialize(buttons, images, total) {
			$('img:nth-child(1)', images).stop().animate({'opacity':1}, 600);
			
			images.data({'current':1});
			$('a', buttons).bind('click', {'images':images}, function(e){
				// Regresamos a su estado inicial al anterior objetivo
				current = e.data.images.data('current');
				$('img:nth-child('+current+')', images).stop().animate({'opacity':0}, 600);
				$('a:nth-child('+current+') span', buttons).removeClass().addClass('fa fa-dot-circle-o');
				// Marcamos como activo al nuevo objetivo
				clicked = $(this).data('index');
				$('img:nth-child('+clicked+')', images).stop().animate({'opacity':1}, 600);
				$('a:nth-child('+clicked+') span', buttons).removeClass().addClass('fa fa-circle active');
				e.data.images.data({'current':clicked});
			});
			$('a:first-child', buttons).trigger('click');
		}
		
        return this.each(function() {
			
			THIS = $(this);
			THIS.options = $.extend({
				'min':0,
				'max':5,
				'step':1,
				'value':0,
				'zindex':parseInt($(THIS).css('z-index'))
			}, options);
			
			container = $('<div class="aebanner-container"></div>').css({'position':'absolute', 'top':0, 'width':'100%'}).appendTo(THIS);
			buttons = $('<div class="aebanner-buttons"></div>').css({'position':'absolute', 'text-align':'center', 'bottom':0, 'width':'100%', 'z-index':THIS.options.zindex+1}).appendTo(container);
			images = $('<div class="aebanner-images"></div>').css({'position':'relative', 'overflow':'hidden', 'height':THIS.height(), 'z-index':THIS.options.zindex}).appendTo(container);			
			items = $('.aebanner-item', THIS);
			total = items.length - 1;
			
			$(window).bind('resize.aebanner', {'images':images}, function(e){
				e.data.images.css({'width':$(window).width()});
			}).trigger('resize.aebanner');
			
			items.each(function(i,o){
				
				$('<img src="'+$(o).data('src')+'"/>').css({
					'position':'absolute',
					'top':'50%',
					'left':'50%',
					'opacity':0,
					'margin':'-255px 0px 0px -800px'
				}).bind('load', {'buttons':buttons, 'images':images, 'total':total, 'current':i}, function(e){
					if (e.data.current == e.data.total) {
						initialize(e.data.buttons, e.data.images, e.data.total);
					}
				}).appendTo(images);
				
				$('<a data-index="'+(i+1)+'" class="aebanner-button"><span class="fa fa-dot-circle-o"></span></a>').appendTo(buttons);
			});

			$('ol', THIS).css({'position':'absolute', 'top':0, 'left':0, 'visibility':'hidden'}).appendTo(container);
		});
	}

});