(function($){
	$.fn.aeSecureInput = function() {
		
		var THIS, TARGET, COPY, container, hash;
		
        return this.each(function() {
			
			THIS = $(this);
			COPY = $('<input type="'+THIS.prop('type')+'" placeholder="'+THIS.prop('placeholder')+'" class="form-control fs" />').insertAfter(THIS);
			TARGET = $('<input id="'+THIS.prop('id')+'" name="'+THIS.prop('name')+'" type="hidden" />');
			THIS.replaceWith(TARGET);
			
			COPY.bind('keyup blur focus', {'parent':TARGET}, function(e){
				hash = $(this).val();
				if (hash.length > 0) { hash = CryptoJS.MD5(hash); }
				e.data.parent.val(hash);
			});
			
			return THIS;
			
		});
	}
})(jQuery);