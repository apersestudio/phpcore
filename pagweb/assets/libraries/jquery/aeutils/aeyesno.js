(function($){
	$.fn.aeYesNo = function() {
		
		var THIS, code, container, selected, index;
		
        return this.each(function() {
			
			THIS = $(this);
			container = $('<div class="aeyesno-container"></div>').insertBefore(THIS);
			
			code = '<div class="btn-group">';
			code+= '	<a class="btn btn-default aeyesno-no" data-value="0">No</a>';
			code+= '	<a class="btn btn-default aeyesno-yes" data-value="1">Si</a>';
			code+= '</div>';
			area = $(code).appendTo(container);
			
			$('a', area).bind('click', {'area':area, 'parent':THIS}, function(e){
				$('a', e.data.area).removeClass('current');
				$(this).addClass('current');
				index = $(this).index()+1;
				e.data.parent.find('option:nth-child('+index+')').prop({'selected':true});
			});
			
			selected = THIS.find('option[selected=selected]').index()+1;
			if (selected>0) { $('a:nth-child('+selected+')', area).trigger('click'); }
			THIS.css({'position':'absolute', 'top':0, 'left':0, 'visibility':'hidden'}).appendTo(container);
			
		});
	}
})(jQuery);