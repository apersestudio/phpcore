(function($){
	$.fn.aeFile = function(options) {
		
		var THIS, container, inputGroup, code, input, image, file, iframe, form, btnDelete, btnUpload, response, uploads;

		var params = $.extend({
			'maxfiles':1
		}, options);
		
        return this.each(function() {
			
			// Contenedor principal
			container = $('<div class="file-container"></div>').css({'position':'relative'}).insertBefore(this);
			code = '<div class="input-group">';
			code+= '	<input type="text" class="form-control" placeholder="Elige un archivo" />';
			code+= '	<a class="addon"><i class="fa fa-fw fa-search"></i></a>';
			code+= '</div>';
			
			// Campo de texto donde se despliega el nombre del archivo
			inputGroup = $(code).appendTo(container);
			inputGroup.css({'z-index':8001, 'position':'relative', 'width':'100%'});
			
			btnUpload = $('a', inputGroup);
			btnUpload.prop({'disabled':false});
			input = $('input[type=text]', inputGroup);
			
			// Campo original
			THIS = $(this);
			THIS.appendTo(container);
			container.data({'uploads':0});
			
			// Funcion que inicializa el proceso de reemplazo por la versión fancy
			THIS.bind('reset', {'file':THIS, 'name':THIS.prop('name'), 'input':input, 'container':container, 'upload':btnUpload}, function(e){
				file = $('<input type="file" name="'+e.data.name+'" />').css({
					'position':'absolute',
					'z-index':8002,
					'top':0,
					'left':0,
					'outline':'none',
					'cursor':'pointer',
					'opacity':0,
					'width':'100%',
					'height':e.data.input.outerHeight()+2
				}).prop({'size':40});

				e.data.file.replaceWith(file);
				e.data.file = file;
				
				e.data.file.bind('change', {'input':e.data.input, 'file':e.data.file, 'container':e.data.container, 'upload':e.data.upload}, function(e){
					if (e.data.container.data('editable')) {
						e.data.file.css({'z-index':8000});
						$('.fa', e.data.upload).removeClass('fa-search').addClass('fa-upload');
					}
					e.data.input.val($(this).val());					
				}).trigger('change');

				$(this).data({'filetarget':file});
				
			}).trigger('reset');

			THIS.bind('checkstatus', {'input':input, 'upload':btnUpload, 'container':container}, function(e){
				if (e.data.container.data('uploads') >= params.maxfiles) {
					$('.fa', e.data.upload).removeClass('fa-search').addClass('fa-dot-circle-o');
					$(this).data('filetarget').hide();
					e.data.input.prop({'disabled':true});
					e.data.upload.prop({'disabled':true});
				} else {
					$('.fa', e.data.upload).removeClass('fa-dot-circle-o').addClass('fa-search');
					$(this).data('filetarget').show();
					e.data.input.prop({'disabled':false});
					e.data.upload.prop({'disabled':false});
				}
			});
			
			THIS.bind('delete', {'container':container}, function(e){
				uploads = e.data.container.data('uploads')-1;
				e.data.container.data({'uploads':uploads});
				$(this).trigger('checkstatus');
			});
			THIS.bind('load', {'upload':btnUpload, 'container':container, 'input':input}, function(e, imgdata) {
				e.data.input.val('');

				$('.fa', e.data.upload).removeClass('fa-upload').addClass('fa-search');
				uploads = e.data.container.data('uploads')+1;
				e.data.container.data({'uploads':uploads});
				$(this).trigger('checkstatus');

				// Campo de texto donde se despliega el nombre del archivo
				image = $('<div class="image wrapper"></div>').appendTo(container);
				image.css({
					'width':200,
					'height':128,
					'margin-top':10,
					'overflow':'hidden',
					'display':'inline-block'
				});
				
				$('<img src="'+imgdata._media+'" />').css({
					'float':'left',
					'max-width':128,
					'max-height':128,
					'overflow':'hidden'
				}).appendTo(image);
				
				$.each(imgdata._sizes, function(i,o){
					$('<a class="btn btn-xs">'+o+'</a>').appendTo(image);
				});
				
				btnDelete = $('<a class="btn btn-xs"><i class="fa fa-fw fa-trash-o"></i></a>').appendTo(image);
				btnDelete.bind('click', {'idmedia':imgdata._idmedia, 'target':$(this), 'image':image}, function(e){
					e.data.target.trigger('delete', [e.data.idmedia, e.data.image, e.data.target]);
				});
				
			});
			
			THIS.bind('editable', {'container':container, 'upload':btnUpload}, function(e, params){
				
				form = $('<form></form>').insertBefore(e.data.container);
				e.data.container.appendTo(form);
				form.prop({
					"method":"post",
					"enctype":"multipart/form-data",
					"target":"postiframe",
					"action":params.handler
				});
				
				$.each(params.data, function(i,o){
					$('<input type="hidden" name="'+i+'" value="'+o+'" />').appendTo(form);
				});
				
				iframe = $('<iframe name="postiframe" style="display: none" />').appendTo(form);
				iframe.data({'firsttime':true}).bind('load', {'target':$(this)}, function(e){
					if ($(this).data('firsttime') == false) {
						response = $.parseJSON($(this).contents().text());
						e.data.target.trigger('load', [response._image, response._imagepath]);
					} else {
						$(this).data('firsttime', false);
					}
				});
				
				e.data.container.data('editable', true);
				e.data.upload.bind('click', {'form':form}, function(e){
					if($(this).prop('disabled') == false) {
						e.data.form.submit();
					}
				});
			});
			
			// Devolvemos el contenedor ya que el campo archivo será reemplazado por la función de reset
			return THIS;
			
		});
	}
})(jQuery);