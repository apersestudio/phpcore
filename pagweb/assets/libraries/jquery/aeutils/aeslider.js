(function($){
	$.fn.aeSlider = function(options) {
		
		var THIS, container, area, tracker, dragger, tick, step;
		
        return this.each(function() {
			
			THIS = $(this);
			THIS.options = $.extend({
				'min':0,
				'max':5,
				'step':1,
				'value':0,
			}, options);
			
			container = $('<div class="aeslider-container"></div>').insertBefore(THIS);
			area = $('<div class="aeslider-area"></div>').appendTo(container);
			tracker = $('<div class="aeslider-tracker"></div>').appendTo(area);
			handler = $('<div class="aeslider-handler"></div>').appendTo(area);
			dragger = $('<div class="aeslider-dragger"></div>').appendTo(handler);
			THIS.css({'position':'absolute', 'top':0, 'left':0, 'visibility':'hidden'}).appendTo(container);
			
			tick = area.width() / (THIS.options.max + 1);
			handler.data({'parent':THIS, 'tracker':tracker, 'stepsize':THIS.options.max/area.width()}).draggable({
				'axis':'x',
				'containment':'parent',
				'grid':[tick,0],
				'drag':$.proxy(function(e, ui){
					ui.helper.data('tracker').width(ui.position.left);
					step = Math.ceil(ui.helper.data('stepsize') * ui.position.left);
					ui.helper.data('parent').trigger({'type':'slide', 'value':step});
				}, THIS)
			});
			
		});
	}
})(jQuery);