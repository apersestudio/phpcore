(function($){

	// El plugin se ejectua cuando el DOM esta listo
	$(function(e){
	
		var THIS, 
		REPLACEMENT,
		attrs,
		counter,
		options, 
		passtypes=[
			"-~^$%&/()¿?¡!=+.,{}[]", // symbols
			'ÁÉÍÓÚABCDEFGHIJKLMNÑOPQRSTUVWXYZ', // uppercase
			'áéíóúabcdefghijklmnñopqrstuvwxyz', // lowercase
			'0123456789' //numbers
		],
		passfield,
		passvalue,
		passindex,
		passchar,
		passpos,
		passrandom;

		function getRandomInt(min, max) {
		    return Math.floor(Math.random() * (max - min + 1)) + min;
		}

		function customClone(element, attributes) {
			attrs = {};
			$(element).each(function() {
				$.each(this.attributes, function(indice, item) {
					if(this.specified) {
						attrs[this.name] = this.value;
					}
				});
			});
			$.extend(attrs, attributes);

			attributes = [];
			$.each(attrs, function(name, value){
				attributes[attributes.length] = name+'="'+value+'"';
			});
			return attributes.join(" ");
		}

		function togglePassView(passfield, visible) {
			passvalue = $(passfield).val();
			passfield = $('<input '+customClone($(passfield), {"type":(visible?"text":"password")})+' />');
			passfield.val(passvalue);
			return passfield;
		}

		function config(passfield, options) {
			if (options.generate) {
				$(options.generate).bind('click', {'passfield':passfield}, function(e){
					e.preventDefault();
					passrandom=[];
					passpos=0;
					for (counter=0; counter<16; counter++) {
						passpos = counter%4;
						passindex = getRandomInt(0, passtypes[passpos].length-1);
						passchar = passtypes[passpos].substr(passindex,1);
						passrandom[passrandom.length] = passchar;
					}
					$(e.data.passfield).val(passrandom.join(""));
				});
			}

			$(passfield).bind('keydown.aefieldtools', function(e){
					if (e.which == 13) {
						e.preventDefault();
						e.stopPropagation();
						$(this).trigger('blur.aefieldtools');
					}
				}).bind('blur.aefieldtools', {'options':options, 'validate':$(passfield).hasClass('validate')}, function(e){

					// Si el campo necesita ser validado
					if (e.data.validate) {
						if ($(this).data('valid') == false) {
							e.data.options.alert.removeClass('success').addClass('danger').text(e.data.options.messageWrong);
							return false;
						} else {
							e.data.options.alert.removeClass('danger').addClass('success').text(e.data.options.messageValid);
						}
					}

					if (!e.data.options.ajax) { return false; }

					// Alertamos al usuario de que vamos a comunicarnos con el servidor
					e.data.options.alert.text("Buscando...");
					// Actualizamos el valor que será enviado al servidor
					e.data.options.ajax.data[e.data.options.ajax.name] = $.trim($(this).val());

					// Comenzamos la operación del envio de datos y esperamos respuesta
					$.when(
						[{'alert':e.data.options.alert, 'messageInitial':e.data.options.messageInitial}],
						$.post(e.data.options.ajax.url, e.data.options.ajax.data, null, 'json')
					).then(function(params, response){
						params = params[0];
						response = response[0];
						// Estilo del componente alerta
						params.alert.removeClass('success danger').addClass(!response.success?'danger':'success');
						// Mensajes de error y exito
						params.alert.text(response.messages);
					});
					
				});
		}

		// Definición del plugin
		$.fn.aeFieldTools = function(useroptions){
		
			// Preparamos al elemento que llama al componente
			return this.each(function(index, item) {
			
				THIS = $(this);
				options = $.extend({
					'messageInitial':'•••',
					'generate':false,
					'view':false,
					'ajax':false
				}, useroptions);

				if (options.view) {
					$(options.view).data({'visible':false}).bind('click', {'options':options}, function(e){
						e.preventDefault();

						// Toggle
						$(this).data({'visible':$(this).data('visible')?false:true});
						REPLACEMENT = togglePassView(THIS, $(this).data('visible'));
						THIS.replaceWith(REPLACEMENT);
						THIS = REPLACEMENT;
						config(THIS, e.data.options);
					});
				}

				config(THIS, options);
				options.alert.text(options.messageInitial);
				
				
				
			});
			
		};
		
	});
})(jQuery);