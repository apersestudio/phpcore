(function($){
	$.fn.aeUrify = function(options) {
		
		var THIS, url;
		
		return this.each(function() {

			THIS = $(this);
			THIS.options = $.extend({'target':THIS}, options);
		
			THIS.bind('blur keyup', {'target':$(THIS.options.target)}, function(e){
				url = $.trim($(this).val()).toLowerCase().latinize().replace(/[^\w\d\s]/g, '').replace(/\s+/g, '-');
				e.data.target.val(url);
			});
			
		});
	}
})(jQuery);