(function($){
	$.fn.aeBridge = function(options) {
		
		var THIS, iframedocument, contentType, content, ciphertext, ssl = $('body').data('ssl').split('.'), encrypted = (ssl[0] == 1), token = ssl[1];		
		
        return this.each(function() {
			
			THIS = $(this);
			THIS.options = $.extend({
				'name':'formIframe1',
				'beforesend':function(){ return true; },
				'verify': function() {
					valid = THIS.hasClass('valid');
					if (valid) {
						$(options.mensages).removeClass().addClass('alert info').html('<strong>Enviando...</strong>');
					} else {
						$(options.mensages).removeClass().addClass('alert warning').html('<strong>Corrija los errores...</strong>');
					}
					return valid;
				},
				'success':function(r) {
					// Mostramos los mensajes que devuelva el servidor
					$(options.mensages).removeClass().addClass('alert '+(r.error?'danger':'success')).html(r.messages);
					
					// Si no hubieron errores regresamos a la pantalla anterior
					//if (r.error == false) { setTimeout(function(){ window.location = $(options.regresar).prop('href'); }, 1000); }

				},
				'url':'about:blank'
			}, options);
			
			$('<iframe src="about:blank" name="'+THIS.options.name+'"></iframe>').data({
				'firsttime':true
			}).css({
				'position':'absolute',
				'top':-9999,
				'left':-9999,
				'opacity':0,
				'z-index':-1
			}).bind('load', {'success':THIS.options.success}, function(e){
				if ($(this).data('firsttime') === false) {
					content = $(this).contents().text();
					if (encrypted) {
						content = CryptoJS.AES.decrypt(content, token);
						content = content.toString(CryptoJS.enc.Utf8);
					}
					content = $.parseJSON(content);
					e.data.success(content);
				}
				$(this).data({'firsttime':false});
				// Activamos los campos
				$('input, select, textarea', THIS).not('input:file').removeProp('disabled');
			}).appendTo('body');
			
			THIS.prop({
				'action':THIS.options.url,
				'target':THIS.options.name,
				'enctype':'multipart/form-data',
				'method':'post'
			}).bind('submit', {'beforesend':THIS.options.beforesend, 'verify':THIS.options.verify}, function(e){
			
				e.data.beforesend();
			
				// Pasamos los datos serializados a un clon del formulario,
				// ese clon ser� el que enviemos encriptado al iframe
				if (encrypted) {
					
					// Solo dejamos los campos de archivo
					$('input, select, textarea', THIS).not('input:file').prop({'disabled':true});
					
					// Agregamos uno campo con los datos encriptados
					$('input[name=encrypted]', THIS).remove();
					ciphertext = CryptoJS.AES.encrypt(THIS.serialize(), token);
					$('<input type="hidden" name="encrypted" value="'+ciphertext+'">').appendTo(THIS);
					
				}
				
				if (e.data.verify() !== true) {
					e.preventDefault();
				};
				
			});
			
		});
	}
})(jQuery);