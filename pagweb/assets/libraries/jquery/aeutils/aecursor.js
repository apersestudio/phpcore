(function($){
	var range;
	$.fn.setPosition = function(pos) {
		return this.each(function(i, o) {
			pos = (typeof(pos) == "undefined") ? $(this).val().length : pos;
			if (o.setSelectionRange) {
				o.setSelectionRange(pos, pos);
			} else if (o.createTextRange) {
				range = o.createTextRange();
				range.collapse(true);
				range.moveEnd('character', pos);
				range.moveStart('character', pos);
				range.select();
			}
		});
	};
	$.fn.selectRange = function(start, end) {
		return this.each(function(i, o) {
			if (o.setSelectionRange) {
				o.focus();
				o.setSelectionRange(start, end);
			} else if (o.createTextRange) {
				range = o.createTextRange();
				range.collapse(true);
				range.moveEnd('character', end);
				range.moveStart('character', start);
				range.select();
			}
		});
	};
})(jQuery);