(function($){
	$.fn.aeSlider = function(options) {
		
		var THIS, container, area, tracker, dragger, tick, step;
		
        return this.each(function() {
			
			THIS = $(this);
			THIS.options = $.extend({
				'min':0,
				'max':5,
				'step':1,
				'value':0,
			}, options);
			
			container = $('<div class="slider-container"></div>').insertBefore(THIS);
			area = $('<div class="slider-area"></div>').appendTo(container);
			tracker = $('<div class="slider-tracker"></div>').appendTo(area);
			handler = $('<div class="slider-handler"></div>').appendTo(area);
			dragger = $('<div class="slider-dragger"></div>').appendTo(handler);
			THIS.css({'position':'absolute', 'top':0, 'left':0, 'visibility':'hidden'}).appendTo(container);
			
			tick = area.width() / (THIS.options.max + 1);
			handler.data({'parent':THIS, 'tracker':tracker, 'stepsize':THIS.options.max/area.width()}).draggable({
				'axis':'x',
				'containment':'parent',
				'grid':[tick,0],
				'drag':$.proxy(function(e, ui){
					ui.helper.data('tracker').width(ui.position.left);
					step = Math.ceil(ui.helper.data('stepsize') * ui.position.left);
					ui.helper.data('parent').trigger({'type':'slide', 'value':step});
				}, THIS)
			});
			
		});
	}
})(jQuery);

(function($){
	$.fn.aeMultiselect = function(options) {
		
		var THIS, button, icon, target, list, selected;
		
        return this.each(function() {
			
			THIS = $(this);
			THIS.options = $.extend({}, options);
			
			container = $('<div class="multi-container"></div>').insertBefore(THIS);
			list = $('<div class="multi-list list-group"></div>').appendTo(container);
			
			THIS.css({'position':'absolute', 'top':0, 'left':0, 'visibility':'hidden'}).appendTo(container);
			
			THIS.bind('update', function(){

				$('option', THIS).each(function(i,o){
					button = $('<a class="list-group-item"><i class="fa fa-fw fa-square-o"></i> '+$(o).text()+'</a>').appendTo(list);
					button.bind('click', {'target':$(o), 'icon':$('i.fa', button)}, function(e){
						target = e.data.target;
						icon = e.data.icon;
						selected = !target.prop('selected');
						icon.removeClass().addClass('fa fa-fw '+(selected?'fa-check-square-o':'fa-square-o'));
						target.prop('selected', selected);
						$(this).toggleClass('active', selected);
					});
				})
			});
			
		});
	}
})(jQuery);

(function($){
	$.fn.aeSecureInput = function() {
		
		var THIS, TARGET, COPY, container, hash;
		
        return this.each(function() {
			
			THIS = $(this);
			COPY = $('<input type="'+THIS.prop('type')+'" placeholder="'+THIS.prop('placeholder')+'" class="form-control" />').insertBefore(THIS);
			TARGET = $('<input id="'+THIS.prop('id')+'" name="'+THIS.prop('name')+'" class="form-control" type="hidden" />');
			THIS.replaceWith(TARGET);
			
			COPY.bind('keyup blur focus', {'parent':TARGET}, function(e){
				hash = $(this).val();
				if (hash.length > 0) { hash = CryptoJS.MD5(hash); }
				e.data.parent.val(hash);
			});
			
			return THIS;
			
		});
	}
})(jQuery);

(function($){
	$.fn.aeYesNo = function() {
		
		var THIS, code, container, selected, index;
		
        return this.each(function() {
			
			THIS = $(this);
			container = $('<div class="yesno-container"></div>').insertBefore(THIS);
			
			code = '<div class="btn-group">';
			code+= '	<a class="btn btn-default yesno-no" data-value="0">No</a>';
			code+= '	<a class="btn btn-default yesno-yes" data-value="1">Si</a>';
			code+= '</div>';
			area = $(code).appendTo(container);
			
			$('a', area).bind('click', {'area':area, 'parent':THIS}, function(e){
				$('a', e.data.area).removeClass('current');
				$(this).addClass('current');
				index = $(this).index()+1;
				e.data.parent.find('option:nth-child('+index+')').prop({'selected':true});
			});
			
			selected = THIS.find('option[selected=selected]').index()+1;
			if (selected>0) { $('a:nth-child('+selected+')', area).trigger('click'); }
			THIS.css({'position':'absolute', 'top':0, 'left':0, 'visibility':'hidden'}).appendTo(container);
			
		});
	}
})(jQuery);

(function($){
	$.fn.aeUrify = function(options) {
		
		var THIS, url;
		
		return this.each(function() {

			THIS = $(this);
			THIS.options = $.extend({'target':THIS}, options);
		
			THIS.bind('blur keyup', {'target':$(THIS.options.target)}, function(e){
				url = $.trim($(this).val()).toLowerCase().latinize().replace(/[^\w\d\s]/g, '').replace(/\s+/g, '-');
				e.data.target.val(url);
			});
			
		});
	}
})(jQuery);

(function($){
	$.fn.aeFile = function() {
		
		var THIS, container, code, input, file;
		
        return this.each(function() {
			
			// Contenedor principal
			container = $('<div class="file-container"></div>').css({'position':'relative'}).insertBefore(this);
			code = '<div class="input-group">';
			code+= '	<input type="text" class="form-control" placeholder="Elige un archivo" />';
			code+= '	<div class="input-group-btn"><a class="btn btn-default">Explorar</a></div>';
			code+= '</div>';
			
			// Campo de texto donde se despliega el nombre del archivo
			input = $(code).appendTo(container).find('input[type=text]');
			
			// Campo original
			THIS = $(this);
			THIS.appendTo(container);

			// Funcion que inicializa el proceso de reemplazo por la versión fancy
			container.bind('reset', {'file':THIS, 'name':THIS.prop('name'), 'input':input}, function(e){
				file = $('<input type="file" name="'+e.data.name+'" />').css({'position':'absolute', 'top':0, 'left':0, 'outline':'none', 'cursor':'pointer', 'opacity':0, 'width':'100%', 'height':'100%'}).prop({'size':40});
				e.data.file.replaceWith(file);
				e.data.file = file;
				e.data.file.bind('change', {'input':e.data.input}, function(e){
					e.data.input.val($(this).val());
				}).trigger('change');
			}).trigger('reset');
			
			// Devolvemos el contenedor ya que el campo archivo será reemplazado por la función de reset
			return container;
			
		});
	}
})(jQuery);

(function($){
	$.fn.aeBridge = function(options) {
		
		var THIS, iframedocument, contentType, content, ciphertext, ssl = $('body').data('ssl').split('.'), encrypted = (ssl[0] == 1), token = ssl[1];		
		
        return this.each(function() {
			
			THIS = $(this);
			THIS.options = $.extend({
				'name':'formIframe1',
				'verify':function(){ return true; },
				'beforesend':$.noop(),
				'success':$.noop(),
				'url':'about:blank'
			}, options);
			
			$('<iframe src="about:blank" name="'+THIS.options.name+'"></iframe>').data({
				'firsttime':true
			}).css({
				'position':'absolute',
				'top':-9999,
				'left':-9999,
				'opacity':0,
				'z-index':-1
			}).bind('load', {'success':THIS.options.success}, function(e){
				if ($(this).data('firsttime') === false) {
					content = $(this).contents().text();
					if (encrypted) {
						content = CryptoJS.AES.decrypt(content, token);
						content = content.toString(CryptoJS.enc.Utf8);
					}
					content = $.parseJSON(content);
					e.data.success(content);
				}
				$(this).data({'firsttime':false});
				// Activamos los campos
				$('input, select, textarea', THIS).not('input:file').removeProp('disabled');
			}).appendTo('body');
			
			THIS.prop({
				'action':THIS.options.url,
				'target':THIS.options.name,
				'enctype':'multipart/form-data',
				'method':'post'
			}).bind('submit', {'beforesend':THIS.options.beforesend, 'verify':THIS.options.verify}, function(e){
			
				e.data.beforesend();
			
				// Pasamos los datos serializados a un clon del formulario,
				// ese clon será el que enviemos encriptado al iframe
				if (encrypted) {
					
					// Solo dejamos los campos de archivo
					$('input, select, textarea', THIS).not('input:file').prop({'disabled':true});
					
					// Agregamos uno campo con los datos encriptados
					$('input[name=encrypted]', THIS).remove();
					ciphertext = CryptoJS.AES.encrypt(THIS.serialize(), token);
					$('<input type="hidden" name="encrypted" value="'+ciphertext+'">').appendTo(THIS);
					
				}
				
				if (e.data.verify() !== true) {
					e.preventDefault();
				};
				
			});
			
		});
	}
})(jQuery);

(function($){
	$.fn.aeComplete = function(options) {
		
		var THIS, originalName, hiddenField, v, parent, position, container, ajaxCall = false;
		
		return this.each(function() {
			
			THIS = $(this);
			THIS.options = $.extend({
				'id':THIS.prop('id'),
				'time':400,
				'lastCall':null,
				'lastSearch':'',
				'data':{},
				'beforesend':$.noop(),
				'success':$.noop(),
				'error':$.noop(),
				'close':$.noop(),
				'defaultValue':THIS.val(),
				'defaultTitle':THIS.data('title'),
				'url':'about:blank'
			}, options);
			
			// Creamos un contenedor para el valor final de este campo
			originalName = THIS.prop('name');
			hiddenField = $('<input type="hidden" name="'+originalName+'" />').insertAfter(THIS);
			
			// Cambiamos el nombre del campo original para que no choque con el campo oculto
			THIS.prop({'name':originalName+'_title'});
			
			// Valor predeterminado
			if (THIS.options.defaultValue != undefined) {
				hiddenField.val(THIS.options.defaultValue);
				THIS.val(THIS.options.defaultTitle);
			}
			
			THIS.bind('keyup', {'parent':THIS, 'hiddenField':hiddenField}, function(e){
				parent = e.data.parent;
				hiddenField = e.data.hiddenField;
				v = $(this).val();
				
				if (v.length>0) {
					if (parent.options.lastSearch!=v) {
						// Si hay un intento previo de busqueda la cancelamos
						// Porque el usuario todavía esta escribiendo
						clearTimeout(parent.options.lastCall);
						
						// La busqueda comenzará después de los milisegundos especificados
						parent.options.lastCall = setTimeout(function(){
							
							// Lo que se va a buscar
							//console.log("Buscando: "+v);
							parent.options.lastSearch = v;
							
							// Si el usuario tiene una busqueda congelada, la cancelamos
							if (!!ajaxCall) { ajaxCall.abort(); }
							
							// Dibujamos la interfaz para que el usuario pueda elegir
							position = parent.offset();
							
							// Si ya hay un contenedor previo, lo eliminamos
							$('#'+parent.options.id+'-complete').remove();
							
							// Creamos un objeto contenedor para la lista
							container = $('<div id="'+parent.options.id+'-complete" class="complete-container" tabindex="-1"></div>').css({
								'position':'absolute',
								'top':position.top+parent.outerHeight(),
								'left':position.left,
								'width':parent.outerWidth(),
								'z-index':9999,
								'outline':'none'
							}).bind('blur', {'parent':THIS}, function(e){
								
								// Cuando el contenedor se cierra lo eliminamos
								$(this).hide().remove();
								e.data.parent.val('');
								e.data.parent.options.close();
								
							}).bind('keydown', function(e){
								
								// Si el usuario presiona la tecla de escape, cerramos el contenedor
								if (e.which == 27) { $(this).trigger('blur'); }
								
							}).appendTo('body');
							
							// Si el usuario ha definido una acción antes de llamar la URL, la ejecutamos
							parent.options.beforesend(container);
							
							// Llamamos a la URL
							ajaxCall = $.ajax({
								'url':parent.options.url,
								'data':{'search':v},
								'type':'post',
								'dataType':'json',
								'error': function(jqXHR, textStatus, errorThrown) {
									parent.options.error(container, jqXHR, textStatus, errorThrown);
								},
								'success': function(r) {
									container.html('');
									parent.options.success(container, hiddenField, r, parent.options.data);
									container.focus();
								}
							});
							
							// Creamos una interfaz para que el usuario pueda cerrar el contenedor cuando desee
							THIS.bind('close', {'container':container}, function(e){
								e.data.container.trigger('blur');
							});

						}, parent.options.time);
						
						// Lo que el usuario esta escribiendo
						//console.log(v);
						
					} else {
						//console.log("Busqueda completada.");
					}
				} else {
					//console.log("No hay nada por buscar");
				}
			});
			
			return THIS;
			
		});
	}
})(jQuery);

(function($){
	$.fn.aeBanner = function(options) {
		
		var THIS, images, buttons, items, total, next;
		
		function initialize(buttons, images, total) {
			$('img:nth-child(1)', images).stop().animate({'opacity':1}, 600);
			$('a:nth-child(1) span', buttons).removeClass().addClass('fa fa-circle');
			
			images.data({'current':1});
			$('a', buttons).bind('click', {'images':images}, function(e){
				// Regresamos a su estado inicial al anterior objetivo
				current = e.data.images.data('current');
				$('img:nth-child('+current+')', images).stop().animate({'opacity':0}, 600);
				$('a:nth-child('+current+') span', buttons).removeClass().addClass('fa fa-circle-o');
				// Marcamos como activo al nuevo objetivo
				clicked = $(this).data('index');
				$('img:nth-child('+clicked+')', images).stop().animate({'opacity':1}, 600);
				$('a:nth-child('+clicked+') span', buttons).removeClass().addClass('fa fa-circle');
				e.data.images.data({'current':clicked});
			});
		}
		
        return this.each(function() {
			
			THIS = $(this);
			THIS.options = $.extend({
				'min':0,
				'max':5,
				'step':1,
				'value':0,
				'zindex':parseInt($(THIS).css('z-index'))
			}, options);
			
			container = $('<div class="banner-container"></div>').css({'position':'relative'}).appendTo(THIS);
			buttons = $('<div class="banner-buttons"></div>').css({'position':'absolute', 'text-align':'center', 'bottom':0, 'width':'100%', 'z-index':THIS.options.zindex+1}).appendTo(container);
			images = $('<div class="banner-images"></div>').css({'position':'relative', 'overflow':'hidden', 'height':THIS.height(), 'z-index':THIS.options.zindex}).appendTo(container);			
			items = $('.banner-item', THIS);
			total = items.length - 1;
			
			$(window).bind('resize.ae', {'images':images}, function(e){
				e.data.images.css({'width':$(window).width()});
			}).trigger('resize.ae');
			
			items.each(function(i,o){
				
				$('<img src="'+$(o).data('src')+'"/>').css({
					'position':'absolute',
					'top':'50%',
					'left':'50%',
					'opacity':0,
					'margin':'-255px 0px 0px -960px'
				}).bind('load', {'buttons':buttons, 'images':images, 'total':total, 'current':i}, function(e){
					if (e.data.current == e.data.total) {
						initialize(e.data.buttons, e.data.images, e.data.total);
					}
				}).appendTo(images);
				
				$('<a data-index="'+(i+1)+'" class="btn btn-link btn-xs"><span class="fa fa-circle-o"></span></a>').appendTo(buttons);
			});

			$('ol', THIS).css({'position':'absolute', 'top':0, 'left':0, 'visibility':'hidden'}).appendTo(container);
		});
	}
})(jQuery);