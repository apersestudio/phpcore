(function($){
	$.fn.aeComplete = function(options) {
		
		var THIS, originalName, hiddenField, v, parent, position, container, ajaxCall = false;
		
		return this.each(function() {
			
			THIS = $(this);
			THIS.options = $.extend({
				'id':THIS.prop('id'),
				'time':400,
				'lastCall':null,
				'lastSearch':'',
				'data':{},
				'beforesend':$.noop(),
				'success':$.noop(),
				'error':$.noop(),
				'close':$.noop(),
				'defaultValue':THIS.val(),
				'defaultTitle':THIS.data('title'),
				'url':'about:blank'
			}, options);
			
			// Creamos un contenedor para el valor final de este campo
			originalName = THIS.prop('name');
			hiddenField = $('<input type="hidden" name="'+originalName+'" />').insertAfter(THIS);
			
			// Cambiamos el nombre del campo original para que no choque con el campo oculto
			THIS.prop({'name':originalName+'_title'});
			
			// Valor predeterminado
			if (THIS.options.defaultValue != undefined) {
				hiddenField.val(THIS.options.defaultValue);
				THIS.val(THIS.options.defaultTitle);
			}
			
			THIS.bind('keyup', {'parent':THIS, 'hiddenField':hiddenField}, function(e){
				parent = e.data.parent;
				hiddenField = e.data.hiddenField;
				v = $(this).val();
				
				if (v.length>0) {
					if (parent.options.lastSearch!=v) {
						// Si hay un intento previo de busqueda la cancelamos
						// Porque el usuario todav�a esta escribiendo
						clearTimeout(parent.options.lastCall);
						
						// La busqueda comenzar� despu�s de los milisegundos especificados
						parent.options.lastCall = setTimeout(function(){
							
							// Lo que se va a buscar
							//console.log("Buscando: "+v);
							parent.options.lastSearch = v;
							
							// Si el usuario tiene una busqueda congelada, la cancelamos
							if (!!ajaxCall) { ajaxCall.abort(); }
							
							// Dibujamos la interfaz para que el usuario pueda elegir
							position = parent.offset();
							
							// Si ya hay un contenedor previo, lo eliminamos
							$('#'+parent.options.id+'-aecomplete').remove();
							
							// Creamos un objeto contenedor para la lista
							container = $('<div id="'+parent.options.id+'-aecomplete" class="aecomplete-container" tabindex="-1"></div>').css({
								'position':'absolute',
								'top':position.top+parent.outerHeight(),
								'left':position.left,
								'width':parent.outerWidth(),
								'z-index':9999,
								'outline':'none'
							}).bind('blur', {'parent':THIS}, function(e){
								
								// Cuando el contenedor se cierra lo eliminamos
								$(this).hide().remove();
								e.data.parent.val('');
								e.data.parent.options.close();
								
							}).bind('keydown', function(e){
								
								// Si el usuario presiona la tecla de escape, cerramos el contenedor
								if (e.which == 27) { $(this).trigger('blur'); }
								
							}).appendTo('body');
							
							// Si el usuario ha definido una acci�n antes de llamar la URL, la ejecutamos
							parent.options.beforesend(container);
							
							// Llamamos a la URL
							ajaxCall = $.ajax({
								'url':parent.options.url,
								'data':{'search':v},
								'type':'post',
								'dataType':'json',
								'error': function(jqXHR, textStatus, errorThrown) {
									parent.options.error(container, jqXHR, textStatus, errorThrown);
								},
								'success': function(r) {
									container.html('');
									parent.options.success(container, hiddenField, r, parent.options.data);
									container.focus();
								}
							});
							
							// Creamos una interfaz para que el usuario pueda cerrar el contenedor cuando desee
							THIS.bind('close', {'container':container}, function(e){
								e.data.container.trigger('blur');
							});

						}, parent.options.time);
						
						// Lo que el usuario esta escribiendo
						//console.log(v);
						
					} else {
						//console.log("Busqueda completada.");
					}
				} else {
					//console.log("No hay nada por buscar");
				}
			});
			
			return THIS;
			
		});
	}
})(jQuery);