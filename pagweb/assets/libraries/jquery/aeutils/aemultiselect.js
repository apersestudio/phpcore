(function($){
	$.fn.aeMultiselect = function(options) {
		
		var THIS, button, icon, target, list, selected;
		
        return this.each(function() {
			
			THIS = $(this);
			THIS.options = $.extend({}, options);
			
			container = $('<div class="aemultiselect-container"></div>').insertBefore(THIS);
			list = $('<div class="aemultiselect-list list-group"></div>').appendTo(container);
			
			THIS.css({'position':'absolute', 'top':0, 'left':0, 'visibility':'hidden'}).appendTo(container);
			
			THIS.bind('update', function(){

				$('option', THIS).each(function(i,o){
					button = $('<a class="list-group-item"><i class="fa fa-fw fa-square-o"></i> '+$(o).text()+'</a>').appendTo(list);
					button.bind('click', {'target':$(o), 'icon':$('i.fa', button)}, function(e){
						target = e.data.target;
						icon = e.data.icon;
						selected = !target.prop('selected');
						icon.removeClass().addClass('fa fa-fw '+(selected?'fa-check-square-o':'fa-square-o'));
						target.prop('selected', selected);
						$(this).toggleClass('active', selected);
					});
				})
			});
			
		});
	}
})(jQuery);