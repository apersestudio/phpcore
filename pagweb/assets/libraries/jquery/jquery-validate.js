(function($) {
	
	var a, b, c, r, t, v, points, valid, weight, validations, restrictions, target, params;
	
	$.extend({
		HAS_MIN: function(value, min) {
			return ($.trim(value).length >= min);
		},
		HAS_MAX: function(value, max) {
			return ($.trim(value).length <= max);
		},
		IS_IN_RANGE: function(value, min, max) {
			return ($.trim(value).length >= min && $.trim(value).length <= max);
		},
		IS_EMAIL: function(value) {
			r = new RegExp($.EMAIL, 'i');
			return r.test(value);
		},
		IS_URL: function(value) {
			r = new RegExp($.URL, 'i');
			return r.test(value);
		},
		IS_DATE: function(value) {
			return !/Invalid|NaN/.test(new Date(value));
		},
		IS_DATE_ISO: function(value) {
			r = new RegExp($.DATEISO, 'g');
			return r.test(value);
		},
		ONLY_NUMBERS: function(value) {
			r = new RegExp("["+$.NUMBERS+"]", 'g');
			return r.test(value);
		},
		ONLY_DECIMALS: function(value) {
			r = new RegExp("["+$.DECIMALS+"]", 'g');
			return r.test(value);
		},
		ONLY_ALPHAS: function(value) {
			r = new RegExp("["+$.ALPHAS+"]", 'g');
			return r.test(value);
		},
		ONLY_LATIN: function(value) {
			r = new RegExp("["+$.LATIN+"]", 'g');
			return r.test(value);
		},
		IS_CREDIT_CARD: function(value) {
			// accept only digits and dashes
			if (/[^0-9-]+/.test(value)) { return false; }
			var nCheck = 0, nDigit = 0, bEven = false, value = value.replace(/\D/g, "");
			for (n = value.length - 1; n >= 0; n--) {
				var cDigit = value.charAt(n);
				var nDigit = parseInt(cDigit, 10);
				if (bEven && (nDigit *= 2) > 9) { nDigit -= 9; }
				nCheck += nDigit;
				bEven = !bEven;
			}
			return (nCheck % 10) == 0;
		},
		IS_RFC: function(value) {
			r = new RegExp($.RCF, 'g');
			return r.test(value);
		},
		HAS_MIN_WEIGHT: function(value) {
			points = 0;
			if (value.length >= 8) { points++; }
			if (/\W/.test(value)) { points++; }
			if (/[A-Z]/.test(value)) { points++; }
			if (/[a-z]/.test(value)) { points++; }
			if (/[0-9]/.test(value)) { points++; }
			return points;
		},
		IS_KEY: function(value) {
			return /^[a-z0-9]+$/i.test(value) && $.IS_IN_RANGE(value, 3, 20);
		},
		NOT_EMPTY: function(e) {
			v = $.trim($(e).val());
			if (v.length == 0) {
				$.VALID(e, false, {"success":false, "messages":"No puede estar vacio"});
				return false;
			}
			return v;
		},
		VALID: function(e, valid, params) {
			// Marcamos el objeto con la propiedad de valido o invalido
			$(e).data('valid', valid);
			
			/* Solo aplica las clases si el objetivo no es de solo lectura y no es un formulario */
			if (!$(e).prop('readonly') && $(e).prop('tagName').toLowerCase() != 'form') {
				c = valid?'valid success':'invalid warning';
				$(e).removeClass('invalid valid success warning');

				// Solo se modifica la clase si el usuario no cancela esta operación
				if ($(e).data('ignoreclass') != true) { $(e).addClass(c); }
				
				// Avisamos al usuario que la validación se ha llevado acabo
				if (params.type == 'AJAX') {
					$(e).trigger('response.validate', [params]);
				} else {
					params.messages = !valid ? params.invalidMessage : "Válido";
					$(e).trigger('response.validate', [params]);
				}
			}
		}
	});

	validations = {
		IS_TITLE: function(params) {
			this.bind('keyup.validate blur.validate', {"params":params}, function(e){
				$.VALID(this, ($.ONLY_ALPHAS($(this).val()) && $.HAS_MIN($(this).val(), e.data.params.min)), e.data.params);
			});			
		},
		IS_KEY: function(params) {
			this.bind('keyup.validate blur.validate', {"params":params}, function(e){
				$.VALID(this, $.IS_KEY($(this).val()), e.data.params);
			});			
		},
		HAS_MIN: function(params) {
			this.bind('keyup.validate blur.validate', {"params":params}, function(e){
				$.VALID(this, $.HAS_MIN($(this).val(), e.data.params.min), e.data.params);
			});			
		},
		HAS_MAX: function(params) {
			this.bind('keyup.validate blur.validate', {"params":params}, function(e){
				$.VALID(this, $.HAS_MAX($(this).val(), e.data.params.max), e.data.params);
			});
		},
		IS_IN_RANGE: function(params) {
			this.bind('keyup.validate blur.validate', {"params":params}, function(e){
				$.VALID(this, (!$.IS_IN_RANGE($(this).val(), e.data.params.min, e.data.params.max)), e.data.params);
			});
		},
		IS_EMAIL: function(params) {
			this.bind('keyup.validate blur.validate', {"params":params}, function(e){
				$.VALID(this, $.IS_EMAIL($(this).val()), e.data.params);
			});
		},
		IS_URL: function(params) {
			this.bind('keyup.validate blur.validate', {"params":params}, function(e){
				$.VALID(this, $.IS_URL($(this).val()), e.data.params);
			});
		},
		IS_DATE: function(params) {
			this.bind('keyup.validate blur.validate', {"params":params}, function(e){
				$.VALID(this, $.IS_DATE($(this).val()), e.data.params);
			});
		},
		IS_DATE_ISO: function(params) {
			this.bind('keyup.validate blur.validate', {"params":params}, function(e){
				$.VALID(this, $.IS_DATE_ISO($(this).val()), e.data.params);
			});
		},
		ONLY_NUMBERS: function(params) {
			this.bind('keyup.validate blur.validate', {"params":params}, function(e){
				$.VALID(this, $.ONLY_NUMBERS($(this).val()), e.data.params);
			});
		},
		ONLY_DECIMALS: function(params) {
			this.bind('keyup.validate blur.validate', {"params":params}, function(e){
				$.VALID(this, $.ONLY_DECIMALS($(this).val()), e.data.params);
			});
		},
		ONLY_ALPHAS: function(params) {
			this.bind('keyup.validate blur.validate', {"params":params}, function(e){
				$.VALID(this, $.ONLY_ALPHAS($(this).val()), e.data.params);
			});
		},
		ONLY_LATIN: function(params) {
			this.bind('keyup.validate blur.validate', {"params":params}, function(e){
				$.VALID(this, $.ONLY_LATIN($(this).val()), e.data.params);
			});
		},
		IS_CREDIT_CARD: function(params) {
			this.bind('keyup.validate blur.validate', {"params":params}, function(e){
				$.VALID(this, $.IS_CREDIT_CARD($(this).val()), e.data.params);
			});
		},
		HAS_MIN_WEIGHT: function(params) {
			this.bind('keyup.validate blur.validate', {"params":params}, function(e){
				weight = $.HAS_MIN_WEIGHT($(this).val());
				$.VALID(this, (weight >= e.data.params.minWeight), e.data.params);
			});
		},
		IS_EQUAL_TO: function(params) {
			this.bind('keyup.validate blur.validate', {"params":params}, function(e){
				a = $.trim($(this).val());
				b = $.trim($(e.data.params.partner).val());
				$.VALID(this, (a == b && a !== ''), e.data.params);
			});			
		},
		NO_DEFAULT: function(params) {
			this.bind('change.validate blur.validate', {"params":params}, function(e){
				$.VALID(this, ($('option:selected',this)[0].defaultSelected), e.data.params);
			});
		},
		AJAX: function(params) {
			this.bind('blur.validate', {'parent':this, "params":params}, function(e){
				if (v = $.NOT_EMPTY(this)) {
					e.data.params.data[e.data.params.name] = v;
					$(this).trigger('response.validate', [{"messages":"Comunicando con el servidor..."}]);

					$.when(
						[{'parent':e.data.parent, "params":e.data.params}],
						$.ajax({"url":e.data.params.url, "data":e.data.params.data, "method":e.data.params.method, "dataType":e.data.params.dataType})
					).then(function(p, response){
						p = p[0];
						response = response[0];
						response.type = p.params.type;
						$.VALID(p.parent, response.success, response);
					});
				}
			});
		}
	};
	
	restrictions = {
		LATIN : function() {
			this.bind('keyup.restrict blur.restrict', function() {
				r = new RegExp($.NOLATIN, 'g');
				$(this).val($(this).val().replace(r, ''));
			});
		},
		DECIMALS : function() {
			this.bind('keyup.restrict blur.restrict', function() {
				r = new RegExp($.NODECIMALS, 'g');
				$(this).val($(this).val().replace(r, ''));
			});
		},
		NUMBERS : function() {
			this.bind('keyup.restrict blur.restrict', function() {
				r = new RegExp($.NONUMBERS, 'g');
				$(this).val($(this).val().replace(r, ''));
			});
		}		
	};
	
	$.fn.validate = function(p) {
		if (typeof(p) == 'object') {
			target = (p.target==undefined) ? this : $(p.target, this);
			type = p.type.toUpperCase();
			params = p;
			// Este parametro sirve para controlar cuando la clase debe modificar la apariencia del campo en caso de error
			target.data({'ignoreclass':p['ignoreclass']});
		} else {
			target = this;
			type = (p==undefined) ? this : p.toUpperCase();
			params = Array.prototype.slice.call(arguments,1);
			target.data({'ignoreclass':false});
		}
		t = $(target).prop('tagName').toLowerCase();
		switch (t) {
			case 'form' :
				$(target).addClass('validate').bind('submit.validate', function(){
					points = $('.validate', target).trigger('blur.validate').length;
					valid = $('.valid', target).length;
					$.VALID(target, (points == valid), params);
				});
			break;
			
			case 'select' : case 'input' : case 'textarea' : 
			if (type && validations[type]) {
				$(target).addClass('validate');
				validations[type].apply(target, [params]);
			}
			break;
		}
		return this;
	};

	$.fn.restrict = function(range) {
		range = range.toUpperCase();
		if (restrictions[range]) {
			restrictions[range].apply(this, Array.prototype.slice.call(arguments,1));
		}
		return this;
	};
	
})($);