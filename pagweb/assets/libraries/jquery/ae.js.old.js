(function($){

	var THIS, HTML, CUSTOMDATA, VALOR, TEXTO, VALIDO, DATOS, OPTIONSELECTED, CONFIG,

	aeFieldEditor = function(target, options) {
		this.init(target, options);
	}

	aeFieldEditor.prototype.alertar = function(tipo, mensaje) {
		THIS = this;
		THIS.alerta.removeClass().addClass('alert '+tipo).html(mensaje);
	}

	aeFieldEditor.prototype.dibujarComplete = function(respuesta) {
		THIS = this;
		// Limpiamos el contenedor
		THIS.contenedor.html('');

		HTML = '<table class="table">';
		$.each(respuesta.lista, function(indice, item){
			HTML+= '<tr>';
			HTML+= THIS.options.completeConfig.render(item);
			HTML+= '</tr>';
		});
		HTML+= '</table></div>';
		THIS.visor = $(HTML);

		$('td', THIS.visor).each(function(i,o){
			$(this).bind('click', {
				'padre':THIS,
				"config":THIS.options.completeConfig,
				'objetivo':THIS.objetivo,
				'oculto':THIS.oculto,
				'contenedor':THIS.contenedor,
				'item':respuesta.lista[i]
			}, function(e){
				e.data.padre.control.data({"valor":e.data.item[e.data.config.valor], "texto":e.data.item[e.data.config.texto]});
				e.data.contenedor.html('');
			});
		});

		THIS.contenedor.css({
			'width':THIS.objetivo.outerWidth(),
			'top':THIS.objetivo.outerHeight(),
			'left':0
		});

		THIS.visor.appendTo(THIS.contenedor);
	}

	aeFieldEditor.prototype.cambiar = function(valor, texto, datos) {
		THIS = this;

		// Actualizamos los controles
		switch (THIS.options.tipo) {
			case 'complete':
			THIS.control.data("texto", TEXTO);
			THIS.control.data("valor", VALOR);
			break;

			case 'boolean' :
			$.each(THIS.control, function(i,o){
				if ($(o).val() == valor) {
					$(o).prop('checked', true);
					return false;
				}
			});
			break;
			
			case 'list' : case 'password' : default :
			THIS.control.val(valor);
			break;
		}

		// Asiganamos valores
		THIS.oculto.val(valor);
		THIS.objetivo.val(texto);

		// Si hay un escuchador lo ejecutamos
		if (typeof(THIS.options.onUpdated) === 'function') { THIS.options.onUpdated(valor, texto, datos); }

		// Alertamos al usuario de los cambios efectuados
		THIS.alertar("alert success", "Cambios guardados.");

		// Cerramos los controles de edición de ser necesario
		THIS.manejador.trigger('cancel');
	}

	aeFieldEditor.prototype.guardar = function() {
		THIS = this;
		CONFIG = THIS.options.guardarConfig;

		// Mostramos mensaje al usuario
		THIS.alertar("alert info", "Procesando...");
		DATOS = false;

		// Almacenamos temporalmente en variables los valores elegidos por el usuario, pero no lo enviamos todavía
		switch (THIS.options.tipo) {
			case 'complete':
			TEXTO = THIS.control.data("texto");
			VALOR = THIS.control.data("valor");
			break;

			case 'boolean' :
			$.each(THIS.control, function(i,o){
				if ($(o).prop('checked')) {
					VALOR = $(o).val();
					TEXTO = $(o).data('text');
					return false;
				}
			});
			break;
			
			case 'list' :
			OPTIONSELECTED = $('option:selected', THIS.control);
			TEXTO = OPTIONSELECTED.text();
			VALOR = OPTIONSELECTED.val();
			DATOS = OPTIONSELECTED.data();
			break;
			
			case 'password' :
			TEXTO = "********";
			VALOR = THIS.control.val();
			if (VALOR.length > 0) { VALOR = CryptoJS.MD5(VALOR).toString(CryptoJS.enc.Hex); }
			break;
			
			default :
			VALOR = THIS.control.val();
			TEXTO = VALOR;
		}

		// Si el usuario no modifica el valor original pero da click en guardar, restauramos el valor inicial
		if (VALOR == undefined) { VALOR = THIS.options.valor; }
	
		// Si el usuario definio la variable beforeSend, significa que debemos de realizar comprobaciones antes de enviar el campo
		VALIDO = (typeof(THIS.options.beforeSend) === 'function') ? THIS.options.beforeSend.apply(THIS.control) : true;
		
		// Si valido la función del usuario, continuamos
		if (VALIDO) {
		
			if (CONFIG) {
			
				// Como estamos llamando a un servidor, mostramos un preloader
				$('i', THIS.guardarBoton).removeClass('fa-floppy-o').addClass('fa-preloader');
				
				// Asignamos el valor que será enviado al servidor
				CONFIG.data[CONFIG.nombre] = VALOR;
				
				// Comprobamos los valores que serán enviados al manejador, y si detectamos objetos, asignamos el valor de dicho objeto a su respectiva variable
				$.each(CONFIG.data, function(i,o){
					if (typeof(o) == 'object') {
						CONFIG.data[i] = o.val();
					}
				});						
				
				// Iniciamos la llamada al servidor
				$.when(
					[{
						'padre':THIS,
						'valor':VALOR,
						'texto':TEXTO,
						'datos':DATOS,
						'icon':$('i', THIS.guardarBoton)
					}],
					$.post(CONFIG.url, CONFIG.data, 'json')
				).then(function(p, r){
					p = p[0];
					r = r[0];
					
					// Regresamos el icono a su estado original
					p.icon.removeClass('fa-preloader').addClass('fa-floppy-o');
					
					// Cerramos el modo editor
					THIS.manejador.trigger('cancel');
					
					// Si el comando tuvo éxito mostramos un mensaje genérico
					if (r.success) {
						
						p.padre.cambiar(p.valor, p.texto, p.datos);
						p.padre.alertar("alert success", r.messages);
					
					// En caso de error, mostramos los mensages de ayuda
					} else {
						
						p.padre.alertar("alert danger", r.messages);

					}
					
				});
				
			} else {

				THIS.cambiar(VALOR, TEXTO, DATOS);
				
			}
		} else {
			THIS.alertar("alert danger", "Dato inválido");
		}
	}

	aeFieldEditor.prototype.prepararControles = function() {
		THIS = this;
		switch (THIS.options.tipo) {
			case 'checkgroup' : case 'radiogroup' : 
			break;

			default :
				// Preparamos el contenedor para todas las alertas del componente
				THIS.alerta = $('<div class="alert">•••</div>').appendTo(THIS.element);
				THIS.control = $('.interface-control', THIS.editor);

				// Cuando el usuario presiona el botón de guardar este manda a llamar al escuchador de guardar en el nodo padre
				THIS.guardarBoton = $('.interface-save', THIS.editor);
				THIS.guardarBoton.bind('click', {"padre":THIS}, function(e){
					e.data.padre.guardar();
				});

				// Cuando el usuario presiona el botón de cancelar
				THIS.cancelar = $('.interface-cancel', THIS.editor);
				THIS.cancelar.bind('click', {"padre":THIS}, function(e){
					e.data.padre.manejador.trigger('cancel');
					e.data.padre.alertar("alert", "•••");
					if (e.data.padre.options.tipo == 'complete') {
						e.data.padre.visorContenedor.html('');
					}
				});
				THIS.editor.hide();

				// Las fechas llevan una mascara de fecha
				if (THIS.options.tipo == 'date') {
					THIS.control.mask("99/99/9999",{placeholder:"dd/mm/yyyy"});
				}
			break;
		}
	}

	aeFieldEditor.prototype.init = function(target, options) {

		THIS = this;
		THIS.element = $(target);
		THIS.id = THIS.element.prop('id');
		THIS.oculto = false;
		THIS.objetivo = false;
		THIS.manejador = false;
		THIS.borrar = false;
		THIS.grupoDeCampos = false;

		THIS.options = $.extend({
			"onUpdated":false,
			"beforeSend":false,
			"grupoDeCampos":false,
			"letrasMinimas":3,
			"valorCampo":false,
			"textoCampo":false,
			"editarBoton":false,
			"borrarBoton":false,
			"borrarConfig":false,
			"guardarConfig":false,
			"completeConfig":false,
			"tipo":"text",
			"titulo":false,
			"texto":false,
			"unidad":false,
			"valor":false,
		}, options);
		
		THIS.element.css({'position':'relative'});
		
		if (THIS.options.modo == 'manual') {
		
			if (THIS.options.grupoDeCampos !== false) { THIS.grupoDeCampos = $(THIS.options.grupoDeCampos, THIS); }
			if (THIS.options.valorCampo !== false) { THIS.oculto = $(THIS.options.valorCampo, THIS); }
			if (THIS.options.textoCampo !== false) { THIS.objetivo = $(THIS.options.textoCampo, THIS); }
			if (THIS.options.editarBoton !== false) { THIS.manejador = $(THIS.options.editarBoton, THIS); }
			if (THIS.options.borrarBoton !== false) { THIS.borrar = $(THIS.options.borrarBoton, THIS); }

		} else if (THIS.options.modo == 'auto') {
		
			THIS.grupoDeCampos = $('<div class="input-group"></div>').appendTo(THIS.element);
			THIS.objetivo = $('<input type="text" class="form-control '+THIS.id+'-texto" readonly="readonly" />').appendTo(THIS.grupoDeCampos);
			THIS.oculto = $('<input type="hidden" class="'+THIS.id+'-valor" readonly="readonly" />').appendTo(THIS.grupoDeCampos);
			THIS.manejador = $('<a class="addon '+THIS.id+'-editar"><i class="fa fa-fw fa-pencil"></i></a>').appendTo(THIS.grupoDeCampos);
			if (THIS.options.borrarBoton !== false) { THIS.borrar = $('<a class="addon '+THIS.id+'-borrar"><i class="fa fa-fw fa-pencil"></i></a>').appendTo(THIS.grupoDeCampos); }

		} else {

			THIS.oculto = $('input.'+THIS.id+'-valor', THIS);
			THIS.objetivo = $('input.'+THIS.id+'-texto', THIS);
			THIS.manejador = $('a.'+THIS.id+'-editar', THIS);
			THIS.borrar = $('a.'+THIS.id+'-borrar', THIS);

		}

		// Si hay un manejador
		if (THIS.manejador.length > 0) {
			THIS.manejador.bind('click', {"padre":THIS}, function(e){
			
				// Vaciamos el contenido de la alerta
				e.data.padre.alertar("alert", "•••");
			
				// Reestablecemos el aspecto del control
				e.data.padre.hijos.hide();
				e.data.padre.editor.show();
				
				if (e.data.padre.options.tipo == 'complete') {
					e.data.padre.control.val(e.data.padre.objetivo.val());
				} else if (e.data.padre.options.tipo == 'number' || e.data.padre.options.tipo == 'password') {
					e.data.padre.control.val(e.data.padre.oculto.val());
				// List | Boolean
				} else {
					e.data.padre.control.val([e.data.padre.oculto.val()]);
				}
				
				// Centramos el foco en los input o select
				if (e.data.padre.options.tipo == 'list') { e.data.padre.control.select(); } else { e.data.padre.control.focus(); }
				
			}).bind('cancel', {'padre':THIS}, function(e){
				
				e.data.padre.hijos.show();
				e.data.padre.editor.hide();
				
			});
		}

		// Si sobreescribio la variable para contener al botón de borrar
		if (THIS.borrar != undefined) {

			// Si la recuperación del botón fué exitosa
			if (THIS.borrar.length) {

				THIS.borrar.bind('click', {'ajaxConfig':THIS.options.borrarConfig, "padre":THIS.element}, function(e){
					$.when(
						[{
							"fila":e.data.ajaxConfig.fila,
							"padre":e.data.padre
						}],
						$.post(e.data.ajaxConfig.url, e.data.ajaxConfig.data, false, "json")
					).done(function(parametros, respuesta){
						parametros = parametros[0];
						respuesta = respuesta[0];
						if (respuesta.success) {
							parametros.fila.remove();
						} else {
							parametros.padre.alertar("info", respuesta);
						}
					});
				});

			}

		}

		if (THIS.options.titulo) {
			if (THIS.grupoDeCampos !== false) {
				$('<span class="addon addon-title">'+THIS.options.titulo+'</span>').prependTo(THIS.grupoDeCampos);
			} else {
				throw new Error(0, "Estableciste un titulo pero no existe el contenedor grupoDeCampos.");
			}
		}
		
		if (THIS.options.texto != undefined) {
			if (THIS.objetivo !== false) {
				THIS.objetivo.val(THIS.options.texto);
			} else {
				throw new Error(0, "Estableciste un texto inicial pero no existe el campo para desplegarlo.");
			}
		}

		if (THIS.options.valor != undefined) {
			if (THIS.oculto !== false) {
				THIS.oculto.val(THIS.options.valor);
			} else {
				throw new Error(0, "Estableciste un valor inicial pero no existe el campo para desplegarlo.");
			}
		}

		THIS.hijos = THIS.element.children(':not(.alert)');
	
		// Preparamos la estructura del componente ///////////////////////////////////////////////////////////////////////////////////////////////////////////// //
		HTML = '';
		
		switch (THIS.options.tipo) {
			case 'checkgroup':
			$.each(THIS.options.data, function(i,o){
				CUSTOMDATA = '';
				if (o.data){
					$.each(o.data, function(prop, value){
						CUSTOMDATA += ' data-'+prop+'="'+value+'"';
					});
				}
				HTML+= '<li><label><input type="checkbox" value="'+o.val+'"'+CUSTOMDATA+' name="control" /> '+o.text+'</label></li>';
			})
			break;

			case 'password' :
			HTML+= '<input type="password" class="form-control interface-control" name="control" placeholder="Escribe una nueva contraseña." />';
			break;
			
			case 'text' : case 'complete' : case 'date' : 
			HTML+= '<input type="text" class="form-control interface-control" name="control" placeholder="Escribe aquí." />';
			break;
			
			case 'textarea' :
			HTML+= '<textarea class="form-control interface-control" name="control">Escribe aquí.</textarea>';
			break;
			
			case 'number' : case 'numericstepper' :
			HTML+= '<input type="text" class="form-control interface-control" name="control" placeholder="Escribe un numero" />';
			break;
			
			case 'boolean' :
			HTML+= '<div class="special-field">';
			HTML+= '<label><input type="radio" class="interface-control" name="control" value="true" data-text="SI" /> <span>SI</span></label>';
			HTML+= '<label><input type="radio" class="interface-control" name="control" value="false" data-text="NO" /> <span>NO</span></label>';
			HTML+= '</div>';
			break;
			
			case 'list' :
			HTML+= '<select class="form-control interface-control" name="control">';
			$.each(THIS.options.data, function(i,o){
				CUSTOMDATA = '';
				if (o.data){
					$.each(o.data, function(prop, value){
						CUSTOMDATA += ' data-'+prop+'="'+value+'"';
					});
				}
				HTML+= '<option value="'+o.val+'"'+CUSTOMDATA+'>'+o.text+'</option>';
			})
			HTML+= '</select>';
			break;
		}
		if (THIS.options.tipo == 'numericstepper') {
			HTML+= '<a class="addon interface-less"><i class="fa fa-fw fa-minus"></i></a>';
			HTML+= '<a class="addon interface-more"><i class="fa fa-fw fa-plus"></i></a>';	
		}
		if (THIS.manejador.length > 0) {
			HTML+= '<a class="addon interface-save"><i class="fa fa-fw fa-floppy-o"></i></a>';
			HTML+= '<a class="addon interface-cancel"><i class="fa fa-fw fa-times"></i></a>';
		}
		
		switch (THIS.options.tipo) {
			case 'checkgroup' : case 'radiogroup' : break;
			default : HTML = '<div class="input-group">'+HTML+'</div>';
		}

		THIS.editor = $(HTML).prependTo(THIS.element);

		THIS.prepararControles();

		// ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// //

		switch (THIS.options.tipo) {
			case 'complete' :
				THIS.visorContenedor = $('<div class="complete"></div>').appendTo(THIS.element);
				THIS.visorContenedor.css({'position':'absolute', 'z-index':1001});

				THIS.control.bind('keyup', {'padre':THIS, "config":THIS.options.completeConfig}, function(e){
					VALOR = $(this).val();
					if (VALOR.length >= e.data.padre.options.letrasMinimas) {

						// Cancelamos cualquier llamada mientras estemos escribiendo
						if (e.data.padre.llamadaServidor) { e.data.padre.llamadaServidor.abort(); }
						
						// Anexamos a la lista de parametros, el generado con nuestra escritura
						e.data.config.data[e.data.config.name] = VALOR;
						
						// Buscamos el datos en el controlador enviado por el usuario
						e.data.padre.llamadaServidor = $.post(
							e.data.config.url, 
							e.data.config.data, 
							function(respuesta){e.data.padre.dibujarComplete(respuesta);},
							'json'
						);
					}
				});
			break;

			case 'numericstepper' :
				//control.bind('keyup')
			break;
		}

	};

	window.aeFieldEditor = aeFieldEditor;
		
})(jQuery);