(function(){

	var counter, fix, media, obj, lastC, firstC, refWidth, refPos, leftPos, finalPos;

	// Window Scope
	var instance = window["BannerMovil"] = function(target) {
		this.target = $(target);
		this.media = $('.banner-media', this.target);
		this.previous = $('a.banner-previous', this.target);
		this.next = $('a.banner-next', this.target);

		// Botón para retroceder el banner
		this.previous.bind("click", $.proxy(function(){
			this.easingSpeed = 1024;
			this.easingEquation = 'easeOutCirc';
			this.updateDirection(-1);
		}, this));

		// Botón para adelantar el banner
		this.next.bind("click", $.proxy(function(){
			this.easingSpeed = 1024;
			this.easingEquation = 'easeOutCirc';
			this.updateDirection(1);
		}, this));

		// Agregamos elementos duplicados para simular un banner infinito
		this.mediaTotal = $('.media', this.media).length;
		fix = 0;
		for (counter=this.mediaTotal; counter>0; counter--) {
			media = $('.media:nth-child('+(counter+fix)+')', this.media).clone();
			media.prependTo(this.media);
			fix++;
		}

		// Establecemos el medio actual
		this.mediaCurrent = this.mediaTotal;

		// Comprobamos la cantidad de imagenes
		this.mediaItems = $('.media', this.media);
		this.mediaTotal = this.mediaItems.length;

		// Si la ventana cambia de tamaño actualizamos el banner
		$(window).bind("resize.BannerMovil", $.proxy(function(){
			this.easingSpeed = 24;
			this.easingEquation = 'easeOutCirc';
			this.animate();
		}, this));

		// Iniciamos el banner
		this.easingSpeed = 24;
		this.easingEquation = 'easeOutQuart';
		this.animating = false;
		this.updateDirection(1);
	}
	instance.prototype.getMedia = function(index) {
		return $('.media:nth-child('+index+')', this.media);
	}

	instance.prototype.updateDirection = function(direction) {
		// Si no hay ninguna animación en curso
		if (this.animating == false) {			
			this.direction = (direction == undefined) ? 0 : direction;
			// Dependiendo de si avanzamos o retrocedemos
			switch (this.direction) {
				case -1:
				// Actualizamos la posicion del elemento que estamos moviendo,
				// para evitar que pase por encima de los otros elementos al realizar la animación
				firstC = $('.media:first-child', this.media);
				lastC = $('.media:last-child', this.media);
				lastC.css("left", firstC.position().left - firstC.width());
				// Si retrocedemos, el último elemento de la fila lo enviamos al inicio
				lastC.prependTo(this.media);
				break;

				case 1:
				// Actualizamos la posicion del elemento que estamos moviendo,
				// para evitar que pase por encima de los otros elementos al realizar la animación
				firstC = $('.media:first-child', this.media);
				lastC = $('.media:last-child', this.media);
				firstC.css("left", lastC.position().left + lastC.width());
				// Si avanzamos, el primer elemento de la fila lo enviamos al final
				firstC.appendTo(this.media);
				break;
			}

			this.animate();
		}
	}
	instance.prototype.animationsStop = function() {
		// La animación ya terminó
		clearInterval(this.animationsInternval);
		this.animating = false;
	}
	instance.prototype.animate = function(){
		this.animating = true;

		// Verificamos el tamaño del contenedor
		this.containerWidth = this.target.width();

		// Seleccionamos el medio de referencia
		this.mediaRef = this.getMedia(this.mediaCurrent);

		// Arreglo con las posiciones finales de los medios
		finalPos = [];

		// El medio seleccionado tiene que estar centrado
		refWidth = this.mediaRef.width();
		refPos = Math.ceil((this.containerWidth - refWidth) / 2);
		finalPos[finalPos.length] = {"media":this.mediaRef, "left":refPos, "opacity":1};

		// Los medios predecesores al medio de referencia se forman a la izqueirda
		if (this.mediaCurrent>1) {
			leftPos = refPos;
			for (counter=(this.mediaCurrent-1); counter>=1; counter--) {
				media = this.getMedia(counter);
				leftPos -= media.width();
				finalPos[finalPos.length] = {"media":media, "left":leftPos, "opacity":0.8};
			}
		}

		// Los medios posteriores al medio de referencia se forman a la derecha
		if (this.mediaCurrent<this.mediaTotal) {
			leftPos = refPos + refWidth;
			for (counter=(this.mediaCurrent+1); counter<=this.mediaTotal; counter++) {
				media = this.getMedia(counter);
				finalPos[finalPos.length] = {"media":media, "left":leftPos, "opacity":0.8};
				leftPos += media.width();
			}
		}

		// Una vez calculadas las posiciones ejecutamos las animaciones
		for (counter=0; counter<this.mediaTotal; counter++) {
			obj = finalPos[counter];
			obj.media.stop().animate({"left":obj.left, "opacity":obj.opacity}, this.easingSpeed, this.easingEquation);
		}

		// Marcamos el medio actual
		$('.media', this.media).removeClass("current");
		this.mediaRef.addClass("current");

		this.animationsInternval = window.setInterval($.proxy(function(){
			this.animationsStop();
		}, this), this.easingSpeed);
	};

}());